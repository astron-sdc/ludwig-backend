# Permissions

Legend:

- X: not allowed
- V: allowed
- A1: single proposal, if assigned to or created by
- AA: all within cycle, if assigned to
- (?): not sure
- NA: not applicable

- <p c>: proposal collaboration
- <c c>: call collaboration
-
Notes:
Reviewers can only see proposals from the calls they are parts of the review committee.
Reviewers can only write comments if they are specifically asigned as a reviewer of that proposal.

| Permissions/Role                              | Authenticated User  | PI         | CO Author        | CO Author Read Only   | Technical Reviewer Chair | Science Reviewer Chair  | Technical Reviewer    | Science Reviewer     | Admin                         |
|-----------------------------------------------|---------------------|------------|------------------|-----------------------|--------------------------|-------------------------|-----------------------|----------------------|-------------------------------|
| Role URN                                      | `<sdc collab>:user` | `<p c>:pi` | `<p c>:co_author` | `<p c>:co_author_ro`  | `<p c>:tech_reviewer_ch` | `<p c>:sci_reviewer_ch` | `<p c>:tech_reviever` | `<p c>:sci_reviewer` | `<scd collab>:proposal_admin` |
| --------------------------------------------- | ------------------- | ---------- | ----------------- | --------------------- | ------------------------ | ----------------------- | --------------------- | -------------------- | ----------------------------- |
| Create Cycle                                  | X                   | X          | X                | X                     | X                        | X                       | X                     | X                    | V                             |
| Create Call for proposals                     | X                   | X          | X                | X                     | X                        | X                       | X                     | X                    | V                             |
| Assign committee members to call (via SRAM)   | X                   | X          | X                | X                     | X                        | X                       | X                     | X                    | V                             |
| See open calls for proposals                  | V                   | V          | V                | V                     | V                        | V                       | V                     | V                    | V                             |
| Create Proposal (becomes PI)                  | V                   | NA         | NA               | NA                    | NA                       | NA                      | NA                    | NA                   | NA                            |
| Read Proposal                                 | X                   | A1         | A1               | A1                    | AA                       | AA                      | A1                    | AA                   | V                             |
| Write on proposals                            | X                   | V          | V                | X                     | X                        | X                       | X                     | X                    | V                             |
| (Un)submit proposals                          | X                   | V          | X (TBC)          | X                     | X                        | X                       | X                     | X                    | V                             |
| Assign reviewer to specific proposals         | X                   | X          | X                | X                     | AA                       | AA                      | X                     | X                    | V                             |
| See reviewers on proposals                    | X                   | X          | X                | X                     | AA                       | AA                      | A1                    | AA                   | V                             |
| Write technical review comments               | X                   | X          | X                | X                     | AA                       | X                       | A1                    | X                    | NA                            |
| Read technical review comments                | X                   | X          | X                | X                     | AA                       | AA                      | A1                    | AA                   | V                             |
| Write science review comments                 | X                   | X          | X                | X                     | X                        | AA                      | X                     | A1                   | NA                            |
| Read science review comments                  | X                   | X          | X                | X                     | X                        | AA                      | X                     | AA                   | V                             |
| Rate proposal scale 1-5 in review             | X                   | X          | X                | X                     | X                        | AA                      | X                     | AA                   | NA                            |
| See proposal ratings                          | X                   | X          | X                | X                     | X                        | AA                      | X                     | AA (own rating)      | V                             |
| Rank/order proposals                          | X                   | X          | X                | X                     | X                        | X                       | X                     | X                    | V                             |
| Accept/reject proposals                       | X                   | X          | X                | X                     | X                        | X                       | X                     | X                    | V                             |
| Sync proposal to TMSS                         | X                   | X          | X                | X                     | X                        | X                       | X                     | X                    | V                             |
| Read Calculation for proposal                 | X                   | A1         | A1               | A1                    | AA                       | AA                      | A1                    | AA                   | V                             |
| Write Calculation for proposal                | X                   | A1         | A1               | X                     | X                        | X                       | X                     | X                    | V                             |
