# Generated by Django 5.0.6 on 2024-08-07 09:36

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proposal', '0026_alter_call_category_alter_proposal_category_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='review',
            name='proposal',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='proposal.proposal'),
        ),
    ]
