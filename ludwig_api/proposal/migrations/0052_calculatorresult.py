# Generated by Django 5.1.5 on 2025-02-06 10:36

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proposal', '0051_call_cpu_time_seconds_call_observing_time_seconds_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='CalculatorResult',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('raw_data_size_bytes', models.BigIntegerField()),
                ('processed_data_size_bytes', models.BigIntegerField()),
                ('pipeline_processing_time_seconds', models.BigIntegerField()),
                ('mean_elevation', models.FloatField()),
                ('theoretical_rms_jansky_beam', models.FloatField()),
                ('effective_rms_jansky_beam', models.FloatField()),
                ('proposal', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='proposal.proposal')),
            ],
        ),
    ]
