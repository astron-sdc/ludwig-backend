# Generated by Django 5.0.6 on 2024-09-10 10:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proposal', '0028_alter_proposal_tmss_project_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='proposal',
            name='summary',
            field=models.TextField(default=''),
        ),
    ]
