# Generated by Django 5.0.6 on 2024-09-09 14:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proposal', '0027_alter_review_proposal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proposal',
            name='tmss_project_id',
            field=models.CharField(max_length=150),
        ),
    ]
