# Generated by Django 5.0.6 on 2024-07-15 12:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proposal', '0017_review_created_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proposal',
            name='collaboration_identifier',
            field=models.CharField(blank=True, default='', max_length=64),
        ),
    ]
