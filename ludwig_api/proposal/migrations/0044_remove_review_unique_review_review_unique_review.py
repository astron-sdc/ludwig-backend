# Generated by Django 5.1.1 on 2024-10-14 10:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proposal', '0043_proposalconfiguration_enable_qa_workflow'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='review',
            name='unique_review',
        ),
        migrations.AddConstraint(
            model_name='review',
            constraint=models.UniqueConstraint(condition=models.Q(('reviewer_id', ''), _negated=True), fields=('proposal', 'reviewer_id', 'review_type'), name='unique_review'),
        ),
    ]
