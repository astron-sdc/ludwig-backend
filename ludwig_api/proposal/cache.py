def invalidate_cached_property(obj, property_name):
    """
    Deletes a property from an object, effectively invalidating its cached value if used with cached_property
    @param obj: the object to delete a property from
    @param property_name: the name of the property to delete
    """
    try:
        delattr(obj, property_name)
    except AttributeError:
        # if the cached_property has not been accessed yet, an AttributeError will be thrown (which we happily ignore here)
        pass
