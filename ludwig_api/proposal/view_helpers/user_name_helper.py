from django.core.cache import cache

from ludwig_api import settings
from ludwig_api.faai import constants
from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.faai.sram.models import collaboration_membership_to_user

name_cache_key = "sram_user_names"


def get_user_name(user_id: str):
    if settings.SRAM_ENABLED == "0":
        return user_id

    id_name_map = cache.get(name_cache_key)

    if id_name_map is None:
        members = (
            SRAMClient()
            .get_collaboration(constants.APPLICATION_COLLABORATON_IDENTIFIER)
            .get("collaboration_memberships", [])
        )
        users = [collaboration_membership_to_user(member) for member in members]
        id_name_map = {user["eduperson_unique_id"]: user["name"] for user in users}
        cache.set(
            name_cache_key, id_name_map, constants.SRAM_NAME_CACHE_TIMEOUT_IN_SECONDS
        )

    return id_name_map.get(user_id, "")
