from django.conf import settings

from ludwig_api.proposal.models import Call, Proposal


def get_proposal_url(proposal: Proposal) -> str:
    return f"{settings.LUDWIG_FRONTEND_BASE_URL}proposals/{proposal.pk}/"


def get_call_url(call: Call) -> str:
    return f"{settings.LUDWIG_FRONTEND_BASE_URL}calls/{call.pk}/"
