from ludwig_api.proposal.models.specification import Specification
from ludwig_api.proposal.models.target_group import TargetGroup
from ludwig_api.proposal.tmss.api import TMSS
from ludwig_api.proposal.tmss.models.scheduling_set import TMSSSchedulingSet


def get_or_create_group(group_id_number, proposal_id):
    group_name = f"Proposal {proposal_id} Group {group_id_number}"
    group_description = "Auto generated from Proposal Tool"
    group, _created = TargetGroup.objects.get_or_create(
        group_id_number=group_id_number,
        proposal_id=proposal_id,  # Ensure that we match the proposal
        defaults={
            "name": group_name,
            "description": group_description,
        },
    )
    return group


def create_tmss_specification_group_if_not_exist(
    tmss_api: TMSS, specification: Specification
):
    try:
        proposal = specification.proposal
        tmss_project_id = proposal.tmss_project_id
        scheduling_sets = tmss_api.get_scheduling_sets(tmss_project_id)
        group = specification.targetgroup
        insert_required = False
        insert_required_acording_group_tracker = not any(
            scheduling_set.id == group.tmss_scheduling_set_id
            for scheduling_set in scheduling_sets
        )

        if insert_required_acording_group_tracker:
            insert_required = True
            tmss_scheduling_record = next(
                (
                    scheduling_set
                    for scheduling_set in scheduling_sets
                    if scheduling_set.name == group.name
                ),
                None,
            )
            if (
                tmss_scheduling_record
                and tmss_scheduling_record.id != group.tmss_scheduling_set_id
            ):
                if (
                    isinstance(tmss_scheduling_record, TMSSSchedulingSet)
                    and tmss_scheduling_record.id > 0
                ):
                    group.tmss_scheduling_set_id = (
                        tmss_scheduling_record.id
                    )  # we do have the record already, but lost tracking (Most likely a Dev killing it's Luwdig DB and then creating the same proposal in TMSS)
                    group.save()  # so just add this registration
                    insert_required = False  # and skip inserting it
                    return (
                        True,
                        "Set relation updated"
                        + tmss_scheduling_record.name
                        + " Synced to Group.",
                        tmss_scheduling_record,
                        None,
                    )
        else:
            tmss_existing_scheduling_record: TMSSSchedulingSet = next(
                (
                    scheduling_set
                    for scheduling_set in scheduling_sets
                    if scheduling_set.id == group.tmss_scheduling_set_id
                ),
                TMSSSchedulingSet(),
            )  # this will allways return the left.
            return (
                True,
                "Existed " + tmss_existing_scheduling_record.name,
                tmss_existing_scheduling_record,
                None,
            )

        if insert_required:
            tmss_project = tmss_api.get_project_with_quotas(tmss_project_id)
            name = group.name
            description = group.description
            if tmss_project.url == "":
                return False, "Project does not exist", None, None
            tmss_specification_set = tmss_api.post_scheduling_set(
                tmss_project.url, name, description
            )
            group.tmss_scheduling_set_id = (
                tmss_specification_set.id
            )  # we do have the record already
            group.save()  # so just add this registration
            return (
                True,
                "Inserted" + tmss_specification_set.name,
                tmss_specification_set,
                None,
            )

        return False, "Should not end here", None, None  # no roads should end here

    except Exception as e:
        return False, f"Exception {e}", None, e
