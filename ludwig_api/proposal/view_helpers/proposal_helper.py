from django.conf import settings
from django.http import FileResponse, HttpResponseNotFound, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from requests import Response
from rest_framework import status

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.faai.sram.collaboration import (
    collaboration_contains_group,
    collaboration_contains_service,
)
from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.models.proposal_configuration import ProposalConfiguration
from ludwig_api.proposal.permissions.application import Prefix
from ludwig_api.proposal.permissions.proposal import (
    ROLE_DESCRIPTIONS,
    ROLE_NAMES,
    Roles,
)
from ludwig_api.proposal.tmss.api import TMSS
from ludwig_api.proposal.tmss.models.project import TMSSProject
from ludwig_api.proposal.tmss.models.projectquota import TMSSProjectQuota
from ludwig_api.proposal.view_helpers.specification_helper import (
    clean_no_longer_needed_export_records,
)
from ludwig_api.proposal.view_helpers.url_constructor import get_proposal_url


def create_proposal_in_sram(proposal: Proposal, user: TokenUser):
    """Creates the necessary objects in SRAM for the proposal.

    @:param proposal: Proposal model; assumed to be present in Ludwig
    @:param user: TokenUser; based on JWT in request
    """
    if settings.SRAM_ENABLED == "0":
        return
    client = SRAMClient()
    if proposal.collaboration_identifier == "":
        collaboration = client.create_collaboration(
            name=f"[{proposal.project_code}] {proposal.title}",
            short_name=f"{Prefix.APP.value}{Prefix.PROPOSAL.value}{proposal.pk}",
            description=f"Collaboration for the proposal: {proposal.title}",
            website_url=get_proposal_url(proposal),
            disable_join_requests=False,  # allow invite links
            disclose_member_information=True,
            administrators=[getattr(user, "email")],
        )
        proposal.collaboration_identifier = collaboration["identifier"]  # big uuid
        proposal.save()

    # create_collaboration does not return complete info
    # use get_collaboration for the complete (including group and service) info
    collaboration = client.get_collaboration(proposal.collaboration_identifier)

    # Create Groups
    for role in Roles:
        if not collaboration_contains_group(collaboration, role.value):
            client.create_group(
                name=ROLE_NAMES[role],
                description=ROLE_DESCRIPTIONS[role],
                short_name=role.value,
                auto_provision_members=False,
                collaboration_identifier=collaboration["identifier"],
            )

    # Our service needs to be connected to each collaboration for
    # the eduperson entitlements to show up in the token.
    if not collaboration_contains_service(
        collaboration, settings.SRAM_LUDWIG_SERVICE_ID
    ):
        client.connect_collaboration_service(
            collaboration["short_name"], settings.SRAM_LUDWIG_SERVICE_ID
        )


def get_scientific_justification(view, proposal_pk: int):
    """Get scientific justification for given proposal pk, or return 404"""
    proposal: Proposal = get_object_or_404(view.get_queryset(), pk=proposal_pk)
    if proposal.scientific_justification:
        return FileResponse(proposal.scientific_justification, as_attachment=True)
    else:
        return HttpResponseNotFound()


def synchronize_save_quota(
    resources, project: TMSSProject, resource_name: str, value: float, tmss_api: TMSS
):
    if project.url == "":
        return {
            "resource_name": resource_name,
            "result:": "Project does not yet exist",
            "result status": 500,
        }
    resource_type = resources.get(resource_name)
    quota_id = -1
    if project.quota:
        for quota_item in project.quota:
            if quota_item["resource_type_id"] == resource_name:
                quota_id = quota_item["id"]
                break

    tmss_project_quota = TMSSProjectQuota(
        project=project.url, resource_type=resource_type, value=value, id=quota_id
    )
    if quota_id == -1:
        result = tmss_api.post_quota(tmss_project_quota)
    else:
        result = tmss_api.put_quota(tmss_project_quota)
        if result.status_code == 404:
            result = tmss_api.post_quota(tmss_project_quota)

    did_succeed = result.status_code == 200 or result.status_code == 201
    return {
        "resource_name": resource_name,
        "result:": result.text,
        "result status": result.status_code,
        "did_succeed": did_succeed,
    }


def synchronize_save_quotas(proposal: Proposal, tmss_api: TMSS):
    project = tmss_api.get_project_with_quotas(
        proposal.tmss_project_id
    )  # We need a fresh retrieval after the saving
    resources = (
        tmss_api.get_resources()
    )  # we need the resources to retrieve the resource url for the given name

    # Resources in TMSS Are not flatmapped, but more dynamicly assignable. However for the shake and ease of use, we have them flatmapped in Ludwig.
    # The  code here takes care of mapping our structure back to tmss it's structure.
    config = proposal.configuration
    results = []
    results.append(
        synchronize_save_quota(
            resources,
            project,
            "CEP Processing Time",
            config.processing_time_seconds,
            tmss_api,
        )
    )
    results.append(
        synchronize_save_quota(
            resources, project, "LTA Storage", config.secondary_storage_bytes, tmss_api
        )
    )
    results.append(
        synchronize_save_quota(
            resources, project, "CEP Storage", config.primary_storage_bytes, tmss_api
        )
    )
    results.append(
        synchronize_save_quota(
            resources,
            project,
            "LOFAR Observing Time",
            config.observing_time_combined_awarded_seconds,
            tmss_api,
        )
    )
    results.append(
        synchronize_save_quota(
            resources,
            project,
            "LOFAR Observing Time prio A",
            config.observing_time_prio_A_awarded_seconds,
            tmss_api,
        )
    )
    results.append(
        synchronize_save_quota(
            resources,
            project,
            "LOFAR Observing Time prio B",
            config.observing_time_prio_B_awarded_seconds,
            tmss_api,
        )
    )
    results.append(
        synchronize_save_quota(
            resources,
            project,
            "LOFAR Observing Time Commissioning",
            config.observing_time_commissioning_awarded_seconds,
            tmss_api,
        )
    )
    results.append(
        synchronize_save_quota(
            resources,
            project,
            "LOFAR Support Time",
            config.support_time_seconds,
            tmss_api,
        )
    )
    results.append(
        synchronize_save_quota(
            resources,
            project,
            "Number of triggers",
            config.trigger_allowed_numbers,
            tmss_api,
        )
    )
    return results


def get_cycles(proposal: Proposal):
    proposal_cycle = proposal.call.cycle.tmss_cycle_id
    tmss_cycles = TMSS().get_cycles()
    fallback_cycle = [proposal_cycle, proposal_cycle]

    if "results" in tmss_cycles and tmss_cycles["results"]:
        for cycle in tmss_cycles["results"]:
            if cycle["url"] == proposal_cycle:
                return [proposal_cycle], [cycle["name"]]

    return fallback_cycle


def synchronize(view, proposal_pk: int, is_clean_attempt: bool = False):

    jsonmessage = {}
    tmss_api = TMSS()
    proposal: Proposal = get_object_or_404(view.get_queryset(), pk=proposal_pk)

    try:
        """Synchronize Proposal to TMSS"""
        cycles, cycles_ids = get_cycles(proposal)
        project: TMSSProject
        if proposal.configuration is None:
            proposal.configuration = ProposalConfiguration()
        tmssproject = TMSSProject(
            name=proposal.project_code,
            description=proposal.title,
            cycles=cycles,
            cycles_ids=cycles_ids,
            trigger_priority=proposal.configuration.trigger_priority,
            auto_ingest=proposal.configuration.auto_ingest,
            can_trigger=proposal.configuration.can_trigger,
            enable_qa_workflow=proposal.configuration.enable_qa_workflow,
            private_data=proposal.configuration.private_data,
            expert=proposal.configuration.expert,
            filler=proposal.configuration.filler,
            auto_pin=proposal.configuration.auto_pin,
            piggyback_allowed_tbb=proposal.configuration.piggyback_allowed_tbb,
            piggyback_allowed_aartfaac=proposal.configuration.piggyback_allowed_aartfaac,
        )

        if proposal.tmss_project_id:
            project = tmss_api.get_project_with_quotas(
                proposal.tmss_project_id
            )  # and we need the project to retrieve the project url. And later on, as well as to reverse the realated quota ids, when we are updating.
            tmssproject.quota = (
                [quota["url"] for quota in project.quota]
                if project.quota is not None
                else []
            )
            tmssproject.quota_ids = project.quota_ids

        result, did_succeed, message, servertext = sync_project(
            view, proposal_pk, is_clean_attempt, proposal, tmssproject
        )
        project_did_succeed = did_succeed
        quota_result = synchronize_save_quotas(proposal, tmss_api)
        quota_all_succeeded = all(
            item.get("did_succeed", False) for item in quota_result
        )
        did_succeed = project_did_succeed and quota_all_succeeded
        jsonmessage = {
            "did_succeed": did_succeed,
            "project_did_succeed": project_did_succeed,
            "upstream_project_message": message,
            "upstream_text": servertext,
            "upstream_status_code": result.status_code,
            "upstream_quota_result": quota_result,
            "quota_did_succeed": quota_all_succeeded,
            "cycle": proposal.call.cycle.tmss_cycle_id,
        }
    except Exception as e:
        jsonmessage["error_message"] = (
            f"An error occurred: {str(e)}"  # we must guard from all evil during sync
        )

    return JsonResponse(jsonmessage)


def sync_project(view, proposal_pk, is_clean_attempt, proposal, tmssproject):
    result: Response
    did_succeed = False
    message = ""
    servertext = ""
    if (
        proposal.tmss_project_id == TMSS.CLEAN_PROJECT_ID
        or tmssproject.name != proposal.tmss_project_id
    ):
        result = TMSS().post_project(tmssproject)
    else:
        result = TMSS().put_project(tmssproject, proposal.tmss_project_id)

    if (
        result.status_code == status.HTTP_201_CREATED
    ):  # 201 is the Happy Creation Flow. We have inserted the proposal as a project
        proposal.tmss_project_id = proposal.project_code
        message = "Created"

    if (
        result.status_code == status.HTTP_200_OK
    ):  # 200 is the Happy Update Flow. We have succesfully updated a project
        message = "Updated"
        proposal.tmss_project_id = proposal.project_code

    if (
        result.status_code == status.HTTP_404_NOT_FOUND
    ):  # We'r trying to update, but our synced project has been removed from tmss :)
        proposal.tmss_project_id = (
            TMSS.CLEAN_PROJECT_ID
        )  # so we remove the project id, and then retry with a insert

    if (
        result.status_code == status.HTTP_400_BAD_REQUEST
    ):  # We'r trying to insert, but our to be synced project did exist already. SOmebody entered it manual there, or a prevous sync from another ludwig dev DB!
        proposal.tmss_project_id = (
            proposal.project_code
        )  # during development, this can happen, a existing project , so let's take that over

    if (
        result.status_code == status.HTTP_400_BAD_REQUEST
        or result.status_code == status.HTTP_404_NOT_FOUND
    ):  # Healing of the unhappy flow
        proposal.save()  # Let's not forget to save the proposal, we updated it!
        if (
            not is_clean_attempt
        ):  # Let's prevemt a dead by recursive loop to only retry it once
            return sync_project(
                view, proposal_pk, True, proposal, tmssproject
            )  # And retry the sync!

    if (
        result.status_code == status.HTTP_200_OK
        or result.status_code == status.HTTP_201_CREATED
    ):  # All went well
        servertext = result.text  # don't we all love some debugging results on a client
        proposal.tmss_project_id_synced_at = (
            timezone.now()
        )  # SO now we take over the synchronization date as granted
        proposal.tmss_project_id_sync_version = (
            proposal.tmss_project_id_sync_version + 1
        )
        proposal.save()  # And save the proposal afterwards
        did_succeed = True  # We have succeeded.
    else:
        # You only want to happen that on a Dev environment :)
        message = result.text  # Absolutely Bad luck
    return result, did_succeed, message, servertext


def finish_sync_and_clean_unneeded_exports(view, proposal_pk):
    proposal: Proposal = get_object_or_404(view.get_queryset(), pk=proposal_pk)
    tmss_api = TMSS()
    clean_no_longer_needed_export_records(tmss_api, proposal)

    jsonmessage = {
        "did_succeed": True,
        "message": ["finalized"],
    }
    return JsonResponse(jsonmessage)
