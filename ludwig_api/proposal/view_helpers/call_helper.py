from django.conf import settings

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.faai.sram.collaboration import (
    collaboration_contains_group,
    collaboration_contains_service,
)
from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.models.call import Call
from ludwig_api.proposal.permissions.application import Prefix
from ludwig_api.proposal.permissions.call import ROLE_DESCRIPTIONS, ROLE_NAMES, Roles
from ludwig_api.proposal.view_helpers.url_constructor import get_call_url


def create_call_in_sram(call: Call, user: TokenUser):
    """Creates the necessary objects in SRAM for the call.

    @:param call: Call model; assumed to be present in Ludwig
    @:param user: TokenUser; based on JWT in request
    """
    if settings.SRAM_ENABLED == "0":
        return
    client = SRAMClient()
    if call.collaboration_identifier == "":
        collaboration = client.create_collaboration(
            name=f"Call for proposals {call.code}",
            short_name=f"{Prefix.APP.value}{Prefix.CALL.value}{call.pk}",
            description=f"Collaboration for call: {call.name}",
            website_url=get_call_url(call),
            disable_join_requests=False,  # allow invite links
            disclose_member_information=True,
            administrators=[getattr(user, "email")],
        )
        call.collaboration_identifier = collaboration["identifier"]  # big uuid
        call.save()

    # create_collaboration does not return complete info
    # use get_collaboration for the complete (including group and service) info
    collaboration = client.get_collaboration(call.collaboration_identifier)

    # Create Groups
    for role in Roles:
        if not collaboration_contains_group(collaboration, role.value):
            client.create_group(
                name=ROLE_NAMES[role],
                description=ROLE_DESCRIPTIONS[role],
                short_name=role.value,
                auto_provision_members=False,
                collaboration_identifier=collaboration["identifier"],
            )

    if not collaboration_contains_service(
        collaboration, settings.SRAM_LUDWIG_SERVICE_ID
    ):
        client.connect_collaboration_service(
            collaboration["short_name"], settings.SRAM_LUDWIG_SERVICE_ID
        )
