from django.db.models import F, Window
from django.db.models.functions import RowNumber
from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework import status

from ludwig_api.proposal.models.export_record import ExportRecord
from ludwig_api.proposal.models.proposal import Proposal
from ludwig_api.proposal.models.specification import Specification
from ludwig_api.proposal.models.target import Target
from ludwig_api.proposal.tmss.api import TMSS
from ludwig_api.proposal.view_helpers.group_helper import (
    create_tmss_specification_group_if_not_exist,
)


def synchronize_specification(view, specificationid: int, proposalid: int):
    # A right check has to be done here for the right on the proposal.
    tmss_api = TMSS()
    to_be_synced_spec: Specification = get_object_or_404(
        view.get_queryset(), pk=specificationid, proposal_id=proposalid
    )
    to_be_synced_spec.tmss_schedulingunit_id_sync_version = (
        to_be_synced_spec.proposal.tmss_project_id_sync_version
    )
    to_be_synced_spec.save()

    return create_tmss_specification_group_if_not_exist(
        tmss_api,
        to_be_synced_spec,
    )


def get_tmss_target_rank(target: Target, specification: Specification):
    if target is None:  # When target factory is broken
        return 0

    # we determine ranking by it's part of the total targets
    target_count = (
        Target.objects.filter(
            proposal=target.proposal, group=specification.targetgroup.id
        )
        .exclude(assigned_priority="X")
        .count()
    )

    if target_count == 0:
        target_count = 1

    # Get the position in the result row, sorter by row numbers
    position_queryset = (
        Target.objects.filter(
            proposal=target.proposal, group=specification.targetgroup.id
        )
        .exclude(assigned_priority="X")
        .annotate(
            row_number=Window(
                expression=RowNumber(),
                order_by=[F("assigned_priority").asc(), F("subgroup").asc()],
            )
        )
        .filter(id=target.id)
    )  # Filter for the specific target

    try:
        specific_target = position_queryset.get()
        specific_target_position = specific_target.row_number
    except Target.DoesNotExist:
        specific_target_position = 0  # Handle case where the target does not exi

    # So we have the order, a number from 1 .... 100drerds
    target_rank = (
        target_count - specific_target_position
    ) / target_count  # Division by zero is guaranted by the fact that synchronize_subgroup_with_specification has targets
    return target_rank


def clean_no_longer_needed_export_records(tmss_api: TMSS, proposal: Proposal):
    export_records = ExportRecord.objects.filter(
        proposal=proposal,
        external_environment=proposal.configuration.telescope,
    ).exclude(sync_version=proposal.tmss_project_id_sync_version)

    for exported_record in export_records:
        draft = tmss_api.get_scheduling_unit_draft(exported_record.external_identifier)
        if draft:
            tmss_api.delete_scheduling_unit_definition(
                exported_record.external_identifier
            )
        exported_record.delete()


def compose_specification_name(specification, targets: list[Target]):
    name = f"{specification.name} {specification.id}"
    if len(targets) > 1:
        return f"{name} - S({targets[0].subgroup}) {targets[0].field_order}-{targets[len(targets)-1].field_order} "[
            :127
        ]
    if len(targets) == 1:
        return f"{name} - T({targets[0].id}) {targets[0].name} "[:127]
    return f"{name}  "[:127]


def determine_priority(specification_priority, target: Target):
    # we cvan have a specification used by multiple targets with a "A" Priority
    # However, a range of subgroup Targets can have a "B" Priority
    # This will result in the specification also being B.
    if (target.assigned_priority == "B") or (
        target.assigned_priority == "?" and target.requested_priority == "B"
    ):
        return "B"
    # X Is already filtered out.
    return f"{specification_priority}"


def safe_get_saps(definition):
    try:
        return (
            definition.get("tasks", {})
            .get("Observation", {})
            .get("specifications_doc", {})
            .get("station_configuration", {})
            .get("SAPs", None)
        )
    except Exception:
        return None


def set_pointing_angles(pointing, target):
    digital_pointing = pointing["digital_pointing"]

    if "angle1" in digital_pointing:
        digital_pointing["angle1"] = target.x_hms_formatted

    if "angle2" in digital_pointing:
        digital_pointing["angle2"] = target.y_dms_formatted

    if "target" in digital_pointing:
        digital_pointing["target"] = target.name

    return pointing


def blend_in_target(definition, target: Target):
    # Mix Target in definition (Temporary not Done, need to check something regarding mixing multiple targets in one spec))
    position_to_blend_in = 0
    if target.field_order:
        position_to_blend_in = target.field_order - 1
    target_message = f"target  {target.name} on position {target.field_order}"
    if position_to_blend_in < 0:
        position_to_blend_in = 0

    saps = safe_get_saps(definition)
    if saps is None:
        return definition, "No Station Config"

    if len(saps) < position_to_blend_in:
        return definition, target_message + " not placed"

    pointing = saps[position_to_blend_in]

    if "digital_pointing" not in pointing:
        return definition, target_message + " no digital pointing structure"

    set_pointing_angles(pointing, target)
    mixed_definition = definition
    return (
        mixed_definition,
        f"target {target.name} on position  {target.field_order}  blended in",
    )


def is_exported_record_present(tmss_api: TMSS, export_record: ExportRecord):
    try:
        remote_existence_status = tmss_api.get_scheduling_unit_draft(
            export_record.external_identifier
        )
        if remote_existence_status.status_code == status.HTTP_200_OK:
            return True
    except Exception:
        return False
    return False


def create_tmss_specification_from_subgroup(
    tmss_api: TMSS,
    specification: Specification,
    proposal: Proposal,
    definition,
    name: str,
    rank,
    priority_queue: str,
    subgroup,
):

    export_record = ExportRecord.objects.filter(
        proposal=proposal,
        specification=specification,
        external_environment=proposal.configuration.telescope,
        subgroup=subgroup,
    ).first()

    try:
        composed_definition = compose_update(
            definition,
            name,
            description=specification.description,
            rank=rank,
            priority_queue=priority_queue,
            scheduling_set_id=specification.targetgroup.tmss_scheduling_set_id,
        )
        strategynumber = specification.strategy.rstrip("/").split("/")[-1]
        is_insert_required = True
        if export_record and is_exported_record_present(tmss_api, export_record):
            result = tmss_api.put_scheduling_unit_definition(
                export_record.external_identifier, composed_definition
            )
            is_insert_required = False
            message = "Updated Scheduling Unit"
        if is_insert_required:
            if not export_record:
                export_record = ExportRecord()  #  Let's make a new record
            result = tmss_api.post_scheduling_unit_definition(
                strategynumber,
                definition,
                specification.targetgroup.tmss_scheduling_set_id,
                name,
                specification.description,
                rank,
                priority_queue,
            )
            externalidentifier = result.get("id")
            export_record.external_identifier = externalidentifier
            if externalidentifier is None:
                message = f"Inserted Scheduling Unit Failed for subgroup {subgroup}"
                return False, message, result, None

        export_record.proposal = proposal
        export_record.subgroup = subgroup
        export_record.specification = specification
        export_record.external_environment = proposal.configuration.telescope
        export_record.sync_version = specification.tmss_schedulingunit_id_sync_version
        export_record.synced_at = timezone.now()

        export_record.save()
        message = f"Inserted Scheduling Unit for subgroup {subgroup}"
        return True, message, result, None
    except Exception as e:
        message = "Failure Syncing Subgroup"
        return False, message, None, e


def compose_update(
    definition, name, description, rank, priority_queue, scheduling_set_id
):
    mixed_definition = definition
    mixed_definition["scheduling_set_id"] = scheduling_set_id
    mixed_definition["name"] = name
    mixed_definition["description"] = description
    mixed_definition["rank"] = rank
    mixed_definition["priority_queue_value"] = priority_queue
    return mixed_definition


def synchronize_subgroup_with_specification(
    view, specification_id: int, proposal_id: int, subgroup: int
):
    combined_messsages = []
    try:

        if int(specification_id) < 0:
            return False, [], None, "specification cannot be negative"

        to_be_synced_spec: Specification = get_object_or_404(
            view.get_queryset(), pk=specification_id, proposal_id=proposal_id
        )

        # in order to sync a specification with subgroups, we need to retrieve the targets related to it
        # so let's load them.
        tmss_api = TMSS()
        related_targets = Target.objects.filter(
            proposal=proposal_id,
            group=to_be_synced_spec.targetgroup.group_id_number,
            subgroup=subgroup,
        ).exclude(
            assigned_priority="X"
        )  # Get all targets related to this specification, excluding the one where the assigned priority was lower .

        definition = to_be_synced_spec.definition
        priority = to_be_synced_spec.determinated_priority()

        for related_target in related_targets:
            priority = determine_priority(priority, related_target)
            definition, message = blend_in_target(definition, related_target)
            combined_messsages.append(message)

        proposal = Proposal.objects.get(id=proposal_id)
        combined_messsages.append("compose_specification_name")
        name = compose_specification_name(to_be_synced_spec, related_targets)
        combined_messsages.append("get_tmss_target_rank")
        rank = get_tmss_target_rank(related_targets.first(), to_be_synced_spec)
        combined_messsages.append("create_tmss_specification_from_subgroup")
        did_succeed, message, upstream_result, exception = (
            create_tmss_specification_from_subgroup(
                tmss_api,
                to_be_synced_spec,
                proposal,
                definition,
                name,
                rank,
                priority,
                subgroup,
            )
        )
        combined_messsages.insert(0, message)
        return did_succeed, combined_messsages, upstream_result, exception
    except Exception as e:
        combined_messsages.append("Failure synchronize_subgroup_with_specification")
        return False, combined_messsages, None, e
