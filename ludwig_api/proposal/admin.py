from django.contrib import admin
from polymorphic.admin import PolymorphicInlineSupportMixin, StackedPolymorphicInline

from .models import Call, Cycle, Questionnaire
from .models.questionnaire.question import (
    ChoiceQuestion,
    NumberQuestion,
    Question,
    TextQuestion,
)


@admin.register(Call)
class CallAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "code", "cycle")


@admin.register(Cycle)
class CycleAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "code")


class QuestionAdminInline(StackedPolymorphicInline):
    class ChoiceQuestionInline(StackedPolymorphicInline.Child):
        model = ChoiceQuestion

    class NumberQuestionInline(StackedPolymorphicInline.Child):
        model = NumberQuestion

    class TextQuestionInline(StackedPolymorphicInline.Child):
        model = TextQuestion

    model = Question
    ordering = ("order",)
    child_inlines = (ChoiceQuestionInline, NumberQuestionInline, TextQuestionInline)


@admin.register(Questionnaire)
class QuestionnaireAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    inlines = (QuestionAdminInline,)
    list_display = ("id", "call")
    list_filter = ("call", "call__cycle")
    actions = ("duplicate",)

    @admin.action(description="Duplicate")
    def duplicate(self, request, queryset):
        for q in queryset:
            new = q.copy_to_call()
            new.save()
