from django.contrib import admin
from polymorphic.admin import PolymorphicInlineSupportMixin, StackedPolymorphicInline
from polymorphic.admin.childadmin import PolymorphicChildModelAdmin

from .models import (
    CalculatorResult,
    Call,
    ChoiceAnswer,
    Cycle,
    Proposal,
    Questionnaire,
    Review,
    ReviewComment,
    Target,
    TextAnswer,
)
from .models.calculator_inputs import CalculatorInput
from .models.questionnaire.answer import Answer, NumberAnswer
from .models.questionnaire.question import (
    ChoiceQuestion,
    NumberQuestion,
    Question,
    TextQuestion,
)


@admin.register(Call)
class CallAdmin(admin.ModelAdmin):
    list_display = ("id", "status", "start", "end", "name", "code", "cycle")


@admin.register(Cycle)
class CycleAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "code")


@admin.register(Proposal)
class ProposalAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "status", "collaboration_identifier")


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Review._meta.fields]


@admin.register(ReviewComment)
class ReviewCommentAdmin(admin.ModelAdmin):
    base_model = ReviewComment
    show_in_index = True
    list_display = [field.name for field in ReviewComment._meta.fields]


@admin.register(Answer)
class AnswerAdmin(PolymorphicChildModelAdmin):
    base_model = Answer
    list_display = ("question", "proposal")
    show_in_index = True


@admin.register(CalculatorResult)
class CalculatorResultAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CalculatorResult._meta.fields]


@admin.register(CalculatorInput)
class CalculatorInputAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CalculatorInput._meta.fields]


@admin.register(Target)
class TargetAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Target._meta.fields]


@admin.register(NumberAnswer)
class NumberAnswerAdmin(AnswerAdmin):
    base_model = NumberAnswer
    show_in_index = True


@admin.register(TextAnswer)
class TextAnswerAdmin(AnswerAdmin):
    base_model = TextAnswer
    show_in_index = True


@admin.register(ChoiceAnswer)
class ChoiceAnswerAdmin(AnswerAdmin):
    base_model = ChoiceAnswer
    show_in_index = True


class QuestionAdminInline(StackedPolymorphicInline):
    class ChoiceQuestionInline(StackedPolymorphicInline.Child):
        model = ChoiceQuestion

    class NumberQuestionInline(StackedPolymorphicInline.Child):
        model = NumberQuestion

    class TextQuestionInline(StackedPolymorphicInline.Child):
        model = TextQuestion

    model = Question
    ordering = ("order",)
    child_inlines = (ChoiceQuestionInline, NumberQuestionInline, TextQuestionInline)


@admin.register(Questionnaire)
class QuestionnaireAdmin(PolymorphicInlineSupportMixin, admin.ModelAdmin):
    inlines = (QuestionAdminInline,)
    list_display = ("id", "call")
    list_filter = ("call", "call__cycle")
    actions = ("duplicate",)

    @admin.action(description="Duplicate")
    def duplicate(self, request, queryset):
        for q in queryset:
            new = q.copy_to_call()
            new.save()
