from django.db import models

from ludwig_api.proposal.models.proposal import Proposal
from ludwig_api.proposal.models.specification import Specification


class ExportRecord(models.Model):
    specification = models.ForeignKey(Specification, on_delete=models.CASCADE)
    subgroup = models.PositiveSmallIntegerField(blank=True, null=True)
    proposal = models.ForeignKey(Proposal, on_delete=models.CASCADE)
    synced_at = models.DateTimeField(auto_now_add=True)
    sync_version = models.PositiveIntegerField(blank=True, null=True)
    external_environment = models.CharField(max_length=255)  # Environment exported to
    external_identifier = models.CharField(max_length=128)  # identified externaly by

    class Meta:
        unique_together = (
            "proposal",
            "specification",
            "subgroup",
            "external_environment",
        )

    def __str__(self):
        return f"Specification: {self.specification.name}, Synced at: {self.synced_at} to {self.external_environment} as {self.external_identifier}"
