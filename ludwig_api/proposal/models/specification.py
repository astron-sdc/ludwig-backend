from django.db import models

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.models.target_group import TargetGroup


class Specification(models.Model):
    targetgroup = models.ForeignKey(
        TargetGroup,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="specifications",
    )  # avoid deleting the specification when somebody accidently deletes a group
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=255)
    strategy = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    definition = models.JSONField(null=True, blank=True)
    proposal = models.ForeignKey(Proposal, on_delete=models.CASCADE)
    requested_priority = models.CharField(max_length=1, default="A")
    assigned_priority = models.CharField(max_length=1, default="?")
    tmss_schedulingunit_id = models.IntegerField(default=-1)
    tmss_schedulingunit_id_synced_at = models.DateTimeField(
        auto_now_add=False, null=True, blank=True
    )
    tmss_schedulingunit_id_sync_version = models.PositiveIntegerField(
        blank=True, null=True, default=0
    )

    def __str__(self):
        return f"{self.name}"

    def determinated_priority(self):
        if self.assigned_priority == "?":
            return (
                self.requested_priority
            )  # fallback, not yet assigned, so use requesting
        return self.assigned_priority

    def strategy_number(self):
        return self.strategy.rstrip("/").split("/")[-1]
