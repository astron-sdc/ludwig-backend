from django.db import models


class Cycle(models.Model):
    name = models.CharField(max_length=150)
    code = models.CharField(max_length=5, unique=True)
    description = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return f"{self.name} ({self.code})"
