from django.db import models


class Cycle(models.Model):
    name = models.CharField(max_length=150)
    code = models.CharField(max_length=150)
    description = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    tmss_cycle_id = models.CharField(max_length=150, blank=True)
    tmss_cycle_synced_at = models.DateTimeField(
        auto_now_add=False, null=True, blank=True
    )

    def __str__(self):
        return f"{self.name} ({self.code})"
