from django.core.validators import MaxValueValidator
from django.db import models

from ludwig_api.proposal.models import Proposal, Target


class Calibrators(models.TextChoices):
    C3_48 = "3C48", "3C48"
    C3_147 = "3C147", "3C147"
    C3_196 = "3C196", "3C196"
    C3_295 = "3C295", "3C295"
    C3_380 = "3C380", "3C380"


class ATeamSources(models.TextChoices):
    VIR_A = "VirA", "VirA"
    CAS_A = "CasA", "CasA"
    CYG_A = "CygA", "CygA"
    TAU_A = "TauA", "TauA"


MAX_NO_SUBBANDS = 488
MAX_NO_CORE_STATIONS = 24
MAX_NO_REMOTE_STATIONS = 14
MAX_NO_INTERNATIONAL_STATIONS = 14


class CalculatorInput(models.Model):
    """Calculator input values for a proposal/target combination"""

    # Proposal/target setup
    proposal = models.ForeignKey(to=Proposal, on_delete=models.CASCADE)
    target = models.ForeignKey(
        to=Target, on_delete=models.CASCADE, default=None, null=True, blank=True
    )

    # Observational setup
    observation_time_seconds = models.PositiveIntegerField()
    no_channels_per_subband = models.PositiveSmallIntegerField(
        choices=[(64, "64"), (128, "128"), (256, "256")]
    )
    no_subbands = models.PositiveIntegerField(
        validators=[MaxValueValidator(MAX_NO_SUBBANDS)]
    )
    integration_time_seconds = models.FloatField()
    antenna_set = models.CharField(
        max_length=20,
        choices=[
            ("lbaouter", "LBA Outer"),
            ("lbasparse", "LBA Sparse"),
            ("hbadual", "HBA Dual"),
            ("hbadualinner", "HBA Dual Inner"),
        ],
    )
    no_core_stations = models.PositiveIntegerField(
        validators=[MaxValueValidator(MAX_NO_CORE_STATIONS)]
    )
    no_remote_stations = models.PositiveIntegerField(
        validators=[MaxValueValidator(MAX_NO_REMOTE_STATIONS)]
    )
    no_international_stations = models.PositiveIntegerField(
        validators=[MaxValueValidator(MAX_NO_INTERNATIONAL_STATIONS)]
    )
    observation_date = models.DateField()
    calibrators = models.JSONField(blank=True)
    a_team_sources = models.JSONField(blank=True)

    # Pipeline setup
    pipeline = models.CharField(
        max_length=20,
        choices=[("none", "None"), ("preprocessing", "Preprocessing")],
        blank=True,
    )
    enable_dysco_compression = models.BooleanField(default=False)
    frequency_averaging_factor = models.PositiveIntegerField(null=True, blank=True)
    time_averaging_factor = models.PositiveIntegerField(null=True, blank=True)
