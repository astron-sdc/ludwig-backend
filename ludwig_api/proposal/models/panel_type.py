from django.db import models


class PanelType(models.TextChoices):
    technical = "technical", "Technical"
    scientific = "scientific", "Scientific"
