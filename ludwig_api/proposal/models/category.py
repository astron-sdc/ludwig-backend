from django.db import models

# Project_category :
# "regular" or "user_shared_support" should be choosen between at the PC meeting for the normal proposal calls. (In call model)
# "commissioning" and "ddt" will have separate calls. (In project model)
# For overwrite, All posible options are available at project level


class Category(models.TextChoices):
    # Often used at Call Level
    commissioning = "commissioning", "Commissioning"
    ddt = "ddt", "Directors discretionary time"
    test = "test", "Test"
    regular = "regular", "Regular"
    # When a regular call, it could be changed from regular to user_shared_support
    user_shared_support = "user_shared_support", "User shared support"
