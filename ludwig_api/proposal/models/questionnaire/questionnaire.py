from django.db import models, transaction

from ludwig_api.proposal.models import Call


class Questionnaire(models.Model):
    call = models.ForeignKey(
        Call,
        on_delete=models.CASCADE,
        related_name="questionnaires",
        null=True,
        blank=True,
    )
    # The logic of show/hide questions based on answer to previous question(s) will be done in the frontend (if needed)

    def __str__(self):
        return f"Questionnaire ID {self.pk} for call '{self.call}'"

    def copy_to_call(self, call: Call | None = None) -> "Questionnaire":
        with transaction.atomic():
            questionnaire_copy = Questionnaire.objects.create(call=call)
            for question in self.questions.all():
                # Below 3 lines (pk, id, _state.adding) are needed because of model inheritance
                # https://docs.djangoproject.com/en/5.0/topics/db/queries/#copying-model-instances
                # otherwise it doesn't work (even though the tests might pass)
                question.pk = None
                question.id = None
                question._state.adding = True
                question.questionnaire = questionnaire_copy
                question.save()
        return questionnaire_copy
