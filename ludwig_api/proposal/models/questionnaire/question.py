from django.db import models
from django_jsonform.models.fields import ArrayField
from polymorphic.models import PolymorphicModel

from ludwig_api.proposal.models.questionnaire.questionnaire import Questionnaire


class Question(PolymorphicModel):
    title = models.CharField()
    is_required = models.BooleanField(default=True)

    questionnaire = models.ForeignKey(
        Questionnaire, on_delete=models.CASCADE, related_name="questions"
    )
    order = models.PositiveIntegerField()

    class Meta:
        unique_together = ("questionnaire", "order")


class TextQuestion(Question):
    max_char_length = models.PositiveIntegerField()
    min_char_length = models.PositiveIntegerField(default=0)
    max_word_count = models.PositiveIntegerField()
    min_word_count = models.PositiveIntegerField(default=0)


class NumberQuestion(Question):
    max_value = models.IntegerField()
    min_value = models.IntegerField()
    unit = models.CharField(null=True, blank=True)


class ChoiceQuestion(Question):
    choices = ArrayField(models.CharField())
