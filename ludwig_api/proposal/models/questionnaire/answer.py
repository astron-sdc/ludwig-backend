from django.db import models
from polymorphic.models import PolymorphicModel

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.models._fields import ArbitraryDecimalField
from ludwig_api.proposal.models.questionnaire.question import Question


class Answer(PolymorphicModel):
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE, related_name="answers"
    )
    proposal = models.ForeignKey(Proposal, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("proposal", "question")


class TextAnswer(Answer):
    answer = models.TextField()


class NumberAnswer(Answer):
    answer = ArbitraryDecimalField(null=True)


class ChoiceAnswer(Answer):
    answer = models.CharField()
