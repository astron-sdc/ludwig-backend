from datetime import UTC, datetime
from enum import Enum

from django.db import models

from ludwig_api.faai.sram.models import CollaborationMixin
from ludwig_api.proposal.models import Cycle
from ludwig_api.proposal.models.category import Category


class CallStatus(str, Enum):
    open = "open"
    closed = "closed"


class Call(CollaborationMixin):
    name = models.CharField(max_length=150)
    code = models.CharField(max_length=10)
    description = models.TextField()
    start = models.DateTimeField()  # of submissions, not observations
    end = models.DateTimeField()  # of submissions, not observations
    requires_direct_action = models.BooleanField()
    cycle = models.ForeignKey(Cycle, on_delete=models.CASCADE)
    proposal_sequence_number = models.IntegerField(default=1)
    category = models.CharField(choices=Category.choices, default=Category.regular)

    # Reviews
    technical_review_deadline = models.DateTimeField(null=True, blank=True)
    science_review_deadline = models.DateTimeField(null=True, blank=True)

    # Resources
    observing_time_seconds = models.BigIntegerField(default=0)
    cpu_time_seconds = models.BigIntegerField(default=0)
    support_time_seconds = models.BigIntegerField(default=0)
    storage_space_bytes = models.BigIntegerField(default=0)

    class Meta:
        unique_together = ["cycle", "code"]

    def increment_proposal_sequence_number(self):
        self.proposal_sequence_number += 1

    def __str__(self):
        return f"{self.name} ({self.code})"

    @property
    def status(self) -> CallStatus:
        if self.start <= datetime.now(UTC) <= self.end:
            return CallStatus.open
        return CallStatus.closed

    @property
    def sky_distribution(self) -> list[dict]:
        sky_distribution = []
        for proposal in self.proposals.all():
            sky_distribution_per_proposal = {
                "proposal_id": proposal.id,
                "proposal_title": proposal.title,
                "proposal_status": proposal.status,
                "sky_distribution": {hour: 0.0 for hour in range(24)},
            }

            for calc_input in proposal.calculatorinput_set.all():
                ra = int(calc_input.target.x_hour)
                sky_distribution_per_proposal["sky_distribution"][
                    ra
                ] += calc_input.observation_time_seconds
            sky_distribution.append(sky_distribution_per_proposal)

        return sky_distribution

    @property
    def requested_resources(self) -> list[dict]:
        total_requested_resources = []
        for proposal in self.proposals.all():
            total_resources_per_proposal = {
                "proposal_id": proposal.id,
                "observation_time_seconds": 0,
                "data_size_bytes": 0,
                "pipeline_processing_time_seconds": 0,
            }

            for result in proposal.calculatorresult_set.filter(proposal_id=proposal.id):
                corresponding_input = proposal.calculatorinput_set.filter(
                    target_id=result.target.id
                ).first()

                if corresponding_input:
                    total_resources_per_proposal[
                        "observation_time_seconds"
                    ] += corresponding_input.observation_time_seconds
                    total_resources_per_proposal["data_size_bytes"] += (
                        result.processed_data_size_bytes
                        if result.processed_data_size_bytes != 0
                        else result.raw_data_size_bytes
                    )
                    total_resources_per_proposal[
                        "pipeline_processing_time_seconds"
                    ] += result.pipeline_processing_time_seconds

            total_requested_resources.append(total_resources_per_proposal)

        return total_requested_resources

    @property
    def allocated_resources(self) -> list:
        allocated = []
        for proposal in self.proposals.all():
            if proposal.allocated_observation_time_seconds:
                allocated.append(
                    {
                        "proposal_id": proposal.id,
                        "allocated_observation_time_seconds": proposal.allocated_observation_time_seconds,
                        "allocated_pipeline_processing_time_seconds": proposal.allocated_pipeline_processing_time_seconds,
                        "allocated_support_time_seconds": proposal.allocated_support_time_seconds,
                        "allocated_data_size_bytes": proposal.allocated_data_size_bytes,
                    }
                )
        return allocated
