from django.db import models

from ludwig_api.proposal.models import Cycle


class Call(models.Model):
    name = models.CharField(max_length=150)
    code = models.CharField(max_length=10)
    description = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    requires_direct_action = models.BooleanField()
    cycle = models.ForeignKey(Cycle, on_delete=models.CASCADE)

    class Meta:
        unique_together = ["cycle", "code"]

    def __str__(self):
        return f"{self.name} ({self.code})"
