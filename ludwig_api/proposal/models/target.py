from astropy import units
from astropy.coordinates import Angle
from django.db import models

from ludwig_api.proposal.models import Proposal


class CoordinateSystem(models.TextChoices):
    ICRS = "ICRS", "International Celestial Reference System"
    GAL = "GAL", "Galactic Coordinate System"


class Target(models.Model):
    name = models.CharField(max_length=255)
    x = models.FloatField()
    y = models.FloatField()
    system = models.CharField(
        max_length=10, choices=CoordinateSystem.choices, default=CoordinateSystem.ICRS
    )
    notes = models.TextField(blank=True)
    proposal = models.ForeignKey(Proposal, on_delete=models.CASCADE)

    @property
    def x_hms_formatted(self):
        angle = Angle(self.x, unit=units.degree)
        hms = angle.to_string(unit=units.hourangle, sep=":", precision=2, pad=True)
        return hms

    @x_hms_formatted.setter
    def x_hms_formatted(self, value: str):
        angle = Angle(value, unit=units.hour)
        self.x = angle.degree

    @property
    def y_dms_formatted(self):
        angle = Angle(self.y, unit=units.degree)
        dms = angle.to_string(sep=":", precision=1, alwayssign=True, pad=True)
        return dms

    @y_dms_formatted.setter
    def y_dms_formatted(self, value: str):
        angle = Angle(value, unit=units.degree)
        self.y = angle.degree

    def __str__(self):
        return f"{self.name} ({self.x} - {self.y})"

    @property
    def system_name(self):
        return self.get_system_display()
