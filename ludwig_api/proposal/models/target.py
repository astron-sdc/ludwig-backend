from astropy import units
from astropy.coordinates import Angle, SkyCoord
from django.db import models

from ludwig_api.proposal.models import Proposal


class CoordinateSystem(models.TextChoices):
    ICRS = "ICRS", "International Celestial Reference System"
    J2000 = "J2000", "Jullian Epoch 2000"
    GAL = "GAL", "Galactic Coordinate System"


class Target(models.Model):
    name = models.CharField(max_length=255)
    x = models.FloatField()
    y = models.FloatField()
    system = models.CharField(
        max_length=10, choices=CoordinateSystem.choices, default=CoordinateSystem.J2000
    )
    group = models.PositiveSmallIntegerField(blank=True, null=True)
    subgroup = models.PositiveSmallIntegerField(blank=True, null=True)
    requested_priority = models.CharField(max_length=1, default="A")
    assigned_priority = models.CharField(max_length=1, default="?")
    field_order = models.PositiveSmallIntegerField(blank=True, null=True)
    notes = models.TextField(blank=True)
    proposal = models.ForeignKey(Proposal, on_delete=models.CASCADE)

    @property
    def x_hour(self) -> int:
        angle = Angle(self.x, unit=units.degree)
        return int(round(angle.hour)) % 24

    @property
    def x_hms_formatted(self):
        angle = Angle(self.x, unit=units.degree)
        hms = angle.to_string(unit=units.hourangle, sep=":", precision=2, pad=True)
        return hms

    @x_hms_formatted.setter
    def x_hms_formatted(self, value: str):
        angle = Angle(value, unit=units.hour)
        self.x = angle.degree

    @property
    def y_dms_formatted(self):
        angle = Angle(self.y, unit=units.degree)
        dms = angle.to_string(sep=":", precision=1, alwayssign=True, pad=True)
        return dms

    @y_dms_formatted.setter
    def y_dms_formatted(self, value: str):
        angle = Angle(value, unit=units.degree)
        self.y = angle.degree

    def __str__(self):
        return f"{self.name} ({self.x} - {self.y})"

    @property
    def coordinates_formatted(self) -> str:
        coords = SkyCoord(ra=self.x * units.degree, dec=self.y * units.degree)
        return coords.to_string("hmsdms")

    @property
    def system_name(self):
        return self.get_system_display()
