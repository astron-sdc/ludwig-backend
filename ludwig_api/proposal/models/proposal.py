from datetime import UTC, datetime

from django.db import models
from django_fsm import FSMField, transition

from ludwig_api.proposal.models import Call


class ProposalStatus(models.TextChoices):
    draft = "draft"
    submitted = "submitted"
    retracted = "retracted"
    under_review = "under_review"
    rejected = "rejected"
    accepted = "accepted"


upload_to = "proposals/"


class Proposal(models.Model):
    title = models.CharField(max_length=150)
    abstract = models.TextField()
    scientific_justification = models.FileField(upload_to=upload_to)
    # technical_justification = models.FileField()  # in the design but not the ticket
    call = models.ForeignKey(Call, on_delete=models.CASCADE)
    status = FSMField(default=ProposalStatus.draft, choices=ProposalStatus.choices)
    created_at = models.DateTimeField(auto_now_add=True)
    submitted_at = models.DateTimeField(auto_now_add=False, null=True, blank=True)

    def __str__(self):
        return f"{self.title} ({self.call})"

    def within_call_window(self) -> bool:
        return self.call.start <= datetime.now(UTC) <= self.call.end

    def deadline_has_passed(self) -> bool:
        return self.call.end < datetime.now(UTC)

    def scientific_justification_exists(self):
        return bool(
            self.scientific_justification
        ) and self.scientific_justification.storage.exists(
            self.scientific_justification.name
        )

    @property
    def scientific_justification_filename(self):
        if self.scientific_justification_exists():
            return self.scientific_justification.name.removeprefix(upload_to)
        return ""

    @property
    def scientific_justification_filesize(self):
        if self.scientific_justification_exists():
            return self.scientific_justification.size
        return 0

    @transition(
        field=status,
        source=ProposalStatus.draft,
        target=ProposalStatus.submitted,
        conditions=[within_call_window],
    )
    def submit(self):
        self.submitted_at = datetime.now(UTC)

    @transition(
        field=status,
        source=ProposalStatus.submitted,
        target=ProposalStatus.draft,
        conditions=[within_call_window],
    )
    def unsubmit(self):
        self.submitted_at = None

    @transition(
        field=status,
        source=ProposalStatus.draft,
        target=ProposalStatus.retracted,
    )
    def retract(self):
        # maybe email the author
        pass

    @transition(
        field=status,
        source=ProposalStatus.submitted,
        target=ProposalStatus.under_review,
    )
    def review(self):
        # not sure if this should be automatic (once deadline has passed, all submitted becomes under review)?
        pass

    @transition(
        field=status, source=ProposalStatus.under_review, target=ProposalStatus.rejected
    )
    def reject(self):
        # maybe email the author
        pass

    @transition(
        field=status, source=ProposalStatus.under_review, target=ProposalStatus.accepted
    )
    def accept(self):
        # maybe email the author
        pass
