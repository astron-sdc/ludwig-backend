import os
from datetime import UTC, datetime
from statistics import StatisticsError, mean

from django.contrib.postgres.fields import ArrayField
from django.db import models, transaction
from django_fsm import FSMField, transition

from ludwig_api.faai.constants import EDUPERSON_ENTITLEMENT_CLAIM
from ludwig_api.faai.sram.models import CollaborationMixin
from ludwig_api.proposal.models import Call
from ludwig_api.proposal.models.call import CallStatus
from ludwig_api.proposal.models.category import Category
from ludwig_api.proposal.models.panel_type import PanelType
from ludwig_api.proposal.models.proposal_configuration import ProposalConfiguration
from ludwig_api.proposal.models.proposal_status import ProposalStatus
from ludwig_api.proposal.permissions.proposal import get_proposal_ids_from_entitlements
from ludwig_api.proposal.tmss.api import TMSS


class CallIsNotOpenException(Exception):
    pass


class ProposalPeriod(models.TextChoices):
    single_cycle = "single_cycle", "Single cycle"
    long_term = "long_term", "Long term"
    unbounded = "unbounded", "Unbounded"


def upload_to(instance, filename):
    return os.path.join("proposals", instance.project_code, filename)


class Proposal(CollaborationMixin):
    title = models.CharField(max_length=150)
    abstract = models.TextField()
    configuration = models.OneToOneField(
        ProposalConfiguration, on_delete=models.CASCADE, null=True, blank=True
    )
    scientific_justification = models.FileField(upload_to=upload_to)
    call = models.ForeignKey(Call, on_delete=models.CASCADE, related_name="proposals")
    status = FSMField(default=ProposalStatus.draft, choices=ProposalStatus.choices)
    created_at = models.DateTimeField(auto_now_add=True)
    submitted_at = models.DateTimeField(auto_now_add=False, null=True, blank=True)
    sequence_number = models.IntegerField(default=0)
    created_by = models.CharField(max_length=400, default="")
    contact_author = models.CharField(max_length=400, default="")
    period = models.CharField(
        choices=ProposalPeriod.choices, default=ProposalPeriod.single_cycle
    )
    category = models.CharField(choices=Category.choices, default=Category.regular)
    tmss_project_id = models.CharField(
        max_length=150, blank=True, default=TMSS.CLEAN_PROJECT_ID
    )
    tmss_project_id_synced_at = models.DateTimeField(
        auto_now_add=False, null=True, blank=True
    )
    tmss_project_id_sync_version = models.PositiveIntegerField(
        blank=True, null=True, default=0
    )
    order = models.PositiveIntegerField(default=0)
    rank = models.FloatField(default=0)

    authors = ArrayField(
        models.CharField(max_length=400, default=""), blank=True, null=True
    )

    allocated_observation_time_seconds = models.BigIntegerField(blank=True, null=True)
    allocated_pipeline_processing_time_seconds = models.BigIntegerField(
        blank=True, null=True
    )
    allocated_support_time_seconds = models.BigIntegerField(blank=True, null=True)
    allocated_data_size_bytes = models.BigIntegerField(blank=True, null=True)

    @transaction.atomic
    def save(self, *args, **kwargs):
        if self._state.adding:
            self.on_create()

        super().save(*args, **kwargs)

    def on_create(self):
        if self.status == ProposalStatus.draft and not self.call_is_open():
            raise CallIsNotOpenException(
                "Draft proposal cannot be created in a call that is not open"
            )

        self.sequence_number = self.call.proposal_sequence_number
        self.call.increment_proposal_sequence_number()
        self.call.save()

    def __str__(self):
        return f"{self.title} ({self.call})"

    @property
    def project_code(self):
        # After Production test, we have decided to remove the cycle nummer away from the project
        return f"{self.call.code}_{self.sequence_number:03}"

    @property
    def cycle_name(self):
        return self.call.cycle.name

    @property
    def current_questionnaire(self):
        return self.call.questionnaires.order_by("id").first()

    def is_under_review(self) -> bool:
        return self.status == ProposalStatus.under_review

    def call_is_open(self) -> bool:
        # django-fsm requires this to be callable, not a property
        return self.call.status == CallStatus.open

    def deadline_has_passed(self) -> bool:
        return self.call.end < datetime.now(UTC)

    def scientific_justification_exists(self):
        return bool(
            self.scientific_justification
        ) and self.scientific_justification.storage.exists(
            self.scientific_justification.name
        )

    def is_user_id_author(self, user_id):
        author_ids = {member["eduperson_unique_id"] for member in self.members}
        return self.created_by == user_id or user_id in author_ids

    def is_user_author(self, user):
        entitlements = getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, [])
        return self.created_by == user.id or (
            self.id in get_proposal_ids_from_entitlements(entitlements)
        )

    def is_user_assigned_as_technical_reviewer(self, user):
        return self.reviews.filter(
            reviewer_id=user.id, panel_type=PanelType.technical
        ).exists()

    def is_user_assigned_as_scientific_reviewer(self, user):
        return self.reviews.filter(
            reviewer_id=user.id, panel_type=PanelType.scientific
        ).exists()

    @property
    def scientific_justification_filename(self):
        if self.scientific_justification_exists():
            return os.path.basename(self.scientific_justification.name)
        return ""

    @property
    def scientific_justification_filesize(self):
        if self.scientific_justification_exists():
            return self.scientific_justification.size
        return 0

    @property
    def review_rating(self) -> float | None:
        try:
            return mean([review.ranking for review in self.reviews.exclude(ranking=0)])
        except StatisticsError:
            return None

    def save_author_list(self):
        self.authors = sorted(
            f"{member['name']} <{member['email']}> ({group['name']})"
            for group in self.groups
            for member in group.get("members", [])
        )

    @transition(
        field=status,
        source=ProposalStatus.draft,
        target=ProposalStatus.submitted,
        conditions=[call_is_open],
    )
    def _submit(self):
        pass  # the transition decorator will execute the state transition

    @transition(
        field=status,
        source=ProposalStatus.draft,
        target=ProposalStatus.under_review,
        conditions=[call_is_open],
    )
    def _submit_with_direct_action(self):
        pass  # the transition decorator will execute the state transition

    def submit(self):
        if self.call.requires_direct_action:
            self._submit_with_direct_action()
        else:
            self._submit()

        self.save_author_list()
        self.submitted_at = datetime.now(UTC)

    @transition(
        field=status,
        source=ProposalStatus.submitted,
        target=ProposalStatus.draft,
        conditions=[call_is_open],
    )
    def unsubmit(self):
        self.submitted_at = None

    @transition(
        field=status,
        source=ProposalStatus.draft,
        target=ProposalStatus.retracted,
    )
    def retract(self):
        # maybe email the author
        pass

    @transition(
        field=status,
        source=ProposalStatus.submitted,
        target=ProposalStatus.under_review,
    )
    def review(self):
        # currently done in bulk via call -> review_submitted_proposals endpoint
        pass

    @transition(
        field=status, source=ProposalStatus.under_review, target=ProposalStatus.rejected
    )
    def reject(self):
        # maybe email the author
        pass

    @transition(
        field=status, source=ProposalStatus.under_review, target=ProposalStatus.accepted
    )
    def accept(self):
        # maybe email the author
        pass
