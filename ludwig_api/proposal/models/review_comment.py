from django.db import models

from ludwig_api.proposal.models import Review


class ReviewCommentType(models.TextChoices):
    PROPOSAL = "PROPOSAL", "Entire proposal"
    ABSTRACT = "ABSTRACT", "Abstract"
    SCIENTIFIC_JUSTIFICATION = "SCIENTIFIC", "Scientific justification"
    TECHNICAL_JUSTIFICATION = "TECHNICAL", "Technical justification"
    SPECIFICATION = "SPECIFICATION", "Specification"
    TARGETS = "TARGETS", "Targets"


class ReviewComment(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    type = models.CharField(
        max_length=20,
        choices=ReviewCommentType.choices,
        default=ReviewCommentType.PROPOSAL,
    )
    review = models.ForeignKey(Review, on_delete=models.CASCADE)
