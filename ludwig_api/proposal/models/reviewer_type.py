from django.db import models


class ReviewerType(models.TextChoices):
    primary = "primary", "Primary"
    secondary = "secondary", "Secondary"
