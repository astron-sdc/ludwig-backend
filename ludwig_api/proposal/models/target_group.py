from django.db import models

from ludwig_api.proposal.models import Proposal


class TargetGroup(
    models.Model
):  # Proposal - Targets - Group and Proposal - Specification - Group
    group_id_number = models.PositiveSmallIntegerField(
        blank=True, null=True
    )  # A group has a identifier like 1,2,3,4 also. and more then one group number 1 exist (each proposal can have a group id 1, hence this field)
    subgroup_id_number = models.PositiveSmallIntegerField(
        blank=True, null=True
    )  # A subgroup  represent a list of bundled targets
    name = models.CharField(
        max_length=128
    )  # This name maximum is aligned with the max length of the tmss scheduling set name
    description = models.CharField(
        max_length=255
    )  # This description maximum is aligned with the max length of the tmss scheduling set description
    tmss_scheduling_set_id = models.IntegerField(
        default=-1
    )  # A group is 1 on 1 related to a tmss_scheduling_set by this id
    proposal = models.ForeignKey(
        Proposal, on_delete=models.CASCADE
    )  # And is unique per proposal
    sync_version = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        unique_together = ("group_id_number", "proposal")

    def __str__(self):
        return f"{self.name}"
