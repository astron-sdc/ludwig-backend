from django.db import models
from django_fsm import FSMField

from ludwig_api.proposal.models.telescopes import Telescopes


class ProposalConfiguration(models.Model):
    telescope = FSMField(default=Telescopes.LOFAR, choices=Telescopes.choices)

    auto_ingest = models.BooleanField(default=True)
    auto_pin = models.BooleanField(default=False)
    can_trigger = models.BooleanField(default=False)
    enable_qa_workflow = models.BooleanField(default=True)
    expert = models.BooleanField(
        default=False
    )  # when commission set default on true perhaps ?
    filler = models.BooleanField(default=False)

    trigger_allowed_numbers = models.IntegerField(default=0)
    trigger_used_numbers = models.IntegerField(default=0)
    trigger_priority = models.IntegerField(default=1000)
    piggyback_allowed_aartfaac = models.BooleanField(default=True)
    piggyback_allowed_tbb = models.BooleanField(default=True)
    private_data = models.BooleanField(
        default=False
    )  # Classified observations . hides info on public schema
    send_qa_workflow_notifications = models.BooleanField(
        default=True
    )  # hides info on public schema

    primary_storage_bytes = models.BigIntegerField(default=0)  # Uses for LTA
    secondary_storage_bytes = models.BigIntegerField(default=0)  # Used for CEP
    processing_time_seconds = models.BigIntegerField(default=0)  # Used for CEP
    support_time_seconds = models.BigIntegerField(default=0)

    observing_time_prio_A_awarded_seconds = models.BigIntegerField(
        default=0
    )  # Used for Lofar
    observing_time_prio_B_awarded_seconds = models.BigIntegerField(default=0)
    observing_time_combined_awarded_seconds = models.BigIntegerField(
        default=0
    )  # usal this is A +B
    observing_time_commissioning_awarded_seconds = models.BigIntegerField(default=0)
