from django.db import models
from django.db.models import Case, Q, Value, When

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.models.panel_type import PanelType
from ludwig_api.proposal.models.reviewer_type import ReviewerType

reviewer_id_is_not_blank = ~Q(reviewer_id="")


class Review(models.Model):
    proposal = models.ForeignKey(
        Proposal, on_delete=models.CASCADE, related_name="reviews"
    )
    ranking = models.PositiveSmallIntegerField(default=0)
    reviewer_id = models.CharField(max_length=400, default="")

    panel_type = models.CharField(choices=PanelType.choices, null=True)
    reviewer_type = models.CharField(
        choices=ReviewerType.choices, blank=True, default=""
    )

    class Meta:
        ordering = [
            Case(
                When(panel_type=PanelType.scientific, then=Value(1)),
                When(panel_type=PanelType.technical, then=Value(2)),
            ),
            Case(
                When(reviewer_type=ReviewerType.primary, then=Value(1)),
                When(reviewer_type=ReviewerType.secondary, then=Value(2)),
            ),
        ]
        constraints = [
            models.UniqueConstraint(
                fields=["proposal", "reviewer_id", "panel_type", "reviewer_type"],
                condition=reviewer_id_is_not_blank,
                name="unique_review",
            )
        ]
