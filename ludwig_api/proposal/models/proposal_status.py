from django.db import models


class ProposalStatus(models.TextChoices):
    draft = "draft"
    submitted = "submitted"
    retracted = "retracted"
    under_review = "under_review"
    rejected = "rejected"
    accepted = "accepted"
