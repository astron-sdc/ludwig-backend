from django.db import models

from ludwig_api.proposal.models import Proposal, Target


class CalculatorResult(models.Model):
    """Results cached from the LUCI calculator."""

    proposal = models.ForeignKey(to=Proposal, on_delete=models.CASCADE)
    target = models.ForeignKey(
        to=Target, on_delete=models.CASCADE, default=None, null=True, blank=True
    )
    raw_data_size_bytes = models.BigIntegerField(default=0)
    processed_data_size_bytes = models.BigIntegerField(default=0)
    pipeline_processing_time_seconds = models.BigIntegerField(default=0)
    mean_elevation = models.FloatField(default=0)
    theoretical_rms_jansky_beam = models.FloatField(default=0)
    effective_rms_jansky_beam = models.FloatField(default=0)
    effective_rms_jansky_beam_err = models.FloatField(default=0)
