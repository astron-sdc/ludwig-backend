from rest_framework.serializers import IntegerField, ModelSerializer

from ludwig_api.proposal.models import Call


class CallSerializer(ModelSerializer):
    cycle_id = IntegerField()

    class Meta:
        model = Call
        fields = [
            "id",
            "name",
            "code",
            "description",
            "start",
            "end",
            "requires_direct_action",
            "cycle_id",
        ]
