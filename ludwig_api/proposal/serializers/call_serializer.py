from datetime import UTC, datetime

from rest_framework.fields import (
    CharField,
    DateTimeField,
    IntegerField,
    SerializerMethodField,
)
from rest_framework.serializers import ModelSerializer, Serializer, ValidationError

from ludwig_api.proposal.models import Call
from ludwig_api.proposal.view_helpers.call_helper import create_call_in_sram


class CallSerializer(ModelSerializer):
    cycle_id = IntegerField()
    status = SerializerMethodField()

    cycle_name = CharField(read_only=True, source="cycle.name")
    cycle_start = CharField(read_only=True, source="cycle.start")
    cycle_end = CharField(read_only=True, source="cycle.end")

    class Meta:
        model = Call
        fields = [
            "id",
            "name",
            "code",
            "description",
            "start",
            "end",
            "requires_direct_action",
            "status",
            "cycle_id",
            "cycle_name",
            "cycle_start",
            "cycle_end",
            "observing_time_seconds",
            "cpu_time_seconds",
            "support_time_seconds",
            "storage_space_bytes",
        ]

    @staticmethod
    def get_status(call):
        return call.status

    def validate(self, data: dict):
        if data["start"] > data["end"]:
            raise ValidationError({"end": "End datetime must be after start datetime"})

        return data

    def create(self, validated_data):
        user = self.context["request"].user
        call = super().create(validated_data)
        create_call_in_sram(call, user)
        return call


class CallReviewSerializer(Serializer):
    technical_review_deadline = DateTimeField(required=True)
    science_review_deadline = DateTimeField(required=True)

    @staticmethod
    def validate_technical_review_deadline(value):
        if value < datetime.now(UTC):
            raise ValidationError("Technical review deadline cannot be in the past")

    @staticmethod
    def validate_science_review_deadline(value):
        if value < datetime.now(UTC):
            raise ValidationError("Science review deadline cannot be in the past")
