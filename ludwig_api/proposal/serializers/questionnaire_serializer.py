from rest_framework.serializers import ModelSerializer
from rest_polymorphic.serializers import PolymorphicSerializer

from ludwig_api.proposal.models import (
    ChoiceQuestion,
    NumberQuestion,
    Questionnaire,
    TextQuestion,
)


class ChoiceQuestionSerializer(ModelSerializer):
    class Meta:
        model = ChoiceQuestion
        # using `fields` would necessitate separate serializers for input vs output
        exclude = ("polymorphic_ctype",)


class NumberQuestionSerializer(ModelSerializer):
    class Meta:
        model = NumberQuestion
        exclude = ("polymorphic_ctype",)


class TextQuestionSerializer(ModelSerializer):
    class Meta:
        model = TextQuestion
        exclude = ("polymorphic_ctype",)


class QuestionSerializer(PolymorphicSerializer):
    resource_type_field_name = "type"
    model_serializer_mapping = {
        ChoiceQuestion: ChoiceQuestionSerializer,
        NumberQuestion: NumberQuestionSerializer,
        TextQuestion: TextQuestionSerializer,
    }


class QuestionnaireSerializer(ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = Questionnaire
        fields = (
            "id",
            "call",
            "questions",
        )
