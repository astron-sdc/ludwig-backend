from rest_framework.fields import IntegerField, SerializerMethodField
from rest_framework.serializers import ModelSerializer

from ludwig_api.proposal.models import Review, ReviewComment
from ludwig_api.proposal.view_helpers.user_name_helper import get_user_name


class ReviewSerializer(ModelSerializer):
    proposal_id = IntegerField(read_only=True)
    reviewer_name = SerializerMethodField()

    @staticmethod
    def get_reviewer_name(review):
        return get_user_name(review.reviewer_id)

    def create(self, validated_data):
        proposal_id = self.context.get("proposal_id")
        reviewer_id = validated_data.get("reviewer_id", self.context["request"].user.id)
        ranking = validated_data.get("ranking", 0)
        reviewer_type = validated_data.get("reviewer_type", "")

        # Get scientific review over technical, then primary over secondary.
        # So a secondary scientific review is returned before a primary technical review
        reviews = Review.objects.filter(
            proposal_id=proposal_id, reviewer_id=reviewer_id
        )

        if reviews:
            review = reviews.first()
            review.ranking = ranking
            review.save()
        else:
            review, _ = Review.objects.update_or_create(
                proposal_id=proposal_id,
                reviewer_id=reviewer_id,
                reviewer_type=reviewer_type,
                defaults={"ranking": ranking},
            )

        return review

    class Meta:
        model = Review
        fields = [
            "id",
            "ranking",
            "proposal_id",
            "panel_type",
            "reviewer_id",
            "reviewer_name",
            "reviewer_type",
        ]


class ReviewCommentSerializer(ModelSerializer):
    review_id = IntegerField(read_only=True)
    reviewer_name = SerializerMethodField()

    @staticmethod
    def get_reviewer_name(review_comment):
        return get_user_name(review_comment.review.reviewer_id)

    def create(self, validated_data):
        validated_data["review_id"] = self.context.get(
            "review_id", self.get_or_create_review_id()
        )
        return super().create(validated_data)

    def get_or_create_review_id(self):
        proposal_id = self.context.get("proposal_id")
        reviewer_id = self.context["request"].user.id

        # There can be more than 1 review for a user (one for each panel_type) return the first one.
        review = Review.objects.filter(
            proposal_id=proposal_id, reviewer_id=reviewer_id
        ).first()
        if review is None:
            review = Review.objects.create(
                proposal_id=proposal_id, reviewer_id=reviewer_id
            )

        return review.id

    class Meta:
        model = ReviewComment
        fields = [
            "id",
            "text",
            "created_at",
            "updated_at",
            "type",
            "review_id",
            "reviewer_name",
        ]
