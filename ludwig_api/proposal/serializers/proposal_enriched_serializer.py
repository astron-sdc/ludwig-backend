from django.db import transaction
from rest_framework.serializers import (
    CharField,
    IntegerField,
    ListSerializer,
    ModelSerializer,
)
from rest_framework.validators import ValidationError

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.serializers.review_serializer import ReviewSerializer


# See https://www.django-rest-framework.org/api-guide/serializers/#customizing-multiple-update
# for a background on ListSerializers and updates
class ProposalListEnrichedSerializer(ListSerializer):

    @transaction.atomic
    def update(self, instances, validated_data):
        proposal_mapping = {proposal.id: proposal for proposal in instances}
        data_mapping = {item["id"]: item for item in validated_data}

        updated_proposals = []
        for proposal_id, data in data_mapping.items():
            proposal = proposal_mapping.get(proposal_id, None)
            if proposal is None:
                raise ValidationError("Proposal not found")
            updated_proposals.append(self.child.update(proposal, data))

        for proposal_id, proposal in proposal_mapping.items():
            if proposal_id not in data_mapping:
                raise ValidationError("Incomplete ranking")
        return updated_proposals


class ProposalEnrichedSerializer(ModelSerializer):
    """Proposal model enriched with Review (Comments) and collaboration information"""

    id = IntegerField()  # allow "write" for multi update
    call_id = IntegerField()
    status = CharField(read_only=True)
    reviews = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Proposal
        list_serializer_class = ProposalListEnrichedSerializer
        fields = [
            "id",
            "title",
            "abstract",
            "project_code",
            "scientific_justification_filename",
            "scientific_justification_filesize",
            "status",
            "created_at",
            "members",
            "groups",
            "reviews",
            "review_rating",
            "submitted_at",
            "order",
            "call_id",
            "period",
            "category",
            "tmss_project_id",
            "tmss_project_id_synced_at",
            "allocated_observation_time_seconds",
            "allocated_pipeline_processing_time_seconds",
            "allocated_support_time_seconds",
            "allocated_data_size_bytes",
        ]
