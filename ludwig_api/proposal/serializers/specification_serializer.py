from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from ludwig_api.proposal.models import Specification
from ludwig_api.proposal.view_helpers.group_helper import get_or_create_group


class SpecificationSerializer(ModelSerializer):
    class Meta:
        model = Specification
        fields = [
            "id",
            "name",
            "description",
            "created_at",
            "updated_at",
            "proposal",
            "strategy",
            "requested_priority",
            "assigned_priority",
            "tmss_schedulingunit_id",
            "tmss_schedulingunit_id_synced_at",
            "targetgroup",
        ]


class SpecificationSerializerWithGroup(SpecificationSerializer):
    group_id_number = serializers.SerializerMethodField()

    class Meta(SpecificationSerializer.Meta):
        fields = SpecificationSerializer.Meta.fields + ["group_id_number"]

    def get_group_id_number(self, specification: Specification):
        if specification.targetgroup:
            return specification.targetgroup.group_id_number
        return None


class SpecificationWithDefinitionSerializer(SpecificationSerializerWithGroup):

    class Meta(SpecificationSerializerWithGroup.Meta):
        fields = SpecificationSerializerWithGroup.Meta.fields + ["definition"]

    def handle_targetgroup(self, validated_data):
        targetgroup_id = self.initial_data.get("group_id_number")
        proposal_id = validated_data["proposal"].id

        if targetgroup_id:
            targetgroup = get_or_create_group(targetgroup_id, proposal_id)
            validated_data["targetgroup"] = targetgroup

    def create(self, validated_data):
        # Call shared logic to handle targetgroup assignment
        self.handle_targetgroup(validated_data)
        return super().create(validated_data)

    def update(self, instance, validated_data):
        # Call shared logic to handle targetgroup assignment
        self.handle_targetgroup(validated_data)
        return super().update(instance, validated_data)
