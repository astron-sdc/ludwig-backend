from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import Serializer

from ludwig_api.proposal.models import ChoiceQuestion, NumberQuestion, TextQuestion
from ludwig_api.proposal.serializers.questionnaire_serializer import (
    ChoiceQuestionSerializer,
    NumberQuestionSerializer,
    QuestionnaireSerializer,
    QuestionSerializer,
    TextQuestionSerializer,
)


class QuestionWithAnswerBaseSerializer(Serializer):
    answer = SerializerMethodField()

    def get_answer(self, question):
        proposal_id = self.context.get("proposal_id")
        answer = question.answers.filter(proposal__pk=proposal_id).first()
        return answer.answer if answer else None


class ChoiceQuestionWithAnswerSerializer(
    ChoiceQuestionSerializer, QuestionWithAnswerBaseSerializer
):
    pass


class NumberQuestionWithAnswerSerializer(
    NumberQuestionSerializer, QuestionWithAnswerBaseSerializer
):
    pass


class TextQuestionWithAnswerSerializer(
    TextQuestionSerializer, QuestionWithAnswerBaseSerializer
):
    pass


class QuestionWithAnswerSerializer(QuestionSerializer):
    model_serializer_mapping = {
        ChoiceQuestion: ChoiceQuestionWithAnswerSerializer,
        NumberQuestion: NumberQuestionWithAnswerSerializer,
        TextQuestion: TextQuestionWithAnswerSerializer,
    }


class QuestionnaireWithAnswerSerializer(QuestionnaireSerializer):
    questions = QuestionWithAnswerSerializer(many=True, read_only=True)
