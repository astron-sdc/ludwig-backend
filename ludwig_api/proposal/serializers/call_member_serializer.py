from rest_framework.serializers import ModelSerializer

from ludwig_api.proposal.models import Call


class CallMemberSerializer(ModelSerializer):
    class Meta:
        model = Call
        fields = [
            "members",
            "groups",
            "collaboration_url",
            "members_url",
            "groups_url",
            "invite_link",
            "join_requests_url",
        ]
        read_only_fields = [
            "members",
            "groups",
            "collaboration_url",
            "members_url",
            "groups_url",
            "invite_link",
            "join_requests_url",
        ]
