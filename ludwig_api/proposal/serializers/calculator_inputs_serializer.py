from rest_framework import serializers

from ludwig_api.proposal.models.calculator_inputs import CalculatorInput


class CalculatorInputSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return CalculatorInput.objects.update_or_create(
            target=validated_data["target"],
            proposal=validated_data["proposal"],
            defaults=validated_data,
        )[0]

    class Meta:
        model = CalculatorInput
        fields = [
            "target",
            "proposal",
            "observation_time_seconds",
            "no_channels_per_subband",
            "no_subbands",
            "integration_time_seconds",
            "antenna_set",
            "no_core_stations",
            "no_remote_stations",
            "no_international_stations",
            "observation_date",
            "calibrators",
            "a_team_sources",
            "pipeline",
            "enable_dysco_compression",
            "frequency_averaging_factor",
            "time_averaging_factor",
        ]
