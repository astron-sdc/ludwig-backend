from rest_framework import serializers

from ludwig_api.proposal.models.panel_type import PanelType


class PreAssignReviewersSerializer(serializers.Serializer):
    reviewer_ids = serializers.ListField(
        child=serializers.CharField(), allow_empty=False
    )
    panel_types = serializers.ListField(
        child=serializers.ChoiceField(PanelType.choices)
    )
