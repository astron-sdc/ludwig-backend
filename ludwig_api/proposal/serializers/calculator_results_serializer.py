from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from ludwig_api.proposal.models import CalculatorResult


class CalculatorOutputSerializer(serializers.ModelSerializer):
    target_name = SerializerMethodField()

    @staticmethod
    def get_target_name(calculator_result):
        return calculator_result.target.name

    def create(self, validated_data):
        return CalculatorResult.objects.update_or_create(
            target=validated_data["target"],
            proposal=validated_data["proposal"],
            defaults=validated_data,
        )[0]

    class Meta:
        model = CalculatorResult
        fields = [
            "target",
            "target_name",
            "proposal",
            "raw_data_size_bytes",
            "processed_data_size_bytes",
            "pipeline_processing_time_seconds",
            "mean_elevation",
            "theoretical_rms_jansky_beam",
            "effective_rms_jansky_beam",
            "effective_rms_jansky_beam_err",
        ]
