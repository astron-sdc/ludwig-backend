from rest_framework import serializers

from ludwig_api.proposal.models.export_record import ExportRecord
from ludwig_api.proposal.models.proposal import Proposal
from ludwig_api.proposal.models.specification import Specification


class ExportRecordSerializer(serializers.ModelSerializer):
    specification = serializers.PrimaryKeyRelatedField(
        queryset=Specification.objects.all()
    )
    proposal = serializers.PrimaryKeyRelatedField(queryset=Proposal.objects.all())

    class Meta:
        model = ExportRecord
        fields = [
            "id",
            "specification",
            "proposal",
            "synced_at",
            "subgroup",
            "external_environment",
            "external_identifier",
            "sync_version",
        ]
