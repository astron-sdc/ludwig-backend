from decimal import Decimal, InvalidOperation

from rest_framework.fields import CharField, IntegerField
from rest_framework.serializers import Serializer, ValidationError

from ludwig_api.proposal.models import (
    ChoiceQuestion,
    NumberQuestion,
    Question,
    TextQuestion,
)
from ludwig_api.proposal.models.questionnaire.answer import (
    ChoiceAnswer,
    NumberAnswer,
    TextAnswer,
)

answer_managers = {
    TextQuestion: TextAnswer.objects,
    NumberQuestion: NumberAnswer.objects,
    ChoiceQuestion: ChoiceAnswer.objects,
}

answer_converters = {
    TextQuestion: lambda value: value if value is not None else "",
    NumberQuestion: lambda value: value if value != "" else None,
    ChoiceQuestion: lambda value: value if value is not None else "",
}


def get_word_count(text: str):
    return len(text.strip().split())


def validate_text_question_answer(question: TextQuestion, answer: str):
    return (
        question.min_char_length <= len(answer) <= question.max_char_length
        and question.min_word_count <= get_word_count(answer) <= question.max_word_count
    )


def validate_number_question_answer(question: NumberQuestion, answer: str):
    try:
        return question.min_value <= Decimal(answer) <= question.max_value
    except InvalidOperation:
        return False


def validate_choice_question_answer(question: ChoiceQuestion, answer: str):
    return answer in question.choices


answer_validators = {
    TextQuestion: validate_text_question_answer,
    NumberQuestion: validate_number_question_answer,
    ChoiceQuestion: validate_choice_question_answer,
}


class AnswerSerializer(Serializer):
    question_id = IntegerField()
    answer = CharField(allow_blank=True, allow_null=True)

    class Meta:
        fields = ["question_id", "answer"]

    def create(self, validated_data):
        question_id = validated_data["question_id"]
        answer = validated_data["answer"]
        proposal = validated_data["proposal"]
        question = Question.objects.get(id=question_id)

        if not proposal.current_questionnaire.questions.filter(id=question_id).exists():
            raise ValidationError(
                f"Question {question_id} does not exist in questionnaire {proposal.current_questionnaire.id} for proposal {proposal.id}"
            )
        question_type = type(question)
        validate_answer = answer_validators[question_type]

        if answer and not validate_answer(question, answer):
            raise ValidationError(
                f"Value {answer} is not in range for question {question}"
            )

        convert_answer = answer_converters[question_type]
        answer_manager = answer_managers[question_type]

        return answer_manager.update_or_create(
            question=question,
            proposal=proposal,
            defaults={"answer": convert_answer(answer)},
        )
