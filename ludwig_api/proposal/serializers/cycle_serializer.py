from rest_framework.serializers import ModelSerializer

from ludwig_api.proposal.models import Cycle


class CycleSerializer(ModelSerializer):
    class Meta:
        model = Cycle
        fields = [
            "id",
            "name",
            "code",
            "description",
            "start",
            "end",
            "tmss_cycle_id",
            "tmss_cycle_synced_at",
        ]
