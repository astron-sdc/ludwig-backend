from rest_framework.serializers import ModelSerializer

from ludwig_api.proposal.models.proposal_configuration import ProposalConfiguration


class ProposalConfigurationSerializer(ModelSerializer):
    class Meta:
        model = ProposalConfiguration
        fields = [
            "id",
            "auto_ingest",
            "auto_pin",
            "can_trigger",
            "enable_qa_workflow",
            "expert",
            "filler",
            "trigger_allowed_numbers",
            "trigger_used_numbers",
            "trigger_priority",
            "piggyback_allowed_aartfaac",
            "piggyback_allowed_tbb",
            "private_data",
            "send_qa_workflow_notifications",
            "primary_storage_bytes",
            "secondary_storage_bytes",
            "processing_time_seconds",
            "support_time_seconds",
            "observing_time_prio_A_awarded_seconds",
            "observing_time_prio_B_awarded_seconds",
            "observing_time_combined_awarded_seconds",
            "observing_time_commissioning_awarded_seconds",
        ]
