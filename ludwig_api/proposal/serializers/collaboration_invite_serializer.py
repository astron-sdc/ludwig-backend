from rest_framework import serializers


class CollaborationInviteSerializer(serializers.Serializer):
    invites = serializers.ListField(child=serializers.CharField(), allow_empty=False)
    groups = serializers.ListField(child=serializers.CharField(), allow_empty=False)

    def validate(self, data):
        groups = set(data.get("groups", []))
        collaboration_groups = set(self.context.get("collaboration_groups", []))
        if groups.issubset(collaboration_groups):
            return data

        raise serializers.ValidationError(
            f"The following groups are not collaboration groups: {groups.difference(collaboration_groups)}"
        )
