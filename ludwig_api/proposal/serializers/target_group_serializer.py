from rest_framework.serializers import ModelSerializer

from ludwig_api.proposal.models.target_group import TargetGroup


class TargetGroupSerializer(ModelSerializer):
    class Meta:
        model = TargetGroup
        fields = [
            "id",
            "group_id_number",
            "name",
            "description",
            "tmss_scheduling_set_id",
            "proposal",
        ]
