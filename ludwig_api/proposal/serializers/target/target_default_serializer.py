from astropy import units
from astropy.coordinates import Angle
from django.forms import IntegerField
from rest_framework.fields import CharField, FloatField
from rest_framework.serializers import ModelSerializer, ValidationError

from ludwig_api.proposal.models import Target


class TargetDefaultSerializer(ModelSerializer):
    x_hms_formatted = CharField()
    y_dms_formatted = CharField()
    x = FloatField(read_only=True)
    y = FloatField(read_only=True)
    group = IntegerField(required=False)

    def validate_x_hms_formatted(self, value):
        try:
            Angle(value, unit=units.hour)
        except ValueError:
            raise ValidationError(
                "x_hms_formatted is not in a correct format (expected format: hh:mm:ss.ss)"
            )
        return value

    def validate_y_dms_formatted(self, value):
        try:
            Angle(value, unit=units.degree)
        except ValueError:
            raise ValidationError(
                "y_dms_formatted is not in a correct format (expected format: [+|-]dd:mm:ss.ss)"
            )
        return value

    class Meta:
        model = Target
        fields = [
            "id",
            "name",
            "x_hms_formatted",
            "y_dms_formatted",
            "x",
            "y",
            "group",
            "system",
            "notes",
            "subgroup",
            "requested_priority",
            "assigned_priority",
            "field_order",
        ]
