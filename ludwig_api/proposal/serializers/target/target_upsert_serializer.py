from rest_framework.serializers import IntegerField

from ludwig_api.proposal.serializers.target.target_default_serializer import (
    TargetDefaultSerializer,
)


class TargetUpsertSerializer(TargetDefaultSerializer):
    id = IntegerField(required=False)
