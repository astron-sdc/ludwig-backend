from django.conf import settings
from django.core.exceptions import ValidationError
from rest_framework.serializers import FileField, IntegerField, ModelSerializer

from ludwig_api.proposal.models import Proposal


def scientific_justification_filesize(value):
    limit = settings.SCIENTIFIC_JUSTIFICATION_MAX_FILE_SIZE_IN_MB * 1024 * 1024
    if value.size > limit:
        raise ValidationError(
            f"File size should not exceed {limit} bytes (provided: {value.size})"
        )


class ProposalSerializer(ModelSerializer):
    call_id = IntegerField()
    scientific_justification = FileField(
        write_only=True, required=False, validators=[scientific_justification_filesize]
    )

    class Meta:
        model = Proposal
        fields = [
            "id",
            "title",
            "abstract",
            "scientific_justification",
            "scientific_justification_filename",
            "scientific_justification_filesize",
            "status",
            "created_at",
            "call_id",
        ]
