from django.conf import settings
from django.core.exceptions import ValidationError
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import (
    BooleanField,
    CharField,
    FileField,
    IntegerField,
    ModelSerializer,
)

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.view_helpers.proposal_helper import create_proposal_in_sram


def scientific_justification_filesize(value):
    limit = settings.SCIENTIFIC_JUSTIFICATION_MAX_FILE_SIZE_IN_MB * 1024 * 1024
    if value.size > limit:
        raise ValidationError(
            f"File size should not exceed {limit} bytes (provided: {value.size})"
        )


class ProposalSerializer(ModelSerializer):
    call_id = IntegerField()
    cycle_name = SerializerMethodField()
    scientific_justification = FileField(
        write_only=True, required=False, validators=[scientific_justification_filesize]
    )
    status = CharField(read_only=True)
    call_is_open = BooleanField(read_only=True)
    created_by = CharField(read_only=True)

    def create(self, validated_data):
        user = self.context["request"].user
        validated_data["created_by"] = user.id
        validated_data["contact_author"] = user.id
        proposal = super().create(validated_data)
        create_proposal_in_sram(proposal, user)
        return proposal

    @staticmethod
    def get_cycle_name(proposal):
        return proposal.cycle_name

    class Meta:
        model = Proposal
        fields = [
            "id",
            "title",
            "abstract",
            "project_code",
            "scientific_justification",
            "scientific_justification_filename",
            "scientific_justification_filesize",
            "status",
            "members",
            "groups",
            "collaboration_url",
            "members_url",
            "groups_url",
            "invite_link",
            "join_requests_url",
            "submitted_at",
            "created_at",
            "created_by",
            "contact_author",
            "authors",
            "call_id",
            "call_is_open",
            "cycle_name",
            "period",
            "category",
            "tmss_project_id",
            "tmss_project_id_synced_at",
            "rank",
        ]
