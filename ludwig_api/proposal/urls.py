from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

from ludwig_api.faai.views.userinfo_viewset import UserInfo
from ludwig_api.proposal.views.calculator_input_viewset import CalculatorInputViewset
from ludwig_api.proposal.views.calculator_viewset import CalculatorViewSet
from ludwig_api.proposal.views.call_viewset import CallViewSet
from ludwig_api.proposal.views.coordinate_viewset import CoordinateViewSet
from ludwig_api.proposal.views.cycle_viewset import CycleViewSet
from ludwig_api.proposal.views.export_record_viewset import ExportRecordViewSet
from ludwig_api.proposal.views.permissions_view import GlobalPermissionsView
from ludwig_api.proposal.views.proposal_configuration_viewset import (
    ProposalConfigurationViewSet,
)
from ludwig_api.proposal.views.proposal_member_viewset import ProposalMemberViewSet
from ludwig_api.proposal.views.proposal_review_viewset import ProposalReviewView
from ludwig_api.proposal.views.proposal_viewset import ProposalViewSet
from ludwig_api.proposal.views.questionnaire_viewset import (
    ProposalQuestionnaireAnswersViewSet,
    ProposalQuestionnaireViewSet,
    QuestionnaireViewSet,
    QuestionViewSet,
)
from ludwig_api.proposal.views.review_viewset import ReviewCommentViewSet, ReviewViewSet
from ludwig_api.proposal.views.specification_viewset import SpecificationViewSet
from ludwig_api.proposal.views.target_group_viewset import TargetGroupViewSet
from ludwig_api.proposal.views.target_viewset import TargetViewSet
from ludwig_api.proposal.views.tmss_viewset import TMSSViewSet

router = DefaultRouter()
router.register(r"cycles", CycleViewSet, "cycle")
router.register(r"calls", CallViewSet, "call")
router.register(r"proposals", ProposalViewSet, "proposal")
router.register(r"coordinates", CoordinateViewSet, "coordinate")
router.register(r"questionnaires", QuestionnaireViewSet, "questionnaire")
router.register(r"specifications", SpecificationViewSet, "specification")
router.register(r"groups", TargetGroupViewSet, "groups")
router.register(r"export_records", ExportRecordViewSet, "exportrecords")
router.register(
    r"proposalconfiguration", ProposalConfigurationViewSet, "proposalconfiguration"
)
router.register(r"tmss", TMSSViewSet, "tmss")
router.register(r"userinfo", UserInfo, "userinfo")

cycle_call_router = routers.NestedSimpleRouter(router, r"cycles", lookup="cycle")
cycle_call_router.register(r"calls", CallViewSet, "cycle-calls")

questionnaire_question_router = routers.NestedSimpleRouter(
    router, r"questionnaires", lookup="questionnaire"
)
questionnaire_question_router.register(
    r"questions", QuestionViewSet, "questionnaire-question"
)

proposal_childs_router = routers.NestedSimpleRouter(
    router, r"proposals", lookup="proposal"
)

proposal_childs_router.register(
    r"questionnaire", ProposalQuestionnaireViewSet, "proposal-questionnaire"
)
proposal_childs_router.register(
    r"questionnaire/current",
    ProposalQuestionnaireAnswersViewSet,
    "proposal-questionnaire-current",
)

proposal_childs_router.register(r"targets", TargetViewSet, "proposal-targets")
proposal_childs_router.register(r"groups", TargetGroupViewSet, "proposal-groups")
proposal_childs_router.register(
    r"exportrecords", ExportRecordViewSet, "proposal-exportrecords"
)

proposal_childs_router.register(
    r"specifications", SpecificationViewSet, basename="proposal-specifications"
)

proposal_childs_router.register(
    r"configuration", ProposalConfigurationViewSet, basename="proposal-configuration"
)

proposal_childs_router.register(r"reviews", ReviewViewSet, basename="proposal-reviews")
proposal_childs_router.register(
    r"comments", ReviewCommentViewSet, basename="proposal-comments"
)
proposal_childs_router.register(
    r"reviews/(?P<review_pk>[^/.]+)/comments",
    ReviewCommentViewSet,
    basename="review-comments",
)

proposal_childs_router.register(
    r"members", ProposalMemberViewSet, basename="proposal-members"
)

proposal_childs_router.register(
    r"calculator-results", CalculatorViewSet, basename="proposal-calculator-results"
)

proposal_childs_router.register(
    r"calculator-inputs", CalculatorInputViewset, basename="proposal-calculator-inputs"
)

urlpatterns = [
    path("", include(router.urls)),
    path("", include(cycle_call_router.urls)),
    path(
        r"calls/<call_pk>/proposals/",
        ProposalReviewView.as_view(),
        name="call-proposals-list",
    ),
    path(
        r"calls/<call_pk>/assign_reviewers/",
        ReviewViewSet.as_view({"put": "assign_reviewers"}),
        name="call-assign-reviewers",
    ),
    path("", include(questionnaire_question_router.urls)),
    path("", include(proposal_childs_router.urls)),
    path(r"access-allowed", GlobalPermissionsView.as_view(), name="access-allowed"),
]
