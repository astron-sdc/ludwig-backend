from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

from ludwig_api.proposal.views.call_viewset import CallViewSet
from ludwig_api.proposal.views.coordinate_viewset import CoordinateViewSet
from ludwig_api.proposal.views.cycle_viewset import CycleViewSet
from ludwig_api.proposal.views.proposal_viewset import ProposalViewSet
from ludwig_api.proposal.views.questionnaire_viewset import (
    QuestionnaireViewSet,
    QuestionViewSet,
)
from ludwig_api.proposal.views.target_viewset import TargetViewSet

router = DefaultRouter()
router.register(r"cycles", CycleViewSet, "cycle")
router.register(r"calls", CallViewSet, "call")
router.register(r"proposals", ProposalViewSet, "proposal")
router.register(r"coordinates", CoordinateViewSet, "coordinate")
router.register(r"questionnaires", QuestionnaireViewSet, "questionnaire")

cycle_call_router = routers.NestedSimpleRouter(router, r"cycles", lookup="cycle")
cycle_call_router.register(r"calls", CallViewSet, "cycle-calls")

call_proposal_router = routers.NestedSimpleRouter(router, r"calls", lookup="call")
call_proposal_router.register(r"proposals", ProposalViewSet, "call-proposals")

proposal_target_router = routers.NestedSimpleRouter(
    router, r"proposals", lookup="proposal"
)
proposal_target_router.register(r"targets", TargetViewSet, "proposal-targets")

questionnaire_question_router = routers.NestedSimpleRouter(
    router, r"questionnaires", lookup="questionnaire"
)
questionnaire_question_router.register(
    r"questions", QuestionViewSet, "questionnaire-question"
)

urlpatterns = [
    path("", include(router.urls)),
    path("", include(cycle_call_router.urls)),
    path("", include(call_proposal_router.urls)),
    path("", include(proposal_target_router.urls)),
    path("", include(questionnaire_question_router.urls)),
]
