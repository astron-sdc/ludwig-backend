from rest_framework.permissions import BasePermission

from ludwig_api.faai.constants import EDUPERSON_ENTITLEMENT_CLAIM
from ludwig_api.proposal.permissions.application import admin_entitlement


class EntitlementPermission(BasePermission):
    """Permission using entitlements."""

    def entitlements_with_permission(self) -> list[str]:
        """Returns a list of valid entitlements with permission"""
        return []

    def entitlements_with_object_permission(self, obj) -> list[str]:
        """Returns a list of valid entitlements with permission for given object
        **NOTE** overwrite has_permission to
        `return True` if entitlements_with_permissions is left by default
        """
        return []

    def has_permission(self, request, view):
        user = request.user
        entitlements = set(getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, []))
        allowed_entitlements = set(self.entitlements_with_permission())
        allowed_entitlements.add(admin_entitlement())
        return len(allowed_entitlements.intersection(entitlements)) > 0

    def has_disqualifying_attribute(self, request, view, obj):
        """
        Override to implement custom authorization logic. Return True to disallow access, regardless of any other entitlements.
        """
        return False

    def has_permitting_attribute(self, request, view, obj):
        """
        Override to implement custom authorization logic. Return True to allow access, regardless of any other entitlements. Is evaluated after disqualifying attributes.
        """
        return False

    def has_object_permission(self, request, view, obj):
        user = request.user

        if self.has_disqualifying_attribute(request, view, obj):
            return False

        if self.has_permitting_attribute(request, view, obj):
            return True

        entitlements = set(getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, []))
        allowed_entitlements = set(self.entitlements_with_object_permission(obj))
        allowed_entitlements.add(admin_entitlement())
        return len(allowed_entitlements.intersection(entitlements)) > 0


class CreatedByPermission(EntitlementPermission):
    """Permission using entitlements and the `created_by` attribute of the object"""

    def has_object_permission(self, request, view, obj):
        user = request.user

        if getattr(obj, "created_by", False) == user.id:
            return True

        entitlements = set(getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, []))
        allowed_entitlements = set(self.entitlements_with_object_permission(obj))
        allowed_entitlements.add(admin_entitlement())
        return len(allowed_entitlements.intersection(entitlements)) > 0


def method_permission_classes(classes):
    """Overwrite permissions classes for decorated method"""

    def decorator(func):
        def decorated_func(self, *args, **kwargs):
            self.permission_classes = classes
            # this call is needed for request permissions
            self.check_permissions(self.request)
            return func(self, *args, **kwargs)

        return decorated_func

    return decorator
