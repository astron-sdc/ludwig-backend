import re
from enum import Enum

from django.conf import settings
from rest_framework.permissions import BasePermission

from ludwig_api.faai.constants import EDUPERSON_ENTITLEMENT_CLAIM, SRAM_URN_PREFIX
from ludwig_api.faai.sram.entitlements import get_collaborations
from ludwig_api.proposal.permissions.application import Prefix, admin_entitlement
from ludwig_api.proposal.permissions.util import (
    CreatedByPermission,
    EntitlementPermission,
)


def get_call_ids_from_entitlements(entitlements: list[str]) -> list[int]:
    """Convert the entitlements strings into a list of ids of calls"""

    prefix_re = re.compile(
        Prefix.APP.value + Prefix.CALL.value + r"(?P<primary_key>\d+)"
    )

    return [
        int(match.group("primary_key"))
        for match in [
            prefix_re.match(collab) for collab in get_collaborations(entitlements)
        ]
        if match is not None
    ]


class Roles(Enum):
    """Groups in SRAM for call collaborations"""

    COMMITTEE_CHAIR = "committee_chair"
    TECHNICAL_REVIEWER = "tech_reviewer"
    TECHNICAL_REVIEWER_CHAIR = "tech_reviewer_ch"
    SCIENCE_REVIEWER = "sci_reviewer"
    SCIENCE_REVIEWER_CHAIR = "sci_reviewer_ch"


ROLE_NAMES = {
    Roles.COMMITTEE_CHAIR: "Committee Chair",
    Roles.TECHNICAL_REVIEWER: "Technical Reviewer",
    Roles.SCIENCE_REVIEWER: "Science Reviewer",
    Roles.TECHNICAL_REVIEWER_CHAIR: "Technical Reviewer Chair",
    Roles.SCIENCE_REVIEWER_CHAIR: "Science Reviewer Chair",
}

ROLE_DESCRIPTIONS = {
    Roles.COMMITTEE_CHAIR: "Committee Chair",
    Roles.TECHNICAL_REVIEWER: "Technical reviewer",
    Roles.SCIENCE_REVIEWER: "Science reviewer",
    Roles.TECHNICAL_REVIEWER_CHAIR: "Chair of the technical reviewers",
    Roles.SCIENCE_REVIEWER_CHAIR: "Chair of the science reviewers",
}


class Create(EntitlementPermission):
    """Permissions to create Calls (only admins)"""

    def entitlements_with_permission(self) -> list[str]:
        return [admin_entitlement()]


class Read(BasePermission):
    """Determines read access for given Call"""

    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):
        return True


class Write(EntitlementPermission):
    """Can write to given Call"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
        ]


class ViewRanking(CreatedByPermission):
    """View ranking for given Call"""

    def has_permission(self, request, view):
        user = request.user
        entitlements = set(getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, []))

        if admin_entitlement() in entitlements:
            return True

        key = r"(\d+)"
        committee_chair_role = re.compile(
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{key}:{Roles.COMMITTEE_CHAIR.value}",
        )
        return any(
            committee_chair_role.match(entitlement) for entitlement in entitlements
        )

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
        ]


class ViewRequestedResources(CreatedByPermission):
    def has_permission(self, request, view):
        user = request.user
        entitlements = set(getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, []))

        if admin_entitlement() in entitlements:
            return True

        key = r"(\d+)"
        committee_chair_role = re.compile(
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{key}:{Roles.COMMITTEE_CHAIR.value}",
        )
        science_reviewer_chair = re.compile(
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{key}:{Roles.SCIENCE_REVIEWER_CHAIR.value}",
        )
        return any(
            committee_chair_role.match(entitlement)
            or science_reviewer_chair.match(entitlement)
            for entitlement in entitlements
        )

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.SCIENCE_REVIEWER_CHAIR.value}",
        ]


class ViewAllocatedResources(CreatedByPermission):
    def has_permission(self, request, view):
        return ViewRequestedResources().has_permission(request, view)

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return ViewRequestedResources().entitlements_with_object_permission(obj)


class IsCallMember(CreatedByPermission):
    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}",
        ]


class ManageMembersSciencePanel(EntitlementPermission):
    """Manage members science panel for given Call"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.SCIENCE_REVIEWER_CHAIR.value}",
        ]


class ManageMembersTechnicalPanel(EntitlementPermission):
    """Manage members technical panel for given Call"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.TECHNICAL_REVIEWER_CHAIR.value}",
        ]


class Export(EntitlementPermission):
    """Can export accepted proposals for given call to TMSS"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
        ]


class AcceptProposalFromCall(EntitlementPermission):

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
        ]


class RejectProposalFromCall(EntitlementPermission):

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.COMMITTEE_CHAIR.value}",
        ]


class ReadTechnicalReviewFromCall(EntitlementPermission):
    """Determines read technical review access for given Call"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            admin_entitlement(),
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.SCIENCE_REVIEWER.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.SCIENCE_REVIEWER_CHAIR.value}",
            # A non-chair technical reviewer needs to be assigned per proposal to be allowed to read reviews.
            # f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.TECHNICAL_REVIEWER.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.TECHNICAL_REVIEWER_CHAIR.value}",
        ]


class WriteTechnicalReviewFromCall(EntitlementPermission):
    """Determines write technical review access for given Call"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        """
        This action is only possible by being assigned technical reviewer (or being proposal_admin)
        """
        return [
            admin_entitlement(),
        ]


class ReadScienceReviewFromCall(EntitlementPermission):
    """Determines read science review access for given Call"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            admin_entitlement(),
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.SCIENCE_REVIEWER.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.CALL.value}{obj.pk}:{Roles.SCIENCE_REVIEWER_CHAIR.value}",
        ]


class WriteScienceReviewFromCall(EntitlementPermission):
    """
    Determines write science review access for given Call
    For now, this action is only possible by being assigned science reviewer
    """

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        """
        This action is only possible by being assigned scientific reviewer (or being proposal_admin)
        """
        return [
            admin_entitlement(),
        ]
