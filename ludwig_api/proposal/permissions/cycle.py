from ludwig_api.proposal.permissions.application import admin_entitlement
from ludwig_api.proposal.permissions.util import EntitlementPermission


class Create(EntitlementPermission):
    """Permissions to create Cycles (only admins)"""

    def entitlements_with_permission(self) -> list[str]:
        return [admin_entitlement()]
