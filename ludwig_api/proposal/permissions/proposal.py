import re
from enum import Enum

from django.conf import settings

from ludwig_api.faai.constants import SRAM_URN_PREFIX
from ludwig_api.faai.sram.entitlements import get_collaborations
from ludwig_api.proposal.permissions.application import Prefix
from ludwig_api.proposal.permissions.call import (
    ReadScienceReviewFromCall,
    ReadTechnicalReviewFromCall,
)
from ludwig_api.proposal.permissions.util import (
    CreatedByPermission,
    EntitlementPermission,
)


class Roles(Enum):
    """Groups in SRAM for proposal collaborations"""

    PI = "pi"
    CO_AUTHOR = "co_author"
    CO_AUTHOR_READ_ONLY = "co_author_ro"
    CONTACT_AUTHOR = "contact_author"


ROLE_NAMES = {
    Roles.PI: "PI",
    Roles.CO_AUTHOR: "CO Author",
    Roles.CO_AUTHOR_READ_ONLY: "CO Author Read Only",
    Roles.CONTACT_AUTHOR: "Contact Author",
}

ROLE_DESCRIPTIONS = {
    Roles.PI: "Principal Investigator",
    Roles.CO_AUTHOR: "CO Author",
    Roles.CO_AUTHOR_READ_ONLY: "CO Author with read only access",
    Roles.CONTACT_AUTHOR: "Contact Author",
}


def get_proposal_ids_from_entitlements(entitlements: list[str]) -> list[int]:
    """Convert the entitlements strings into a list of ids of proposals"""

    prefix_re = re.compile(
        Prefix.APP.value + Prefix.PROPOSAL.value + r"(?P<primary_key>\d+)"
    )

    return [
        int(match.group("primary_key"))
        for match in [
            prefix_re.match(collab) for collab in get_collaborations(entitlements)
        ]
        if match is not None
    ]


class Read(CreatedByPermission):
    """Determines read access for given Proposal"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}",
        ]


class Write(CreatedByPermission):
    """Determines write access for given Proposal"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}:{Roles.PI.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}:{Roles.CONTACT_AUTHOR.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}:{Roles.CO_AUTHOR.value}",
        ]


class Submit(CreatedByPermission):
    """Determines submit access for given Proposal"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}:{Roles.PI.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}:{Roles.CONTACT_AUTHOR.value}",
        ]


class ManageMembers(CreatedByPermission):
    """Determines access management access for given Proposal"""

    def has_permission(self, request, view):
        return True

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return [
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}:{Roles.PI.value}",
            f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{Prefix.APP.value}{Prefix.PROPOSAL.value}{obj.pk}:{Roles.CONTACT_AUTHOR.value}",
        ]


class ReadTechnicalReviewFromProposal(EntitlementPermission):
    def has_permission(self, request, view):
        return True

    def has_disqualifying_attribute(self, request, view, obj):
        if obj.is_user_author(request.user):
            return True

    def has_permitting_attribute(self, request, view, obj):
        return ReadTechnicalReviewFromCall().has_object_permission(
            request, self, obj.call
        ) or obj.is_user_assigned_as_technical_reviewer(request.user)

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return []


class WriteTechnicalReviewFromProposal(EntitlementPermission):
    def has_permission(self, request, view):
        return True

    def has_disqualifying_attribute(self, request, view, obj):
        if obj.is_user_author(request.user):
            return True

    def has_permitting_attribute(self, request, view, obj):
        return obj.is_user_assigned_as_technical_reviewer(request.user)

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return []


class ReadScienceReviewFromProposal(EntitlementPermission):
    def has_permission(self, request, view):
        return True

    def has_disqualifying_attribute(self, request, view, obj):
        if obj.is_user_author(request.user):
            return True

    def has_permitting_attribute(self, request, view, obj):
        return ReadScienceReviewFromCall().has_object_permission(
            request, self, obj.call
        ) or obj.is_user_assigned_as_scientific_reviewer(request.user)

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return []


class WriteScienceReviewFromProposal(EntitlementPermission):
    def has_permission(self, request, view):
        return True

    def has_disqualifying_attribute(self, request, view, obj):
        if obj.is_user_author(request.user):
            return True

    def has_permitting_attribute(self, request, view, obj):
        return obj.is_user_assigned_as_scientific_reviewer(request.user)

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return []


class ReadCalculationFromProposal(EntitlementPermission):
    def has_permission(self, request, view):
        return True

    def has_disqualifying_attribute(self, request, view, obj):
        return False

    def has_permitting_attribute(self, request, view, obj):
        return (
            Read().has_object_permission(request, self, obj)
            or Write().has_object_permission(request, self, obj)
            or WriteTechnicalReviewFromProposal().has_object_permission(
                request, self, obj
            )
            or ReadScienceReviewFromCall().has_object_permission(
                request, self, obj.call
            )
        )

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return []


class WriteCalculationFromProposal(EntitlementPermission):
    def has_permission(self, request, view):
        return True

    def has_disqualifying_attribute(self, request, view, obj):
        return False

    def has_permitting_attribute(self, request, view, obj):
        return Write().has_object_permission(request, self, obj)

    def entitlements_with_object_permission(self, obj) -> list[str]:
        return []
