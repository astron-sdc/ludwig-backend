from enum import Enum

from django.conf import settings

from ludwig_api.faai.constants import SRAM_URN_PREFIX


class Roles(Enum):
    """Groups in SRAM for the SDC Service collaboration"""

    USER = "user"
    ADMIN = "proposal_admin"


class Prefix(Enum):
    """Prefixes for names of collaborations in SRAM"""

    PROPOSAL = "p"
    CALL = "c"
    APP = settings.SRAM_COLLABORATION_PREFIX


def admin_entitlement() -> str:
    return f"{SRAM_URN_PREFIX}:{settings.SRAM_ORGANISATION_NAME}:{settings.SRAM_APPLICATION_COLLABORATION_NAME}:{Roles.ADMIN.value}"


def is_admin(entitlements: list[str]) -> bool:
    return admin_entitlement() in entitlements
