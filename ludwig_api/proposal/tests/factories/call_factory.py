from datetime import datetime, timedelta, timezone

import factory

from ludwig_api.proposal.models import Call
from ludwig_api.proposal.tests.factories.cycle_factory import CycleFactory


class CallFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Call

    name = factory.Sequence(lambda n: f"Call {n + 1}")
    code = factory.Sequence(lambda n: n + 1)
    description = "Dummy"
    start = datetime.now(timezone.utc) + timedelta(days=-5)
    end = datetime.now(timezone.utc) + timedelta(days=5)
    requires_direct_action = False
    cycle = factory.SubFactory(CycleFactory)
