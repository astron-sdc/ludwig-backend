import factory

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.tests.factories.call_factory import CallFactory
from ludwig_api.proposal.tests.factories.proposal_configuration_factory import (
    ProposalConfigurationFactory,
)


class ProposalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Proposal

    call = factory.SubFactory(CallFactory)

    configuration = factory.SubFactory(ProposalConfigurationFactory)
