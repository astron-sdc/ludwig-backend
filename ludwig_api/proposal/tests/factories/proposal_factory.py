import factory

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.tests.factories.call_factory import CallFactory


class ProposalFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Proposal

    call = factory.SubFactory(CallFactory)
