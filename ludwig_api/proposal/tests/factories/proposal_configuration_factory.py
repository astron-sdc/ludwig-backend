import factory.fuzzy

from ludwig_api.proposal.models.proposal_configuration import ProposalConfiguration


class ProposalConfigurationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProposalConfiguration

    observing_time_prio_A_awarded_seconds = factory.Sequence(lambda n: n + 1)
