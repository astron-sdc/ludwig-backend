import factory
from factory.django import DjangoModelFactory

from ludwig_api.proposal.models import Review, ReviewComment
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory


class ReviewFactory(DjangoModelFactory):
    class Meta:
        model = Review

    ranking = factory.Faker("pyint", min_value=1, max_value=5)
    proposal = factory.SubFactory(ProposalFactory)


class ReviewCommentFactory(DjangoModelFactory):
    class Meta:
        model = ReviewComment

    review = factory.SubFactory(ReviewFactory)
