import factory

from ludwig_api.proposal.models import Target
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory


class TargetFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Target

    name = factory.Sequence(lambda n: f"Target {n + 1}")
    x = factory.Sequence(lambda n: n * 1.0)
    y = factory.Sequence(lambda n: n * 1.0)
    proposal = factory.SubFactory(ProposalFactory)
    group = factory.Sequence(lambda n: n + 1)
