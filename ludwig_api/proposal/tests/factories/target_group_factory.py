import factory

from ludwig_api.proposal.models.target_group import TargetGroup
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory


class TargetGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TargetGroup

    name = factory.Sequence(lambda n: f"Group {n + 1}")
    group_id_number = factory.Sequence(lambda n: n + 1)
    subgroup_id_number = factory.Sequence(lambda n: n + 1)
    name = factory.Sequence(lambda n: f"Group Description {n + 1}")
    proposal = factory.SubFactory(ProposalFactory)
