import factory.fuzzy

from ludwig_api.proposal.models.specification import Specification


class SpecificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Specification

    name = factory.Sequence(lambda n: f"Specification {n + 1}")
