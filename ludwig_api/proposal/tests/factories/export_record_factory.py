import factory.fuzzy

from ludwig_api.proposal.models.export_record import ExportRecord


class ExportRecordFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExportRecord
