import factory.fuzzy

from ludwig_api.proposal.models import (
    ChoiceAnswer,
    ChoiceQuestion,
    NumberQuestion,
    Questionnaire,
    TextQuestion,
)
from ludwig_api.proposal.tests.factories.call_factory import CallFactory


class TextQuestionFactory(factory.django.DjangoModelFactory):
    order = factory.Faker("pyint", min_value=0, max_value=9999999)
    min_char_length = 0
    max_char_length = 100
    min_word_count = 0
    max_word_count = 10

    class Meta:
        model = TextQuestion


class NumberQuestionFactory(factory.django.DjangoModelFactory):
    order = factory.Faker("pyint", min_value=0, max_value=9999999)
    min_value = 0
    max_value = 10

    class Meta:
        model = NumberQuestion


class ChoiceQuestionFactory(factory.django.DjangoModelFactory):
    order = factory.Faker("pyint", min_value=0, max_value=9999999)
    choices = ["yes", "no"]

    class Meta:
        model = ChoiceQuestion


class ChoiceAnswerFactory(factory.django.DjangoModelFactory):
    answer = factory.fuzzy.FuzzyText(length=40)

    class Meta:
        model = ChoiceAnswer


class QuestionnaireFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Questionnaire

    call = factory.SubFactory(CallFactory)
    questions = factory.RelatedFactoryList(
        ChoiceQuestionFactory, size=2, factory_related_name="questionnaire"
    )


class QuestionnaireWithoutQuestionsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Questionnaire

    call = factory.SubFactory(CallFactory)
