import factory

from ludwig_api.proposal.models import ChoiceQuestion, Questionnaire
from ludwig_api.proposal.tests.factories.call_factory import CallFactory


class ChoiceQuestionFactory(factory.django.DjangoModelFactory):
    order = factory.Faker("pyint", min_value=0, max_value=9999999)
    choices = ["yes", "no"]

    class Meta:
        model = ChoiceQuestion


class QuestionnaireFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Questionnaire

    call = factory.SubFactory(CallFactory)
    questions = factory.RelatedFactoryList(
        ChoiceQuestionFactory, size=2, factory_related_name="questionnaire"
    )


class QuestionnaireWithoutQuestionsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Questionnaire

    call = factory.SubFactory(CallFactory)
