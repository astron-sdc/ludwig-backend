from datetime import datetime, timedelta, timezone

import factory.fuzzy

from ludwig_api.proposal.models import Cycle


class CycleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Cycle

    name = factory.Sequence(lambda n: f"Cycle {n + 1}")
    code = factory.Sequence(lambda n: n + 1)
    description = factory.fuzzy.FuzzyText(length=40)
    start = datetime.now(timezone.utc) + timedelta(days=-10)
    end = datetime.now(timezone.utc) + timedelta(days=10)
    tmss_cycle_id = factory.Sequence(lambda n: f"http:/b/{n + 1}")
