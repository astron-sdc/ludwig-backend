import factory

from ludwig_api.proposal.models import CalculatorResult
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.target_factory import TargetFactory


class CalculatorResultFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CalculatorResult

    proposal = factory.SubFactory(ProposalFactory)
    target = factory.SubFactory(TargetFactory)
    raw_data_size_bytes = 1024
    processed_data_size_bytes = 1024
    pipeline_processing_time_seconds = 60
    mean_elevation = 1.0
    theoretical_rms_jansky_beam = 1.0
    effective_rms_jansky_beam = 1.0
    effective_rms_jansky_beam_err = 1.0
