import datetime

import factory

from ludwig_api.proposal.models.calculator_inputs import (
    MAX_NO_CORE_STATIONS,
    MAX_NO_INTERNATIONAL_STATIONS,
    MAX_NO_REMOTE_STATIONS,
    CalculatorInput,
)
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.target_factory import TargetFactory


class CalculatorInputFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CalculatorInput

    proposal = factory.SubFactory(ProposalFactory)
    target = factory.SubFactory(TargetFactory)
    observation_time_seconds = 42
    no_channels_per_subband = 256
    no_subbands = 42
    integration_time_seconds = 1.0
    antenna_set = "hbadualinner"
    no_core_stations = MAX_NO_CORE_STATIONS
    no_remote_stations = MAX_NO_REMOTE_STATIONS
    no_international_stations = MAX_NO_INTERNATIONAL_STATIONS
    observation_date = datetime.datetime(2021, 6, 1)
    calibrators: dict = {}
    a_team_sources: dict = {}

    # Pipeline setup
    pipeline = "preprocessing"
    enable_dysco_compression = True
    frequency_averaging_factor = 1.0
    time_averaging_factor = 2.0
