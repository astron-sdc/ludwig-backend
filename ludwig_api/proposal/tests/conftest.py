import pytest
from django.conf import settings
from pytest_factoryboy import register
from rest_framework.test import APIClient

from ludwig_api.faai.sram.models import (
    CollaborationGroup,
    CollaborationMembership,
    SRAMUser,
)
from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.permissions.application import Prefix
from ludwig_api.proposal.permissions.application import Roles as AppRoles
from ludwig_api.proposal.permissions.call import Roles
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.tests.factories.calculator_input_factory import (
    CalculatorInputFactory,
)
from ludwig_api.proposal.tests.factories.calculator_result_factory import (
    CalculatorResultFactory,
)
from ludwig_api.proposal.tests.factories.call_factory import CallFactory
from ludwig_api.proposal.tests.factories.cycle_factory import CycleFactory
from ludwig_api.proposal.tests.factories.export_record_factory import (
    ExportRecordFactory,
)
from ludwig_api.proposal.tests.factories.proposal_configuration_factory import (
    ProposalConfigurationFactory,
)
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.review_factory import (
    ReviewCommentFactory,
    ReviewFactory,
)
from ludwig_api.proposal.tests.factories.specification_factory import (
    SpecificationFactory,
)
from ludwig_api.proposal.tests.factories.target_factory import TargetFactory

register(CallFactory)
register(ProposalFactory)
register(CycleFactory)
register(ReviewFactory)
register(ReviewCommentFactory)
register(TargetFactory)
register(ProposalConfigurationFactory)
register(SpecificationFactory)
register(ExportRecordFactory)
register(CalculatorInputFactory)
register(CalculatorResultFactory)


def get_user(collaboration: str, group: str):
    return TokenUser(
        {
            "sub": "fc5b1b91-db71-4884-81f4-579e37b2f0f6",  # Keycloak ID
            "name": "Piebe Gealle",
            "preferred_username": "piebe",
            "given_name": "Piebe",
            "family_name": "Gealle",
            "email": "p.gealle@example.com",
            "email_verified": "true",
            "eduperson_entitlement": [
                f"urn:mace:surf.nl:sram:group:{settings.SRAM_ORGANISATION_NAME}:{collaboration}:{group}",
                f"urn:mace:surf.nl:sram:group:{settings.SRAM_ORGANISATION_NAME}:{collaboration}",
                "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
            ],
            "eduperson_unique_id": "6dd48eb203fcd9ce61d0bcfa7ad69a9632ef0d9c@sram.surf.nl",  # SRAM ID
            "token_type": None,
        }
    )


def merge_user_entitlements(users: list[TokenUser]) -> TokenUser:
    entitlements = {
        entitlement
        for user in users
        for entitlement in user.token["eduperson_entitlement"]
    }
    return TokenUser(
        {
            "eduperson_unique_id": users[0].token["eduperson_unique_id"],
            "eduperson_entitlement": list(entitlements),
        }
    )


@pytest.fixture
def get_user_for_proposal():
    def get(proposal: Proposal, role: ProposalRoles) -> TokenUser:
        return get_user(
            f"{Prefix.APP.value}{Prefix.PROPOSAL.value}{proposal.pk}", role.value
        )

    return get


@pytest.fixture
def get_user_for_call():
    def get(call, role: CallRoles) -> TokenUser:
        return get_user(f"{Prefix.APP.value}{Prefix.CALL.value}{call.pk}", role.value)

    return get


@pytest.fixture
def admin_user() -> TokenUser:
    return get_user(settings.SRAM_APPLICATION_COLLABORATION_NAME, AppRoles.ADMIN.value)


@pytest.fixture
def user() -> TokenUser:
    return get_user(settings.SRAM_APPLICATION_COLLABORATION_NAME, AppRoles.USER.value)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def authenticated_api_client():
    client = APIClient()
    client.force_authenticate(
        user=get_user(settings.SRAM_APPLICATION_COLLABORATION_NAME, AppRoles.USER.value)
    )
    return client


@pytest.fixture
def admin_api_client():
    client = APIClient()
    client.force_authenticate(
        user=get_user(
            settings.SRAM_APPLICATION_COLLABORATION_NAME, AppRoles.ADMIN.value
        )
    )
    return client


@pytest.fixture
def single_call(call_factory):
    return call_factory()


def user_id_to_collaboration_group(group_name: str, user_id: str) -> CollaborationGroup:
    return CollaborationGroup(
        identifier=group_name,
        name=group_name,
        description="description",
        short_name=group_name,
        collaboration_memberships=[user_id_to_collaboration_membership(user_id)],
    )


def user_id_to_collaboration_membership(user_id: str) -> CollaborationMembership:
    return CollaborationMembership(
        user=SRAMUser(
            uid=user_id,
            name="Lorem Ipsum",
            given_name="Lorem",
            family_name="Ipsum",
            email="li@example.com",
            schac_home_organisation="",
            scoped_affiliation="",
        )
    )


@pytest.fixture
def collaboration_groups() -> list[CollaborationGroup]:
    return [
        {
            "description": "Principal Investigator",
            "name": "PI",
            "short_name": "pi",
            "identifier": "123124",
            "collaboration_memberships": [
                {
                    "user": {
                        "uid": "pi@sram.surf.nl",
                        "name": "Bobby Tables",
                        "given_name": "pi",
                        "family_name": "",
                        "email": "bobbytables@example.com",
                        "schac_home_organisation": "",
                        "scoped_affiliation": "",
                    }
                }
            ],
        },
        {
            "description": "CO Author",
            "name": "CO Author",
            "short_name": "co_author",
            "identifier": "1234",
            "collaboration_memberships": [
                {
                    "user": {
                        "uid": "foo@sram.surf.nl",
                        "name": "Foo Baz",
                        "given_name": "foo",
                        "family_name": "",
                        "email": "foobaz@example.com",
                        "schac_home_organisation": "",
                        "scoped_affiliation": "",
                    }
                },
                {
                    "user": {
                        "uid": "6dd48eb203fcd9ce61d0bcfa7ad69a9632ef0d9c@sram.surf.nl",
                        "name": "Piebe Gealle",
                        "given_name": "Piebe",
                        "family_name": "Gealle",
                        "email": "p.gealle@example.com",
                        "schac_home_organisation": "",
                        "scoped_affiliation": "",
                    }
                },
            ],
        },
        {
            "description": "CO Author with read only access",
            "name": "CO Author Read Only",
            "short_name": "co_author_ro",
            "identifier": "123456",
            "collaboration_memberships": [
                {
                    "user": {
                        "uid": "foo@sram.surf.nl",
                        "name": "Foo Baz",
                        "given_name": "foo",
                        "family_name": "",
                        "email": "foobaz@example.com",
                        "schac_home_organisation": "",
                        "scoped_affiliation": "",
                    }
                }
            ],
        },
    ]


@pytest.fixture
def call_panel_groups_json() -> list[CollaborationGroup]:
    return [
        {
            "identifier": "committee_chair_id",
            "name": "Committee Chair",
            "short_name": Roles.COMMITTEE_CHAIR.value,
            "description": "Committee Chair",
            "collaboration_memberships": [],
        },
        {
            "identifier": "tech_reviewer_id",
            "name": "Technical Reviewer",
            "short_name": Roles.TECHNICAL_REVIEWER.value,
            "description": "Technical reviewer",
            "collaboration_memberships": [],
        },
        {
            "identifier": "tech_reviewer_chair_id",
            "name": "Technical Reviewer Chair",
            "short_name": Roles.TECHNICAL_REVIEWER_CHAIR.value,
            "description": "Chair of the technical reviewers",
            "collaboration_memberships": [],
        },
        {
            "identifier": "sci_reviewer_id",
            "name": "Science Reviewer",
            "short_name": Roles.SCIENCE_REVIEWER.value,
            "description": "Science reviewer",
            "collaboration_memberships": [],
        },
        {
            "identifier": "sci_reviewer_chair_id",
            "name": "Science Reviewer Chair",
            "short_name": Roles.SCIENCE_REVIEWER_CHAIR.value,
            "description": "Chair of the science reviewers",
            "collaboration_memberships": [],
        },
    ]
