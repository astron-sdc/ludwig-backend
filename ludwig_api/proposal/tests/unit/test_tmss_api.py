import pytest
import requests_mock
from django.conf import settings

from ludwig_api.proposal.tmss.api import TMSS
from ludwig_api.proposal.tmss.models.project import TMSSProject
from ludwig_api.proposal.tmss.models.projectquota import TMSSProjectQuota


@pytest.fixture
def mock_requests():
    with requests_mock.Mocker() as mocker:
        mocker.post(settings.LUDWIG_TMSS_TOKEN_URL, json={"token": "12345678"})
        yield mocker


def test_fetch_strategies(mock_requests):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/scheduling_unit_observing_strategy_template?state=active",
        json={},
    )
    TMSS().get_strategies()


def test_fetch_cycles(mock_requests):
    mock_requests.get(settings.LUDWIG_TMSS_URI + "/api/cycle", json={})
    TMSS().get_cycles()


def test_fetch_cycles_with_500(mock_requests):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/cycle", json="BAD", status_code=500
    )
    result = TMSS().get_cycles()
    assert result.get("error") == '"BAD"'


def test_fetch_utc(mock_requests):
    mock_requests.get(settings.LUDWIG_TMSS_URI + "/api/util/utc", json={})
    TMSS().get_utc_now()


def test_fetch_commonschematemplate(mock_requests):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/common_schema_template?name=stations&ordering=-id&state=active",
        json={},
    )
    TMSS().get_common_schema_template()


def test_fetch_scheduling_contraints_template(mock_requests):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/scheduling_constraints_template/", json={}
    )
    TMSS().get_scheduling_constraints_template()


def test_fetch_task_template(mock_requests):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/task_template?limit=10&offset=12", json={}
    )
    TMSS().get_task_template(10, 12)


def test_fetch_task_draft(mock_requests):
    mock_requests.options(settings.LUDWIG_TMSS_URI + "/api/task_draft/", json={})
    TMSS().get_task_draft()


def test_caching(mock_requests):
    tmssapi = TMSS()
    tmssapi.allow_token_caching()
    assert tmssapi.is_caching_of_token_allowed
    mock_requests.get(settings.LUDWIG_TMSS_URI + "/api/cycle", json={})
    TMSS().get_cycles()
    TMSS().get_cycles()  # Second run for caching, should not throw exceptions


def test_delete_scheduling_unit_definition(mock_requests):
    mock_requests.delete(
        settings.LUDWIG_TMSS_URI + "/api/scheduling_unit_draft/2", json={}
    )
    TMSS().delete_scheduling_unit_definition(2)


def test_put_scheduling_unit_definition(mock_requests):
    mock_requests.put(
        settings.LUDWIG_TMSS_URI + "/api/scheduling_unit_draft/1", json={}
    )
    TMSS().put_scheduling_unit_definition(1, {})


def test_post_project(mock_requests):
    mock_requests.post(settings.LUDWIG_TMSS_URI + "/api/project/", json={})
    TMSS().post_project(TMSSProject())


def test_put_project(mock_requests):
    mock_requests.put(settings.LUDWIG_TMSS_URI + "/api/project/1/", json={})
    TMSS().put_project(TMSSProject(), 1)


def test_post_quota(mock_requests):
    mock_requests.post(settings.LUDWIG_TMSS_URI + "/api/project_quota/", json={})
    TMSS().post_quota(TMSSProjectQuota(project="p", resource_type="r", value=1))


def test_put_quota(mock_requests):
    mock_requests.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/2/", json={})
    TMSS().put_quota(TMSSProjectQuota(project="p", resource_type="r", value=1, id=2))


def test_post_is_observable(mock_requests):
    mock_requests.post(
        settings.LUDWIG_TMSS_URI
        + "/api/scheduling_unit_observing_strategy_template/1/is_observable",
        json={},
    )
    TMSS().post_is_observable(1, {})


def test_delete_scheduling_set(mock_requests):
    mock_requests.delete(
        settings.LUDWIG_TMSS_URI + "/api/scheduling_set/1",
        json={},
    )
    TMSS().delete_scheduling_set(1)
