from enum import Enum

import pytest
from django.conf import settings
from django.test import RequestFactory, override_settings

from ludwig_api.proposal.permissions.application import Prefix
from ludwig_api.proposal.permissions.call import (
    Create,
    Export,
    ManageMembersSciencePanel,
    ManageMembersTechnicalPanel,
    Read,
    Roles,
    ViewAllocatedResources,
    ViewRanking,
    ViewRequestedResources,
    Write,
    get_call_ids_from_entitlements,
)

factory = RequestFactory()


def test_get_call_ids_from_entitlements():
    value = get_call_ids_from_entitlements(
        [
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:pi",
            "urn:mace:surf.nl:sram:group:astron_org_1",
            "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1:scientists",
            "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1",
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{Prefix.APP.value}c123",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{Prefix.APP.value}c123:chair",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{Prefix.APP.value}p456",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{Prefix.APP.value}p456:pi",
            "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
        ]
    )
    expected_call_ids = [123]
    assert value == expected_call_ids


@override_settings(SRAM_COLLABORATION_PREFIX="")
def test_get_callids_from_entitlements__with_extra_prefixes(mocker):
    # need to mock here, since the enum is set before the temp settings are applied
    class MockedPrefix(Enum):
        APP = settings.SRAM_COLLABORATION_PREFIX
        PROPOSAL = Prefix.PROPOSAL.value
        CALL = Prefix.CALL.value

    mocker.patch("ludwig_api.proposal.permissions.call.Prefix", new=MockedPrefix)

    entitlements = [
        "urn:mace:surf.nl:sram:group:astron_org_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:user",
        "urn:mace:surf.nl:sram:group:astron_org_1:cees_c42",  # this other prefix, starts with the Prefixes.Call...
        "urn:mace:surf.nl:sram:group:astron_org_1:cees_c42:astronomer",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}c123",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}c123:chair",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}p456",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}p456:pi",
        "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
    ]

    value = get_call_ids_from_entitlements(entitlements)
    expected_call_ids = [123]
    assert value == expected_call_ids


def test_create_permissions__as_admin(admin_user):
    request = factory.get("/")  # dummy uri
    request.user = admin_user
    assert Create().has_permission(request, None)


def test_create_permissions__as_user(user):
    request = factory.get("/")  # dummy uri
    request.user = user
    assert not Create().has_permission(request, None)


def test_call_ranking__as_admin(admin_user):
    request = factory.get("/")
    request.user = admin_user
    assert ViewRanking().has_permission(request, None)


def test_call_ranking__as_user(user):
    request = factory.get("/")
    request.user = user
    assert not ViewRanking().has_permission(request, None)


@pytest.mark.django_db
def test_call_ranking__as_committee_chair(get_user_for_call, single_call):
    request = factory.get("/")
    request.user = get_user_for_call(single_call, Roles.COMMITTEE_CHAIR)
    assert ViewRanking().has_permission(request, None)


def test_call_requested_allocated_resources__as_admin(admin_user):
    request = factory.get("/")
    request.user = admin_user
    assert ViewRequestedResources().has_permission(request, None)
    assert ViewAllocatedResources().has_permission(request, None)


def test_call_requested_allocated_resources__as_user(user):
    request = factory.get("/")
    request.user = user
    assert not ViewRequestedResources().has_permission(request, None)
    assert not ViewAllocatedResources().has_permission(request, None)


@pytest.mark.django_db
def test_call_requested_allocated_resources__as_committee_chair(
    get_user_for_call, single_call
):
    request = factory.get("/")
    request.user = get_user_for_call(single_call, Roles.COMMITTEE_CHAIR)
    assert ViewRequestedResources().has_permission(request, None)
    assert ViewAllocatedResources().has_permission(request, None)


@pytest.mark.django_db
def test_call_requested_allocated_resources__as_reviewer_roles(
    get_user_for_call, single_call
):
    request = factory.get("/")
    request.user = get_user_for_call(single_call, Roles.SCIENCE_REVIEWER_CHAIR)
    assert ViewRequestedResources().has_permission(request, None)
    assert ViewAllocatedResources().has_permission(request, None)

    request.user = get_user_for_call(single_call, Roles.SCIENCE_REVIEWER)
    assert not ViewRequestedResources().has_permission(request, None)
    assert not ViewAllocatedResources().has_permission(request, None)

    request.user = get_user_for_call(single_call, Roles.TECHNICAL_REVIEWER_CHAIR)
    assert not ViewRequestedResources().has_permission(request, None)
    assert not ViewAllocatedResources().has_permission(request, None)

    request.user = get_user_for_call(single_call, Roles.TECHNICAL_REVIEWER)
    assert not ViewRequestedResources().has_permission(request, None)
    assert not ViewAllocatedResources().has_permission(request, None)


@pytest.mark.django_db
def test_read_permission__as_admin(admin_user, single_call):
    request = factory.get("/")
    request.user = admin_user
    assert Read().has_object_permission(request, None, single_call)


@pytest.mark.django_db
def test_read_permission__as_user(user, single_call):
    request = factory.get("/")
    request.user = user
    assert Read().has_object_permission(request, None, single_call)


@pytest.mark.django_db
def test_write_permission__as_admin(admin_user, single_call):
    request = factory.get("/")
    request.user = admin_user
    assert Write().has_object_permission(request, None, single_call)


@pytest.mark.django_db
def test_write_permission__as_user(user, single_call):
    request = factory.get("/")
    request.user = user
    assert not Write().has_object_permission(request, None, single_call)


@pytest.mark.django_db
def test_manage_members_science_panel_permission__as_admin(admin_user, single_call):
    request = factory.get("/")
    request.user = admin_user
    assert ManageMembersSciencePanel().has_object_permission(request, None, single_call)


@pytest.mark.django_db
def test_manage_members_science_panel_permission__as_user(user, single_call):
    request = factory.get("/")
    request.user = user
    assert not ManageMembersSciencePanel().has_object_permission(
        request, None, single_call
    )


@pytest.mark.django_db
def test_manage_members_technical_panel_permission__as_admin(admin_user, single_call):
    request = factory.get("/")
    request.user = admin_user
    assert ManageMembersTechnicalPanel().has_object_permission(
        request, None, single_call
    )


@pytest.mark.django_db
def test_manage_members_technical_panel_permission__as_user(user, single_call):
    request = factory.get("/")
    request.user = user
    assert not ManageMembersTechnicalPanel().has_object_permission(
        request, None, single_call
    )


@pytest.mark.django_db
def test_export_permission__as_admin(admin_user, single_call):
    request = factory.get("/")
    request.user = admin_user
    assert Export().has_object_permission(request, None, single_call)


@pytest.mark.django_db
def test_export_permission__as_user(user, single_call):
    request = factory.get("/")
    request.user = user
    assert not Export().has_object_permission(request, None, single_call)
