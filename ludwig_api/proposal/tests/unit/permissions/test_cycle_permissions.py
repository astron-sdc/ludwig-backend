import pytest
from django.test import RequestFactory

from ludwig_api.proposal.permissions.cycle import Create

factory = RequestFactory()


@pytest.mark.django_db
def test_create_permissions__as_admin(admin_user):
    request = factory.get("/")
    request.user = admin_user
    assert Create().has_permission(request, None)


@pytest.mark.django_db
def test_create_permissions__as_user(user):
    request = factory.get("/")
    request.user = user
    assert not Create().has_permission(request, None)
