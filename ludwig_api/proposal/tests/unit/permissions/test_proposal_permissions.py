from enum import Enum

import pytest
from django.conf import settings
from django.test import RequestFactory, override_settings

from ludwig_api.proposal.permissions.application import Prefix
from ludwig_api.proposal.permissions.proposal import (
    Read,
    Roles,
    Submit,
    Write,
    get_proposal_ids_from_entitlements,
)

factory = RequestFactory()


def test_get_proposal_ids_from_entitlements():
    value = get_proposal_ids_from_entitlements(
        [
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:pi",
            "urn:mace:surf.nl:sram:group:astron_org_1",
            "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1:scientists",
            "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1",
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}c123",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}c123:chair",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}p456",
            f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}p456:pi",
            "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
        ]
    )
    expected_proposal_ids = [456]
    assert value == expected_proposal_ids


@override_settings(SRAM_COLLABORATION_PREFIX="")
def test_get_proposal_ids_from_entitlements__with_extra_prefixes(mocker):
    # need to mock here, since the enum is set before the temp settings are applied
    class MockedPrefix(Enum):
        APP = settings.SRAM_COLLABORATION_PREFIX
        PROPOSAL = Prefix.PROPOSAL.value
        CALL = Prefix.CALL.value

    mocker.patch("ludwig_api.proposal.permissions.proposal.Prefix", new=MockedPrefix)

    entitlements = [
        "urn:mace:surf.nl:sram:group:astron_org_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:user",
        "urn:mace:surf.nl:sram:group:astron_org_1:peer_p42",  # this other prefix, starts with the Prefixes.Proposal...
        "urn:mace:surf.nl:sram:group:astron_org_1:peer_p42:astronomer",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}c123",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}c123:chair",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}p456",
        f"urn:mace:surf.nl:sram:group:astron_org_1:{settings.SRAM_COLLABORATION_PREFIX}p456:pi",
        "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
    ]

    value = get_proposal_ids_from_entitlements(entitlements)
    expected_proposal_ids = [456]
    assert value == expected_proposal_ids


@pytest.mark.django_db
def test_read_permissions__as_admin(admin_user, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = admin_user
    assert Read().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_read_permissions__as_user(user, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = user
    assert not Read().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_read_permissions__as_user_created_by(user, proposal_factory):
    proposal = proposal_factory(created_by=user.id)

    request = factory.get("/")  # dummy uri
    request.user = user
    assert Read().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_read_permissions__as_pi(get_user_for_proposal, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.PI)
    assert Read().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_read_permissions__as_co_author(get_user_for_proposal, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.CO_AUTHOR)
    assert Read().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_write_permissions__as_admin(admin_user, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = admin_user
    assert Write().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_write_permissions__as_user_created_by(user, proposal_factory):
    proposal = proposal_factory(created_by=user.id)

    request = factory.get("/")  # dummy uri
    request.user = user
    assert Write().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_write_permissions__as_pi(get_user_for_proposal, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.PI)
    assert Write().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_write_permissions__as_co_author(get_user_for_proposal, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.CO_AUTHOR)
    assert Write().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_write_permissions__as_co_author_read_only(
    get_user_for_proposal, proposal_factory
):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.CO_AUTHOR_READ_ONLY)
    assert not Write().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_submit_permissions__as_admin(admin_user, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = admin_user
    assert Submit().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_submit_permissions__as_user_created_by(user, proposal_factory):
    proposal = proposal_factory(created_by=user.id)

    request = factory.get("/")  # dummy uri
    request.user = user
    assert Submit().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_submit_permissions__as_pi(get_user_for_proposal, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.PI)
    assert Submit().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_submit_permissions__as_co_author(get_user_for_proposal, proposal_factory):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.CO_AUTHOR)
    assert not Submit().has_object_permission(request, None, proposal)


@pytest.mark.django_db
def test_submit_permissions__as_co_author_read_only(
    get_user_for_proposal, proposal_factory
):
    proposal = proposal_factory()
    request = factory.get("/")  # dummy uri
    request.user = get_user_for_proposal(proposal, Roles.CO_AUTHOR_READ_ONLY)
    assert not Submit().has_object_permission(request, None, proposal)
