from django.urls import reverse


def test_cycle_list():
    uri = reverse("cycle-list")
    assert uri.endswith("/cycles/")


def test_cycle_detail():
    uri = reverse("cycle-detail", args=[3])
    assert uri.endswith("/cycles/3/")


def test_cycle_calls_list():
    uri = reverse("cycle-calls-list", args=[5])
    assert uri.endswith("/cycles/5/calls/")


def test_call_list():
    uri = reverse("call-list")
    assert uri.endswith("/calls/")


def test_call_detail():
    uri = reverse("call-detail", args=[7])
    assert uri.endswith("/calls/7/")


def test_proposal_list():
    uri = reverse("proposal-list")
    assert uri.endswith("/proposals/")


def test_call_proposals_list():
    uri = reverse("call-proposals-list", args=[9])
    assert uri.endswith("/calls/9/proposals/")


def test_proposal_targets_list():
    uri = reverse("proposal-targets-list", args=[54])
    assert uri.endswith("/proposals/54/targets/")


def test_proposal_detail():
    uri = reverse("proposal-detail", args=[3])
    assert uri.endswith("/proposals/3/")
