import pytest

from ludwig_api.proposal.serializers.answer_serializer import get_word_count


@pytest.mark.parametrize(
    "text,expectation",
    [
        ("", 0),
        ("hello", 1),
        ("one two three four five", 5),
        ("\n  hel lo  \t", 2),
        ("\n  hel    lo  \t", 2),
    ],
)
def test_get_word_count(text, expectation):
    assert get_word_count(text) == expectation
