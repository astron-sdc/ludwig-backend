from unittest import mock

import pytest
import requests_mock
from django.conf import settings
from django.http import Http404, JsonResponse
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.proposal import Proposal
from ludwig_api.proposal.models.specification import Specification
from ludwig_api.proposal.models.target import Target
from ludwig_api.proposal.tests.factories.export_record_factory import (
    ExportRecordFactory,
)
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.specification_factory import (
    SpecificationFactory,
)
from ludwig_api.proposal.tests.factories.target_factory import TargetFactory
from ludwig_api.proposal.tests.factories.target_group_factory import TargetGroupFactory
from ludwig_api.proposal.tmss.api import TMSS
from ludwig_api.proposal.view_helpers.group_helper import (
    create_tmss_specification_group_if_not_exist,
)
from ludwig_api.proposal.view_helpers.proposal_helper import (
    finish_sync_and_clean_unneeded_exports,
    synchronize,
)
from ludwig_api.proposal.view_helpers.specification_helper import (
    blend_in_target,
    is_exported_record_present,
    set_pointing_angles,
    synchronize_subgroup_with_specification,
)

QUOTA_RESULT = [
    {
        "resource_name": "CEP Processing Time",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "LTA Storage",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "CEP Storage",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "LOFAR Observing Time",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "LOFAR Observing Time prio A",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "LOFAR Observing Time prio B",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "LOFAR Observing Time Commissioning",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "LOFAR Support Time",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
    {
        "resource_name": "Number of triggers",
        "result:": "{}",
        "result status": 200,
        "did_succeed": True,
    },
]
SCHEDULING_SET_JSON = json = {
    "count": 1,
    "results": [
        {
            "id": 4,
            "url": "http://localhost:8008/api/scheduling_set/4",
            "description": "",
            "name": "Group Description 1",
            "project": "http://localhost:8008/api/project/COM_LOFAR2",
            "project_id": "COM_LOFAR2",
        }
    ],
}

PROJECT_JSON = json = {
    "name": "2_test_003",
    "url": "http://localhost:8008/api/project/2_test_003",
    "quota": [
        {
            "id": 18,
            "url": "http://localhost:8008/api/project_quota/18",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/CEP%20Processing%20Time",
            "resource_type_id": "CEP Processing Time",
            "value": 25200.0,
        },
        {
            "id": 10,
            "url": "http://localhost:8008/api/project_quota/10",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/LTA%20Storage",
            "resource_type_id": "LTA Storage",
            "value": 65000000000000.0,
        },
        {
            "id": 11,
            "url": "http://localhost:8008/api/project_quota/11",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/CEP%20Storage",
            "resource_type_id": "CEP Storage",
            "value": 43000000000000.0,
        },
        {
            "id": 19,
            "url": "http://localhost:8008/api/project_quota/19",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time",
            "resource_type_id": "LOFAR Observing Time",
            "value": 39600.0,
        },
        {
            "id": 13,
            "url": "http://localhost:8008/api/project_quota/13",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time%20prio%20A",
            "resource_type_id": "LOFAR Observing Time prio A",
            "value": 32400.0,
        },
        {
            "id": 20,
            "url": "http://localhost:8008/api/project_quota/20",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time%20prio%20B",
            "resource_type_id": "LOFAR Observing Time prio B",
            "value": 36000.0,
        },
        {
            "id": 15,
            "url": "http://localhost:8008/api/project_quota/15",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time%20Commissioning",
            "resource_type_id": "LOFAR Observing Time Commissioning",
            "value": 43200.0,
        },
        {
            "id": 21,
            "url": "http://localhost:8008/api/project_quota/21",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/LOFAR%20Support%20Time",
            "resource_type_id": "LOFAR Support Time",
            "value": 28800.0,
        },
        {
            "id": 17,
            "url": "http://localhost:8008/api/project_quota/17",
            "project": "http://localhost:8008/api/project/2_test_003",
            "project_id": "2_test_003",
            "resource_type": "http://localhost:8008/api/resource_type/Number%20of%20triggers",
            "resource_type_id": "Number of triggers",
            "value": 10.0,
        },
    ],
    "quota_ids": [18, 10, 11, 19, 13, 20, 15, 21, 17],
}


@pytest.fixture
def mock_requests():
    with requests_mock.Mocker() as mocker:
        mocker.post(settings.LUDWIG_TMSS_TOKEN_URL, json={"token": "12345678"})
        mocker.post(settings.LUDWIG_TMSS_URI + "/api/project", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/18/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/10/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/11/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/19/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/13/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/20/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/15/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/21/", json={})
        mocker.put(settings.LUDWIG_TMSS_URI + "/api/project_quota/17/", json={})

        mocker.get(
            settings.LUDWIG_TMSS_URI + "/api/resource_type",
            json={
                "count": 9,
                "results": [
                    {
                        "name": "LTA Storage",
                        "url": "http://localhost:8008/api/resource_type/LTA%20Storage?format=json",
                        "created_at": "2024-09-24T15:39:24.647506",
                        "description": "Amount of storage in the LTA (in bytes)",
                        "quantity": "http://localhost:8008/api/quantity/bytes?format=json",
                        "quantity_value": "bytes",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647512",
                    },
                    {
                        "name": "CEP Storage",
                        "url": "http://localhost:8008/api/resource_type/CEP%20Storage?format=json",
                        "created_at": "2024-09-24T15:39:24.647519",
                        "description": "Amount of storage on the CEP processing cluster (in bytes)",
                        "quantity": "http://localhost:8008/api/quantity/bytes?format=json",
                        "quantity_value": "bytes",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647520",
                    },
                    {
                        "name": "CEP Processing Time",
                        "url": "http://localhost:8008/api/resource_type/CEP%20Processing%20Time?format=json",
                        "created_at": "2024-09-24T15:39:24.647525",
                        "description": "Processing time on the CEP processing cluster (in seconds)",
                        "quantity": "http://localhost:8008/api/quantity/time?format=json",
                        "quantity_value": "time",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647527",
                    },
                    {
                        "name": "LOFAR Observing Time",
                        "url": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time?format=json",
                        "created_at": "2024-09-24T15:39:24.647532",
                        "description": "Observing time (in seconds)",
                        "quantity": "http://localhost:8008/api/quantity/time?format=json",
                        "quantity_value": "time",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647534",
                    },
                    {
                        "name": "LOFAR Observing Time prio A",
                        "url": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time%20prio%20A?format=json",
                        "created_at": "2024-09-24T15:39:24.647538",
                        "description": "Observing time with priority A (in seconds)",
                        "quantity": "http://localhost:8008/api/quantity/time?format=json",
                        "quantity_value": "time",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647540",
                    },
                    {
                        "name": "LOFAR Observing Time prio B",
                        "url": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time%20prio%20B?format=json",
                        "created_at": "2024-09-24T15:39:24.647545",
                        "description": "Observing time with priority B (in seconds)",
                        "quantity": "http://localhost:8008/api/quantity/time?format=json",
                        "quantity_value": "time",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647546",
                    },
                    {
                        "name": "LOFAR Observing Time Commissioning",
                        "url": "http://localhost:8008/api/resource_type/LOFAR%20Observing%20Time%20Commissioning?format=json",
                        "created_at": "2024-09-24T15:39:24.647551",
                        "description": "Observing time for Commissioning/DDT (in seconds)",
                        "quantity": "http://localhost:8008/api/quantity/time?format=json",
                        "quantity_value": "time",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647553",
                    },
                    {
                        "name": "LOFAR Support Time",
                        "url": "http://localhost:8008/api/resource_type/LOFAR%20Support%20Time?format=json",
                        "created_at": "2024-09-24T15:39:24.647558",
                        "description": "Support time by human (in seconds)",
                        "quantity": "http://localhost:8008/api/quantity/time?format=json",
                        "quantity_value": "time",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647560",
                    },
                    {
                        "name": "Number of triggers",
                        "url": "http://localhost:8008/api/resource_type/Number%20of%20triggers?format=json",
                        "created_at": "2024-09-24T15:39:24.647565",
                        "description": "Number of trigger events (as integer)",
                        "quantity": "http://localhost:8008/api/quantity/number?format=json",
                        "quantity_value": "number",
                        "tags": [],
                        "updated_at": "2024-09-24T15:39:24.647566",
                    },
                ],
            },
        )
        yield mocker


# Test for synchronize function
@pytest.mark.django_db
def test_synchronize_create_proposal(mock_requests):
    mock_requests.post(settings.LUDWIG_TMSS_URI + "/api/project/", json={})
    proposal = ProposalFactory(tmss_project_id=TMSS.CLEAN_PROJECT_ID)
    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/cycle",
        json={"results": [{"name": "a", "url": proposal.call.cycle.tmss_cycle_id}]},
    )
    tunedjson = PROJECT_JSON
    tunedjson["name"] = proposal.project_code
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/project/"
        + proposal.project_code
        + "?expand=quota",
        json=tunedjson,
    )

    view = mock.Mock()
    view.get_queryset.return_value = Proposal.objects.filter(pk=proposal.pk)

    with mock.patch.object(
        TMSS,
        "post_project",
        return_value=mock.Mock(status_code=201, text="Project created"),
    ):
        response = synchronize(view, proposal.pk)

        # Validate the response and state
        json_response = JsonResponse(
            {
                "did_succeed": True,
                "project_did_succeed": True,
                "upstream_project_message": "Created",
                "upstream_text": "Project created",
                "upstream_status_code": 201,
                "upstream_quota_result": QUOTA_RESULT,
                "quota_did_succeed": True,
                "cycle": proposal.call.cycle.tmss_cycle_id,
            }
        )
        assert response.status_code == json_response.status_code
        assert response.content == json_response.content


@pytest.mark.django_db
def test_synchronize_update_proposal(mock_requests):

    proposal = ProposalFactory(tmss_project_id="1_1_001")
    mock_requests.put(
        settings.LUDWIG_TMSS_URI + "/api/project/" + proposal.project_code, json={}
    )
    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/cycle",
        json={"results": [{"name": "a", "url": proposal.call.cycle.tmss_cycle_id}]},
    )
    tunedjson = PROJECT_JSON
    tunedjson["name"] = proposal.project_code
    proposal.tmss_project_id = proposal.project_code
    proposal.save()
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/project/"
        + proposal.project_code
        + "?expand=quota",
        json=tunedjson,
    )

    view = mock.Mock()
    view.get_queryset.return_value = Proposal.objects.filter(pk=proposal.pk)

    tunedjson = PROJECT_JSON
    tunedjson["name"] = proposal.project_code
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/project/"
        + proposal.project_code
        + "?expand=quota",
        json=tunedjson,
    )

    with mock.patch.object(
        TMSS,
        "put_project",
        return_value=mock.Mock(status_code=200, text="Project updated"),
    ):
        response = synchronize(view, proposal.pk)

        # Validate the response and state
        json_response = JsonResponse(
            {
                "did_succeed": True,
                "project_did_succeed": True,
                "upstream_project_message": "Updated",
                "upstream_text": "Project updated",
                "upstream_status_code": 200,
                "upstream_quota_result": QUOTA_RESULT,
                "quota_did_succeed": True,
                "cycle": proposal.call.cycle.tmss_cycle_id,
            }
        )
        print(response.content)
        assert response.status_code == json_response.status_code
        assert response.content == json_response.content


@pytest.mark.django_db
def test_synchronize_existing_already_proposal(mock_requests):
    mock_requests.put(settings.LUDWIG_TMSS_URI + "/api/project/1_1_001/", json={})

    proposal = ProposalFactory(tmss_project_id="1_1_001")
    tunedjson = PROJECT_JSON
    tunedjson["name"] = proposal.project_code
    proposal.tmss_project_id = proposal.project_code
    proposal.save()
    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/cycle",
        json={"results": [{"name": "a", "url": proposal.call.cycle.tmss_cycle_id}]},
    )
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/project/"
        + proposal.project_code
        + "?expand=quota",
        json=tunedjson,
    )

    view = mock.Mock()
    view.get_queryset.return_value = Proposal.objects.filter(pk=proposal.pk)

    with mock.patch.object(
        TMSS,
        "put_project",
        return_value=mock.Mock(status_code=400, text="Project exist Already"),
    ):
        response = synchronize(view, proposal.pk)

        # Validate the response and state
        json_response = JsonResponse(
            {
                "did_succeed": False,
                "project_did_succeed": False,
                "upstream_project_message": "Project exist Already",
                "upstream_text": "",
                "upstream_status_code": 400,
                "upstream_quota_result": QUOTA_RESULT,
                "quota_did_succeed": True,
                "cycle": proposal.call.cycle.tmss_cycle_id,
            }
        )

        assert response.status_code == json_response.status_code
        assert response.content == json_response.content


@pytest.mark.django_db
def test_synchronize_project_not_found(mock_requests):
    proposal = Proposal()
    mock_requests.post(settings.LUDWIG_TMSS_URI + "/api/project/123/", json={})
    proposal.tmss_project_id = "123"
    view = mock.Mock()
    view.get_queryset.return_value = Proposal.objects.none()

    with pytest.raises(Http404):
        synchronize(
            view, 9999, is_clean_attempt=True
        )  # Assuming 9999 is a non-existent pk


def get_combined_proposal():
    view = mock.Mock()
    proposal_id = 42
    proposal = ProposalFactory(
        id=proposal_id, title="Observing nebulous readings from Andromeda"
    )
    proposal.save()
    group = TargetGroupFactory()

    target = TargetFactory(group=group.id, proposal=proposal, subgroup=2, field_order=1)
    view.get_queryset.return_value = Target.objects.filter(pk=target.id)
    specification: Specification = SpecificationFactory(
        id=2, proposal=proposal, targetgroup=group
    )
    return proposal_id, target, specification, view


@pytest.mark.django_db
def test_synchronize_subgroup_with_specification_does_fail(mock_requests):
    proposal_id, target, specification, view = get_combined_proposal()
    view = mock.Mock()
    view.get_queryset.return_value = Specification.objects.filter(pk=specification.id)
    did_succeed, *_ = synchronize_subgroup_with_specification(
        view,
        specification_id=specification.id,
        subgroup=target.subgroup,
        proposal_id=proposal_id,
    )
    assert not did_succeed


@pytest.mark.django_db
def test_synchronize_subgroup_with_specification_does_succeed(mock_requests):
    mock_requests.post(
        settings.LUDWIG_TMSS_URI
        + "/api/scheduling_unit_observing_strategy_template/11/create_scheduling_unit/",
        json={"id": "99"},
    )
    proposal_id, target, specification, view = get_combined_proposal()
    specification.definition = {}
    specification.strategy = "https://somewhere/11"
    specification.save()

    view.get_queryset.return_value = Specification.objects.filter(pk=specification.id)
    did_succeed, *_ = synchronize_subgroup_with_specification(
        view,
        specification_id=specification.id,
        subgroup=target.subgroup,
        proposal_id=proposal_id,
    )
    assert did_succeed


@pytest.mark.django_db
def test_is_exported_record_present(mock_requests):

    _proposal_id, target, specification, _view = get_combined_proposal()
    export_record = ExportRecordFactory(
        subgroup=target.subgroup,
        proposal=target.proposal,
        specification=specification,
        external_identifier="D7",
    )
    mock_requests.post(
        settings.LUDWIG_TMSS_URI + "/api/scheduling_unit_draft/D7/?fields=id&limit=1",
        json={},
    )
    is_present = is_exported_record_present(TMSS(), export_record)
    assert not is_present


@pytest.mark.django_db
def test_create_tmss_specification_group_if_not_exist_does_fail(mock_requests):
    _proposal_id, _target, specification, _view = get_combined_proposal()
    did_succeed, message, *_ = create_tmss_specification_group_if_not_exist(
        TMSS(), specification
    )
    assert not did_succeed
    assert message == "Project does not exist"


@pytest.mark.django_db
def test_create_tmss_specification_group(mock_requests):
    _proposal_id, _target, specification, _view = get_combined_proposal()
    specification.proposal.tmss_project_id = "COM_LOFAR2"
    specification.targetgroup.tmss_scheduling_set_id = 1
    set_specification_mocks(mock_requests, specification)

    did_succeed, message, *_ = create_tmss_specification_group_if_not_exist(
        TMSS(), specification
    )
    assert "Inserted" in message
    assert did_succeed


def set_specification_mocks(mock_requests, specification):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/scheduling_set/?project="
        + specification.proposal.tmss_project_id
        + "&fields=id,name,description,url",
        json=SCHEDULING_SET_JSON,
    )

    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/project/"
        + specification.proposal.tmss_project_id
        + "?expand=quota",
        json=PROJECT_JSON,
    )
    mock_requests.post(settings.LUDWIG_TMSS_URI + "/api/scheduling_set", json={})


@pytest.mark.django_db
def test_create_tmss_specification_group_mismatch_fix(mock_requests):
    _proposal_id, _target, specification, _view = get_combined_proposal()
    specification.proposal.tmss_project_id = "COM_LOFAR2"
    specification.targetgroup.tmss_scheduling_set_id = 3
    set_specification_mocks(mock_requests, specification)
    did_succeed, message, *_ = create_tmss_specification_group_if_not_exist(
        TMSS(), specification
    )
    assert did_succeed
    assert "Inserted" in message


@pytest.mark.django_db
def test_specification_sync(authenticated_api_client):
    proposal_id, _target, specification, _view = get_combined_proposal()
    url = reverse(
        "specification-sync",
        kwargs={
            "proposalid": proposal_id,
            "specificationid": specification.id,
        },
    )
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_specification_subsync(authenticated_api_client):
    _proposal_id, target, specification, _view = get_combined_proposal()
    url = reverse(
        "specification-subsync",
        kwargs={
            "specificationid": specification.id,
            "subgroupid": target.subgroup,
        },
    )
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_specification_subsync_exception(authenticated_api_client):
    url = reverse(
        "specification-subsync",
        kwargs={
            "specificationid": -100,
            "subgroupid": -200,
        },
    )
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_create_tmss_specification_group_insert_not_required(mock_requests):
    _proposal_id, _target, specification, _view = get_combined_proposal()
    specification.proposal.tmss_project_id = "COM_LOFAR2"
    specification.targetgroup.tmss_scheduling_set_id = 4
    set_specification_mocks(mock_requests, specification)
    did_succeed, message, *_ = create_tmss_specification_group_if_not_exist(
        TMSS(), specification
    )
    assert did_succeed
    assert "Existed" in message


@pytest.mark.django_db
def test_proposal_sync(authenticated_api_client):
    proposal_id, _target, _specification, _view = get_combined_proposal()
    url = reverse(
        "proposal-sync",
        kwargs={
            "pk": proposal_id,
        },
    )
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_finishsync(authenticated_api_client):
    proposal_id, _target, _specification, _view = get_combined_proposal()
    url = reverse(
        "proposal-finish-sync",
        kwargs={
            "pk": proposal_id,
        },
    )
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_sync_direct(authenticated_api_client, mock_requests):
    proposal_id, _target, specification, _view = get_combined_proposal()
    mock_requests.post(
        settings.LUDWIG_TMSS_URI + "/api/project/",
        json={"id": "22", "name": "21"},
    )

    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/cycle",
        json={
            "results": [
                {"name": "a", "url": specification.proposal.call.cycle.tmss_cycle_id}
            ]
        },
    )
    mock_requests.post(
        settings.LUDWIG_TMSS_URI + "/api/project_quota/",
        json={"id": "21"},
    )

    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/project/21?expand=quota",
        json={"id": "21", "url": "//nowhere", "name": "21"},
    )

    view = mock.Mock()
    view.get_queryset.return_value = Proposal.objects.filter(pk=proposal_id)
    specification.proposal.tmss_project_id = "21"
    specification.proposal.save()
    result = synchronize(view, proposal_id, True)

    assert result.status_code == status.HTTP_200_OK

    finalresult = finish_sync_and_clean_unneeded_exports(view, proposal_id)
    assert finalresult.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_set_pointing_angles():
    _proposal_id, target, _specification, _view = get_combined_proposal()
    structure = set_pointing_angles(
        {"digital_pointing": {"angle1": 0, "angle2": 1, "target": 0}}, target
    )
    assert structure.get("digital_pointing").get("target") == target.name


@pytest.mark.django_db
def test_set_blending():
    _proposal_id, target, _specification, _view = get_combined_proposal()
    definition = {
        "tasks": {
            "Observation": {
                "specifications_doc": {
                    "station_configuration": {
                        "SAPs": [
                            {
                                "digital_pointing": {
                                    "angle1": 0,
                                    "angle2": 1,
                                    "target": 0,
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
    blend_in_target(definition, target)
