from io import StringIO

import pytest
from django.conf import settings
from django.core.management import CommandError, call_command

from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.permissions.proposal import (
    ROLE_DESCRIPTIONS,
    ROLE_NAMES,
    Roles,
)
from ludwig_api.proposal.tests.factories.call_factory import CallFactory
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.view_helpers.proposal_helper import SRAMClient


def test_management_sync_sram__requires_admin_email():
    out = StringIO()
    with pytest.raises(CommandError):
        call_command("sync_sram", stdout=out)
        assert (
            out.getvalue().strip()
            == "Error: the following arguments are required: admin_email"
        )


@pytest.mark.django_db
def test_management_sync_sram__with_proposal_and_call(mocker):
    out = StringIO()

    proposal = ProposalFactory(collaboration_identifier="")
    call = CallFactory(collaboration_identifier="")

    mocked_create_proposal_in_sram = mocker.patch(
        "ludwig_api.proposal.management.commands.sync_sram.create_proposal_in_sram",
        autospec=True,
    )
    mocked_create_call_in_sram = mocker.patch(
        "ludwig_api.proposal.management.commands.sync_sram.create_call_in_sram",
        autospec=True,
    )

    call_command("sync_sram", "test@example.com", stdout=out)

    mocked_create_proposal_in_sram.assert_called_with(
        proposal, TokenUser({"email": "test@example.com", "eduperson_unique_id": ""})
    )

    mocked_create_call_in_sram.assert_called_with(
        call, TokenUser({"email": "test@example.com", "eduperson_unique_id": ""})
    )


@pytest.mark.django_db
def test_management_sync_sram__with_proposal_adding_new_groups(mocker):
    out = StringIO()

    proposal = ProposalFactory(collaboration_identifier="ABCD")

    # Don't call the call api
    _ = mocker.patch(
        "ludwig_api.proposal.management.commands.sync_sram.create_call_in_sram",
        autospec=True,
    )

    mocked_get_collaboration = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )

    mocked_create_group = mocker.patch.object(
        SRAMClient,
        "create_group",
        autospec=True,
    )

    mocked_get_collaboration.return_value = {
        "identifier": proposal.collaboration_identifier,
        "groups": [
            {"short_name": Roles.PI.value},
            {"short_name": Roles.CO_AUTHOR.value},
            {"short_name": Roles.CO_AUTHOR_READ_ONLY.value},
        ],
        "services": [{"entity_id": settings.SRAM_LUDWIG_SERVICE_ID}],
    }

    call_command("sync_sram", "test@example.com", stdout=out)

    mocked_create_group.assert_called_with(
        self=mocker.ANY,
        name=ROLE_NAMES[Roles.CONTACT_AUTHOR],
        description=ROLE_DESCRIPTIONS[Roles.CONTACT_AUTHOR],
        short_name=Roles.CONTACT_AUTHOR.value,
        auto_provision_members=False,
        collaboration_identifier=proposal.collaboration_identifier,
    )


@pytest.mark.django_db
def test_management_sync_sram__dry_run(mocker):
    out = StringIO()

    proposal = ProposalFactory(collaboration_identifier="", title="My Helpful Proposal")

    mocked_create_proposal_in_sram = mocker.patch(
        "ludwig_api.proposal.management.commands.sync_sram.create_proposal_in_sram",
        autospec=True,
    )
    mocked_create_call_in_sram = mocker.patch(
        "ludwig_api.proposal.management.commands.sync_sram.create_call_in_sram",
        autospec=True,
    )

    call_command("sync_sram", "test@example.com", "--dry-run", stdout=out)

    mocked_create_proposal_in_sram.assert_not_called()
    mocked_create_call_in_sram.assert_not_called()
    assert (
        out.getvalue().strip()
        == f"Proposals to be updated (1):\n{proposal.title} ({proposal.pk})\nCalls to be updated (1):\n{proposal.call.name} ({proposal.call.pk})"
    )
