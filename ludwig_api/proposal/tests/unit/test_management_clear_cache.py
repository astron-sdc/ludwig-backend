from io import StringIO

import pytest
from django.core.management import CommandError, call_command


def test_management_clear_cache__no_cache_key():
    out = StringIO()
    with pytest.raises(
        CommandError, match="Error: the following arguments are required: key"
    ):
        call_command("clear_cache", stdout=out)
