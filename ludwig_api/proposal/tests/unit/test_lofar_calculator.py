from lofar_calculator import backend


def test_lofar_calculator():
    n_baselines = backend.compute_baselines(1, 2, 3, "hba_high")
    assert n_baselines == 28.0
