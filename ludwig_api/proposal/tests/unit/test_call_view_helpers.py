import pytest

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.permissions.call import ROLE_DESCRIPTIONS, ROLE_NAMES, Roles
from ludwig_api.proposal.tests.factories.call_factory import CallFactory
from ludwig_api.proposal.view_helpers.call_helper import create_call_in_sram


@pytest.mark.django_db
def test_create_call_in_sram(
    mocker,
):
    mocked_create = mocker.patch.object(
        SRAMClient, "create_collaboration", autospec=True
    )
    mocked_create.return_value = {"identifier": "ABC"}

    mocked_group_create = mocker.patch.object(SRAMClient, "create_group", autospec=True)

    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": [],
        "services": [],
        "short_name": "abc",
    }

    mocked_connect_service = mocker.patch.object(
        SRAMClient, "connect_collaboration_service", autospec=True
    )

    call_object = CallFactory(id=42, name="Last call")
    user = TokenUser({"email": "no-reply@example.com"})

    create_call_in_sram(call_object, user)

    mocked_create.assert_called_once()
    mocked_get.assert_called_once()

    calls = [
        mocker.call(
            self=mocker.ANY,
            name=ROLE_NAMES[r],
            description=ROLE_DESCRIPTIONS[r],
            short_name=r.value,
            auto_provision_members=False,
            collaboration_identifier="ABC",
        )
        for r in Roles
    ]

    mocked_group_create.assert_has_calls(calls, any_order=True)
    mocked_connect_service.assert_called_once()
