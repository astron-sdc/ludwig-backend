from ludwig_api.proposal.tmss.models.project import TMSSProject


def test_tmss_project():
    tmss_project = TMSSProject(name="a name", rank=2)
    assert tmss_project.name == "a name"
    assert tmss_project.rank == 2
