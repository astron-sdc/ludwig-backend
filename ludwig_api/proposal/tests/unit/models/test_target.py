import pytest

from ludwig_api.proposal.models import Target
from ludwig_api.proposal.models.target import CoordinateSystem


def test_to_string():
    target = Target(name="Betelgeuse", x=13.37, y=2.71)
    assert target.__str__() == "Betelgeuse (13.37 - 2.71)"


def test_default_system():
    target = Target()
    assert target.system == CoordinateSystem.J2000


@pytest.mark.parametrize(
    "x_hms_formatted,expected",
    [
        ("09:55:33.170", 10),
        ("07:02:44.16", 7),
        ("00:00:00", 0),
        ("23:29:59.59", 23),
        ("23:30:00.00", 0),
    ],
)
def test_coordinate_hour(x_hms_formatted, expected):
    target = Target(x_hms_formatted=x_hms_formatted)
    assert target.x_hour == expected


@pytest.mark.django_db
def test_formatted_coordinates():
    target = Target(name="M31", x=10.68470, y=41.26875)
    assert target.coordinates_formatted == "00h42m44.328s +41d16m07.5s"
