from ludwig_api.proposal.models import Target
from ludwig_api.proposal.models.target import CoordinateSystem


def test_to_string():
    target = Target(name="Betelgeuse", x=13.37, y=2.71)
    assert target.__str__() == "Betelgeuse (13.37 - 2.71)"


def test_default_system():
    target = Target()
    assert target.system == CoordinateSystem.ICRS
