import pytest

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.tests.conftest import user_id_to_collaboration_group


@pytest.mark.django_db
def test_unrelated_user_is_no_author(proposal_factory, user, mocker):
    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration")
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": [],
        "services": [],
        "short_name": "abc",
    }

    proposal = proposal_factory()
    assert not proposal.is_user_author(user)


@pytest.mark.django_db
def test_pi_user_is_proposal_author(proposal_factory, get_user_for_proposal, mocker):
    proposal = proposal_factory()
    pi_user = get_user_for_proposal(proposal, ProposalRoles.PI)

    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration")
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": [user_id_to_collaboration_group("pi", pi_user.id)],
        "services": [],
        "short_name": "abc",
    }
    assert proposal.is_user_author(pi_user)


@pytest.mark.django_db
def test_user_not_accepted_invite_is_proposal_author(
    proposal_factory, get_user_for_call, mocker
):
    proposal = proposal_factory()
    user = get_user_for_call(proposal, CallRoles.SCIENCE_REVIEWER)
    proposal.created_by = user.id
    proposal.save()
    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration")
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": [],
        "services": [],
        "short_name": "abc",
    }
    assert proposal.is_user_author(user)
