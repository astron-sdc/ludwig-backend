from ludwig_api.proposal.models import Call, Proposal


def test_to_string():
    call = Call(name="First call", code="01")
    proposal = Proposal(call=call, title="My first prop")
    assert proposal.__str__() == "My first prop (First call (01))"
