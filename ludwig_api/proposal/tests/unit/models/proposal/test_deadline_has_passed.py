from datetime import UTC, datetime, timedelta
from unittest import mock

import pytest

from ludwig_api.proposal.models import Call, Proposal


@pytest.fixture
def times():
    utc_now = datetime.now(UTC)
    end = utc_now + timedelta(minutes=10)
    before_end = end + timedelta(minutes=-5)
    after_end = end + timedelta(minutes=10)

    return end, before_end, after_end


@mock.patch("ludwig_api.proposal.models.proposal.datetime")
def test_deadline_has_passed(mocked_datetime, times):
    end, _, after_end = times
    mocked_datetime.now.return_value = after_end
    call = Call(end=end)
    proposal = Proposal(call=call)
    assert proposal.deadline_has_passed()


@mock.patch("ludwig_api.proposal.models.proposal.datetime")
def test_before_deadline_has_passed(mocked_datetime, times):
    end, before_end, _ = times
    mocked_datetime.now.return_value = before_end
    call = Call(end=end)
    proposal = Proposal(call=call)
    assert not proposal.deadline_has_passed()


@mock.patch("ludwig_api.proposal.models.proposal.datetime")
def test_on_deadline_has_passed(mocked_datetime, times):
    end, _, end = times
    mocked_datetime.now.return_value = end
    call = Call(end=end)
    proposal = Proposal(call=call)
    assert not proposal.deadline_has_passed()
