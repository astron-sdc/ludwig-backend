from datetime import datetime, timedelta, timezone

import pytest

from ludwig_api.proposal.models import Call, Proposal
from ludwig_api.proposal.models.proposal import CallIsNotOpenException


@pytest.mark.django_db
def test_create_proposal_in_closed_call_should_error():
    now = datetime.now(timezone.utc)
    closed_call = Call(
        start=now - timedelta(minutes=1),
        end=now - timedelta(seconds=1),
        requires_direct_action=False,
    )
    proposal = Proposal(call=closed_call)

    with pytest.raises(CallIsNotOpenException):
        proposal.save()
