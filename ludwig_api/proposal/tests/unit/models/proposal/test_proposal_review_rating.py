import pytest


@pytest.mark.django_db
def test_get_review_rating__without_reviews(proposal_factory):
    proposal = proposal_factory()
    expected = None
    assert proposal.review_rating == expected


@pytest.mark.django_db
def test_get_review_rating__with_reviews(proposal_factory, review_factory):
    proposal = proposal_factory()
    reviews = review_factory.create_batch(
        3,
        proposal=proposal,
    )
    reviews[0].ranking = 1
    reviews[0].save()
    reviews[1].ranking = 3
    reviews[1].save()
    reviews[2].ranking = 5
    reviews[2].save()

    proposal.refresh_from_db()

    expected = 3
    assert proposal.review_rating == expected


@pytest.mark.django_db
def test_get_review_rating__all_reviews_with_no_ranking(
    proposal_factory, review_factory
):
    proposal = proposal_factory()
    reviews = review_factory.create_batch(
        5,
        proposal=proposal,
    )
    reviews[0].ranking = 0
    reviews[0].save()
    reviews[1].ranking = 0
    reviews[1].save()
    reviews[2].ranking = 0
    reviews[2].save()
    reviews[3].ranking = 0
    reviews[3].save()
    reviews[4].ranking = 0
    reviews[4].save()

    proposal.refresh_from_db()

    expected = None
    assert proposal.review_rating == expected


@pytest.mark.django_db
def test_get_review_rating__with_reviews_some_with_no_ranking(
    proposal_factory, review_factory
):
    proposal = proposal_factory()
    reviews = review_factory.create_batch(
        5,
        proposal=proposal,
    )
    reviews[0].ranking = 1
    reviews[0].save()
    reviews[1].ranking = 3
    reviews[1].save()
    reviews[2].ranking = 5
    reviews[2].save()
    reviews[3].ranking = 0
    reviews[3].save()
    reviews[4].ranking = 0
    reviews[4].save()

    proposal.refresh_from_db()

    expected = 3
    assert proposal.review_rating == expected
