from datetime import UTC, datetime, timedelta
from unittest import mock

import pytest

from ludwig_api.proposal.models import Call, Proposal


@pytest.fixture
def times():
    utc_now = datetime.now(UTC)
    start = utc_now
    end = start + timedelta(minutes=10)
    before_start = start + timedelta(minutes=-10)
    after_end = end + timedelta(minutes=10)
    in_between_start_and_end = start + timedelta(minutes=5)
    return start, end, before_start, after_end, in_between_start_and_end


@mock.patch("ludwig_api.proposal.models.call.datetime")
def test_before_within_call_window(mocked_datetime, times):
    start, end, before_start, _, _ = times
    mocked_datetime.now.return_value = before_start
    call = Call(start=start, end=end)
    proposal = Proposal(call=call)
    assert not proposal.call_is_open()


@mock.patch("ludwig_api.proposal.models.call.datetime")
def test_after_within_call_window(mocked_datetime, times):
    start, end, _, after_end, _ = times
    mocked_datetime.now.return_value = after_end
    call = Call(start=start, end=end)
    proposal = Proposal(call=call)
    assert not proposal.call_is_open()


@mock.patch("ludwig_api.proposal.models.call.datetime")
def test_within_call_window(mocked_datetime, times):
    start, end, _, _, in_between_start_and_end = times
    mocked_datetime.now.return_value = in_between_start_and_end
    call = Call(start=start, end=end)
    proposal = Proposal(call=call)
    assert proposal.call_is_open()


@mock.patch("ludwig_api.proposal.models.call.datetime")
def test_within_call_window_left_boundary(mocked_datetime, times):
    start, end, _, _, _ = times
    mocked_datetime.now.return_value = start
    call = Call(start=start, end=end)
    proposal = Proposal(call=call)
    assert proposal.call_is_open()


@mock.patch("ludwig_api.proposal.models.call.datetime")
def test_within_call_window_right_boundary(mocked_datetime, times):
    start, end, _, _, _ = times
    mocked_datetime.now.return_value = end
    call = Call(start=start, end=end)
    proposal = Proposal(call=call)
    assert proposal.call_is_open()
