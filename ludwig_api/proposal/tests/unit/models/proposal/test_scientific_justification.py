import pytest
from django.conf import settings
from django.core.files.base import ContentFile
from pytest_factoryboy import register

from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory

register(ProposalFactory)


@pytest.mark.django_db
def test_scientific_justification_filename_removes_folder_prefix():
    settings.STORAGES["default"][
        "BACKEND"
    ] = "django.core.files.storage.InMemoryStorage"
    settings.STORAGES["default"]["OPTIONS"] = {}

    proposal = ProposalFactory()
    proposal.scientific_justification.save(
        name="proposal_v3.pdf", content=ContentFile(content=b"hi"), save=True
    )
    assert proposal.scientific_justification_filename == "proposal_v3.pdf"


@pytest.mark.django_db
def test_scientific_justification_filesize_is_zero_when_file_is_empty():
    settings.STORAGES["default"][
        "BACKEND"
    ] = "django.core.files.storage.InMemoryStorage"
    settings.STORAGES["default"]["OPTIONS"] = {}

    proposal = ProposalFactory()
    assert proposal.scientific_justification_filesize == 0


@pytest.mark.django_db
def test_scientific_justification_filesize_is_correct():
    settings.STORAGES["default"][
        "BACKEND"
    ] = "django.core.files.storage.InMemoryStorage"
    settings.STORAGES["default"]["OPTIONS"] = {}

    proposal = ProposalFactory()
    proposal.scientific_justification.save(
        name="proposal_v3.pdf", content=ContentFile(content=b"12345"), save=True
    )
    assert proposal.scientific_justification_filesize == 5
