import pytest

from ludwig_api.proposal.models.panel_type import PanelType


@pytest.mark.django_db
def test_user_is_not_assigned_as_reviewer__no_reviews_exist(proposal_factory, user):
    proposal = proposal_factory()
    assert not proposal.is_user_assigned_as_technical_reviewer(user)
    assert not proposal.is_user_assigned_as_scientific_reviewer(user)


@pytest.mark.django_db
def test_user_is_not_assigned_as_reviewer__some_reviews_exist(
    proposal_factory, review_factory, user
):
    proposal = proposal_factory()
    review_factory.create_batch(
        3,
        proposal=proposal,
    )

    assert not proposal.is_user_assigned_as_technical_reviewer(user)
    assert not proposal.is_user_assigned_as_scientific_reviewer(user)


@pytest.mark.django_db
def test_user_is_assigned_as_technical_reviewer(proposal_factory, review_factory, user):
    proposal = proposal_factory()
    reviews = review_factory.create_batch(
        3,
        proposal=proposal,
    )

    reviews[0].reviewer_id = "123"
    reviews[1].reviewer_id = "456"

    reviews[2].reviewer_id = user.id
    reviews[2].panel_type = PanelType.technical

    for review in reviews:
        review.save()

    proposal.refresh_from_db()

    assert proposal.is_user_assigned_as_technical_reviewer(user)
    assert not proposal.is_user_assigned_as_scientific_reviewer(user)


@pytest.mark.django_db
def test_user_is_assigned_as_scientific_reviewer(
    proposal_factory, review_factory, user
):
    proposal = proposal_factory()
    reviews = review_factory.create_batch(
        3,
        proposal=proposal,
    )

    reviews[0].reviewer_id = "123"

    reviews[1].reviewer_id = user.id
    reviews[1].panel_type = PanelType.scientific

    reviews[2].reviewer_id = "789"

    for review in reviews:
        review.save()

    proposal.refresh_from_db()

    assert not proposal.is_user_assigned_as_technical_reviewer(user)
    assert proposal.is_user_assigned_as_scientific_reviewer(user)


@pytest.mark.django_db
def test_user_is_assigned_as_scientific_and_technical_reviewer(
    proposal_factory, review_factory, user
):
    proposal = proposal_factory()
    reviews = review_factory.create_batch(
        3,
        proposal=proposal,
    )

    reviews[0].reviewer_id = "123"

    reviews[1].reviewer_id = user.id
    reviews[1].panel_type = PanelType.scientific

    reviews[2].reviewer_id = user.id
    reviews[2].panel_type = PanelType.technical

    for review in reviews:
        review.save()

    proposal.refresh_from_db()

    assert proposal.is_user_assigned_as_technical_reviewer(user)
    assert proposal.is_user_assigned_as_scientific_reviewer(user)
