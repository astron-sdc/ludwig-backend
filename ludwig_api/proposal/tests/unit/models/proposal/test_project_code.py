from ludwig_api.proposal.models import Call, Cycle, Proposal


def test_project_code():
    cycle = Cycle(code="ABC")
    call = Call(cycle=cycle, code="07")
    proposal = Proposal(call=call, sequence_number=18)

    assert proposal.project_code == "07_018"
