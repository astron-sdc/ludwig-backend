import pytest

from ludwig_api.proposal.models import Review
from ludwig_api.proposal.models.review_comment import ReviewComment
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory


@pytest.mark.django_db
def test_created_at():
    proposal = ProposalFactory()

    review = Review(proposal=proposal, ranking=1)
    review.save()

    review_comment = ReviewComment(review=review)
    review_comment.save()

    assert review_comment.created_at is not None


@pytest.mark.django_db
def test_updated_at():
    proposal = ProposalFactory()

    review = Review(proposal=proposal, ranking=1)
    review.save()

    review_comment = ReviewComment(review=review)
    review_comment.save()

    previous_updated_at = review_comment.updated_at

    review_comment.save()
    assert review_comment.updated_at > previous_updated_at
