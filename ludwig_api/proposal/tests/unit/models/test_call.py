from ludwig_api.proposal.models import Call


def test_to_string():
    call = Call(name="2nd call", description="who you gonna call", code=420)
    assert call.__str__() == "2nd call (420)"
