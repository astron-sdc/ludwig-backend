from datetime import datetime, timedelta, timezone

import pytest

from ludwig_api.proposal.models import Call
from ludwig_api.proposal.models.call import CallStatus


def test_to_string():
    call = Call(name="2nd call", description="who you gonna call", code=420)
    assert call.__str__() == "2nd call (420)"


def test_status():
    now = datetime.now(timezone.utc)
    call = Call(start=now - timedelta(minutes=1), end=now - timedelta(seconds=1))
    assert call.status == CallStatus.closed
    call.end = now + timedelta(seconds=1)
    assert call.status == CallStatus.open
    call.start = now + timedelta(seconds=1)
    assert call.status == CallStatus.closed


def test_increment_proposal_sequence_number_default_value():
    call = Call()
    assert call.proposal_sequence_number == 1


def test_increment_proposal_sequence_number_increment_once():
    call = Call()
    call.increment_proposal_sequence_number()
    assert call.proposal_sequence_number == 2


def test_increment_proposal_sequence_number_increment_multiple_times():
    call = Call()
    call.increment_proposal_sequence_number()
    call.increment_proposal_sequence_number()
    call.increment_proposal_sequence_number()
    assert call.proposal_sequence_number == 4


@pytest.mark.django_db
def test_sky_distribution_empty(call_factory):
    call = call_factory()

    sky_distribution = call.sky_distribution

    assert sky_distribution == []


@pytest.mark.django_db
def test_sky_distribution_non_empty(
    call_factory, proposal_factory, target_factory, calculator_input_factory
):
    call = call_factory()

    proposals = proposal_factory.create_batch(10, call=call)
    for idx, proposal in enumerate(proposals):
        targets = target_factory.create_batch(
            10, proposal=proposal, x_hms_formatted=f"0{idx}:00:00.000"
        )
        for target in targets:
            calculator_input_factory.create(
                proposal=proposal, target=target, observation_time_seconds=1
            )

    call.refresh_from_db()

    sky_distribution = call.sky_distribution

    assert len(sky_distribution) == 10  # should have 10 entries
    for idx, proposal in enumerate(sky_distribution):
        assert len(proposal["sky_distribution"]) == 24  # always have all hours
        assert proposal["sky_distribution"][idx] == 10  # 10 times 1 second observation


@pytest.mark.django_db
def test_requested_resources_empty(call_factory):
    call = call_factory()
    # Requested resources should be 0 entries because no calculator results exist
    for idx, proposal in enumerate(call.requested_resources):
        assert proposal["observation_time_seconds"] == 0
        assert proposal["data_size_bytes"] == 0
        assert proposal["pipeline_processing_time_seconds"] == 0


@pytest.mark.django_db
def test_requested_resources_results(
    call_factory,
    proposal_factory,
    target_factory,
    calculator_result_factory,
    calculator_input_factory,
):
    call = call_factory()

    proposals = proposal_factory.create_batch(10, call=call)
    for idx, proposal in enumerate(proposals):
        targets = target_factory.create_batch(
            2, proposal=proposal, x_hms_formatted=f"0{idx}:00:00.000"
        )
        for target in targets:
            calculator_input_factory.create_batch(
                2,
                proposal=proposal,
                target=target,
                observation_time_seconds=3,
            )
            calculator_result_factory.create(
                proposal=proposal,
                target=target,
            )

    call.refresh_from_db()

    assert len(call.requested_resources) == 10

    # Requested resources per proposal should be the calculator input/result values multiplied by the number of targets
    for idx, proposal in enumerate(call.requested_resources):
        assert proposal["observation_time_seconds"] == 6
        assert proposal["data_size_bytes"] == 2048
        assert proposal["pipeline_processing_time_seconds"] == 120
