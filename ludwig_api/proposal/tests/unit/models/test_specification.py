import pytest

from ludwig_api.proposal.models import Specification
from ludwig_api.proposal.serializers.specification_serializer import (
    SpecificationSerializerWithGroup,
    SpecificationWithDefinitionSerializer,
)
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.specification_factory import (
    SpecificationFactory,
)
from ludwig_api.proposal.tests.factories.target_group_factory import TargetGroupFactory


def test_to_string():
    specification_name = "a specification"
    specification = Specification(
        name=specification_name, description="with a description"
    )
    assert specification.__str__() == specification_name


@pytest.mark.django_db
def test_specification_group_id_number():
    proposal = ProposalFactory()
    group = TargetGroupFactory(group_id_number=68)
    specification = SpecificationFactory(targetgroup=group, proposal=proposal)
    serializer = SpecificationSerializerWithGroup(specification)
    assert serializer.data["group_id_number"] == 68


@pytest.mark.django_db
def test_specification_priority():
    proposal = ProposalFactory()
    specification = SpecificationFactory(proposal=proposal)
    specification.assigned_priority = "B"
    specification.requested_priority = "A"
    assert specification.determinated_priority() == "B"


@pytest.mark.django_db
def test_specification_strategy():
    proposal = ProposalFactory()
    specification = SpecificationFactory(proposal=proposal)
    specification.strategy = "a/num/b/3"
    assert specification.strategy_number() == "3"


@pytest.mark.django_db
def test_specification_definition_serializer():
    proposal = ProposalFactory()
    group = TargetGroupFactory(group_id_number=68)
    serializer = SpecificationWithDefinitionSerializer(
        data={
            "proposal": proposal.id,
            "targetgroup": group.id,
            "group_id_number": 6,
            "definition": "da df",
            "name": "da",
            "description": "la",
            "strategy": "nowhere:",
            "proposal_id": 1,
        }
    )
    assert serializer.is_valid()
