from ludwig_api.proposal.models.target_group import TargetGroup


def test_group_to_string():
    group_name = "a group"
    group = TargetGroup(name=group_name)
    assert group.__str__() == group_name
