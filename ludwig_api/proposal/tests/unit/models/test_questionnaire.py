import pytest

from ludwig_api.proposal.models import Questionnaire
from ludwig_api.proposal.serializers.questionnaire_serializer import QuestionSerializer
from ludwig_api.proposal.tests.factories.call_factory import CallFactory
from ludwig_api.proposal.tests.factories.questionnaire_factory import (
    QuestionnaireFactory,
)


@pytest.fixture
def questionnaire_fixture():
    return QuestionnaireFactory()


def compare_questions(old_questionnaire, new_questionnaire):
    old_questions = QuestionSerializer(
        old_questionnaire.questions.all(), many=True
    ).data
    new_questions = QuestionSerializer(
        new_questionnaire.questions.all(), many=True
    ).data
    assert not {q["id"] for q in new_questions}.intersection(
        {q["id"] for q in old_questions}
    )
    for field in ["title", "is_required", "order", "type"]:
        assert [q[field] for q in old_questions] == [q[field] for q in new_questions]


@pytest.mark.django_db
def test_questionnaire_copy_to_call__another_call(questionnaire_fixture):
    new_call = CallFactory()
    new_questionnaire = questionnaire_fixture.copy_to_call(new_call)
    assert Questionnaire.objects.count() == 2
    assert questionnaire_fixture.call != new_questionnaire.call
    compare_questions(questionnaire_fixture, new_questionnaire)


@pytest.mark.django_db
def test_questionnaire_copy_to_call__same_call(questionnaire_fixture):
    call = questionnaire_fixture.call
    new_questionnaire = questionnaire_fixture.copy_to_call(call)
    assert Questionnaire.objects.count() == 2
    assert questionnaire_fixture.call == new_questionnaire.call
    compare_questions(questionnaire_fixture, new_questionnaire)


@pytest.mark.django_db
def test_questionnaire_copy_to_call__not_a_call(questionnaire_fixture):
    with pytest.raises(ValueError) as error:
        questionnaire_fixture.copy_to_call(questionnaire_fixture.call.cycle)
        assert '"Questionnaire.call" must be a "Call" instance.' in error.value


@pytest.mark.django_db
def test_questionnaire_copy_to_call__none(questionnaire_fixture):
    new_questionnaire = questionnaire_fixture.copy_to_call()
    assert Questionnaire.objects.count() == 2
    assert questionnaire_fixture.call
    assert new_questionnaire.call is None
    compare_questions(questionnaire_fixture, new_questionnaire)
