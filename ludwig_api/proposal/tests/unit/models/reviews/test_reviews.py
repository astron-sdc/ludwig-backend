import pytest

from ludwig_api.proposal.models.review import PanelType, Review, ReviewerType


@pytest.mark.django_db
def test_create_reviews_check_correct_ordering(user, review_factory, proposal_factory):
    proposal = proposal_factory(created_by=user.id)
    reviews = review_factory.create_batch(4, proposal=proposal, reviewer_id=user.id)
    reviews[0].reviewer_type = ReviewerType.secondary
    reviews[0].panel_type = PanelType.technical
    reviews[0].save()
    reviews[1].reviewer_type = ReviewerType.primary
    reviews[1].panel_type = PanelType.technical
    reviews[1].save()
    reviews[2].reviewer_type = ReviewerType.secondary
    reviews[2].panel_type = PanelType.scientific
    reviews[2].save()
    reviews[3].reviewer_type = ReviewerType.primary
    reviews[3].panel_type = PanelType.scientific
    reviews[3].save()

    created_reviews = Review.objects.filter(
        proposal_id=proposal.id, reviewer_id=user.id
    )

    assert Review.objects.all().count() == 4
    # Expected ordering: primary scientific, secondary scientific, primary technical, secondary technical
    assert created_reviews.first().panel_type == PanelType.scientific
    assert created_reviews.first().reviewer_type == ReviewerType.primary
    assert created_reviews.last().panel_type == PanelType.technical
    assert created_reviews.last().reviewer_type == ReviewerType.secondary

    created_reviews.first().delete()
    assert Review.objects.all().count() == 3
    assert created_reviews.first().panel_type == PanelType.scientific
    assert created_reviews.first().reviewer_type == ReviewerType.secondary

    created_reviews.first().delete()
    assert created_reviews.first().panel_type == PanelType.technical
    assert created_reviews.first().reviewer_type == ReviewerType.primary
