import pytest

from ludwig_api.proposal.tests.factories.export_record_factory import (
    ExportRecordFactory,
)
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.specification_factory import (
    SpecificationFactory,
)


@pytest.mark.django_db
def test_export_record_to_string():
    proposal = ProposalFactory(id=1, title="Human")
    specification = SpecificationFactory(proposal=proposal)
    export_record = ExportRecordFactory(
        proposal=proposal, subgroup=1, specification=specification
    )
    export_record.external_environment = "Testing"
    export_record.external_identifier = "ABC"

    assert "Synced" in export_record.__str__()
