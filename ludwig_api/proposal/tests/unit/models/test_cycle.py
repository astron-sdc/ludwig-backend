from ludwig_api.proposal.models import Cycle


def test_to_string():
    cycle = Cycle(name="Cycle of the wolf", description="recycle", code=1337)
    assert cycle.__str__() == "Cycle of the wolf (1337)"
