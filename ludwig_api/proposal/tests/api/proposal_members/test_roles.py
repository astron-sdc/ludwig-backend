from unittest.mock import ANY

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.faai.sram.models import SRAMClient


@pytest.mark.django_db
def test_roles_with_no_roles_supplied_should_error(
    authenticated_api_client, submitted_proposal
):
    response = authenticated_api_client.put(
        reverse("proposal-members-roles", args=[submitted_proposal.id]),
        data={"user_id": "123"},
        format="json",
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_roles_with_no_user_id_supplied_should_error(
    authenticated_api_client, submitted_proposal
):
    response = authenticated_api_client.put(
        reverse("proposal-members-roles", args=[submitted_proposal.id]),
        data={"roles": []},
        format="json",
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_roles_with_unauthenticated_user_should_error(api_client, submitted_proposal):
    response = api_client.put(
        reverse("proposal-members-roles", args=[submitted_proposal.id]),
        data={"user_id": "123", "roles": []},
        format="json",
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_roles_with_unauthorized_user_should_error(
    authenticated_api_client, submitted_proposal
):
    response = authenticated_api_client.put(
        reverse("proposal-members-roles", args=[submitted_proposal.id]),
        data={"user_id": "123", "roles": []},
        format="json",
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_roles_should_make_correct_calls_to_sram(
    admin_api_client, submitted_proposal, collaboration, mocker
):
    mocked_collab = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )

    mocked_collab.return_value = collaboration

    mocked_add_member_call = mocker.patch.object(
        SRAMClient,
        "add_member_to_group",
        autospec=True,
    )

    mocked_delete_member_call = mocker.patch.object(
        SRAMClient,
        "delete_member_from_group",
        autospec=True,
    )

    response = admin_api_client.put(
        reverse("proposal-members-roles", args=[submitted_proposal.id]),
        data={"user_id": "111", "roles": ["456"]},
        format="json",
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT

    # Expect user 111 to be added to group 456 of which they are not yet part
    mocked_add_member_call.assert_called_once_with(ANY, "456", "111")

    # Expect user 111 to be deleted from group 123 of which they already part
    mocked_delete_member_call.assert_called_once_with(ANY, "123", "111")
