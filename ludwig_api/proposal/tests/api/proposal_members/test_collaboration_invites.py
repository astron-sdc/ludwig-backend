import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.permissions.proposal import Roles
from ludwig_api.proposal.views.proposal_member_viewset import SRAMClient


@pytest.mark.django_db
def test_sending_invite_as_pi(
    api_client, proposal_factory, get_user_for_proposal, mocker
):
    proposal = proposal_factory()
    proposal.collaboration_identifier = "PROP_ID"
    proposal.save()

    mocked_collab = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_collab.return_value = {
        "identifier": "PROP_ID",
        "groups": [{"identifier": "GROUP_ID"}],
    }

    mocked_invite = mocker.patch.object(
        SRAMClient,
        "create_collaboration_invite",
        autospec=True,
    )

    api_client.force_authenticate(get_user_for_proposal(proposal, Roles.PI))
    response = api_client.put(
        reverse("proposal-members-invites", args=[proposal.id]),
        data={"invites": ["test0@example.com"], "groups": ["GROUP_ID"]},
        format="json",
    )
    assert response.status_code == status.HTTP_202_ACCEPTED
    assert mocked_invite.call_count == 1


@pytest.mark.django_db
def test_sending_invite_as_co_author(
    api_client, proposal_factory, get_user_for_proposal, mocker
):
    proposal = proposal_factory()
    proposal.collaboration_identifier = "PROP_ID"
    proposal.save()

    mocked_collab = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_collab.return_value = {
        "identifier": "PROP_ID",
        "groups": [{"identifier": "GROUP_ID"}],
    }

    mocked_invite = mocker.patch.object(
        SRAMClient,
        "create_collaboration_invite",
        autospec=True,
    )

    api_client.force_authenticate(
        get_user_for_proposal(proposal, Roles.CO_AUTHOR_READ_ONLY)
    )
    response = api_client.put(
        reverse("proposal-members-invites", args=[proposal.id]),
        data={"invites": ["test0@example.com"], "groups": ["GROUP_ID"]},
        format="json",
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert mocked_invite.call_count == 0


@pytest.mark.django_db
def test_sending_invite_as_pi_wrong_group(
    api_client, proposal_factory, get_user_for_proposal, mocker
):
    proposal = proposal_factory()
    proposal.collaboration_identifier = "PROP_ID"
    proposal.save()

    mocked_collab = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_collab.return_value = {
        "identifier": "PROP_ID",
        "groups": [{"identifier": "GROUP_ID"}],
    }

    mocked_invite = mocker.patch.object(
        SRAMClient,
        "create_collaboration_invite",
        autospec=True,
    )

    api_client.force_authenticate(get_user_for_proposal(proposal, Roles.PI))
    response = api_client.put(
        reverse("proposal-members-invites", args=[proposal.id]),
        data={"invites": ["test0@example.com"], "groups": ["WRONG_GROUP_ID"]},
        format="json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert mocked_invite.call_count == 0
