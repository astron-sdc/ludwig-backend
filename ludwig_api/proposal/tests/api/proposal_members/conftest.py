from datetime import UTC, datetime

import pytest

from ludwig_api.proposal.models.proposal_status import ProposalStatus


@pytest.fixture
def submitted_proposal(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.submitted,
        submitted_at=datetime.now(UTC),
        collaboration_identifier="123",
    )


@pytest.fixture
def collaboration():
    memberships = [
        {
            "user": {
                "uid": "111",
                "name": "Donald",
                "given_name": "Donaldus",
                "family_name": "Duck",
                "email": "",
                "schac_home_organisation": "",
                "scoped_affiliation": "",
            }
        },
        {
            "user": {
                "uid": "222",
                "name": "Mickey",
                "given_name": "Mickus",
                "family_name": "Mouse",
                "email": "",
                "schac_home_organisation": "",
                "scoped_affiliation": "",
            }
        },
    ]

    groups = [
        {
            "identifier": "123",
            "name": "Principial Investigators",
            "short_name": "PI",
            "description": "",
            "collaboration_memberships": memberships,
        },
        {
            "identifier": "456",
            "name": "Co Investigators",
            "short_name": "COI",
            "description": "",
            "collaboration_memberships": [],
        },
    ]

    return {"groups": groups}
