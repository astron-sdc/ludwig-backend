import pytest


@pytest.fixture
def proposal_list(proposal_factory):
    return proposal_factory.create_batch(20)
