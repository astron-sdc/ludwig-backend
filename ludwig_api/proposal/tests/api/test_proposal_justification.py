import io

import pytest
from django.conf import settings
from django.core.files.base import ContentFile
from django.urls import reverse
from pytest_factoryboy import register
from rest_framework import status

import ludwig_api
from ludwig_api.proposal.permissions.proposal import Roles
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory

register(ProposalFactory)


@pytest.mark.django_db
def test_proposal_justification_proposal_not_found(authenticated_api_client):
    response = authenticated_api_client.get(
        reverse("proposal-scientific-justification", args=[1337])
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_justification_without_file(authenticated_api_client):
    proposal = ProposalFactory()
    response = authenticated_api_client.get(
        reverse("proposal-scientific-justification", args=[proposal.id])
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_justification_upload_file__as_admin(admin_api_client, mocker):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    settings.STORAGES["default"][
        "BACKEND"
    ] = "django.core.files.storage.InMemoryStorage"
    settings.STORAGES["default"]["OPTIONS"] = {}
    proposal = ProposalFactory()
    response = admin_api_client.patch(
        reverse("proposal-detail", args=[proposal.id]),
        data={"scientific_justification": io.BytesIO(b"\x13\x37")},
        format="multipart",
    )

    assert response.status_code == status.HTTP_200_OK
    data = response.json()
    assert data["scientific_justification_filesize"] == 2
    assert (
        data["scientific_justification_filename"] == "scientific_justification"
    )  # silly test

    # Check file exists
    proposal.refresh_from_db()
    assert proposal.scientific_justification is not None


@pytest.mark.django_db
def test_proposal_justification_download_file(api_client, get_user_for_proposal):
    settings.STORAGES["default"][
        "BACKEND"
    ] = "django.core.files.storage.InMemoryStorage"
    settings.STORAGES["default"]["OPTIONS"] = {}

    proposal = ProposalFactory()
    proposal.scientific_justification.save(
        name="proposal_v4.pdf", content=ContentFile(content=b"12345"), save=True
    )

    api_client.force_authenticate(
        get_user_for_proposal(proposal, Roles.CO_AUTHOR_READ_ONLY)
    )
    response = api_client.get(
        reverse("proposal-scientific-justification", args=[proposal.id])
    )
    assert response.status_code == status.HTTP_200_OK
    assert (
        response.headers["Content-Disposition"]
        == 'attachment; filename="proposal_v4.pdf"'
    )
    assert io.BytesIO(b"".join(response.streaming_content)).getvalue() == b"12345"
