import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models import Cycle


@pytest.mark.django_db
def test_cycle_create__as_admin_success(admin_api_client, cycle_to_create_json):
    assert Cycle.objects.count() == 0
    response = admin_api_client.post(
        path=reverse("cycle-list"), data=cycle_to_create_json
    )
    assert response.status_code == status.HTTP_201_CREATED
    assert Cycle.objects.count() == 1


@pytest.mark.django_db
def test_cycle_create__as_user_failure(authenticated_api_client, cycle_to_create_json):
    response = authenticated_api_client.post(
        path=reverse("cycle-list"), data=cycle_to_create_json
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert Cycle.objects.count() == 0


@pytest.mark.django_db
def test_cycle_create__unauthorized_failure(api_client, cycle_to_create_json):
    response = api_client.post(path=reverse("cycle-list"), data=cycle_to_create_json)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert Cycle.objects.count() == 0
