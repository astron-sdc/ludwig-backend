import pytest


@pytest.fixture
def single_cycle(cycle_factory):
    return cycle_factory()


@pytest.fixture
def list_of_cycles(cycle_factory):
    return cycle_factory.create_batch(10)


@pytest.fixture
def cycle_to_create_json():
    return {
        "name": "01",
        "code": "123",
        "description": "Lorem ipsum dolor sit amet.",
        "start": "2025-11-14T13:54:24.427Z",
        "end": "2025-11-14T13:54:24.427Z",
    }
