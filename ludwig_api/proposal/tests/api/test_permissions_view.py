from django.conf import settings
from django.urls import reverse
from rest_framework import status


def test_get_permissions__unauthenticated(api_client):
    response = api_client.get(reverse("access-allowed"))
    if settings.SRAM_ENABLED != "0":
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_get_permissions__authenticated(authenticated_api_client):
    response = authenticated_api_client.get(reverse("access-allowed"))
    assert response.status_code == status.HTTP_200_OK

    expected = {
        "is_admin": False,
        "create_cycle": False,
        "create_call": False,
        "view_call_ranking": False,
    }

    assert response.data == expected


def test_get_permissions__authenticated_as_admin(admin_api_client):
    response = admin_api_client.get(reverse("access-allowed"))
    assert response.status_code == status.HTTP_200_OK
    expected = {
        "is_admin": True,
        "create_cycle": True,
        "create_call": True,
        "view_call_ranking": True,
    }
    assert response.data == expected
