import pytest
from django.urls import reverse
from pytest_factoryboy import register
from rest_framework import status
from rest_framework.test import APIClient

from ludwig_api.proposal.serializers.call_serializer import CallSerializer
from ludwig_api.proposal.tests.factories.call_factory import CallFactory

register(CallFactory)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def list_of_calls(call_factory):
    return call_factory.create_batch(20)


@pytest.fixture
def single_call(call_factory):
    return call_factory()


@pytest.mark.django_db
def test_get_calls_returns_http_ok(api_client, list_of_calls):
    response = api_client.get(reverse("call-list"))
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_get_call_returns_http_ok(api_client, single_call):
    response = api_client.get(reverse("call-detail", args=[single_call.id]))
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_get_calls_returns_calls(api_client, list_of_calls):
    response = api_client.get(reverse("call-list"))
    response_json = response.json()
    calls_json = CallSerializer(list_of_calls, many=True).data
    assert response_json == calls_json


@pytest.mark.django_db
def test_get_call_returns_call(api_client, single_call):
    response = api_client.get(reverse("call-detail", args=[single_call.id]))
    response_json = response.json()
    call_json = CallSerializer(single_call).data
    assert response_json == call_json
