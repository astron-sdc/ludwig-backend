import pytest
import requests_mock
from django.conf import settings
from django.urls import reverse
from rest_framework import status


@pytest.fixture
def mock_requests():
    with requests_mock.Mocker() as mocker:
        mocker.post(settings.LUDWIG_TMSS_TOKEN_URL, json={"token": "12345678"})
        yield mocker


def test_utc_now_request(authenticated_api_client, mock_requests):
    path = reverse("tmss-utc-now")
    thedate = "2024-09-04T14:51:18.570412"
    mock_requests.get(settings.LUDWIG_TMSS_URI + "/api/util/utc", json=thedate)
    response = authenticated_api_client.get(path=path)
    assert response.status_code == status.HTTP_200_OK
    response_data = response.json()
    assert response_data[0] == f'"{thedate}"'


def test_common_template_request(authenticated_api_client, mock_requests):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI
        + "/api/common_schema_template?name=stations&ordering=-id&state=active",
        json={},
    )
    path = reverse("tmss-common-template")
    response = authenticated_api_client.get(path=path)
    assert response.status_code == status.HTTP_200_OK


def test_scheduling_constraints_template_request(
    authenticated_api_client, mock_requests
):
    mock_requests.get(
        settings.LUDWIG_TMSS_URI + "/api/scheduling_constraints_template/", json={}
    )
    path = reverse("tmss-scheduling-constraints-template")
    response = authenticated_api_client.get(path=path)
    assert response.status_code == status.HTTP_200_OK


def test_task_template_request(authenticated_api_client, mock_requests):
    mock_requests.get(settings.LUDWIG_TMSS_URI + "/api/task_template", json={})
    path = reverse("tmss-task-template")
    response = authenticated_api_client.get(path=path)
    assert response.status_code == status.HTTP_200_OK
