from datetime import UTC, datetime, timedelta

import pytest
from pytest_factoryboy import register
from rest_framework.test import APIClient

from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory

register(ProposalFactory)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def proposal_list(proposal_factory):
    return proposal_factory.create_batch(20)


@pytest.fixture
def proposal_detail_draft(proposal_factory):
    return proposal_factory(status=ProposalStatus.draft, submitted_at=None)


@pytest.fixture
def proposal_detail_submitted(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.submitted, submitted_at=datetime.now(UTC)
    )


@pytest.fixture
def proposal_detail_review(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.under_review,
        submitted_at=datetime.now(UTC) - timedelta(days=1),
    )
