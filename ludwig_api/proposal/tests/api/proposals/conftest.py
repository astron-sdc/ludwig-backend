from datetime import UTC, datetime, timedelta

import pytest

from ludwig_api.proposal.models.proposal import ProposalStatus


@pytest.fixture
def proposal_detail_draft(proposal_factory):
    return proposal_factory(status=ProposalStatus.draft, submitted_at=None)


@pytest.fixture
def proposal_detail_draft_requires_direct_action(proposal_factory):
    proposal = proposal_factory(status=ProposalStatus.draft, submitted_at=None)
    proposal.call.requires_direct_action = True
    proposal.call.save()

    return proposal


@pytest.fixture
def proposal_detail_submitted(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.submitted, submitted_at=datetime.now(UTC)
    )


@pytest.fixture
def proposal_detail_review(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.under_review,
        submitted_at=datetime.now(UTC) - timedelta(days=1),
    )


@pytest.fixture
def single_call(call_factory):
    return call_factory(code="10")
