import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.serializers.proposal_serializer import ProposalSerializer


@pytest.mark.django_db
def test_proposal_list(api_client, proposal_list):
    response = api_client.get(reverse("proposal-list"))
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalSerializer(proposal_list, many=True).data
    assert response.json() == expected


@pytest.mark.django_db
def test_proposal_detail(api_client, proposal_detail_draft):
    response = api_client.get(
        reverse("proposal-detail", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalSerializer(proposal_detail_draft).data
    assert response.json() == expected
