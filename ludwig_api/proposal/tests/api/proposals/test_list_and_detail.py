import pytest
from django.urls import reverse
from rest_framework import status

import ludwig_api
from ludwig_api.proposal.models.panel_type import PanelType
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.serializers.proposal_serializer import ProposalSerializer


@pytest.mark.django_db
def test_proposal_list__as_admin(admin_api_client, proposal_list, mocker):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    response = admin_api_client.get(reverse("proposal-list"))
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalSerializer(proposal_list, many=True).data
    assert response.json() == expected


@pytest.mark.django_db
def test_proposal_list__as_user_without_proposal(
    authenticated_api_client, proposal_list, mocker
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    response = authenticated_api_client.get(reverse("proposal-list"))
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == []


@pytest.mark.django_db
def test_proposal_list__as_user_with_proposal(
    api_client, get_user_for_proposal, proposal_factory, mocker
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    proposal_owned = proposal_factory.create()
    user = get_user_for_proposal(proposal_owned, ProposalRoles.PI)
    proposal_owned.created_by = user.id
    proposal_owned.save()
    proposal_factory.create_batch(10)  # make sure extra's exist

    api_client.force_authenticate(user)

    response = api_client.get(reverse("proposal-list"))
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalSerializer([proposal_owned], many=True).data
    assert response.json() == expected


@pytest.mark.django_db
def test_proposal_list__access_by_call_as_reviewer(
    api_client, get_user_for_call, proposal_list, mocker
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    first_proposal = proposal_list[0]
    api_client.force_authenticate(
        get_user_for_call(first_proposal.call, CallRoles.SCIENCE_REVIEWER)
    )
    response = api_client.get(f"{reverse('proposal-list')}")
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalSerializer([first_proposal], many=True).data
    assert response.json() == expected


@pytest.mark.django_db
def test_proposal_list__as_user_with_proposal_only_created_by(
    api_client, user, proposal_factory, mocker
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    proposal_owned = proposal_factory.create()
    proposal_owned.created_by = user.id
    proposal_owned.save()
    proposal_factory.create_batch(10)  # make sure extra's exist

    api_client.force_authenticate(user)

    response = api_client.get(reverse("proposal-list"))
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalSerializer([proposal_owned], many=True).data
    assert response.json() == expected


@pytest.mark.django_db
def test_proposal_detail(admin_api_client, proposal_detail_draft, mocker):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    response = admin_api_client.get(
        reverse("proposal-detail", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalSerializer(proposal_detail_draft).data
    assert response.json() == expected


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code",
    [
        (ProposalRoles.PI, status.HTTP_200_OK),
        (ProposalRoles.CO_AUTHOR_READ_ONLY, status.HTTP_200_OK),
        (ProposalRoles.CO_AUTHOR, status.HTTP_200_OK),
    ],
)
def test_see_own_proposal_contents(
    api_client,
    proposal_detail_draft,
    get_user_for_proposal,
    mocker,
    role,
    expected_status_code,
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    api_client.force_authenticate(get_user_for_proposal(proposal_detail_draft, role))
    response = api_client.get(
        reverse("proposal-detail", args=[proposal_detail_draft.id])
    )
    assert response.status_code == expected_status_code


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code",
    [
        (ProposalRoles.PI, status.HTTP_404_NOT_FOUND),
        (ProposalRoles.CO_AUTHOR_READ_ONLY, status.HTTP_404_NOT_FOUND),
        (ProposalRoles.CO_AUTHOR, status.HTTP_404_NOT_FOUND),
    ],
)
def test_see_other_proposal_contents(
    api_client,
    proposal_factory,
    get_user_for_proposal,
    mocker,
    role,
    expected_status_code,
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    own_proposal, other_proposal = proposal_factory.create_batch(2)

    api_client.force_authenticate(get_user_for_proposal(own_proposal, role))
    response = api_client.get(reverse("proposal-detail", args=[other_proposal.id]))
    assert response.status_code == expected_status_code


@pytest.mark.django_db
def test_reviewer_can_see_proposals_and_reviews_for_call_except_own(
    api_client,
    admin_api_client,
    get_user_for_proposal,
    get_user_for_call,
    mocker,
    review_factory,
    proposal_list,
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    science_reviewer = get_user_for_call(
        proposal_list[0].call, CallRoles.SCIENCE_REVIEWER
    )
    api_client.force_authenticate(science_reviewer)

    for proposal in proposal_list:
        review_factory(
            proposal=proposal,
            reviewer_id=get_user_for_call(
                proposal.call, CallRoles.TECHNICAL_REVIEWER
            ).id,
            panel_type=PanelType.technical,
        )

    response_for_science_reviewer = api_client.get(
        # proposal-review-viewset
        reverse("call-proposals-list", args=[proposal_list[0].call.id])
    )
    assert response_for_science_reviewer.status_code == status.HTTP_200_OK

    # Make science reviewer the PI for one of the proposals
    proposal_list[0].created_by = science_reviewer.id
    proposal_list[0].save()

    response_for_science_reviewer_pi = api_client.get(
        reverse("call-proposals-list", args=[proposal_list[0].call.id])
    )

    assert response_for_science_reviewer_pi.status_code == status.HTTP_200_OK
    assert (
        response_for_science_reviewer.json() != response_for_science_reviewer_pi.json()
    )
