import pytest
from django.urls import reverse
from rest_framework.status import HTTP_200_OK

from ludwig_api.proposal.permissions.proposal import Roles
from ludwig_api.proposal.serializers.calculator_inputs_serializer import (
    CalculatorInputSerializer,
)


@pytest.mark.django_db
def test_calculator__inputs_list(
    api_client, target_factory, proposal_detail_draft, get_user_for_proposal
):
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_draft, Roles.PI)
    )

    target = target_factory(
        proposal=proposal_detail_draft,
        name="M31",
        x=10.684708333333331,
        y=41.26875,
        group=1,
        subgroup=1,
    )

    calculator_input = {
        "target": target.id,
        "proposal": proposal_detail_draft.id,
        "observation_time_seconds": 28800,
        "no_core_stations": 24,
        "no_remote_stations": 14,
        "no_international_stations": 14,
        "no_channels_per_subband": "64",
        "no_subbands": 488,
        "integration_time_seconds": 1,
        "antenna_set": "hbadualinner",
        "observation_date": "2025-02-12",
        "calibrators": [],
        "a_team_sources": [],
        "pipeline": "none",
        "enable_dysco_compression": False,
        "frequency_averaging_factor": 4,
        "time_averaging_factor": 1,
    }

    data_in = CalculatorInputSerializer(data=calculator_input)
    data_in.is_valid(raise_exception=True)
    data_in.save()

    response = api_client.get(
        reverse("proposal-calculator-inputs-list", args=[proposal_detail_draft.id]),
    )
    assert response.status_code == HTTP_200_OK
    response_json = response.json()
    serializer = CalculatorInputSerializer(data=calculator_input)
    serializer.is_valid(raise_exception=True)
    expected = serializer.data

    assert response_json[0] == expected
