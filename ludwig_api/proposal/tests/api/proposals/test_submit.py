from datetime import UTC, datetime, timedelta

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.proposal import ProposalStatus


@pytest.mark.django_db
def test_proposal_submit_404(api_client, proposal_detail_draft):
    response = api_client.put(reverse("proposal-submit", args=["12345"]))
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_submit__success(api_client, proposal_detail_draft):
    proposal_detail_draft.call.end = datetime.now(UTC) + timedelta(minutes=1)
    proposal_detail_draft.call.save()
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_200_OK
    proposal_detail_draft.refresh_from_db()
    assert proposal_detail_draft.status == ProposalStatus.submitted
    assert proposal_detail_draft.submitted_at - datetime.now(UTC) < timedelta(minutes=1)


@pytest.mark.django_db
def test_proposal_submit__fail_deadline(api_client, proposal_detail_draft):
    proposal_detail_draft.call.end = datetime.now(UTC) - timedelta(minutes=1)
    proposal_detail_draft.call.save()
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["error"] == "submission deadline has passed"


@pytest.mark.django_db
def test_proposal_submit__fail_status(api_client, proposal_detail_submitted):
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_submitted.id])
    )
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    assert response.json()["error"] == "invalid transition from source to target"
    proposal_detail_submitted.refresh_from_db()
    assert proposal_detail_submitted.status == ProposalStatus.submitted
