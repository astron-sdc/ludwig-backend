from datetime import UTC, datetime, timedelta

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.permissions.proposal import Roles


@pytest.mark.django_db
def test_proposal_submit_404(api_client, proposal_detail_draft):
    response = api_client.put(reverse("proposal-submit", args=["12345"]))
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.parametrize(
    "proposal_fixture,expected_status",
    [
        ("proposal_detail_draft", ProposalStatus.submitted),
        ("proposal_detail_draft_requires_direct_action", ProposalStatus.under_review),
    ],
)
def test_proposal_submit__success(
    api_client,
    get_user_for_proposal,
    proposal_fixture,
    expected_status,
    request,
    mocker,
):
    _ = mocker.patch("ludwig_api.faai.sram.models.SRAMClient", autospec=True)
    proposal = request.getfixturevalue(proposal_fixture)
    proposal.call.end = datetime.now(UTC) + timedelta(minutes=1)
    proposal.call.save()
    api_client.force_authenticate(get_user_for_proposal(proposal, Roles.PI))
    response = api_client.put(reverse("proposal-submit", args=[proposal.id]))
    assert response.status_code == status.HTTP_200_OK
    proposal.refresh_from_db()
    assert proposal.status == expected_status
    assert proposal.submitted_at - datetime.now(UTC) < timedelta(minutes=1)


@pytest.mark.django_db
def test_proposal_submit__fail_deadline(
    api_client, get_user_for_proposal, proposal_detail_draft
):
    proposal_detail_draft.call.end = datetime.now(UTC) - timedelta(minutes=1)
    proposal_detail_draft.call.save()
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_draft, Roles.PI)
    )
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["error"] == "submission deadline has passed"


@pytest.mark.django_db
def test_proposal_submit__fail_status(
    api_client, get_user_for_proposal, proposal_detail_submitted
):
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_submitted, Roles.PI)
    )
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_submitted.id])
    )
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    assert response.json()["error"] == "invalid transition from source to target"
    proposal_detail_submitted.refresh_from_db()
    assert proposal_detail_submitted.status == ProposalStatus.submitted


@pytest.mark.django_db
def test_proposal_submit_forbidden(
    api_client, get_user_for_proposal, proposal_detail_draft
):
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_draft, Roles.CO_AUTHOR_READ_ONLY)
    )
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_proposal_submit___check_author_list(
    api_client,
    get_user_for_proposal,
    proposal_detail_draft,
    mocker,
    collaboration_groups,
):
    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": collaboration_groups,
        "services": [],
        "short_name": "abc",
    }
    proposal_detail_draft.collaboration_identifier = "ABC"
    proposal_detail_draft.call.save()
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_draft, Roles.PI)
    )
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_200_OK
    proposal_detail_draft.refresh_from_db()

    expected_authors = []
    for group in collaboration_groups:
        for user_membership in group["collaboration_memberships"]:
            expected_authors.append(
                f"{user_membership['user']['name']} <{user_membership['user']['email']}> ({group['name']})"
            )

    assert proposal_detail_draft.authors == sorted(set(expected_authors))
    assert proposal_detail_draft.status == ProposalStatus.submitted


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code",
    [
        (Roles.PI, status.HTTP_200_OK),
        (Roles.CO_AUTHOR_READ_ONLY, status.HTTP_403_FORBIDDEN),
        (Roles.CO_AUTHOR, status.HTTP_403_FORBIDDEN),
    ],
)
def test_submit_own_proposal(
    api_client,
    proposal_detail_draft,
    get_user_for_proposal,
    mocker,
    role,
    expected_status_code,
):
    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    api_client.force_authenticate(get_user_for_proposal(proposal_detail_draft, role))
    response = api_client.put(
        reverse("proposal-submit", args=[proposal_detail_draft.id])
    )
    assert response.status_code == expected_status_code


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code",
    [
        (Roles.PI, status.HTTP_404_NOT_FOUND),
        (Roles.CO_AUTHOR_READ_ONLY, status.HTTP_404_NOT_FOUND),
        (Roles.CO_AUTHOR, status.HTTP_404_NOT_FOUND),
    ],
)
def test_submit_other_proposal(
    api_client,
    proposal_factory,
    get_user_for_proposal,
    mocker,
    role,
    expected_status_code,
):
    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}
    own_proposal, other_proposal = proposal_factory.create_batch(2)
    api_client.force_authenticate(get_user_for_proposal(own_proposal, role))
    response = api_client.put(reverse("proposal-submit", args=[other_proposal.id]))
    assert response.status_code == expected_status_code
