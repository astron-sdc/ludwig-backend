import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.faai.sram.models import SRAMClient


@pytest.mark.django_db
def test_proposal_create(authenticated_api_client, single_call, user, mocker):
    proposal_to_create = {
        "title": "Proposal 1",
        "abstract": "There and back again",
        "call_id": single_call.id,
    }

    mocked_create_proposal_in_sram = mocker.patch(
        "ludwig_api.proposal.serializers.proposal_serializer.create_proposal_in_sram",
        autospec=True,
    )

    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    response = authenticated_api_client.post(
        path=reverse("proposal-list"), data=proposal_to_create
    )

    assert response.status_code == status.HTTP_201_CREATED
    json = response.json()

    assert json["created_by"] == user.id
    assert json["contact_author"] == user.id

    mocked_create_proposal_in_sram.assert_called_once()
