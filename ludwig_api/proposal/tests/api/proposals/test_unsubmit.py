from datetime import UTC, datetime, timedelta

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.proposal import ProposalStatus


@pytest.mark.django_db
def test_proposal_unsubmit__success(api_client, proposal_detail_submitted):
    response = api_client.put(
        reverse("proposal-unsubmit", args=[proposal_detail_submitted.id])
    )
    assert response.status_code == status.HTTP_200_OK
    proposal_detail_submitted.refresh_from_db()
    assert proposal_detail_submitted.status == ProposalStatus.draft
    assert proposal_detail_submitted.submitted_at is None


@pytest.mark.django_db
def test_proposal_unsubmit__fail_status(api_client, proposal_detail_draft):
    response = api_client.put(
        reverse("proposal-unsubmit", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    assert response.json()["error"] == "invalid transition from source to target"
    assert proposal_detail_draft.status == ProposalStatus.draft


@pytest.mark.django_db
def test_proposal_unsubmit__fail_deadline(api_client, proposal_detail_submitted):
    proposal_detail_submitted.call.end = datetime.now(UTC) - timedelta(minutes=1)
    proposal_detail_submitted.call.save()
    response = api_client.put(
        reverse("proposal-unsubmit", args=[proposal_detail_submitted.id])
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json()["error"] == "submission deadline has passed"
