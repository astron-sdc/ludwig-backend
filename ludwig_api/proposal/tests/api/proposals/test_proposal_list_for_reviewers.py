from unittest import mock

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.review import PanelType
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.serializers.proposal_enriched_serializer import (
    ProposalEnrichedSerializer,
)
from ludwig_api.proposal.tests.conftest import merge_user_entitlements


@pytest.mark.django_db
def test_proposal_list__as_admin_nothing_assigned(admin_api_client, proposal_list):
    response = admin_api_client.get(reverse("proposal-as-reviewer"))
    assert response.status_code == status.HTTP_200_OK
    expected: list[dict] = []  # nothing is assigned
    assert response.json() == expected


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_proposal_list__as_user_as_assigned(
    _, api_client, get_user_for_call, proposal_list, review_factory, mocker
):
    _ = mocker.patch("ludwig_api.faai.sram.models.SRAMClient", autospec=True)
    first_proposal = proposal_list[0]
    user = get_user_for_call(first_proposal.call, CallRoles.SCIENCE_REVIEWER)
    api_client.force_authenticate(user)

    _ = review_factory(
        proposal=first_proposal, reviewer_id=user.id, panel_type=PanelType.scientific
    )
    response = api_client.get(reverse("proposal-as-reviewer"))
    assert response.status_code == status.HTTP_200_OK
    expected = ProposalEnrichedSerializer(first_proposal)
    assert response.json() == [expected.data]


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_proposal_list__as_science_reviewer_and_pi_returns_nothing(
    _,
    api_client,
    get_user_for_call,
    get_user_for_proposal,
    proposal_list,
    review_factory,
    mocker,
):
    _ = mocker.patch("ludwig_api.faai.sram.models.SRAMClient", autospec=True)
    first_proposal = proposal_list[0]
    science_reviewer_and_pi_user = merge_user_entitlements(
        [
            get_user_for_call(first_proposal.call, CallRoles.SCIENCE_REVIEWER),
            get_user_for_proposal(first_proposal, ProposalRoles.PI),
        ]
    )
    api_client.force_authenticate(science_reviewer_and_pi_user)

    _ = review_factory(
        proposal=first_proposal,
        reviewer_id=science_reviewer_and_pi_user.id,
        panel_type=PanelType.scientific,
    )
    response = api_client.get(reverse("proposal-as-reviewer"))
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == []
