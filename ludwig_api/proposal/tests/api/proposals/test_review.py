import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.proposal import ProposalStatus


@pytest.mark.django_db
def test_proposal_review__success_as_admin(admin_api_client, proposal_detail_submitted):
    response = admin_api_client.put(
        reverse("proposal-review", args=[proposal_detail_submitted.id])
    )
    assert response.status_code == status.HTTP_200_OK
    proposal_detail_submitted.refresh_from_db()
    assert proposal_detail_submitted.status == ProposalStatus.under_review


@pytest.mark.django_db
def test_proposal_review__fail_as_admin(admin_api_client, proposal_detail_review):
    response = admin_api_client.put(
        reverse("proposal-review", args=[proposal_detail_review.id])
    )
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    assert response.json()["error"] == "invalid transition from source to target"
    proposal_detail_review.refresh_from_db()
    assert proposal_detail_review.status == ProposalStatus.under_review
