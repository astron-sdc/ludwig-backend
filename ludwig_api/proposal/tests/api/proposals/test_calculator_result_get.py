import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.permissions.proposal import Roles


@pytest.mark.django_db
def test_calculator_result_list(
    api_client, proposal_detail_draft, get_user_for_proposal
):
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_draft, Roles.PI)
    )

    res = api_client.get(
        reverse("proposal-calculator-results-list", args=[proposal_detail_draft.id])
    )

    assert res.status_code == status.HTTP_200_OK
