import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.faai.sram.models import SRAMClient
from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.permissions.proposal import Roles


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code,expected_title",
    [
        (Roles.PI, status.HTTP_200_OK, "edited title"),
        (Roles.CO_AUTHOR, status.HTTP_200_OK, "edited title"),
        (Roles.CO_AUTHOR_READ_ONLY, status.HTTP_403_FORBIDDEN, "default title"),
        (None, status.HTTP_401_UNAUTHORIZED, "default title"),
    ],
)
def test_edit_own_proposal_contents(
    api_client,
    proposal_detail_submitted,
    get_user_for_proposal,
    mocker,
    role,
    expected_status_code,
    expected_title,
):
    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    proposal_detail_submitted.title = "default title"
    proposal_detail_submitted.save()

    if role:
        api_client.force_authenticate(
            get_user_for_proposal(proposal_detail_submitted, role)
        )

    assert (
        Proposal.objects.get(id=proposal_detail_submitted.id).title == "default title"
    )

    response = api_client.patch(
        reverse("proposal-detail", args=[proposal_detail_submitted.id]),
        data={"title": expected_title},
    )
    assert response.status_code == expected_status_code
    assert Proposal.objects.get(id=proposal_detail_submitted.id).title == expected_title


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code,expected_title",
    [
        (Roles.PI, status.HTTP_200_OK, "edited title"),
        (Roles.CO_AUTHOR, status.HTTP_200_OK, "edited title"),
        (Roles.CO_AUTHOR_READ_ONLY, status.HTTP_403_FORBIDDEN, "default title"),
    ],
)
@pytest.mark.django_db
def test_edit_own_proposal_contents_via_put(
    api_client,
    proposal_detail_submitted,
    get_user_for_proposal,
    mocker,
    role,
    expected_status_code,
    expected_title,
):
    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    proposal_detail_submitted.title = "default title"
    proposal_detail_submitted.save()

    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_submitted, role)
    )

    response = api_client.put(
        reverse("proposal-detail", args=[proposal_detail_submitted.id]),
        data={
            "title": "edited title",
            "abstract": "foo",
            "call_id": proposal_detail_submitted.call.id,
        },
    )
    assert response.status_code == expected_status_code
    assert Proposal.objects.get(id=proposal_detail_submitted.id).title == expected_title


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code",
    [
        (Roles.PI, status.HTTP_404_NOT_FOUND),
        (Roles.CO_AUTHOR, status.HTTP_404_NOT_FOUND),
        (Roles.CO_AUTHOR_READ_ONLY, status.HTTP_404_NOT_FOUND),
    ],
)
def test_edit_other_proposal_contents(
    api_client,
    proposal_factory,
    get_user_for_proposal,
    mocker,
    role,
    expected_status_code,
):
    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}
    own_proposal, other_proposal = proposal_factory.create_batch(2)
    api_client.force_authenticate(get_user_for_proposal(own_proposal, role))
    response = api_client.patch(
        reverse("proposal-detail", args=[other_proposal.id]),
        data={"title": "edited title"},
    )
    assert response.status_code == expected_status_code
