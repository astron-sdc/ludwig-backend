import pytest
from django.urls import reverse
from pytest_factoryboy import register

import ludwig_api
from ludwig_api.proposal.tests.factories.call_factory import CallFactory

register(CallFactory)


@pytest.mark.django_db
def test_proposal_project_code_on_create(authenticated_api_client, single_call, mocker):
    proposal_data = {
        "title": "Single proposal",
        "abstract": "Short but sweet",
        "call_id": single_call.id,
    }

    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    mocked_create_proposal_in_sram = mocker.patch(
        "ludwig_api.proposal.serializers.proposal_serializer.create_proposal_in_sram"
    )

    response = authenticated_api_client.post(
        reverse("proposal-list"), data=proposal_data
    )
    assert response.json()["project_code"] == f"{single_call.code}_{1:03}"

    response = authenticated_api_client.post(
        reverse("proposal-list"), data=proposal_data
    )
    assert response.json()["project_code"] == f"{single_call.code}_{2:03}"

    response = authenticated_api_client.post(
        reverse("proposal-list"), data=proposal_data
    )
    assert response.json()["project_code"] == f"{single_call.code}_{3:03}"

    assert mocked_create_proposal_in_sram.call_count == 3
