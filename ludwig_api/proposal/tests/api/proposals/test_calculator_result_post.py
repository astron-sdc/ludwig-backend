import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.status import HTTP_201_CREATED

from ludwig_api.proposal.models.calculator_inputs import CalculatorInput
from ludwig_api.proposal.permissions.proposal import Roles
from ludwig_api.proposal.serializers.calculator_inputs_serializer import (
    CalculatorInputSerializer,
)


@pytest.mark.django_db
def test_calculate_for_target_success(
    api_client, target_factory, proposal_detail_draft, get_user_for_proposal
):
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_draft, Roles.PI)
    )

    target = target_factory(
        proposal=proposal_detail_draft,
        name="M31",
        x=10.684708333333331,
        y=41.26875,
        group=1,
        subgroup=1,
    )

    calculator_input = {
        "target": target.id,
        "proposal": proposal_detail_draft.id,
        "observation_time_seconds": 28800,
        "no_core_stations": 24,
        "no_remote_stations": 14,
        "no_international_stations": 14,
        "no_channels_per_subband": "64",
        "no_subbands": 488,
        "integration_time_seconds": 1,
        "antenna_set": "hbadualinner",
        "observation_date": "2025-02-12",
        "calibrators": [],
        "a_team_sources": ["VirA"],
        "pipeline": "preprocessing",
        "enable_dysco_compression": True,
        "frequency_averaging_factor": 4,
        "time_averaging_factor": 1,
    }

    data_in = CalculatorInputSerializer(data=calculator_input)
    data_in.is_valid(raise_exception=True)

    response = api_client.post(
        reverse(
            "proposal-calculator-results-calculate", args=[proposal_detail_draft.id]
        ),
        calculator_input,
        format="json",
    )

    expected_json = {
        "target": target.id,
        "target_name": target.name,
        "proposal": proposal_detail_draft.id,
        "raw_data_size_bytes": 89645407396160,
        "processed_data_size_bytes": 13433103860586,
        "pipeline_processing_time_seconds": 35136,
        "mean_elevation": 57.14853320353321,
        "theoretical_rms_jansky_beam": 12.308388610784826,
        "effective_rms_jansky_beam": 46.04702733357935,
        "effective_rms_jansky_beam_err": 14.039109590207346,
    }
    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == expected_json

    result = CalculatorInput.objects.get(
        proposal__id=proposal_detail_draft.id, target__id=target.id
    )
    assert (
        result.observation_time_seconds == calculator_input["observation_time_seconds"]
    )
    assert result.observation_time_seconds == 28800
    assert result.calibrators == []
    assert result.a_team_sources == ["VirA"]

    # Modify the inputs, post again and check the same db entry is modified
    calculator_input["observation_time_seconds"] = 10
    calculator_input["calibrators"] = ["3C48"]

    assert (
        api_client.post(
            reverse(
                "proposal-calculator-results-calculate", args=[proposal_detail_draft.id]
            ),
            calculator_input,
            format="json",
        ).status_code
        == HTTP_201_CREATED
    )

    result = CalculatorInput.objects.get(id=result.id)
    assert result.observation_time_seconds == 10
    assert result.calibrators == ["3C48"]

    assert (
        CalculatorInput.objects.filter(
            proposal__id=proposal_detail_draft.id, target__id=target.id
        ).count()
        == 1
    )


@pytest.mark.django_db
def test_calculate_for_target_below_horizon_failure(
    api_client, target_factory, proposal_detail_draft, get_user_for_proposal
):
    api_client.force_authenticate(
        get_user_for_proposal(proposal_detail_draft, Roles.PI)
    )

    target = target_factory(
        proposal=proposal_detail_draft,
        name="NGC 4755",
        x=193.41499999999996,
        y=-60.371,
        group=1,
        subgroup=1,
    )

    calculator_input = {
        "target": target.id,
        "proposal": proposal_detail_draft.id,
        "observation_time_seconds": 28800,
        "no_core_stations": 24,
        "no_remote_stations": 14,
        "no_international_stations": 14,
        "no_channels_per_subband": "64",
        "no_subbands": 488,
        "integration_time_seconds": 1,
        "antenna_set": "hbadualinner",
        "observation_date": "2025-02-12",
        "calibrators": [],
        "a_team_sources": [],
        "pipeline": "none",
        "enable_dysco_compression": False,
        "frequency_averaging_factor": 4,
        "time_averaging_factor": 1,
    }

    data_in = CalculatorInputSerializer(data=calculator_input)
    data_in.is_valid(raise_exception=True)

    response = api_client.post(
        reverse(
            "proposal-calculator-results-calculate", args=[proposal_detail_draft.id]
        ),
        calculator_input,
        format="json",
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
