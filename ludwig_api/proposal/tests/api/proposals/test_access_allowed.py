import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.status import HTTP_204_NO_CONTENT

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.proposal.models.panel_type import PanelType
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.tests.api.reviews.conftest import assign_user_as_reviewer
from ludwig_api.proposal.tests.conftest import merge_user_entitlements
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory


@pytest.mark.django_db
def test_proposal_access_allowed_not_found(authenticated_api_client):
    response = authenticated_api_client.get(
        reverse("proposal-access-allowed", args=[1337])
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_access_allowed__without_token(api_client):
    proposal = ProposalFactory()
    response = api_client.get(reverse("proposal-access-allowed", args=[proposal.id]))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_proposal_access_allowed__with_token(
    api_client, user, proposal_detail_draft_requires_direct_action
):
    api_client.force_authenticate(user)
    response = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    )
    # It actually filters out the proposal; so it's not found
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_access_allowed__as_pi(
    api_client,
    get_user_for_proposal,
    proposal_detail_draft_requires_direct_action,
    mocker,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    api_client.force_authenticate(
        get_user_for_proposal(
            proposal_detail_draft_requires_direct_action, ProposalRoles.PI
        )
    )
    response = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    )
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_proposal_access_allowed__as_science_reviewer_via_call(
    api_client, get_user_for_call, proposal_detail_draft_requires_direct_action, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    api_client.force_authenticate(
        get_user_for_call(
            proposal_detail_draft_requires_direct_action.call,
            CallRoles.SCIENCE_REVIEWER,
        )
    )
    response = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json()["read_science_review"] is True


@pytest.mark.django_db
def test_proposal_access_allowed__as_co_author_read_only(
    api_client,
    get_user_for_proposal,
    proposal_detail_draft_requires_direct_action,
    mocker,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    api_client.force_authenticate(
        get_user_for_proposal(
            proposal_detail_draft_requires_direct_action,
            ProposalRoles.CO_AUTHOR_READ_ONLY,
        )
    )
    response = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    )
    assert response.status_code == status.HTTP_200_OK
    ro_author_access = response.json()

    assert ro_author_access["read_calculation"] is True
    assert ro_author_access["write_proposal"] is False
    assert ro_author_access["write_calculation"] is False
    assert ro_author_access["read_science_review"] is False
    assert ro_author_access["read_technical_review"] is False
    assert ro_author_access["write_science_review"] is False
    assert ro_author_access["write_technical_review"] is False


@pytest.mark.django_db
def test_proposal_access_allowed__as_pi_and_science_and_technical_reviewer_via_call(
    api_client,
    get_user_for_call,
    get_user_for_proposal,
    proposal_detail_draft_requires_direct_action,
    mocker,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    pi_user = get_user_for_proposal(
        proposal_detail_draft_requires_direct_action, ProposalRoles.PI
    )
    sci_user = get_user_for_call(
        proposal_detail_draft_requires_direct_action.call, CallRoles.SCIENCE_REVIEWER
    )
    pre_assigned_only_tech_user = get_user_for_call(
        proposal_detail_draft_requires_direct_action.call, CallRoles.TECHNICAL_REVIEWER
    )
    pi_and_sci_user = merge_user_entitlements([pi_user, sci_user])

    api_client.force_authenticate(pi_user)
    pi_user_access = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    ).json()

    api_client.force_authenticate(sci_user)
    sci_user_access = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    ).json()

    api_client.force_authenticate(pre_assigned_only_tech_user)
    pre_assigned_tech_user_access = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    ).json()

    # Assign the pre-assign tech reviewer as reviewer to the proposal, which should give them read access.
    assert (
        assign_user_as_reviewer(
            pre_assigned_only_tech_user.id,
            proposal_detail_draft_requires_direct_action,
            PanelType.technical,
        ).status_code
        == HTTP_204_NO_CONTENT
    )

    assigned_tech_user_access = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    ).json()

    api_client.force_authenticate(pi_and_sci_user)
    pi_and_sci_user_access = api_client.get(
        reverse(
            "proposal-access-allowed",
            args=[proposal_detail_draft_requires_direct_action.id],
        )
    ).json()

    assert pi_user_access["read_calculation"] is True
    assert pi_user_access["write_calculation"] is True
    assert pi_user_access["read_science_review"] is False
    assert pi_user_access["read_technical_review"] is False
    assert pi_user_access["write_science_review"] is False
    assert pi_user_access["write_technical_review"] is False

    assert sci_user_access["read_calculation"] is True
    assert sci_user_access["write_calculation"] is False
    assert sci_user_access["read_science_review"] is True
    assert sci_user_access["read_technical_review"] is True
    assert sci_user_access["write_science_review"] is False
    assert sci_user_access["write_technical_review"] is False

    assert pre_assigned_tech_user_access["read_calculation"] is False
    assert pre_assigned_tech_user_access["write_calculation"] is False
    assert pre_assigned_tech_user_access["read_science_review"] is False
    assert pre_assigned_tech_user_access["read_technical_review"] is False
    assert pre_assigned_tech_user_access["write_science_review"] is False
    assert pre_assigned_tech_user_access["write_technical_review"] is False

    assert assigned_tech_user_access["read_calculation"] is True
    assert assigned_tech_user_access["write_calculation"] is False
    assert assigned_tech_user_access["read_science_review"] is False
    assert assigned_tech_user_access["read_technical_review"] is True
    assert assigned_tech_user_access["write_science_review"] is False
    assert assigned_tech_user_access["write_technical_review"] is True

    assert pi_and_sci_user_access["read_calculation"] is True
    assert pi_and_sci_user_access["write_calculation"] is True
    assert pi_and_sci_user_access["read_science_review"] is False
    assert pi_and_sci_user_access["read_technical_review"] is False
    assert pi_and_sci_user_access["write_science_review"] is False
    assert pi_and_sci_user_access["write_technical_review"] is False

    assert pi_user_access != sci_user_access != pi_and_sci_user_access
