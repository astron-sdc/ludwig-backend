import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.permissions.proposal import Roles


@pytest.mark.django_db
def test_proposal_retract__success_as_admin(admin_api_client, proposal_detail_draft):
    response = admin_api_client.put(
        reverse("proposal-retract", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_200_OK
    proposal_detail_draft.refresh_from_db()
    assert proposal_detail_draft.status == ProposalStatus.retracted


@pytest.mark.django_db
def test_proposal_retract__fail_as_admin(admin_api_client, proposal_detail_submitted):
    response = admin_api_client.put(
        reverse("proposal-retract", args=[proposal_detail_submitted.id])
    )
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    assert response.json()["error"] == "invalid transition from source to target"
    proposal_detail_submitted.refresh_from_db()
    assert proposal_detail_submitted.status == ProposalStatus.submitted


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code,expected_proposal_status",
    [
        (Roles.PI, status.HTTP_200_OK, ProposalStatus.retracted),
        (Roles.CO_AUTHOR_READ_ONLY, status.HTTP_403_FORBIDDEN, ProposalStatus.draft),
        (Roles.CO_AUTHOR, status.HTTP_403_FORBIDDEN, ProposalStatus.draft),
    ],
)
@pytest.mark.django_db
def test_retract_own_proposal(
    api_client,
    proposal_detail_draft,
    get_user_for_proposal,
    role,
    expected_status_code,
    expected_proposal_status,
):
    api_client.force_authenticate(get_user_for_proposal(proposal_detail_draft, role))
    response = api_client.put(
        reverse("proposal-retract", args=[proposal_detail_draft.id])
    )
    assert response.status_code == expected_status_code
    proposal_detail_draft.refresh_from_db()
    assert proposal_detail_draft.status == expected_proposal_status


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role,expected_status_code",
    [
        (Roles.PI, status.HTTP_404_NOT_FOUND),
        (Roles.CO_AUTHOR_READ_ONLY, status.HTTP_404_NOT_FOUND),
        (Roles.CO_AUTHOR, status.HTTP_404_NOT_FOUND),
    ],
)
@pytest.mark.django_db
def test_retract_other_proposal(
    api_client, proposal_factory, get_user_for_proposal, role, expected_status_code
):
    own_proposal, other_proposal = proposal_factory.create_batch(
        2, status=ProposalStatus.draft, submitted_at=None
    )
    api_client.force_authenticate(get_user_for_proposal(own_proposal, role))
    response = api_client.put(reverse("proposal-retract", args=[other_proposal.id]))
    assert response.status_code == expected_status_code
    other_proposal.refresh_from_db()
    assert other_proposal.status == ProposalStatus.draft
