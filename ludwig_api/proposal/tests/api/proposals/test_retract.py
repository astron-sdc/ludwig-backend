import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.proposal import ProposalStatus


@pytest.mark.django_db
def test_proposal_retract__success(api_client, proposal_detail_draft):
    response = api_client.put(
        reverse("proposal-retract", args=[proposal_detail_draft.id])
    )
    assert response.status_code == status.HTTP_200_OK
    proposal_detail_draft.refresh_from_db()
    assert proposal_detail_draft.status == ProposalStatus.retracted


@pytest.mark.django_db
def test_proposal_retract__fail(api_client, proposal_detail_submitted):
    response = api_client.put(
        reverse("proposal-retract", args=[proposal_detail_submitted.id])
    )
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
    assert response.json()["error"] == "invalid transition from source to target"
    proposal_detail_submitted.refresh_from_db()
    assert proposal_detail_submitted.status == ProposalStatus.submitted
