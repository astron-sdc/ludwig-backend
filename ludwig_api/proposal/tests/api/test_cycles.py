from http import HTTPStatus

import pytest
from django.urls import reverse
from pytest_factoryboy import register
from rest_framework.test import APIClient

from ludwig_api.proposal.serializers.call_serializer import CallSerializer
from ludwig_api.proposal.serializers.cycle_serializer import CycleSerializer
from ludwig_api.proposal.tests.factories.call_factory import CallFactory
from ludwig_api.proposal.tests.factories.cycle_factory import CycleFactory

register(CycleFactory)
register(CallFactory)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def list_of_cycles(cycle_factory):
    return cycle_factory.create_batch(10)


@pytest.fixture
def single_cycle(cycle_factory):
    return cycle_factory()


@pytest.mark.django_db
def test_get_cycles_returns_http_ok(api_client, list_of_cycles):
    response = api_client.get(reverse("cycle-list"))
    assert response.status_code == HTTPStatus.OK


@pytest.mark.django_db
def test_get_cycle_returns_http_ok(api_client, single_cycle):
    response = api_client.get(reverse("cycle-detail", args=[single_cycle.id]))
    assert response.status_code == HTTPStatus.OK


@pytest.mark.django_db
def test_get_calls_in_cycle_returns_http_ok(api_client, single_cycle):
    response = api_client.get(reverse("cycle-calls-list", args=[single_cycle.id]))
    assert response.status_code == HTTPStatus.OK


@pytest.mark.django_db
def test_get_cycles_returns_cycles(api_client, list_of_cycles):
    response = api_client.get(reverse("cycle-list"))
    response_json = response.json()
    cycles_json = CycleSerializer(list_of_cycles, many=True).data
    assert response_json == cycles_json


@pytest.mark.django_db
def test_get_cycle_returns_cycle(api_client, single_cycle):
    response = api_client.get(reverse("cycle-detail", args=[single_cycle.id]))
    response_json = response.json()
    cycle_json = CycleSerializer(single_cycle).data
    assert response_json == cycle_json


@pytest.mark.django_db
def test_get_calls_in_cycle_returns_cycle_calls(api_client, single_cycle, call_factory):
    list_of_cycle_calls = call_factory.create_batch(size=6, cycle=single_cycle)
    response = api_client.get(reverse("cycle-calls-list", args=[single_cycle.id]))
    response_json = response.json()
    calls_json = CallSerializer(list_of_cycle_calls, many=True).data
    assert response_json == calls_json


@pytest.mark.django_db
def test_get_calls_in_empty_cycle_returns_no_calls(
    api_client, cycle_factory, call_factory
):
    cycle_with_calls = cycle_factory()
    cycle_without_calls = cycle_factory()
    call_factory.create_batch(size=6, cycle=cycle_with_calls)

    response = api_client.get(
        reverse("cycle-calls-list", args=[cycle_without_calls.id])
    )
    response_json = response.json()
    assert response_json == []
