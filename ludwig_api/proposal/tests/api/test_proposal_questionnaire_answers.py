from decimal import Decimal

import pytest
from django.urls import reverse
from pytest_factoryboy import register
from rest_framework import status

from ludwig_api.proposal.models import ChoiceAnswer, NumberAnswer, TextAnswer
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.questionnaire_factory import (
    ChoiceQuestionFactory,
    NumberQuestionFactory,
    QuestionnaireFactory,
    TextQuestionFactory,
)

register(ProposalFactory)
register(QuestionnaireFactory)
register(TextQuestionFactory)
register(NumberQuestionFactory)
register(ChoiceQuestionFactory)


@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_proposal_not_found(
    authenticated_api_client,
):
    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[1337])
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_questionnaire_not_found(
    authenticated_api_client, proposal_factory
):
    proposal = proposal_factory()
    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id])
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_missing_question(
    authenticated_api_client, proposal_factory
):
    proposal = proposal_factory()
    answers = [{"answer": "correct"}]

    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id]),
        data=answers,
        format="json",
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_missing_answer(
    authenticated_api_client, proposal_factory
):
    proposal = proposal_factory()
    answers = [{"question_id": 17}]

    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id]),
        data=answers,
        format="json",
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_succes(
    authenticated_api_client,
    proposal_factory,
    questionnaire_factory,
    text_question_factory,
    number_question_factory,
    choice_question_factory,
):
    questionnaire = questionnaire_factory()

    text_question = text_question_factory(questionnaire=questionnaire)
    number_question = number_question_factory(questionnaire=questionnaire)
    choice_question = choice_question_factory(questionnaire=questionnaire)

    proposal = proposal_factory(call=questionnaire.call)

    answers = [
        {"question_id": text_question.id, "answer": "hello there"},
        {"question_id": number_question.id, "answer": "3.37"},
        {"question_id": choice_question.id, "answer": "yes"},
    ]

    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id]),
        data=answers,
        format="json",
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT

    text_answer = TextAnswer.objects.get(question=text_question, proposal=proposal)
    assert text_answer.answer == "hello there"

    number_answer = NumberAnswer.objects.get(
        question=number_question, proposal=proposal
    )
    assert number_answer.answer == Decimal("3.37")

    choice_answer = ChoiceAnswer.objects.get(
        question=choice_question, proposal=proposal
    )
    assert choice_answer.answer == "yes"


@pytest.mark.parametrize("answer", [None, ""])
@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_succes_with_null_or_empty(
    authenticated_api_client,
    proposal_factory,
    questionnaire_factory,
    text_question_factory,
    number_question_factory,
    choice_question_factory,
    answer,
):
    questionnaire = questionnaire_factory()

    text_question = text_question_factory(questionnaire=questionnaire)
    number_question = number_question_factory(questionnaire=questionnaire)
    choice_question = choice_question_factory(questionnaire=questionnaire)

    proposal = proposal_factory(call=questionnaire.call)

    answers = [
        {"question_id": text_question.id, "answer": answer},
        {"question_id": number_question.id, "answer": answer},
        {"question_id": choice_question.id, "answer": answer},
    ]

    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id]),
        data=answers,
        format="json",
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT

    text_answer = TextAnswer.objects.get(question=text_question, proposal=proposal)
    assert text_answer.answer == ""

    number_answer = NumberAnswer.objects.get(
        question=number_question, proposal=proposal
    )
    assert number_answer.answer is None

    choice_answer = ChoiceAnswer.objects.get(
        question=choice_question, proposal=proposal
    )
    assert choice_answer.answer == ""


@pytest.mark.parametrize(
    "answer, min_char_length, max_char_length, min_word_count, max_word_count, expectation",
    [
        ("abc", 5, 10, 0, 10, status.HTTP_400_BAD_REQUEST),
        ("abcde", 2, 3, 0, 10, status.HTTP_400_BAD_REQUEST),
        ("abc", 0, 100, 2, 4, status.HTTP_400_BAD_REQUEST),
        ("a b c d e", 0, 100, 2, 4, status.HTTP_400_BAD_REQUEST),
        ("", 0, 0, 0, 0, status.HTTP_204_NO_CONTENT),
        ("abc", 3, 5, 0, 10, status.HTTP_204_NO_CONTENT),
        ("abcde", 3, 5, 0, 10, status.HTTP_204_NO_CONTENT),
        ("abc def", 0, 100, 2, 4, status.HTTP_204_NO_CONTENT),
        ("a b c d", 0, 100, 2, 4, status.HTTP_204_NO_CONTENT),
    ],
)
@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_validate_text_answer(
    authenticated_api_client,
    proposal_factory,
    questionnaire_factory,
    text_question_factory,
    answer,
    min_char_length,
    max_char_length,
    min_word_count,
    max_word_count,
    expectation,
):
    questionnaire = questionnaire_factory()

    text_question = text_question_factory(
        questionnaire=questionnaire,
        min_char_length=min_char_length,
        max_char_length=max_char_length,
        min_word_count=min_word_count,
        max_word_count=max_word_count,
    )

    proposal = proposal_factory(call=questionnaire.call)

    answers = [
        {"question_id": text_question.id, "answer": answer},
    ]

    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id]),
        data=answers,
        format="json",
    )

    assert response.status_code == expectation


@pytest.mark.parametrize(
    "answer, min_value, max_value, expectation",
    [
        ("abc", 0, 10, status.HTTP_400_BAD_REQUEST),
        ("9.99", 10, 20, status.HTTP_400_BAD_REQUEST),
        ("20.01", 10, 20, status.HTTP_400_BAD_REQUEST),
        ("0", 0, 0, status.HTTP_204_NO_CONTENT),
        ("10", 10, 20, status.HTTP_204_NO_CONTENT),
        ("20", 10, 20, status.HTTP_204_NO_CONTENT),
    ],
)
@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_validate_number_answer(
    authenticated_api_client,
    proposal_factory,
    questionnaire_factory,
    number_question_factory,
    answer,
    min_value,
    max_value,
    expectation,
):
    questionnaire = questionnaire_factory()

    number_question = number_question_factory(
        questionnaire=questionnaire, min_value=min_value, max_value=max_value
    )

    proposal = proposal_factory(call=questionnaire.call)

    answers = [
        {"question_id": number_question.id, "answer": answer},
    ]

    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id]),
        data=answers,
        format="json",
    )

    assert response.status_code == expectation


@pytest.mark.parametrize(
    "answer, expectation",
    [
        ("Red", status.HTTP_204_NO_CONTENT),
        ("White", status.HTTP_204_NO_CONTENT),
        ("Blue", status.HTTP_204_NO_CONTENT),
        ("Yellow", status.HTTP_400_BAD_REQUEST),
    ],
)
@pytest.mark.django_db
def test_proposal_current_questionnaire_post_answers_validate_choice_answer(
    authenticated_api_client,
    proposal_factory,
    questionnaire_factory,
    choice_question_factory,
    answer,
    expectation,
):
    questionnaire = questionnaire_factory()

    choice_question = choice_question_factory(
        questionnaire=questionnaire, choices=["Red", "White", "Blue"]
    )

    proposal = proposal_factory(call=questionnaire.call)

    answers = [
        {"question_id": choice_question.id, "answer": answer},
    ]

    response = authenticated_api_client.post(
        reverse("proposal-questionnaire-current-answers", args=[proposal.id]),
        data=answers,
        format="json",
    )

    assert response.status_code == expectation
