import pytest


@pytest.fixture
def expected_proposal_authors():
    return [
        "Bobby Tables <bobbytables@example.com> (PI)",
        "Foo Baz <foobaz@example.com> (CO Author Read Only)",
        "Foo Baz <foobaz@example.com> (CO Author)",
        "Piebe Gealle <p.gealle@example.com> (CO Author)",
    ]


@pytest.fixture
def user_collaboration_membership():
    return {
        "uid": "lorem@ipsum.org",
        "name": "Lorem Ipsum",
        "given_name": "Lorem",
        "family_name": "Ipsum",
        "email": "li@example.com",
        "schac_home_organisation": "",
        "scoped_affiliation": "",
    }
