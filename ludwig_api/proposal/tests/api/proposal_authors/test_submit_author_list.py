import pytest

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory


@pytest.mark.django_db
def test_submit_proposal_check_author_list(
    mocker, collaboration_groups, expected_proposal_authors
):
    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": collaboration_groups,
        "services": [],
        "short_name": "abc",
    }
    proposal = ProposalFactory(collaboration_identifier="ABC")
    assert proposal.authors is None
    proposal.submit()
    assert proposal.authors == expected_proposal_authors
    mocked_get.assert_called_once()
    del proposal.collaboration  # del property to invalidate cached_property

    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": [],
        "services": [],
        "short_name": "abc",
    }
    proposal.unsubmit()
    proposal.submit()

    assert proposal.authors == []


@pytest.mark.django_db
def test_submit_proposal_mutate_authors_resubmit_check_author_list(
    mocker,
    collaboration_groups,
    user_collaboration_membership,
    expected_proposal_authors,
):
    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": collaboration_groups,
        "services": [],
        "short_name": "abc",
    }
    proposal = ProposalFactory(collaboration_identifier="ABC")
    assert proposal.authors is None
    proposal.submit()
    assert proposal.authors == expected_proposal_authors
    mocked_get.assert_called_once()
    del proposal.collaboration  # del property to invalidate cached_property

    updated_group = collaboration_groups
    for group in updated_group:
        if group["short_name"] == "co_author_ro":
            group["collaboration_memberships"].append(
                {"user": user_collaboration_membership}
            )

    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": updated_group,
        "services": [],
        "short_name": "abc",
    }

    proposal.unsubmit()
    proposal.submit()

    assert proposal.authors == sorted(
        [
            *expected_proposal_authors,
            "Lorem Ipsum <li@example.com> (CO Author Read Only)",
        ]
    )
