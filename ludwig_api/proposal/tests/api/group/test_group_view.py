import pytest
from django.urls import reverse
from rest_framework import status


@pytest.mark.django_db
def test_group_view(authenticated_api_client):

    url = reverse("groups-list")
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_group_view_with_query(authenticated_api_client):
    url = reverse("proposal-groups-list", args=[2])
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_group_view_specific_item(authenticated_api_client):
    url = reverse("groups-detail", args=[2])
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_404_NOT_FOUND
