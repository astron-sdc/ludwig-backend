import pytest


@pytest.fixture
def single_proposal(proposal_factory):
    return proposal_factory()


@pytest.fixture
def target_list(target_factory, single_proposal):
    return target_factory.create_batch(10, proposal=single_proposal)
