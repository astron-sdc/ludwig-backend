import pytest
from pytest_factoryboy import register
from rest_framework.test import APIClient

from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.target_factory import TargetFactory

register(ProposalFactory)
register(TargetFactory)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def single_proposal(proposal_factory):
    return proposal_factory()


@pytest.fixture
def target_list(target_factory, single_proposal):
    return target_factory.create_batch(10, proposal=single_proposal)
