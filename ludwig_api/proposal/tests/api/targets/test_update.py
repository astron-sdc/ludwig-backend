import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models import Target
from ludwig_api.proposal.serializers.target.target_default_serializer import (
    TargetDefaultSerializer,
)


@pytest.mark.django_db
def test_update_target_fully(authenticated_api_client, single_proposal, target_factory):
    existing_target = target_factory(proposal=single_proposal)
    target_to_update = {
        "id": existing_target.id,
        "name": "THE MOON",
        "x_hms_formatted": "16:34:33.81",
        "y_dms_formatted": "+62:45:36.0",
        "system": "J2000",
        "notes": "Some notes",
        "group": 999,
        "subgroup": 2,
        "field_order": 1,
        "requested_priority": "A",
        "assigned_priority": "B",
    }

    response = authenticated_api_client.put(
        path=reverse(
            "proposal-targets-detail", args=[single_proposal.id, existing_target.id]
        ),
        data=target_to_update,
    )

    assert response.status_code == status.HTTP_200_OK

    updated_target = Target.objects.get(id=existing_target.id)

    assert updated_target.proposal_id == single_proposal.id

    target_json = TargetDefaultSerializer(updated_target).data

    expected_target = target_to_update
    expected_target["x"] = 248.64087499999997
    expected_target["y"] = 62.76

    assert target_json == target_to_update


@pytest.mark.django_db
def test_update_target_partially(
    authenticated_api_client, single_proposal, target_factory
):
    existing_target = target_factory(proposal=single_proposal)

    target_to_update = {
        "id": existing_target.id,
        "name": "NOT THE MOON",
    }

    response = authenticated_api_client.patch(
        path=reverse(
            "proposal-targets-detail", args=[single_proposal.id, existing_target.id]
        ),
        data=target_to_update,
    )

    assert response.status_code == status.HTTP_200_OK

    updated_target = Target.objects.get(id=existing_target.id)

    assert updated_target.proposal_id == single_proposal.id

    assert updated_target.name == target_to_update["name"]
