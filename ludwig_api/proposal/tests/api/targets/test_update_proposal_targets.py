import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models import Target
from ludwig_api.proposal.serializers.target.target_default_serializer import (
    TargetDefaultSerializer,
)


@pytest.mark.django_db
def test_update_proposal_targets(
    authenticated_api_client, single_proposal, proposal_factory, target_factory
):
    existing_target = target_factory(proposal=single_proposal)
    existing_target.name = "I am so updated"
    existing_target.x_hms_formatted = "16:34:33.81"
    existing_target.y_dms_formatted = "+62:45:36.0"

    # this target should be deleted, because it will not be in the request data
    target_factory(pk=2000, proposal=single_proposal)

    other_proposal = proposal_factory()
    other_proposal_target = target_factory(pk=1000, proposal=other_proposal)

    path = reverse(
        "proposal-targets-update-proposal-targets", args=[single_proposal.id]
    )
    data = [
        # update
        TargetDefaultSerializer(existing_target).data,
        # create (id does not exist for this proposal)
        {
            "id": other_proposal_target.id,
            "name": "target for some other proposal",
            "x_hms_formatted": "16:34:33.81",
            "y_dms_formatted": "+62:45:36.0",
            "system": "J2000",
            "notes": "juicy",
            "group": 9,
            "subgroup": 8,
            "requested_priority": "A",
            "assigned_priority": "B",
            "field_order": 1,
        },
        # create
        {
            "name": "the sun",
            "x_hms_formatted": "16:34:33.81",
            "y_dms_formatted": "+62:45:36.0",
            "system": "J2000",
            "notes": "sunny",
            "group": 99,
            "subgroup": 88,
            "requested_priority": "A",
            "assigned_priority": "B",
            "field_order": 1,
        },
        # create
        {
            "name": "the moon",
            "x_hms_formatted": "16:34:33.81",
            "y_dms_formatted": "+62:45:36.0",
            "system": "J2000",
            "notes": "moony",
            "group": 999,
            "subgroup": 888,
            "requested_priority": "A",
            "assigned_priority": "B",
            "field_order": 1,
        },
        # create
        {
            "name": "betelgeuse",
            "x_hms_formatted": "16:34:33.81",
            "y_dms_formatted": "+62:45:36.0",
            "system": "J2000",
            "notes": "juicy",
            "group": 9,
            "subgroup": 8,
            "requested_priority": "A",
            "assigned_priority": "B",
            "field_order": 1,
        },
        # create (id does not exist for this proposal, or any proposal for that matter)
        {
            "id": 1337,
            "name": "non-existent id: andromeda",
            "x_hms_formatted": "16:34:33.81",
            "y_dms_formatted": "+62:45:36.0",
            "system": "J2000",
            "notes": "juicy",
            "group": 99,
            "subgroup": 88,
            "requested_priority": "A",
            "assigned_priority": "B",
            "field_order": 1,
        },
    ]

    response = authenticated_api_client.post(path=path, data=data, format="json")

    assert response.status_code == status.HTTP_204_NO_CONTENT

    created_proposal_targets = Target.objects.filter(proposal=single_proposal)

    created_json = TargetDefaultSerializer(created_proposal_targets, many=True).data

    for i, target in enumerate(data):
        target["id"] = existing_target.id + i
        target["x"] = 248.64087499999997
        target["y"] = 62.76

    assert created_json == data


# @pytest.mark.django_db
@pytest.mark.skip(
    "Temporary disabled. It is incompatible input, but i do get a different error"
)
def test_update_proposal_targets_bad_request(authenticated_api_client, single_proposal):
    path = reverse(
        "proposal-targets-update-proposal-targets", args=[single_proposal.id]
    )
    response = authenticated_api_client.post(
        path=path, data=[{"name": "incomplete"}], format="json"
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
