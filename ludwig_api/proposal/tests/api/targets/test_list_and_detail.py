import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.serializers.target.target_default_serializer import (
    TargetDefaultSerializer,
)


@pytest.mark.django_db
def test_target_list(authenticated_api_client, single_proposal, target_list):
    response = authenticated_api_client.get(
        reverse("proposal-targets-list", args=[single_proposal.id])
    )
    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    targets_json = TargetDefaultSerializer(target_list, many=True).data
    assert response_json == targets_json


@pytest.mark.django_db
def test_target_detail(authenticated_api_client, single_proposal, target_list):
    response = authenticated_api_client.get(
        reverse(
            "proposal-targets-detail",
            args=[single_proposal.id, target_list[0].id],
        )
    )
    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    target_json = TargetDefaultSerializer(target_list[0]).data
    assert response_json == target_json
