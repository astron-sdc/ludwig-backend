import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models import Target


@pytest.mark.django_db
def test_create_target_correct_proposal_id(authenticated_api_client, single_proposal):
    target_to_create = {
        "name": "THE SUN",
        "x_hms_formatted": "16:34:33.81",
        "y_dms_formatted": "+62:45:35.9",
        "system": "J2000",
        "notes": "Some notes",
    }
    response = authenticated_api_client.post(
        path=reverse("proposal-targets-list", args=[single_proposal.id]),
        data=target_to_create,
    )

    assert response.status_code == status.HTTP_201_CREATED

    response_json = response.json()
    created_target = Target.objects.get(id=response_json["id"])

    assert created_target.proposal_id == single_proposal.id
