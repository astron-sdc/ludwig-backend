import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.serializers.questionnaire_serializer import (
    QuestionnaireSerializer,
    QuestionSerializer,
)
from ludwig_api.proposal.tests.factories.questionnaire_factory import (
    QuestionnaireFactory,
    QuestionnaireWithoutQuestionsFactory,
)


@pytest.fixture
def questionnaire_fixture():
    return QuestionnaireFactory()


@pytest.mark.django_db
def test_get_questionnaires(authenticated_api_client):
    list_of_questionnaires = QuestionnaireFactory.create_batch(10)
    path = reverse("questionnaire-list")
    response = authenticated_api_client.get(path)
    assert response.status_code == status.HTTP_200_OK
    questionnaires_json = QuestionnaireSerializer(
        list_of_questionnaires, many=True
    ).data
    assert response.json() == questionnaires_json


@pytest.mark.parametrize(
    "questionnaire_factory",
    [QuestionnaireFactory, QuestionnaireWithoutQuestionsFactory],
)
@pytest.mark.django_db
def test_get_questionnaire_detail(authenticated_api_client, questionnaire_factory):
    questionnaire = questionnaire_factory()
    path = reverse("questionnaire-detail", args=[questionnaire.pk])
    response = authenticated_api_client.get(path)
    assert response.status_code == status.HTTP_200_OK
    # test that it shows all the questions as well
    questions_json = QuestionSerializer(questionnaire.questions.all(), many=True).data
    assert response.json()["questions"] == questions_json


@pytest.mark.django_db
def test_get_questionnaire_question(authenticated_api_client, questionnaire_fixture):
    # test that questionnaire/[id]/questions only lists questions from 1 questionnaire
    other_questionnaire = QuestionnaireFactory()
    path = reverse("questionnaire-question-list", args=[questionnaire_fixture.pk])
    response_json = authenticated_api_client.get(path).json()
    questions_json = QuestionSerializer(
        questionnaire_fixture.questions.all().order_by("order"), many=True
    ).data
    assert response_json == questions_json
    for question in response_json:
        assert question["questionnaire"] == questionnaire_fixture.pk
        assert question["questionnaire"] != other_questionnaire.pk


@pytest.mark.django_db
def test_update_questionnaire_question__valid(
    authenticated_api_client, questionnaire_fixture
):
    question = questionnaire_fixture.questions.first()
    path = reverse(
        "questionnaire-question-detail",
        kwargs={"questionnaire_pk": questionnaire_fixture.pk, "pk": question.pk},
    )
    old_title = question.title
    new_title = old_title + "V2"
    # change the question.title
    response = authenticated_api_client.patch(path, data={"title": new_title})
    assert response.status_code == status.HTTP_200_OK
    assert authenticated_api_client.get(path).json()["title"] == new_title


@pytest.mark.django_db
def test_update_questionnaire_question__invalid(
    authenticated_api_client, questionnaire_fixture
):
    questions = questionnaire_fixture.questions
    assert questions.count() > 1
    path = reverse(
        "questionnaire-question-detail",
        kwargs={
            "questionnaire_pk": questionnaire_fixture.pk,
            "pk": questions.first().pk,
        },
    )
    # change the question.order to non-unique value
    response = authenticated_api_client.patch(
        path, data={"order": questions.last().order}
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.parametrize(
    "data",
    [
        {
            "type": "ChoiceQuestion",
            "choices": ["yes", "no"],
        },
        {
            "type": "NumberQuestion",
            "max_value": 10,
            "min_value": -10,
        },
        {
            "type": "TextQuestion",
            "max_char_length": 1,
            "max_word_count": 1,
        },
    ],
)
@pytest.mark.django_db
def test_create_questionnaire_question__valid(
    authenticated_api_client, data, questionnaire_fixture
):
    original_num_questions = questionnaire_fixture.questions.count()
    path = reverse("questionnaire-question-list", args=[questionnaire_fixture.pk])
    data.update(
        {
            "title": "question title",
            "is_required": False,
            "questionnaire": questionnaire_fixture.pk,
            "order": 1,
        }
    )
    response = authenticated_api_client.post(path, data=data)
    assert response.status_code == status.HTTP_201_CREATED
    assert questionnaire_fixture.questions.count() == original_num_questions + 1


@pytest.mark.parametrize(
    "data",
    [
        {
            "type": "choice",
            "choices": ["yes", "no"],
        },
        {
            "type": "IntegerQuestion",
            "max_value": 10,
            "min_value": -10,
        },
        {
            "type": "",
            "max_char_length": 1,
            "max_word_count": 1,
        },
    ],
)
@pytest.mark.django_db
def test_create_questionnaire_question__invalid(
    authenticated_api_client, data, questionnaire_fixture
):
    original_num_questions = questionnaire_fixture.questions.count()
    path = reverse("questionnaire-question-list", args=[questionnaire_fixture.pk])
    data.update(
        {
            "title": "question title",
            "is_required": False,
            "questionnaire": questionnaire_fixture.pk,
            "order": 1,
        }
    )
    response = authenticated_api_client.post(path, data=data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"type": "Invalid type"}
    # test that no questions were created
    assert questionnaire_fixture.questions.count() == original_num_questions
