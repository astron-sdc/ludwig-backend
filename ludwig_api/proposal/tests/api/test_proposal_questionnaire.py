import pytest
from django.urls import reverse
from pytest_factoryboy import register
from rest_framework import status

from ludwig_api.proposal.tests.factories.proposal_factory import ProposalFactory
from ludwig_api.proposal.tests.factories.questionnaire_factory import (
    ChoiceAnswerFactory,
    ChoiceQuestionFactory,
    QuestionnaireFactory,
)

register(ProposalFactory)
register(QuestionnaireFactory)
register(ChoiceQuestionFactory)
register(ChoiceAnswerFactory)


@pytest.mark.django_db
def test_proposal_current_questionnaire_proposal_not_found(authenticated_api_client):
    response = authenticated_api_client.get(
        reverse("proposal-questionnaire-current", args=[1337])
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_current_questionnaire_not_found(
    authenticated_api_client, proposal_factory
):
    proposal = proposal_factory()
    response = authenticated_api_client.get(
        reverse("proposal-questionnaire-current", args=[proposal.id])
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_proposal_current_questionnaire_with_answers(
    authenticated_api_client,
    proposal_factory,
    questionnaire_factory,
    choice_answer_factory,
):
    questionnaire_with_questions = questionnaire_factory()
    proposal = proposal_factory(call=questionnaire_with_questions.call)
    other_proposal = proposal_factory(call=questionnaire_with_questions.call)

    answer = choice_answer_factory(
        question=questionnaire_with_questions.questions.first(), proposal=proposal
    )

    response = authenticated_api_client.get(
        reverse("proposal-questionnaire-current", args=[proposal.id])
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json()["questions"][0]["answer"] == answer.answer

    response = authenticated_api_client.get(
        reverse("proposal-questionnaire-current", args=[other_proposal.id])
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json()["questions"][0]["answer"] != answer.answer
