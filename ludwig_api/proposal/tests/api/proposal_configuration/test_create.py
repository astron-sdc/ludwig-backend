import pytest
from django.urls import reverse
from rest_framework import status


@pytest.mark.django_db
def test_proposal_configuration_create(
    authenticated_api_client, proposal_detail_accepted, user, mocker
):
    proposal_id = proposal_detail_accepted.id

    proposal_configuration_to_create = {
        "observing_time_prio_A_awarded_seconds": "123",
    }

    url = reverse("proposal-configuration-list", kwargs={"proposal_pk": proposal_id})

    response = authenticated_api_client.post(
        path=url, data=proposal_configuration_to_create
    )

    assert response.status_code == status.HTTP_201_CREATED
    json = response.json()
    assert json["observing_time_prio_A_awarded_seconds"] == 123
