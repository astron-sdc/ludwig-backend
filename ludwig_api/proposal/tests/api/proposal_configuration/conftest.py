from datetime import UTC, datetime

import pytest

from ludwig_api.proposal.models.proposal import ProposalStatus


@pytest.fixture
def proposal_detail_accepted(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.accepted, submitted_at=datetime.now(UTC)
    )


@pytest.fixture
def single_call(call_factory):
    return call_factory(code="10")
