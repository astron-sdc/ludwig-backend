from django.urls import reverse
from rest_framework import status


def test_resolve_name_bad_request(authenticated_api_client):
    path = reverse("coordinate-resolve-name")
    response = authenticated_api_client.get(path=path)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_resolve_name_j2000(authenticated_api_client):
    path = reverse("coordinate-resolve-name")
    response = authenticated_api_client.get(path=path + "?name=M42&system=J2000")
    assert response.status_code == status.HTTP_200_OK


def test_resolve_name_icrs(authenticated_api_client):
    path = reverse("coordinate-resolve-name")
    response = authenticated_api_client.get(path=path + "?name=M42&system=icrs")
    assert response.status_code == status.HTTP_200_OK


def test_resolve_name_icrs_not_found(authenticated_api_client):
    path = reverse("coordinate-resolve-name")
    response = authenticated_api_client.get(path=path + "?name=moon&system=icrs")
    assert response.status_code == status.HTTP_404_NOT_FOUND
