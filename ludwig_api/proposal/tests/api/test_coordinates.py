from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


def test_resolve_name_bad_request():
    api_client = APIClient()
    path = reverse("coordinate-resolve-name")
    response = api_client.get(path=path)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
