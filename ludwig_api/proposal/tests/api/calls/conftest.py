from datetime import UTC, datetime

import pytest

from ludwig_api.proposal.models.proposal import ProposalStatus


@pytest.fixture
def single_cycle(cycle_factory):
    return cycle_factory()


@pytest.fixture
def proposals_submitted(proposal_factory):
    return proposal_factory.create_batch(
        3, status=ProposalStatus.submitted, submitted_at=datetime.now(UTC)
    )


@pytest.fixture
def proposals_not_submitted(proposal_factory):
    return proposal_factory.create_batch(
        3, status=ProposalStatus.draft, submitted_at=datetime.now(UTC)
    )
