from unittest import mock

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.serializers.proposal_enriched_serializer import (
    ProposalEnrichedSerializer,
)


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_proposal_list__filtering_by_call_as_admin(
    _,
    admin_api_client,
    proposal_list,
    mocker,
    review_factory,
    review_comment_factory,
):
    client = mocker.patch("ludwig_api.faai.sram.models.SRAMClient", autospec=True)

    first_proposal = proposal_list[0]
    review = review_factory.create(proposal=first_proposal)
    review_comment_factory.create(review=review)

    response = admin_api_client.get(
        reverse("call-proposals-list", args=[first_proposal.call.pk])
    )

    assert mocker.call().get_collaboration("") in client.method_calls

    assert response.status_code == status.HTTP_200_OK
    expected = ProposalEnrichedSerializer([first_proposal], many=True).data
    assert response.json() == expected
