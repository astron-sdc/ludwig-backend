import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models import Call
from ludwig_api.proposal.permissions.call import Roles


@pytest.mark.django_db
def test_call_edit__as_admin_success(admin_api_client, single_cycle, mocker):
    mocked_create_call_in_sram = mocker.patch(
        "ludwig_api.proposal.serializers.call_serializer.create_call_in_sram",
        autospec=True,
    )

    call_to_create = {
        "code": "01",
        "cycle_id": single_cycle.id,
        "description": "I love cycling",
        "name": "CYC",
        "requires_direct_action": False,
        "start": "2023-07-11T07:06:16.282Z",
        "end": "2025-07-11T07:06:16.282Z",
    }

    response = admin_api_client.post(path=reverse("call-list"), data=call_to_create)
    assert response.status_code == status.HTTP_201_CREATED
    mocked_create_call_in_sram.assert_called_once()

    call = Call.objects.get(code=call_to_create["code"])

    call_to_create["description"] = "I love calling"
    response = admin_api_client.patch(
        reverse("call-detail", args=[call.id]), data=call_to_create
    )
    assert response.status_code == status.HTTP_200_OK
    assert (
        Call.objects.get(code=call_to_create["code"]).description
        == call_to_create["description"]
    )


@pytest.mark.django_db
def test_call_edit__as_authenticated_failure(
    admin_api_client, authenticated_api_client, single_cycle, mocker
):
    mocked_create_call_in_sram = mocker.patch(
        "ludwig_api.proposal.serializers.call_serializer.create_call_in_sram",
        autospec=True,
    )
    call_to_create = {
        "code": "01",
        "cycle_id": single_cycle.id,
        "description": "I love cycling",
        "name": "CYC",
        "requires_direct_action": False,
        "start": "2023-07-11T07:06:16.282Z",
        "end": "2025-07-11T07:06:16.282Z",
    }

    response = admin_api_client.post(path=reverse("call-list"), data=call_to_create)
    assert response.status_code == status.HTTP_201_CREATED
    mocked_create_call_in_sram.assert_called_once()

    call = Call.objects.get(code=call_to_create["code"])
    call_to_create["description"] = "I love calling"
    response = authenticated_api_client.patch(
        reverse("call-detail", args=[call.id]), data=call_to_create
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_call_edit__as_committee_chair_failure(
    admin_api_client, authenticated_api_client, get_user_for_call, single_cycle, mocker
):
    mocked_create_call_in_sram = mocker.patch(
        "ludwig_api.proposal.serializers.call_serializer.create_call_in_sram",
        autospec=True,
    )
    call_to_create = {
        "code": "01",
        "cycle_id": single_cycle.id,
        "description": "I love cycling",
        "name": "CYC",
        "requires_direct_action": False,
        "start": "2023-07-11T07:06:16.282Z",
        "end": "2025-07-11T07:06:16.282Z",
    }

    response = admin_api_client.post(path=reverse("call-list"), data=call_to_create)
    assert response.status_code == status.HTTP_201_CREATED
    mocked_create_call_in_sram.assert_called_once()

    call = Call.objects.get(code=call_to_create["code"])
    call_to_create["description"] = "I love calling"
    authenticated_api_client.force_authenticate(
        user=get_user_for_call(call, Roles.COMMITTEE_CHAIR)
    )
    response = authenticated_api_client.patch(
        reverse("call-detail", args=[call.id]), data=call_to_create
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
