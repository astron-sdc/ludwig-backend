import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.status import HTTP_200_OK, HTTP_403_FORBIDDEN

from ludwig_api.proposal.permissions.call import Roles as CallRoles


@pytest.mark.django_db
def test_call_allocated_resources_as_anonymous_user_unauthorized(
    api_client, single_call
):
    response = api_client.get(
        reverse("call-allocated-resources", args=[single_call.pk])
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_call_allocated_resources_as_authenticated_user_forbidden(
    authenticated_api_client, single_call
):
    response = authenticated_api_client.get(
        reverse("call-allocated-resources", args=[single_call.pk])
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_call_allocated_resources_as_admin_success(
    admin_api_client, single_call, mocker
):
    response = admin_api_client.get(
        reverse("call-allocated-resources", args=[single_call.pk])
    )
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
@pytest.mark.parametrize(
    "role, expected_status",
    [
        (CallRoles.TECHNICAL_REVIEWER, HTTP_403_FORBIDDEN),
        (CallRoles.TECHNICAL_REVIEWER_CHAIR, HTTP_403_FORBIDDEN),
        (CallRoles.SCIENCE_REVIEWER, HTTP_403_FORBIDDEN),
        (CallRoles.SCIENCE_REVIEWER_CHAIR, HTTP_200_OK),
        (CallRoles.COMMITTEE_CHAIR, HTTP_200_OK),
    ],
)
def test_call_allocated_resources_as_call_roles_expected_status(
    authenticated_api_client,
    single_call,
    get_user_for_call,
    role,
    expected_status,
):
    authenticated_api_client.force_authenticate(get_user_for_call(single_call, role))
    response = authenticated_api_client.get(
        reverse("call-allocated-resources", args=[single_call.pk])
    )
    assert response.status_code == expected_status
