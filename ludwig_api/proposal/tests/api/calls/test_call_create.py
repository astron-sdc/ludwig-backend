import pytest
from django.urls import reverse
from rest_framework import status


@pytest.mark.django_db
def test_call_create__as_admin(admin_api_client, single_cycle, mocker):
    mocked_create_call_in_sram = mocker.patch(
        "ludwig_api.proposal.serializers.call_serializer.create_call_in_sram",
        autospec=True,
    )

    call_to_create = {
        "code": "01",
        "cycle_id": single_cycle.id,
        "description": "I love cycling",
        "name": "CYC",
        "requires_direct_action": False,
        "start": "2024-07-11T07:06:16.282Z",
        "end": "2024-07-11T07:06:16.282Z",
    }

    response = admin_api_client.post(path=reverse("call-list"), data=call_to_create)

    assert response.status_code == status.HTTP_201_CREATED
    mocked_create_call_in_sram.assert_called_once()


@pytest.mark.django_db
def test_call_create__without_permissions(
    authenticated_api_client, single_cycle, mocker
):
    mocked_create_call_in_sram = mocker.patch(
        "ludwig_api.proposal.serializers.call_serializer.create_call_in_sram",
        autospec=True,
    )

    call_to_create = {
        "code": "01",
        "cycle_id": single_cycle.id,
        "description": "I love cycling",
        "name": "CYC",
        "requires_direct_action": False,
        "start": "2024-07-11T07:06:16.282Z",
        "end": "2024-07-11T07:06:16.282Z",
    }

    response = authenticated_api_client.post(
        path=reverse("call-list"), data=call_to_create
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN
    mocked_create_call_in_sram.assert_not_called()
