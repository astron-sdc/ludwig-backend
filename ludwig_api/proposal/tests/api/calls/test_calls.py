from datetime import datetime, timezone

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.serializers.call_serializer import CallSerializer

past = datetime(2020, 1, 1, tzinfo=timezone.utc)
future = datetime(2120, 1, 1, tzinfo=timezone.utc)


@pytest.fixture
def list_of_calls(call_factory):
    return call_factory.create_batch(20)


@pytest.fixture
def list_of_open_calls(call_factory):
    return call_factory.create_batch(5, start=past, end=future)


@pytest.fixture
def list_of_closed_calls(call_factory):
    return call_factory.create_batch(5, start=past, end=past)


@pytest.mark.django_db
def test_get_calls_returns_http_ok(authenticated_api_client, list_of_calls):
    response = authenticated_api_client.get(reverse("call-list"))
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_get_call_returns_http_ok(authenticated_api_client, single_call):
    response = authenticated_api_client.get(
        reverse("call-detail", args=[single_call.id])
    )
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_get_calls_returns_calls(authenticated_api_client, list_of_calls):
    response = authenticated_api_client.get(reverse("call-list"))
    response_json = response.json()
    calls_json = CallSerializer(list_of_calls, many=True).data
    assert response_json == calls_json


@pytest.mark.django_db
def test_get_open_calls_returns_open_calls(
    authenticated_api_client, list_of_open_calls, list_of_closed_calls
):
    response = authenticated_api_client.get(
        reverse("call-list"), query_params={"open": True}
    )
    response_json = response.json()
    calls_json = CallSerializer(list_of_open_calls, many=True).data
    assert response_json == calls_json


@pytest.mark.django_db
def test_get_closed_calls_returns_closed_calls(
    authenticated_api_client, list_of_open_calls, list_of_closed_calls
):
    response = authenticated_api_client.get(
        reverse("call-list"), query_params={"open": False}
    )
    response_json = response.json()
    calls_json = CallSerializer(list_of_closed_calls, many=True).data
    assert response_json == calls_json


@pytest.mark.django_db
def test_get_calls_returns_all_calls(
    authenticated_api_client, list_of_open_calls, list_of_closed_calls
):
    response = authenticated_api_client.get(reverse("call-list"))
    response_json = response.json()
    calls_json = CallSerializer(
        [*list_of_open_calls, *list_of_closed_calls], many=True
    ).data
    assert response_json == calls_json


@pytest.mark.django_db
def test_get_call_returns_call(authenticated_api_client, single_call):
    response = authenticated_api_client.get(
        reverse("call-detail", args=[single_call.id])
    )
    response_json = response.json()
    call_json = CallSerializer(single_call).data
    assert response_json == call_json
