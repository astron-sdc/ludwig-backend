import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.faai.sram.models import SRAMClient
from ludwig_api.proposal.permissions.call import Roles


@pytest.mark.django_db
def test_call_members__not_found(authenticated_api_client):
    response = authenticated_api_client.get(reverse("call-members", args=[1337]))
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_call_members__unauthorized(api_client, single_call):
    response = api_client.get(reverse("call-members", args=[single_call.pk]))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_call_members__forbidden(authenticated_api_client, single_call):
    response = authenticated_api_client.get(
        reverse("call-members", args=[single_call.pk])
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_call_members__as_call_member(
    api_client, single_call, get_user_for_call, mocker
):
    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    api_client.force_authenticate(get_user_for_call(single_call, Roles.COMMITTEE_CHAIR))
    response = api_client.get(reverse("call-members", args=[single_call.pk]))
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_call_members__as_admin(admin_api_client, single_call, mocker):
    mocked_client = mocker.patch.object(
        SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}
    response = admin_api_client.get(reverse("call-members", args=[single_call.pk]))
    assert response.status_code == status.HTTP_200_OK


# TODO: add tests based on the content
