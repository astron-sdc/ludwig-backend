from unittest.mock import call

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.proposal.models.panel_type import PanelType


def make_review_pre_assignment_request(api_client, payload, call_id):
    url = reverse("call-pre-assign-reviewers", args=[call_id])
    return api_client.put(url, payload, format="json")


@pytest.mark.parametrize(
    "reviewer_ids, panel_types, existing_members, expected_http_status, expected_added_group_ids, expected_removed_group_ids",
    [
        (
            ["1337", "1338", "1339"],
            [PanelType.scientific],
            [],
            status.HTTP_200_OK,
            ["sci_reviewer_id"],
            [],
        ),
        (
            ["1340", "1341"],
            [PanelType.technical],
            [],
            status.HTTP_200_OK,
            ["tech_reviewer_id"],
            [],
        ),
        (
            ["1342"],
            [PanelType.scientific, PanelType.technical],
            [],
            status.HTTP_200_OK,
            ["tech_reviewer_id", "sci_reviewer_id"],
            [],
        ),
        ([], [PanelType.scientific], [], status.HTTP_400_BAD_REQUEST, [], []),
        (["123"], [], [], status.HTTP_200_OK, [], []),
        (["1337"], [], ["1337"], status.HTTP_200_OK, [], ["sci_reviewer_id"]),
    ],
)
@pytest.mark.django_db
def test_assign_check_assignee_added_to_panel(
    single_call,
    admin_api_client,
    call_panel_groups_json,
    mocker,
    reviewer_ids,
    panel_types,
    existing_members,
    expected_http_status,
    expected_added_group_ids,
    expected_removed_group_ids,
):
    mocked_add_member = mocker.patch(
        "ludwig_api.faai.sram.client.SRAMClient.add_member_to_group"
    )

    mocked_delete_member = mocker.patch(
        "ludwig_api.faai.sram.client.SRAMClient.delete_member_from_group"
    )

    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)

    for expected_removed_group_id in expected_removed_group_ids:
        for existing_member in existing_members:
            groups_in_json = [
                grp
                for grp in call_panel_groups_json
                if grp["identifier"] == expected_removed_group_id
            ]
            for group in groups_in_json:
                group["collaboration_memberships"].append(
                    {
                        "user": {
                            "uid": existing_member,
                            "name": "",
                            "given_name": "",
                            "family_name": "",
                            "email": "",
                        }
                    }
                )

    mocked_get.return_value = {
        "identifier": "XYZ",
        "groups": call_panel_groups_json,
        "services": [],
        "short_name": "xyz",
    }

    payload = {"panel_types": panel_types, "reviewer_ids": reviewer_ids}

    response = make_review_pre_assignment_request(
        admin_api_client, payload, single_call.id
    )

    assert response.status_code == expected_http_status

    for expected_added_group_id in expected_added_group_ids:
        expected_calls = [
            call(
                group_identifier=expected_added_group_id,
                eduperson_unique_id=reviewer_id,
            )
            for reviewer_id in reviewer_ids
        ]

        mocked_add_member.assert_has_calls(expected_calls)

    for expected_removed_group_id in expected_removed_group_ids:
        expected_calls = [
            call(
                group_identifier=expected_removed_group_id,
                eduperson_unique_id=reviewer_id,
            )
            for reviewer_id in reviewer_ids
        ]

        mocked_delete_member.assert_has_calls(expected_calls)
