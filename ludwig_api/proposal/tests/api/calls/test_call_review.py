from datetime import UTC, datetime, timedelta

import pytest
from django.urls import reverse
from requests import Response
from rest_framework import status

import ludwig_api
from ludwig_api.proposal.models.call import CallStatus
from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.serializers.proposal_serializer import ProposalSerializer

pytestmark = pytest.mark.django_db


@pytest.fixture
def closed_call(single_call):
    single_call.end = datetime.now(UTC) - timedelta(days=1)
    single_call.save()
    assert single_call.status == CallStatus.closed
    return single_call


def _put_request_with_deadlines(
    deadline: datetime, call_pk: int, authenticated_api_client
) -> Response:
    path = reverse("call-review-submitted-proposals", args=[call_pk])
    response = authenticated_api_client.put(
        path,
        data={
            "technical_review_deadline": deadline.isoformat(),
            "science_review_deadline": deadline.isoformat(),
        },
    )
    return response


def test_review_submitted_proposals__deadlines_invalid(
    authenticated_api_client, single_call
):
    deadline = datetime.now(UTC) - timedelta(days=1)
    response = _put_request_with_deadlines(
        deadline, single_call.pk, authenticated_api_client
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "technical_review_deadline": [
            "Technical review deadline cannot be in the past"
        ],
        "science_review_deadline": ["Science review deadline cannot be in the past"],
    }


def test_review_submitted_proposals__call_not_closed(
    authenticated_api_client, single_call
):
    assert single_call.status == CallStatus.open
    deadline = datetime.now(UTC) + timedelta(days=1)
    response = _put_request_with_deadlines(
        deadline, single_call.pk, authenticated_api_client
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json() == {"error": "Call status is not closed"}


def test_review_submitted_proposals__no_proposals(
    authenticated_api_client, closed_call, proposals_not_submitted
):
    for proposal in proposals_not_submitted:
        proposal.call = closed_call
        proposal.save()
    assert closed_call.proposals.count() == len(proposals_not_submitted)

    deadline = datetime.now(UTC) + timedelta(days=1)
    response = _put_request_with_deadlines(
        deadline, closed_call.pk, authenticated_api_client
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert response.json() == {"error": "Call has no proposals with submitted status"}


def test_review_submitted_proposals__ok(
    authenticated_api_client,
    closed_call,
    proposals_submitted,
    proposals_not_submitted,
    mocker,
):
    mocked_client = mocker.patch.object(
        ludwig_api.faai.sram.models.SRAMClient,
        "get_collaboration",
        autospec=True,
    )
    mocked_client.return_value = {}

    for proposal in proposals_submitted + proposals_not_submitted:
        proposal.call = closed_call
        proposal.save()

    assert closed_call.proposals.count() == len(proposals_submitted) + len(
        proposals_not_submitted
    )

    deadline = datetime.now(UTC) + timedelta(days=1)
    response = _put_request_with_deadlines(
        deadline, closed_call.pk, authenticated_api_client
    )
    assert response.status_code == status.HTTP_200_OK

    for proposal in proposals_submitted:
        proposal.refresh_from_db()
        assert proposal.status == ProposalStatus.under_review

    for proposal in proposals_not_submitted:
        proposal.refresh_from_db()
        assert proposal.status == ProposalStatus.draft

    assert response.json() == {
        "proposals": ProposalSerializer(proposals_submitted, many=True).data
    }
