import pytest
from django.urls import reverse
from pytest_factoryboy import register
from rest_framework import status

from ludwig_api.proposal.permissions.call import Roles
from ludwig_api.proposal.tests.factories.proposal_factory import CallFactory

register(CallFactory)


@pytest.mark.django_db
def test_call_access_allowed_not_found(authenticated_api_client):
    response = authenticated_api_client.get(reverse("call-access-allowed", args=[1337]))
    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_call_access_allowed__without_access(authenticated_api_client):
    call = CallFactory()
    response = authenticated_api_client.get(
        reverse("call-access-allowed", args=[call.id])
    )
    assert response.status_code == status.HTTP_200_OK
    # TODO: check response content


@pytest.mark.django_db
def test_call_access_allowed__with_access(api_client, get_user_for_call):
    call = CallFactory()
    api_client.force_authenticate(get_user_for_call(call, Roles.COMMITTEE_CHAIR))
    response = api_client.get(reverse("call-access-allowed", args=[call.id]))
    assert response.status_code == status.HTTP_200_OK
    # TODO: check response content
