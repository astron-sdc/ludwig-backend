import pytest
from django.urls import reverse
from rest_framework import status


@pytest.mark.django_db
def test_proposal_reranking__as_admin(
    admin_api_client,
    mocker,
    call_factory,
    proposal_factory,
):
    mocker.patch("ludwig_api.faai.sram.models.SRAMClient", autospec=True)

    call = call_factory.create()
    proposals = proposal_factory.create_batch(10, call=call)

    data = [
        {"id": proposal.id, "order": index} for index, proposal in enumerate(proposals)
    ]

    response = admin_api_client.patch(
        reverse("call-proposals-list", args=[call.pk]), format="json", data=data
    )

    assert response.status_code == status.HTTP_200_OK
    [proposal.refresh_from_db() for proposal in proposals]
    assert set(range(10)) == {proposal.order for proposal in proposals}
