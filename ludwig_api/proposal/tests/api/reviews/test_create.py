from unittest import mock

import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.status import HTTP_204_NO_CONTENT

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.models import Review, ReviewComment
from ludwig_api.proposal.models.review import PanelType, ReviewerType
from ludwig_api.proposal.models.review_comment import ReviewCommentType
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.tests.api.reviews.conftest import assign_user_as_reviewer
from ludwig_api.proposal.tests.conftest import (
    merge_user_entitlements,
    user_id_to_collaboration_group,
)


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_create_review_success(
    _,
    authenticated_api_client,
    single_proposal_under_review,
    user,
    mocker,
    get_user_for_call,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)

    review_to_create = {
        "ranking": 3,
    }

    authenticated_api_client.force_authenticate(
        get_user_for_call(
            single_proposal_under_review.call, CallRoles.SCIENCE_REVIEWER_CHAIR
        )
    )
    response = authenticated_api_client.post(
        path=reverse("proposal-reviews-list", args=[single_proposal_under_review.id]),
        data=review_to_create,
    )
    assert response.status_code == status.HTTP_201_CREATED

    response_json = response.json()
    created_review = Review.objects.get(id=response_json["id"])

    assert created_review.proposal_id == single_proposal_under_review.id
    assert created_review.reviewer_id == user.id
    assert created_review.ranking == 3


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_create_review_upsert(
    _,
    authenticated_api_client,
    single_proposal_under_review,
    user,
    mocker,
    get_user_for_call,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)

    review_to_create = {
        "ranking": 3,
    }

    authenticated_api_client.force_authenticate(
        get_user_for_call(
            single_proposal_under_review.call, CallRoles.SCIENCE_REVIEWER_CHAIR
        )
    )
    response = authenticated_api_client.post(
        path=reverse("proposal-reviews-list", args=[single_proposal_under_review.id]),
        data=review_to_create,
    )

    assert response.status_code == status.HTTP_201_CREATED

    response_json = response.json()
    created_review = Review.objects.get(id=response_json["id"])

    review_to_update = {
        "ranking": 4,
    }

    response = authenticated_api_client.post(
        path=reverse("proposal-reviews-list", args=[single_proposal_under_review.id]),
        data=review_to_update,
    )

    updated_review_id = response.json()["id"]
    created_review.refresh_from_db()

    assert created_review.id == updated_review_id
    assert created_review.ranking == 4


@pytest.mark.django_db
def test_create_review_failure_wrong_status(
    authenticated_api_client, single_proposal_draft, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)

    review_to_create = {
        "ranking": 3,
    }

    response = authenticated_api_client.post(
        path=reverse("proposal-reviews-list", args=[single_proposal_draft.id]),
        data=review_to_create,
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_create_review__while_pi_failure(
    authenticated_api_client,
    single_proposal_under_review,
    get_user_for_proposal,
    mocker,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    pi_user = get_user_for_proposal(single_proposal_under_review, ProposalRoles.PI)
    review_to_create = {
        "ranking": 3,
        "reviewer_id": pi_user.id,
    }
    authenticated_api_client.force_authenticate(pi_user)
    response = authenticated_api_client.post(
        path=reverse("proposal-reviews-list", args=[single_proposal_under_review.id]),
        data=review_to_create,
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert len(Review.objects.filter(reviewer_id=pi_user.id)) == 0


@pytest.mark.django_db
def test_create_review_comment__for_review_success(
    authenticated_api_client,
    single_proposal_under_review,
    single_review,
    get_user_for_call,
    mocker,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    comment_to_create = {
        "text": "I like the scientific merits of this proposal",
        "type": str(ReviewCommentType.SCIENTIFIC_JUSTIFICATION),
    }
    user: TokenUser = get_user_for_call(
        single_proposal_under_review.call, CallRoles.SCIENCE_REVIEWER
    )
    assert (
        assign_user_as_reviewer(
            user.id, single_proposal_under_review, PanelType.scientific
        ).status_code
        == HTTP_204_NO_CONTENT
    )
    authenticated_api_client.force_authenticate(user)
    response = authenticated_api_client.post(
        path=reverse(
            "review-comments-list",
            args=[single_proposal_under_review.id, single_review.id],
        ),
        data=comment_to_create,
    )
    assert response.status_code == status.HTTP_201_CREATED

    response_json = response.json()
    created_review_comment = ReviewComment.objects.get(id=response_json["id"])

    assert created_review_comment.review_id == single_review.id


@pytest.mark.django_db
def test_create_review_comment_when_multiple_reviews_exist_success(
    admin_api_client,
    single_proposal_under_review,
    single_review,
    user,
    mocker,
    get_user_for_call,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    # Create two reviews for the proposal and user
    reviews = [
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": PanelType.technical,
            "reviewer_id": user.id,
        },
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": PanelType.scientific,
            "reviewer_id": user.id,
        },
    ]
    payload = {"reviews": reviews, "reviewer_type": "primary"}
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=user.id
            )
        )
        == 0
    )

    user = get_user_for_call(
        single_proposal_under_review.call, CallRoles.SCIENCE_REVIEWER
    )
    assert (
        assign_user_as_reviewer(
            user.id, single_proposal_under_review, PanelType.scientific
        ).status_code
        == HTTP_204_NO_CONTENT
    )

    response = admin_api_client.put(
        reverse("call-assign-reviewers", args=[single_proposal_under_review.call.id]),
        payload,
        format="json",
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=user.id
            )
        )
        == 2
    )

    # Now that there are two reviews for this proposal and user, leave a comment.
    comment_to_create = {
        "text": "I like the scientific merits of this proposal",
        "type": str(ReviewCommentType.SCIENTIFIC_JUSTIFICATION),
    }

    response = admin_api_client.post(
        path=reverse(
            "review-comments-list",
            args=[single_proposal_under_review.id, single_review.id],
        ),
        data=comment_to_create,
    )
    assert response.status_code == status.HTTP_201_CREATED

    response_json = response.json()
    created_review_comment = ReviewComment.objects.get(id=response_json["id"])

    assert created_review_comment.review_id == single_review.id


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_create_review_comment_for_proposal_success(
    _, authenticated_api_client, single_proposal_under_review, mocker, get_user_for_call
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    comment_to_create = {
        "text": "Proposal schmoposal",
        "type": str(ReviewCommentType.SCIENTIFIC_JUSTIFICATION),
    }

    user = get_user_for_call(
        single_proposal_under_review.call, CallRoles.SCIENCE_REVIEWER
    )
    assert (
        assign_user_as_reviewer(
            user.id, single_proposal_under_review, PanelType.scientific
        ).status_code
        == HTTP_204_NO_CONTENT
    )
    authenticated_api_client.force_authenticate(user)
    response = authenticated_api_client.post(
        path=reverse(
            "proposal-comments-list",
            args=[single_proposal_under_review.id],
        ),
        data=comment_to_create,
    )

    assert response.status_code == status.HTTP_201_CREATED

    review_id = response.json()["review_id"]

    response = authenticated_api_client.post(
        path=reverse(
            "proposal-comments-list",
            args=[single_proposal_under_review.id],
        ),
        data=comment_to_create,
    )

    assert response.status_code == status.HTTP_201_CREATED
    assert response.json()["review_id"] == review_id


@pytest.mark.django_db
def test_create_review_comment_for_review_failure_wrong_status(
    authenticated_api_client, single_proposal_draft, single_review, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    comment_to_create = {
        "text": "I do not like the scientific merits of this proposal",
        "type": str(ReviewCommentType.SCIENTIFIC_JUSTIFICATION),
    }

    response = authenticated_api_client.post(
        path=reverse(
            "review-comments-list", args=[single_proposal_draft.id, single_review.id]
        ),
        data=comment_to_create,
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN


# Rank proposal, assign user as reviewer and make sure the created reviews are re-used correctly
@pytest.mark.django_db
def test_rank_proposal_create_review_for_proposal_check_review_reused_success(
    admin_api_client, single_proposal_under_review, user, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    review_to_create = {
        "ranking": 3,
    }
    response = admin_api_client.post(
        path=reverse("proposal-reviews-list", args=[single_proposal_under_review.id]),
        data=review_to_create,
    )
    assert response.status_code == status.HTTP_201_CREATED

    response_json = response.json()
    created_review = Review.objects.get(id=response_json["id"])

    assert created_review.proposal_id == single_proposal_under_review.id
    assert created_review.reviewer_id == user.id
    assert created_review.ranking == 3
    assert created_review.reviewer_type == ""
    assert created_review.panel_type is None
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=user.id
            )
        )
        == 1
    )

    # Assign the user as reviewer and check the review created by the ranking is re-used
    reviews = [
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": PanelType.technical,
            "reviewer_id": user.id,
        },
    ]
    response = admin_api_client.put(
        reverse("call-assign-reviewers", args=[single_proposal_under_review.call.id]),
        {"reviews": reviews, "reviewer_type": ReviewerType.primary},
        format="json",
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=user.id
            )
        )
        == 1
    )

    review = Review.objects.get(id=response_json["id"])
    assert review.proposal_id == single_proposal_under_review.id
    assert review.reviewer_id == user.id
    assert review.ranking == 3
    assert review.reviewer_type == ReviewerType.primary
    assert review.panel_type == PanelType.technical


# Assign reviewers to proposal, rank the proposal, make sure the ranking was applied to the correct review
@pytest.mark.django_db
def test_assign_two_reviews_rank_proposal_success(
    admin_api_client, single_proposal_under_review, user, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    # Assign the user as both types of reviewer
    reviews = [
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": PanelType.technical,
            "reviewer_id": user.id,
        },
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": PanelType.scientific,
            "reviewer_id": user.id,
        },
    ]
    response = admin_api_client.put(
        reverse("call-assign-reviewers", args=[single_proposal_under_review.call.id]),
        {"reviews": reviews, "reviewer_type": ReviewerType.primary},
        format="json",
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=user.id
            )
        )
        == 2
    )

    # Rank proposal and make sure the right proposal (scientific) is (re-)used
    review_to_create = {
        "ranking": 3,
    }
    response = admin_api_client.post(
        path=reverse("proposal-reviews-list", args=[single_proposal_under_review.id]),
        data=review_to_create,
    )
    assert response.status_code == status.HTTP_201_CREATED

    response_json = response.json()
    created_review = Review.objects.get(id=response_json["id"])

    assert created_review.proposal_id == single_proposal_under_review.id
    assert created_review.reviewer_id == user.id
    assert created_review.ranking == 3
    assert created_review.reviewer_type == ReviewerType.primary
    assert created_review.panel_type == PanelType.scientific
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=user.id
            )
        )
        == 2
    )
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id,
                reviewer_id=user.id,
                panel_type=PanelType.scientific,
            )
        )
        == 1
    )


@pytest.mark.django_db
def test_create_comment_unauthorized_failure(
    api_client, single_proposal_under_review, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    response = api_client.post(
        path=reverse("proposal-comments-list", args=[single_proposal_under_review.id]),
        data={
            "text": "I like the scientific merits of this proposal",
        },
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_create_comment__while_unassigned_failure(
    _, authenticated_api_client, single_proposal_under_review, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    response = authenticated_api_client.post(
        path=reverse("proposal-comments-list", args=[single_proposal_under_review.id]),
        data={
            "text": "I like the scientific merits of this proposal",
        },
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_create_comment_as_admin_unassigned_success(
    _, admin_api_client, single_proposal_under_review, admin_user, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    response = admin_api_client.post(
        path=reverse("proposal-comments-list", args=[single_proposal_under_review.id]),
        data={
            "text": "I like the scientific merits of this proposal",
        },
    )
    assert response.status_code == status.HTTP_201_CREATED
    response_json = response.json()
    created_review = Review.objects.get(id=response_json["review_id"])
    assert created_review.panel_type == PanelType.scientific
    assert created_review.reviewer_id == admin_user.id


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_create_review_comment_while_pi_failure(
    _,
    authenticated_api_client,
    single_proposal_under_review,
    single_review,
    mocker,
    get_user_for_call,
    get_user_for_proposal,
):
    sci_reviewer = get_user_for_call(
        single_proposal_under_review.call, CallRoles.SCIENCE_REVIEWER
    )
    pi_user = get_user_for_proposal(single_proposal_under_review, ProposalRoles.PI)
    pi_and_sci_user = merge_user_entitlements([pi_user, sci_reviewer])
    authenticated_api_client.force_authenticate(pi_and_sci_user)

    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration")
    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": [user_id_to_collaboration_group("pi", pi_and_sci_user.id)],
        "services": [],
        "short_name": "abc",
    }

    comment_to_create = {
        "text": "I think the PI is very smart",
        "type": str(ReviewCommentType.SCIENTIFIC_JUSTIFICATION),
    }

    response = authenticated_api_client.post(
        path=reverse(
            "review-comments-list",
            args=[single_proposal_under_review.id, single_review.id],
        ),
        data=comment_to_create,
    )
    assert response.status_code == status.HTTP_403_FORBIDDEN
    assert Review.objects.filter(reviewer_id=pi_and_sci_user.id).count() == 0

    assert (
        authenticated_api_client.get(
            reverse("proposal-access-allowed", args=[single_proposal_under_review.id])
        ).json()["write_science_review"]
        is False
    )
