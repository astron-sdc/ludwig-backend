import pytest
from rest_framework import status

from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.proposal.models import Review
from ludwig_api.proposal.models.review import PanelType, ReviewerType
from ludwig_api.proposal.permissions.call import Roles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.tests.api.reviews.conftest import (
    make_review_assignment_request,
)


@pytest.mark.django_db
def test_assign_multiple_reviews_with_reviewer_id_success(
    admin_api_client, single_proposal_under_review, user, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)

    assignee_id = "foo"
    reviews = [
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": PanelType.technical,
            "reviewer_id": assignee_id,
        },
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": PanelType.scientific,
            "reviewer_id": assignee_id,
        },
    ]

    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=assignee_id
            )
        )
        == 0
    )

    response = make_review_assignment_request(
        admin_api_client,
        {"reviews": reviews, "reviewer_type": ReviewerType.secondary},
        single_proposal_under_review.call.id,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id, reviewer_id=assignee_id
            )
        )
        == 2
    )

    created_review = Review.objects.filter(
        proposal_id=single_proposal_under_review.id, reviewer_id=assignee_id
    ).first()
    assert created_review.reviewer_id == assignee_id
    assert created_review.panel_type == PanelType.scientific
    assert created_review.ranking == 0
    assert created_review.proposal_id == single_proposal_under_review.id


@pytest.mark.django_db
def test_assign_multiple_reviews_with_reviewer_id_in_open_direct_action_call_success(
    admin_api_client,
    single_proposal_under_review_in_open_direct_action_call,
    user,
    mocker,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    assignee_id = "bar"
    reviews = [
        {
            "proposal_id": single_proposal_under_review_in_open_direct_action_call.id,
            "panel_type": PanelType.technical,
            "reviewer_id": assignee_id,
        },
        {
            "proposal_id": single_proposal_under_review_in_open_direct_action_call.id,
            "panel_type": PanelType.scientific,
            "reviewer_id": assignee_id,
        },
    ]

    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review_in_open_direct_action_call.id,
                reviewer_id=assignee_id,
            )
        )
        == 0
    )

    response = make_review_assignment_request(
        admin_api_client,
        {"reviews": reviews, "reviewer_type": ReviewerType.secondary},
        single_proposal_under_review_in_open_direct_action_call.call.id,
    )

    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.django_db
def test_assign_review_without_panel_type_failure(
    admin_api_client, single_proposal_under_review, user, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    review = [
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": "Invalid",
        }
    ]
    response = make_review_assignment_request(
        admin_api_client,
        {"reviews": review, "reviewer_type": "secondary"},
        single_proposal_under_review.call.id,
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_assign_review_without_admin_failure(
    authenticated_api_client, single_proposal_under_review, user
):
    response = make_review_assignment_request(
        authenticated_api_client,
        ["Does not matter, since permission check precedes data validation."],
        single_proposal_under_review.call.id,
    )
    assert response.json()["detail"] == "user is not admin or program committee chair"
    assert response.status_code == status.HTTP_403_FORBIDDEN


@pytest.mark.django_db
def test_assign_with_reviewer_id_and_reviewer_type_success(
    admin_api_client, single_proposal_under_review, user, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    assignee_id = "foo"
    reviewer_type = ReviewerType.primary
    panel_type = PanelType.scientific
    reviews = [
        {
            "proposal_id": single_proposal_under_review.id,
            "panel_type": panel_type,
            "reviewer_id": assignee_id,
        },
    ]

    assert (
        len(
            Review.objects.filter(
                proposal_id=single_proposal_under_review.id,
                reviewer_id=assignee_id,
                reviewer_type=reviewer_type,
                panel_type=panel_type,
                ranking=0,
            )
        )
        == 0
    )

    response = make_review_assignment_request(
        admin_api_client,
        {"reviews": reviews, "reviewer_type": reviewer_type},
        single_proposal_under_review.call.id,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT

    matching_reviews = Review.objects.filter(
        proposal_id=single_proposal_under_review.id,
        reviewer_id=assignee_id,
        reviewer_type=reviewer_type,
        panel_type=panel_type,
    )
    assert len(matching_reviews) == 1

    assert matching_reviews.first().reviewer_id == assignee_id
    assert matching_reviews.first().panel_type == panel_type
    assert matching_reviews.first().reviewer_type == reviewer_type
    assert matching_reviews.first().ranking == 0
    assert matching_reviews.first().proposal_id == single_proposal_under_review.id


@pytest.mark.django_db
def test_assign_reviewer_as_committee_chair_success(
    get_user_for_call, single_proposal_under_review, authenticated_api_client, mocker
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    science_reviewer_chair = get_user_for_call(
        single_proposal_under_review.call, Roles.SCIENCE_REVIEWER_CHAIR
    )
    review_payload = {
        "reviews": [
            {
                "proposal_id": single_proposal_under_review.id,
                "panel_type": PanelType.scientific,
                "reviewer_id": science_reviewer_chair.id,
            }
        ],
        "reviewer_type": ReviewerType.primary,
    }
    authenticated_api_client.force_authenticate(science_reviewer_chair)
    response = make_review_assignment_request(
        authenticated_api_client, review_payload, single_proposal_under_review.call.id
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.django_db
def test_assign_check_assignee_added_to_panel_success(
    get_user_for_call,
    single_proposal_under_review,
    admin_api_client,
    call_panel_groups_json,
    mocker,
):
    mocked_get = mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)

    mocked_get.return_value = {
        "identifier": "ABC",
        "groups": call_panel_groups_json,
        "services": [],
        "short_name": "abc",
    }
    single_proposal_under_review.call.collaboration_identifier = "ABC"
    single_proposal_under_review.call.save()
    review_payload = {
        "reviews": [
            {
                "proposal_id": single_proposal_under_review.id,
                "panel_type": PanelType.scientific,
                "reviewer_id": "assignee_user_id",
            }
        ],
        "reviewer_type": ReviewerType.primary,
    }
    response = make_review_assignment_request(
        admin_api_client, review_payload, single_proposal_under_review.call.id
    )

    group_identifier = [
        g["identifier"]
        for g in single_proposal_under_review.call.groups
        if g["short_name"] == Roles.SCIENCE_REVIEWER.value
    ][0]
    assert group_identifier == "sci_reviewer_id"
    assert response.status_code == status.HTTP_204_NO_CONTENT


@pytest.mark.django_db
def test_assign__pi_as_reviewer_failure(
    get_user_for_call,
    get_user_for_proposal,
    single_proposal_under_review,
    admin_api_client,
    mocker,
):
    mocker.patch.object(SRAMClient, "get_collaboration", autospec=True)
    pi_user = get_user_for_proposal(single_proposal_under_review, ProposalRoles.PI)
    single_proposal_under_review.created_by = pi_user.id
    single_proposal_under_review.save()
    assert single_proposal_under_review.created_by == pi_user.id

    review_payload = {
        "reviews": [
            {
                "proposal_id": single_proposal_under_review.id,
                "panel_type": PanelType.scientific,
                "reviewer_id": pi_user.id,
            }
        ],
        "reviewer_type": ReviewerType.primary,
    }
    response = make_review_assignment_request(
        admin_api_client, review_payload, single_proposal_under_review.call.id
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert (
        response.json()["error"]
        == "proposal author cannot be assigned as a reviewer for their own proposal"
    )
