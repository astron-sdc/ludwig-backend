from unittest import mock

import pytest
from django.urls import reverse
from rest_framework import status

from ludwig_api.proposal.models.panel_type import PanelType
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.proposal import Roles as ProposalRoles
from ludwig_api.proposal.serializers.review_serializer import (
    ReviewCommentSerializer,
    ReviewSerializer,
)
from ludwig_api.proposal.tests.conftest import merge_user_entitlements


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_review_list(
    _,
    authenticated_api_client,
    single_proposal,
    review_list,
    review_factory,
    proposal_factory,
):
    other_proposal = proposal_factory()
    review_factory.create_batch(size=3, proposal=other_proposal)

    response = authenticated_api_client.get(
        reverse("proposal-reviews-list", args=[single_proposal.id])
    )
    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    review_json = ReviewSerializer(review_list, many=True).data

    assert response_json == review_json


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_current_user_review_ok(
    _, authenticated_api_client, single_proposal, review_factory, user
):
    review = review_factory(proposal=single_proposal, reviewer_id=user.id)

    response = authenticated_api_client.get(
        reverse("proposal-reviews-current-user-review", args=[single_proposal.id])
    )

    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    review_json = ReviewSerializer(review).data

    assert response_json == review_json


@pytest.mark.django_db
def test_current_user_review_not_found(authenticated_api_client, single_proposal):
    response = authenticated_api_client.get(
        reverse("proposal-reviews-current-user-review", args=[single_proposal.id])
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_review_detail(_, authenticated_api_client, single_proposal, single_review):
    response = authenticated_api_client.get(
        reverse("proposal-reviews-detail", args=[single_proposal.id, single_review.id])
    )

    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    review_json = ReviewSerializer(single_review).data

    assert response_json == review_json


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_review_comment_list(
    _,
    authenticated_api_client,
    single_proposal,
    single_review,
    review_comment_list,
    user,
):
    # Assign the user as a technical reviewer
    review_comment_list[0].review.reviewer_id = user.id
    review_comment_list[0].review.panel_type = PanelType.technical
    review_comment_list[0].review.save()
    review_comment_list[0].save()

    response = authenticated_api_client.get(
        reverse("review-comments-list", args=[single_proposal.id, single_review.id])
    )

    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    review_comment_json = ReviewCommentSerializer(review_comment_list, many=True).data

    assert response_json == review_comment_json


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_proposal_comment_list(
    _,
    authenticated_api_client,
    single_proposal,
    proposal_comment_list,
    get_user_for_call,
):
    for comment in proposal_comment_list:
        comment.review.panel_type = PanelType.scientific
        comment.review.save()

    authenticated_api_client.force_authenticate(
        get_user_for_call(single_proposal.call, CallRoles.SCIENCE_REVIEWER)
    )

    response = authenticated_api_client.get(
        reverse("proposal-comments-list", args=[single_proposal.id])
    )

    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    review_comment_json = ReviewCommentSerializer(proposal_comment_list, many=True).data

    assert response_json == review_comment_json


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_proposal_comment_list_as_pi_returns_no_comments(
    _,
    authenticated_api_client,
    single_proposal,
    proposal_comment_list,
    get_user_for_call,
    get_user_for_proposal,
):
    for comment in proposal_comment_list:
        comment.review.panel_type = PanelType.scientific
        comment.review.save()

    sci_reviewer = get_user_for_call(single_proposal.call, CallRoles.SCIENCE_REVIEWER)
    pi_user = get_user_for_proposal(single_proposal, ProposalRoles.PI)
    pi_and_sci_user = merge_user_entitlements([pi_user, sci_reviewer])
    authenticated_api_client.force_authenticate(pi_and_sci_user)

    response = authenticated_api_client.get(
        reverse("proposal-comments-list", args=[single_proposal.id])
    )

    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    assert response_json == []


@pytest.mark.django_db
@mock.patch(
    "ludwig_api.proposal.serializers.review_serializer.get_user_name",
    return_value="admin",
)
def test_review_comment_detail(
    _,
    authenticated_api_client,
    single_proposal,
    proposal_comment_list,
    get_user_for_call,
):
    tech_reviewer = get_user_for_call(
        single_proposal.call, CallRoles.TECHNICAL_REVIEWER
    )
    single_comment = proposal_comment_list[0]
    single_comment.review.panel_type = PanelType.technical
    single_comment.review.reviewer_id = tech_reviewer.id
    single_comment.review.save()

    authenticated_api_client.force_authenticate(tech_reviewer)
    response = authenticated_api_client.get(
        reverse(
            "proposal-comments-detail", args=[single_proposal.id, single_comment.id]
        )
    )

    assert response.status_code == status.HTTP_200_OK

    response_json = response.json()
    review_comment_json = ReviewCommentSerializer(single_comment).data

    assert response_json == review_comment_json
