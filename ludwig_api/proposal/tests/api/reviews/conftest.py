from datetime import datetime, timedelta

import pytest
from django.urls import reverse
from rest_framework.test import APIClient

from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.models.reviewer_type import ReviewerType
from ludwig_api.proposal.permissions.application import Prefix
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.tests.conftest import get_user


@pytest.fixture
def review_list(review_factory, single_proposal):
    return review_factory.create_batch(size=5, proposal=single_proposal)


@pytest.fixture
def single_proposal(proposal_factory):
    return proposal_factory()


@pytest.fixture
def single_proposal_draft(proposal_factory):
    return proposal_factory(status=ProposalStatus.draft)


@pytest.fixture
def single_proposal_under_review(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.under_review,
        call__end=datetime.now() - timedelta(days=1),  # so that call status is "closed"
    )


@pytest.fixture
def single_proposal_under_review_in_open_direct_action_call(proposal_factory):
    return proposal_factory(
        status=ProposalStatus.under_review,
        call__requires_direct_action=True,
        call__end=datetime.now() + timedelta(days=1),  # so that call status is "open"
    )


@pytest.fixture
def single_review(review_factory, single_proposal):
    return review_factory(proposal=single_proposal)


@pytest.fixture
def review_comment_list(review_comment_factory, single_review):
    return review_comment_factory.create_batch(size=4, review=single_review)


@pytest.fixture
def proposal_comment_list(review_factory, review_comment_factory, single_proposal):
    review = review_factory(proposal=single_proposal)
    review_comments = review_comment_factory.create_batch(size=4, review=review)

    other_review = review_factory(proposal=single_proposal)
    other_review_comments = review_comment_factory.create_batch(
        size=5, review=other_review
    )

    proposal_comments = review_comments + other_review_comments

    return proposal_comments


def assign_user_as_reviewer(
    user_id, proposal, panel_type, reviewer_type=ReviewerType.primary
):
    review = {
        "proposal_id": proposal.id,
        "panel_type": panel_type,
        "reviewer_id": user_id,
    }
    api_client = APIClient()
    api_client.force_authenticate(
        get_user(
            f"{Prefix.APP.value}{Prefix.CALL.value}{proposal.call.pk}",
            CallRoles.SCIENCE_REVIEWER_CHAIR.value,
        )
    )

    return make_review_assignment_request(
        api_client,
        {"reviews": [review], "reviewer_type": reviewer_type},
        proposal.call.id,
    )


def make_review_assignment_request(api_client, payload, call_id):
    return api_client.put(
        reverse("call-assign-reviewers", args=[call_id]), payload, format="json"
    )
