import pytest
from django.urls import reverse
from rest_framework import status


@pytest.mark.django_db
def test_export_record_view(authenticated_api_client):

    url = reverse("exportrecords-list")
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_proposal_export_record_view(authenticated_api_client):
    url = reverse("proposal-exportrecords-list", args=[2])
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_proposal_export_record_detail_view(authenticated_api_client):
    url = reverse("exportrecords-detail", args=[2])
    response = authenticated_api_client.get(path=url)
    assert response.status_code == status.HTTP_404_NOT_FOUND
