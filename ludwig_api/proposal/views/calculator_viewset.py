from lofar_calculator import backend, targetvis
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from ludwig_api.proposal.models import CalculatorResult, Proposal, Target
from ludwig_api.proposal.permissions.proposal import (
    ReadCalculationFromProposal,
    WriteCalculationFromProposal,
)
from ludwig_api.proposal.permissions.util import method_permission_classes
from ludwig_api.proposal.serializers.calculator_inputs_serializer import (
    CalculatorInputSerializer,
)
from ludwig_api.proposal.serializers.calculator_results_serializer import (
    CalculatorOutputSerializer,
)


class CalculatorViewSet(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """List and get CalculatorResults"""

    permission_classes = [ReadCalculationFromProposal]

    serializer_class = CalculatorOutputSerializer

    @method_permission_classes([ReadCalculationFromProposal])
    def get_queryset(self):
        proposal_pk = self.kwargs.get("proposal_pk")
        return CalculatorResult.objects.filter(proposal__id=proposal_pk)

    @action(
        detail=False,
        methods=["post"],
        permission_classes=[WriteCalculationFromProposal],
    )
    def calculate(self, request, proposal_pk):
        proposal = Proposal.objects.get(pk=proposal_pk)
        self.check_object_permissions(request, proposal)

        target = Target.objects.get(
            proposal__id=proposal.id, id=request.data.get("target")
        )

        # TODO maybe use backend.validate_inputs() to get below the horizon checking etc.
        data_in = CalculatorInputSerializer(
            data={**request.data, "proposal": proposal_pk, "target": target.id}
        )
        data_in.is_valid(raise_exception=True)
        data_in.save()

        n_baselines = backend.compute_baselines(
            n_core=data_in.data["no_core_stations"],
            n_remote=data_in.data["no_remote_stations"],
            n_int=data_in.data["no_international_stations"],
            hba_mode=data_in.data["antenna_set"],
        )

        try:
            elevations = targetvis.find_target_max_mean_elevation(
                src_name_list=[target.name],
                coord=target.coordinates_formatted,
                obs_date=data_in.data["observation_date"],
                obs_t=float(data_in.data["observation_time_seconds"]),
                n_int=int(data_in.data["integration_time_seconds"]),
            )
        except ValueError:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={"error": "Calculator error: Is the target below the horizon?"},
            )

        im_noise_theoretical = backend.calculate_im_noise(
            n_core=int(data_in.data["no_core_stations"]),
            n_remote=int(data_in.data["no_remote_stations"]),
            n_int=int(data_in.data["no_international_stations"]),
            hba_mode=data_in.data["antenna_set"],
            obs_t=float(data_in.data["observation_time_seconds"]),
            n_sb=int(data_in.data["no_subbands"]),
        )
        geo_corr_im_noise_theoretical = targetvis.geo_correct_noise(
            im_noise_theoretical, elevations[0]
        )

        im_noise_eff, im_noise_eff_err = targetvis.calculate_effective_noise(
            im_noise_theoretical, data_in.data["antenna_set"], elevations
        )

        raw_size_bytes = backend.calculate_raw_size_bytes(
            obs_t=data_in.data["observation_time_seconds"],
            int_time=data_in.data["integration_time_seconds"],
            n_baselines=n_baselines,
            n_chan=data_in.data["no_channels_per_subband"],
            n_sb=data_in.data["no_subbands"],
        )

        proc_size_bytes = int(
            backend.calculate_proc_size_bytes(
                obs_t=data_in.data["observation_time_seconds"],
                int_time=data_in.data["integration_time_seconds"],
                n_baselines=n_baselines,
                n_chan=data_in.data["no_channels_per_subband"],
                n_sb=data_in.data["no_subbands"],
                pipe_type=data_in.data["pipeline"],
                t_avg=data_in.data["time_averaging_factor"],
                f_avg=data_in.data["frequency_averaging_factor"],
                dy_compress=(
                    "enable" if data_in.data["enable_dysco_compression"] else "none"
                ),
            )
        )

        pipe_time_seconds = int(
            backend.calculate_pipe_time_seconds(
                obs_t=float(data_in.data["observation_time_seconds"]),
                n_sb=data_in.data["no_subbands"],
                array_mode=data_in.data["antenna_set"],
                ateam_names=data_in.data["a_team_sources"],
                pipe_type=data_in.data["pipeline"],
            )
        )

        res = {
            "target": target.id,
            "proposal": proposal.id,
            "mean_elevation": elevations,
            "theoretical_rms_jansky_beam": geo_corr_im_noise_theoretical,
            "effective_rms_jansky_beam": im_noise_eff,
            "effective_rms_jansky_beam_err": im_noise_eff_err,
            "raw_data_size_bytes": raw_size_bytes,
            "processed_data_size_bytes": proc_size_bytes,
            "pipeline_processing_time_seconds": pipe_time_seconds,
        }
        data_out = CalculatorOutputSerializer(data=res)
        data_out.is_valid(raise_exception=True)
        data_out.save()
        return Response(data_out.data, status=status.HTTP_201_CREATED)
