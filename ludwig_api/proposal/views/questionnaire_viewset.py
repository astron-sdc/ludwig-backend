from django.db import transaction
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ModelViewSet

from ludwig_api.proposal.models import Proposal, Question, Questionnaire
from ludwig_api.proposal.serializers.answer_serializer import AnswerSerializer
from ludwig_api.proposal.serializers.questionnaire_serializer import (
    QuestionnaireSerializer,
    QuestionSerializer,
)
from ludwig_api.proposal.serializers.questionnaire_with_answers_serializer import (
    QuestionnaireWithAnswerSerializer,
)


class ProposalQuestionnaireAnswersViewSet(GenericViewSet):
    @transaction.atomic
    @action(detail=False, methods=["POST"])
    def answers(self, request, proposal_pk):
        proposal = get_object_or_404(Proposal, pk=proposal_pk)

        if proposal.current_questionnaire is None:
            return Response(status=400)

        serializer = AnswerSerializer(data=request.data, many=True)

        serializer.is_valid(raise_exception=True)

        serializer.save(proposal=proposal)

        return Response(status=status.HTTP_204_NO_CONTENT)


class ProposalQuestionnaireViewSet(GenericViewSet):
    @action(detail=False, methods=["GET"])
    def current(self, _request, proposal_pk):
        proposal = get_object_or_404(Proposal.objects.all(), pk=proposal_pk)

        if proposal.current_questionnaire is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = QuestionnaireWithAnswerSerializer(
            proposal.current_questionnaire, context={"proposal_id": proposal.pk}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)


class QuestionnaireViewSet(ModelViewSet):
    serializer_class = QuestionnaireSerializer
    queryset = Questionnaire.objects.all()


class QuestionViewSet(ModelViewSet):
    serializer_class = QuestionSerializer

    def get_queryset(self):
        if questionnaire := self.kwargs.get("questionnaire_pk"):
            return Question.objects.filter(questionnaire=questionnaire).order_by(
                "order"
            )
        return Question.objects.all().order_by("order")
