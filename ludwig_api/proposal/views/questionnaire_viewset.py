from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Question, Questionnaire
from ludwig_api.proposal.serializers.questionnaire_serializer import (
    QuestionnaireSerializer,
    QuestionSerializer,
)


class QuestionnaireViewSet(ModelViewSet):
    serializer_class = QuestionnaireSerializer
    queryset = Questionnaire.objects.all()


class QuestionViewSet(ModelViewSet):
    serializer_class = QuestionSerializer

    def get_queryset(self):
        if questionnaire := self.kwargs.get("questionnaire_pk"):
            return Question.objects.filter(questionnaire=questionnaire).order_by(
                "order"
            )
        return Question.objects.all().order_by("order")
