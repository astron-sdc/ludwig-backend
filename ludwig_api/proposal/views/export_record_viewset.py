from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models.export_record import ExportRecord
from ludwig_api.proposal.serializers.export_record_serializer import (
    ExportRecordSerializer,
)


class ExportRecordViewSet(ModelViewSet):
    permission_classes = [AllowAny]
    serializer_class = ExportRecordSerializer

    def get_queryset(self):
        queryset = ExportRecord.objects.all()
        proposal_pk = self.kwargs.get("proposal_pk")
        if proposal_pk is not None:
            queryset = queryset.filter(proposal=proposal_pk)
        pk = self.kwargs.get("pk")
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        return queryset
