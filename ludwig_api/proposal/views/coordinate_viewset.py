from astropy import units
from astropy.coordinates import Latitude, Longitude, SkyCoord
from astropy.coordinates.name_resolve import NameResolveError
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet


class CoordinateViewSet(ViewSet):
    @action(detail=False, methods=["get"])
    def resolve_name(self, request):
        name = request.query_params.get("name")
        system = request.query_params.get("system", "icrs")

        if not name:
            return Response(
                status=status.HTTP_400_BAD_REQUEST, data={"error": "name is required"}
            )

        try:
            coords = SkyCoord.from_name(name=name, frame=system)
            x: Longitude = coords.ra if system == "icrs" else coords.l
            y: Latitude = coords.dec if system == "icrs" else coords.b

            response_data = {
                "reference_frame": coords.frame.name,
                "x_degree": x.degree,
                "x_hms": x.hms,
                "x_hms_formatted": x.to_string(
                    unit=units.hourangle, sep=":", precision=2, pad=True
                ),
                "y_degree": y.degree,
                "y_dms": y.dms,
                "y_dms_formatted": y.to_string(
                    sep=":", precision=1, alwayssign=True, pad=True
                ),
            }
            return Response(status=status.HTTP_200_OK, data=response_data)
        except NameResolveError:
            return Response(
                status=status.HTTP_404_NOT_FOUND, data={"error": "name not found"}
            )
