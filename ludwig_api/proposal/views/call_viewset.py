from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Call
from ludwig_api.proposal.serializers.call_serializer import CallSerializer


class CallViewSet(ModelViewSet):
    serializer_class = CallSerializer

    def get_queryset(self):
        if self.kwargs.get("cycle_pk"):
            return Call.objects.filter(cycle=self.kwargs["cycle_pk"])
        return Call.objects.all()
