import logging
from datetime import UTC, datetime

from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import Q
from django.forms import NullBooleanSelect
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.faai.constants import EDUPERSON_ENTITLEMENT_CLAIM
from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.faai.sram.collaboration import group_contains_user
from ludwig_api.faai.sram.exception import SRAMAPIError
from ludwig_api.proposal.cache import invalidate_cached_property
from ludwig_api.proposal.models import Call
from ludwig_api.proposal.models.call import CallStatus
from ludwig_api.proposal.models.panel_type import PanelType
from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.permissions.application import is_admin
from ludwig_api.proposal.permissions.call import (
    AcceptProposalFromCall,
    Create,
    Export,
    IsCallMember,
    ManageMembersSciencePanel,
    ManageMembersTechnicalPanel,
    RejectProposalFromCall,
)
from ludwig_api.proposal.permissions.call import Roles as CallRoles
from ludwig_api.proposal.permissions.call import (
    ViewAllocatedResources,
    ViewRequestedResources,
    Write,
)
from ludwig_api.proposal.permissions.util import method_permission_classes
from ludwig_api.proposal.serializers.call_member_serializer import CallMemberSerializer
from ludwig_api.proposal.serializers.call_serializer import (
    CallReviewSerializer,
    CallSerializer,
)
from ludwig_api.proposal.serializers.pre_assign_reviewers_serializer import (
    PreAssignReviewersSerializer,
)
from ludwig_api.proposal.serializers.proposal_serializer import ProposalSerializer

logger = logging.getLogger(__name__)


def user_is_admin_or_can_manage_science_panel_or_403(panel_types, request, call):
    user = getattr(request, "user", None)
    entitlements = getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, [])
    user_is_admin = is_admin(entitlements)

    user_can_manage_panel_types = all(
        [user_can_manage_panel(panel_type, request, call) for panel_type in panel_types]
    )
    if not (user_is_admin or user_can_manage_panel_types):
        raise PermissionDenied("user is not admin or program committee chair")


def user_can_manage_panel(panel_type, request, call):
    if panel_type == PanelType.technical:
        return ManageMembersTechnicalPanel().has_object_permission(request, None, call)
    elif panel_type == PanelType.scientific:
        return ManageMembersSciencePanel().has_object_permission(request, None, call)


class CallViewSet(ModelViewSet):
    serializer_class = CallSerializer

    def get_queryset(self):
        calls = Call.objects.all()

        if cycle_pk := self.kwargs.get("cycle_pk"):
            calls = calls.filter(cycle=cycle_pk)

        filter_on_open = NullBooleanSelect().value_from_datadict(
            data=self.request.query_params, files=None, name="open"
        )

        if filter_on_open is not None:
            utc_now = datetime.now(UTC)
            filter_open = Q(start__lte=utc_now) & Q(end__gte=utc_now)
            calls = calls.filter(filter_open if filter_on_open else ~filter_open)
        return calls

    @method_permission_classes([Create])
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_permission_classes([Create])
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @action(detail=True, methods=["get"], name="access-allowed")
    def access_allowed(self, request, pk):
        call = get_object_or_404(Call, pk=pk)
        data = {
            "write_call": Write().has_object_permission(request, self, call),
            "view_call_members": IsCallMember().has_object_permission(
                request, self, call
            ),
            "manage_members_science_panel": ManageMembersSciencePanel().has_object_permission(
                request, self, call
            ),
            "manage_members_technical_panel": ManageMembersTechnicalPanel().has_object_permission(
                request, self, call
            ),
            "export_call": Export().has_object_permission(request, self, call),
            "accept_proposal": AcceptProposalFromCall().has_object_permission(
                request, self, call
            ),
            "reject_proposal": RejectProposalFromCall().has_object_permission(
                request, self, call
            ),
        }
        return Response(status=status.HTTP_200_OK, data=data)

    @action(detail=True, methods=["put"])
    def review_submitted_proposals(self, request, pk):
        # Returns 400 if request has invalid data
        CallReviewSerializer(data=request.data).is_valid(raise_exception=True)

        call = get_object_or_404(Call, pk=pk)
        if call.status != CallStatus.closed:
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data={"error": "Call status is not closed"},
            )

        proposals = call.proposals.filter(status=ProposalStatus.submitted)
        if not proposals.count():
            return Response(
                status=status.HTTP_403_FORBIDDEN,
                data={"error": "Call has no proposals with submitted status"},
            )

        with transaction.atomic():
            call.technical_review_deadline = request.data["technical_review_deadline"]
            call.science_review_deadline = request.data["science_review_deadline"]
            call.save()

            for proposal in proposals:
                proposal.review()
                proposal.save()

        return Response(
            status=status.HTTP_200_OK,
            data={"proposals": ProposalSerializer(proposals, many=True).data},
        )

    @action(detail=True, methods=["put"])
    def pre_assign_reviewers(self, request, pk):
        call = get_object_or_404(Call, pk=pk)
        self.check_object_permissions(request, call)

        request_serializer = PreAssignReviewersSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)

        panel_types = request_serializer.validated_data["panel_types"]

        user_is_admin_or_can_manage_science_panel_or_403(panel_types, request, call)

        tech_panel_group = call.get_group_by_short_name(
            CallRoles.TECHNICAL_REVIEWER.value
        )
        science_panel_group = call.get_group_by_short_name(
            CallRoles.SCIENCE_REVIEWER.value
        )

        panel_group_map = {
            PanelType.technical: tech_panel_group,
            PanelType.scientific: science_panel_group,
        }

        provided_panel_groups = [
            panel_group
            for panel_type, panel_group in panel_group_map.items()
            if panel_type in panel_types
        ]

        reviewer_ids = request_serializer.validated_data["reviewer_ids"]

        sram_failure_count = 0
        for reviewer_id in reviewer_ids:
            for panel_group in [tech_panel_group, science_panel_group]:
                if panel_group in provided_panel_groups and not group_contains_user(
                    panel_group, reviewer_id
                ):
                    try:
                        SRAMClient().add_member_to_group(
                            group_identifier=panel_group["identifier"],
                            eduperson_unique_id=reviewer_id,
                        )
                    except SRAMAPIError:
                        sram_failure_count += 1
                        logger.warning(
                            f"SRAM api call failed adding member to group {panel_group['name']}, identifier: {panel_group['identifier']} and user {reviewer_id}:",
                            exc_info=True,
                        )
                elif panel_group not in provided_panel_groups and group_contains_user(
                    panel_group, reviewer_id
                ):
                    try:
                        SRAMClient().delete_member_from_group(
                            group_identifier=panel_group["identifier"],
                            eduperson_unique_id=reviewer_id,
                        )
                    except SRAMAPIError:
                        sram_failure_count += 1
                        logger.warning(
                            f"SRAM api call failed deleting member from group {panel_group['name']}, identifier: {panel_group['identifier']} and user {reviewer_id}:",
                            exc_info=True,
                        )

        invalidate_cached_property(call, "collaboration")
        response_serializer = CallMemberSerializer(call)

        if sram_failure_count > 0:
            return Response(
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                data=response_serializer.data,
            )
        else:
            return Response(status=status.HTTP_200_OK, data=response_serializer.data)

    @action(detail=True, methods=["get"], name="sky_distribution")
    def sky_distribution(self, _request, pk):
        call = get_object_or_404(Call, pk=pk)
        return Response(call.sky_distribution)

    @action(
        detail=True,
        methods=["get"],
        name="requested_resources",
        permission_classes=[ViewRequestedResources],
    )
    def requested_resources(self, request, pk):
        call = get_object_or_404(Call, pk=pk)
        return Response(call.requested_resources)

    @action(
        detail=True,
        methods=["get"],
        name="allocated_resources",
        permission_classes=[ViewAllocatedResources],
    )
    def allocated_resources(self, request, pk):
        call = get_object_or_404(Call, pk=pk)
        return Response(call.allocated_resources)

    @action(
        detail=True, methods=["get"], name="members", permission_classes=[IsCallMember]
    )
    def members(self, request, pk):
        call = get_object_or_404(Call, pk=pk)
        self.check_object_permissions(request, call)
        serializer = CallMemberSerializer(call)
        return Response(serializer.data)
