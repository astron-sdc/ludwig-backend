from django_fsm import TransitionNotAllowed
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.permissions.call import (
    AcceptProposalFromCall,
    RejectProposalFromCall,
)
from ludwig_api.proposal.permissions.proposal import (
    ManageMembers,
    ReadCalculationFromProposal,
    ReadScienceReviewFromProposal,
    ReadTechnicalReviewFromProposal,
    Submit,
    Write,
    WriteCalculationFromProposal,
    WriteScienceReviewFromProposal,
    WriteTechnicalReviewFromProposal,
)
from ludwig_api.proposal.permissions.util import method_permission_classes
from ludwig_api.proposal.serializers.proposal_enriched_serializer import (
    ProposalEnrichedSerializer,
)
from ludwig_api.proposal.view_helpers.proposal_helper import (
    finish_sync_and_clean_unneeded_exports,
    get_scientific_justification,
    synchronize,
)
from ludwig_api.proposal.views.proposal_authorized_viewset import (
    ProposalAuthorizedViewSetMixin,
)


def submission_deadline_passed(deadline) -> Response:
    return Response(
        status=status.HTTP_400_BAD_REQUEST,
        data={
            "error": "submission deadline has passed",
            "deadline": deadline,
        },
    )


def status_not_allowed(source, target) -> Response:
    return Response(
        status=status.HTTP_405_METHOD_NOT_ALLOWED,
        data={
            "error": "invalid transition from source to target",
            "source": source,
            "target": target,
        },
    )


class ProposalViewSet(ProposalAuthorizedViewSetMixin, ModelViewSet):

    @method_permission_classes([Write])
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @action(
        detail=False,
        methods=["get"],
        name="as_reviewer",
        serializer_class=ProposalEnrichedSerializer,
    )
    def as_reviewer(self, request):
        user = request.user
        # get_queryset filters based on permissions
        # filter extra based on assigned as reviewer
        # TODO: only when type is set? (i.e. not where you left a comment)?
        proposals = [
            proposal
            for proposal in self.get_queryset().filter(reviews__reviewer_id=user.id)
            if not proposal.is_user_author(user)
        ]
        proposals_data = self.get_serializer(proposals, many=True)
        return Response(status=status.HTTP_200_OK, data=proposals_data.data)

    @action(detail=True, methods=["put"], name="submit", permission_classes=[Submit])
    def submit(self, request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        self.check_object_permissions(request, proposal)
        try:
            proposal.submit()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            if proposal.deadline_has_passed():
                return submission_deadline_passed(proposal.call.end)
            return status_not_allowed(proposal.status, ProposalStatus.submitted)

    @action(detail=True, methods=["put"], name="unsubmit", permission_classes=[Submit])
    def unsubmit(self, request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        self.check_object_permissions(request, proposal)
        try:
            proposal.unsubmit()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            if proposal.deadline_has_passed():
                return submission_deadline_passed(proposal.call.end)
            return status_not_allowed(proposal.status, ProposalStatus.draft)

    @action(detail=True, methods=["put"], name="retract", permission_classes=[Submit])
    def retract(self, request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        self.check_object_permissions(request, proposal)
        try:
            proposal.retract()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.retracted)

    @action(detail=True, methods=["put"], name="review")
    def review(self, request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.review()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.under_review)

    @action(detail=True, methods=["put"], name="accept")
    def accept(self, _request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.accept()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.accepted)

    @action(detail=True, methods=["put"], name="reject")
    def reject(self, _request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.reject()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.rejected)

    @action(detail=True, methods=["get"], name="scientific-justification")
    def scientific_justification(self, _request, pk) -> Response:
        return get_scientific_justification(self, pk)

    @action(detail=True, methods=["get"], name="sync")
    def sync(self, request, pk):
        return synchronize(self, pk)

    @action(detail=True, methods=["get"], name="finish_sync")
    def finish_sync(self, request, pk):
        return finish_sync_and_clean_unneeded_exports(self, pk)

    @action(detail=True, methods=["get"], name="access-allowed")
    def access_allowed(self, request, pk):
        # Actual read permissions are enforced via get_queryset()
        proposal = get_object_or_404(self.get_queryset(), pk=pk)

        data = {
            "write_proposal": Write().has_object_permission(request, self, proposal),
            "submit_proposal": Submit().has_object_permission(request, self, proposal),
            "read_technical_review": ReadTechnicalReviewFromProposal().has_object_permission(
                request, self, proposal
            ),
            "write_technical_review": WriteTechnicalReviewFromProposal().has_object_permission(
                request, self, proposal
            ),
            "read_science_review": ReadScienceReviewFromProposal().has_object_permission(
                request, self, proposal
            ),
            "write_science_review": WriteScienceReviewFromProposal().has_object_permission(
                request, self, proposal
            ),
            "manage_members": ManageMembers().has_object_permission(
                request, self, proposal
            ),
            "accept_proposal": AcceptProposalFromCall().has_object_permission(
                request, self, proposal.call
            ),
            "reject_proposal": RejectProposalFromCall().has_object_permission(
                request, self, proposal.call
            ),
            "read_calculation": ReadCalculationFromProposal().has_object_permission(
                request, self, proposal
            ),
            "write_calculation": WriteCalculationFromProposal().has_object_permission(
                request, self, proposal
            ),
        }
        return Response(status=status.HTTP_200_OK, data=data)
