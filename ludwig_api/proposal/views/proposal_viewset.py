from django_fsm import TransitionNotAllowed
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.models.proposal import ProposalStatus
from ludwig_api.proposal.serializers.proposal_serializer import ProposalSerializer


def submission_deadline_passed(deadline) -> Response:
    return Response(
        status=status.HTTP_400_BAD_REQUEST,
        data={
            "error": "submission deadline has passed",
            "deadline": deadline,
        },
    )


def status_not_allowed(source, target) -> Response:
    return Response(
        status=status.HTTP_405_METHOD_NOT_ALLOWED,
        data={
            "error": "invalid transition from source to target",
            "source": source,
            "target": target,
        },
    )


class ProposalViewSet(ModelViewSet):
    serializer_class = ProposalSerializer

    def get_queryset(self):
        if self.kwargs.get("call_pk"):
            return Proposal.objects.filter(call=self.kwargs["call_pk"])
        return Proposal.objects.all()

    @action(detail=True, methods=["put"], name="submit")
    def submit(self, _request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.submit()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            if proposal.deadline_has_passed():
                return submission_deadline_passed(proposal.call.end)
            return status_not_allowed(proposal.status, ProposalStatus.submitted)

    @action(detail=True, methods=["put"], name="unsubmit")
    def unsubmit(self, _request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.unsubmit()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            if proposal.deadline_has_passed():
                return submission_deadline_passed(proposal.call.end)
            return status_not_allowed(proposal.status, ProposalStatus.draft)

    @action(detail=True, methods=["put"], name="retract")
    def retract(self, _request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.retract()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.retracted)

    @action(detail=True, methods=["put"], name="review")
    def review(self, request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.review()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.under_review)

    @action(detail=True, methods=["put"], name="accept")
    def accept(self, _request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.accept()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.accepted)

    @action(detail=True, methods=["put"], name="reject")
    def reject(self, _request, pk) -> Response:
        proposal = get_object_or_404(self.get_queryset(), pk=pk)
        try:
            proposal.reject()
            proposal.save()
            return Response(status=status.HTTP_200_OK)
        except TransitionNotAllowed:
            return status_not_allowed(proposal.status, ProposalStatus.rejected)
