import json

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from ludwig_api.proposal.tmss.api import TMSS


class TMSSViewSet(ViewSet):
    @action(detail=False, methods=["get"])
    def strategies(self, *args, **kwargs):
        strategies = TMSS().get_strategies()
        return Response(status=status.HTTP_200_OK, data=strategies)

    @action(detail=False, methods=["get"])
    def cycles(self, *args, **kwargs):
        cycles = TMSS().get_cycles()
        return Response(status=status.HTTP_200_OK, data=cycles)

    @action(detail=False, methods=["get"])
    def tmss_url(self, *args, **kwargs):
        return Response(status=status.HTTP_200_OK, data=TMSS().BASE_URL)

    @action(detail=False, methods=["get"])
    def utc_now(self, *args, **kwargs):
        utc_now = TMSS().get_utc_now()
        return Response(status=status.HTTP_200_OK, data=utc_now)

    @action(detail=False, methods=["get"])
    def common_template(self, *args, **kwargs):
        commontemplate = TMSS().get_common_schema_template()
        return Response(status=status.HTTP_200_OK, data=commontemplate)

    @action(detail=False, methods=["get"])
    def scheduling_constraints_template(self, *args, **kwargs):
        scheduling_constraints_template = TMSS().get_scheduling_constraints_template()
        return Response(status=status.HTTP_200_OK, data=scheduling_constraints_template)

    @action(detail=False, methods=["post"])
    def scheduling_is_observable(self, request):
        strategy_template_id = request.query_params.get("strategy_template_id")

        if not strategy_template_id:
            raise RuntimeError({"strategy_template_id": "This field is required."})

        is_observable_response = TMSS().post_is_observable(
            strategy_template_id=strategy_template_id,
            specification_definition=json.dumps(request.data),
        )

        combined_data = {
            "is_observable": is_observable_response.json(),
            "server_response_status_code": is_observable_response.status_code,
        }
        return Response(status=status.HTTP_200_OK, data=combined_data)

    @action(detail=False, methods=["get"])
    def task_template(self, request):
        limit = request.query_params.get("limit")
        offset = request.query_params.get("offset")
        task_template = TMSS().get_task_template(limit, offset)
        return Response(status=status.HTTP_200_OK, data=task_template)

    @action(detail=False, methods=["get"])
    def task_draft(self, *args, **kwargs):
        task_draft = TMSS().get_task_draft()
        return Response(status=status.HTTP_200_OK, data=task_draft)
