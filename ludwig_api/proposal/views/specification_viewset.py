from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Specification
from ludwig_api.proposal.serializers.specification_serializer import (
    SpecificationSerializerWithGroup,
    SpecificationWithDefinitionSerializer,
)
from ludwig_api.proposal.view_helpers.specification_helper import (
    synchronize_specification,
    synchronize_subgroup_with_specification,
)


class SpecificationViewSet(ModelViewSet):
    permission_classes = [AllowAny]

    def get_serializer_class(self):
        # Use the detailed serializer if a specific ID is provided
        if self.kwargs.get("pk") or self.request.method in [
            "POST",
            "PUT",
            "PATCH",
            "DELETE",
        ]:
            return SpecificationWithDefinitionSerializer
        return SpecificationSerializerWithGroup

    def get_queryset(self):
        queryset = Specification.objects.all()

        # Filter op proposal_pk when present
        proposal_pk = self.kwargs.get("proposal_pk")
        if proposal_pk is not None:
            queryset = queryset.filter(proposal=proposal_pk)

        # Filter op primaire key when present
        pk = self.kwargs.get("pk")
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        return queryset

    def destroy(self, request, *args, **kwargs):
        # when authentication is here, specific checks have to be made that only  Specifications with the correct permissions get deleted.
        return super().destroy(request, *args, **kwargs)

    @action(
        detail=False,
        methods=["delete"],
        url_path="delete-by-ids/(?P<proposalid>[^/.]+)/(?P<specificationid>[^/.]+)",
    )
    def delete_by_ids(self, request, specificationid=None, proposalid=None):
        try:
            specification = Specification.objects.get(
                pk=specificationid, proposal_id=proposalid
            )
            specification.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Specification.DoesNotExist:
            return Response(
                {"error": "Specification not found."}, status=status.HTTP_404_NOT_FOUND
            )

    @action(
        detail=False,
        methods=["get"],
        url_path="sync/(?P<proposalid>[^/.]+)/(?P<specificationid>[^/.]+)",
    )
    def sync(self, request, specificationid=None, proposalid=None):
        syncresult, message, record, exception = synchronize_specification(
            self, specificationid, proposalid
        )
        exception_message = ""
        if exception:
            exception_message = str(exception)

        jsonmessage = {
            "did_succeed": syncresult,
            "message": [message],
            "upstream_scheduling_set": record.__dict__ if record is not None else None,
            "exception": exception_message,
        }

        return JsonResponse(jsonmessage)

    @action(
        detail=False,
        methods=["get"],
        url_path="subsync/(?P<specificationid>[^/.]+)/(?P<subgroupid>[^/.]+)",
    )
    def subsync(self, request, specificationid=None, proposal_pk=None, subgroupid=1):
        syncresult, message, record, exception = (
            synchronize_subgroup_with_specification(
                self, specificationid, proposal_pk, subgroupid
            )
        )
        exception_message = str(exception) if exception else ""
        jsonmessage = {
            "did_succeed": syncresult,
            "message": message,
            "upstream_scheduling_set": record if record is not None else None,
            "exception": exception_message,
        }

        return JsonResponse(jsonmessage)
