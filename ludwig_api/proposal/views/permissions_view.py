from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ludwig_api.proposal.permissions.call import Create as CallCreate
from ludwig_api.proposal.permissions.call import ViewRanking as CallViewRanking
from ludwig_api.proposal.permissions.util import EntitlementPermission


class GlobalPermissionsView(APIView):
    def get(self, request):
        data = {
            "is_admin": EntitlementPermission().has_permission(request, self),
            "create_cycle": EntitlementPermission().has_permission(request, self),
            "create_call": CallCreate().has_permission(request, self),
            "view_call_ranking": CallViewRanking().has_permission(request, self),
        }
        return Response(status=status.HTTP_200_OK, data=data)
