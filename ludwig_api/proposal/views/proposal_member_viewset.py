import logging

from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from ludwig_api.faai.sram.client import Role as CollaborationRole
from ludwig_api.faai.sram.client import SRAMClient
from ludwig_api.faai.sram.exception import SRAMAPIError
from ludwig_api.faai.sram.models import Group
from ludwig_api.proposal.permissions.proposal import ManageMembers
from ludwig_api.proposal.serializers.collaboration_invite_serializer import (
    CollaborationInviteSerializer,
)
from ludwig_api.proposal.views.proposal_authorized_viewset import (
    ProposalAuthorizedViewSetMixin,
)

logger = logging.getLogger(__name__)


class ProposalMemberViewSet(ProposalAuthorizedViewSetMixin, ViewSet):

    @action(detail=False, methods=["PUT"], permission_classes=[ManageMembers])
    def invites(self, request, proposal_pk):
        proposal = get_object_or_404(self.get_queryset(), pk=proposal_pk)
        self.check_object_permissions(request, proposal)

        collaboration_identifier = proposal.collaboration_identifier

        client = SRAMClient()
        collaboration = client.get_collaboration(collaboration_identifier)

        # Check if provided groups exist in the collaboration
        serializer = CollaborationInviteSerializer(
            data=request.data,
            context={
                "collaboration_groups": [
                    group["identifier"] for group in collaboration["groups"]
                ]
            },
        )
        serializer.is_valid(raise_exception=True)

        try:
            client.create_collaboration_invite(
                collaboration_identifier=collaboration_identifier,
                message="You are invited to become a collaborator in a proposal.",
                intended_role=CollaborationRole.MEMBER,
                sender_name=request.user.name,
                invites=serializer.validated_data["invites"],
                groups=serializer.validated_data["groups"],
            )
        except SRAMAPIError:
            logger.error(
                f"Call to the SRAM api failed for sending invite for: {collaboration_identifier}",
                exc_info=True,
            )
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # Process is continued in the SRAM systems
        return Response(status=status.HTTP_202_ACCEPTED)

    @action(detail=False, methods=["PUT"])
    def roles(self, request, proposal_pk):
        provided_roles = request.data.get("roles")
        if provided_roles is None:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={"error": "'roles' is required"},
            )

        user_id = request.data.get("user_id")
        if not user_id:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={"user_id": "'user_id' is required"},
            )

        proposal = get_object_or_404(self.get_queryset(), pk=proposal_pk)

        client = SRAMClient()

        proposal_group: Group
        for proposal_group in proposal.groups:
            group_identifier = proposal_group["identifier"]
            user_ids_in_group = [
                member.get("eduperson_unique_id")
                for member in proposal_group.get("members", [])
            ]
            user_is_already_in_group = user_id in user_ids_in_group

            try:
                if group_identifier in provided_roles and not user_is_already_in_group:
                    client.add_member_to_group(
                        group_identifier=group_identifier, eduperson_unique_id=user_id
                    )
                elif (
                    group_identifier not in provided_roles and user_is_already_in_group
                ):
                    client.delete_member_from_group(
                        group_identifier=group_identifier, eduperson_unique_id=user_id
                    )
            except SRAMAPIError:
                group_name = proposal_group.get("name")
                logger.warning(
                    f"Call to the SRAM api failed for group {group_name} and user {user_id}: ",
                    exc_info=True,
                )

        return Response(status=status.HTTP_204_NO_CONTENT)
