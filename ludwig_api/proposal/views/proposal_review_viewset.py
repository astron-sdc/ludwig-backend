from rest_framework.generics import ListAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.validators import ValidationError

from ludwig_api.faai.constants import EDUPERSON_ENTITLEMENT_CLAIM
from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.permissions.application import is_admin
from ludwig_api.proposal.permissions.call import get_call_ids_from_entitlements
from ludwig_api.proposal.serializers.proposal_enriched_serializer import (
    ProposalEnrichedSerializer,
)


def validate_ids(instances, data):
    proposal_ids = {proposal.id for proposal in instances}
    item_ids = {item["id"] for item in data}
    if proposal_ids != item_ids:
        raise ValidationError("Proposal IDs do not match with proposals in call")


class ProposalReviewView(ListAPIView, UpdateAPIView):
    """View for the proposals of a single call"""

    serializer_class = ProposalEnrichedSerializer

    def get_queryset(self):
        user = self.request.user
        call_pk = int(self.kwargs["call_pk"])
        entitlements = getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, [])
        user_is_admin = is_admin(entitlements)
        call_ids = get_call_ids_from_entitlements(entitlements)
        status = self.request.query_params.get("status")

        if user_is_admin or call_pk in call_ids:
            queryset = Proposal.objects.filter(call=call_pk).prefetch_related("reviews")
            # TODO filter out the entire proposal, or only the reviews if the user is author?
            if not user_is_admin:
                queryset = queryset.exclude(
                    id__in=[
                        proposal.id
                        for proposal in queryset
                        if proposal.is_user_author(user)
                    ]
                )

            if status is not None:
                queryset = queryset.filter(status=status)

            return queryset
        return []

    def partial_update(self, request, *args, **kwargs):
        proposals = self.get_queryset()
        validate_ids(proposals, request.data)
        updated_proposals = self.get_serializer(
            proposals, data=request.data, partial=True, many=True
        )
        updated_proposals.is_valid(raise_exception=True)
        self.perform_update(updated_proposals)

        return Response(updated_proposals.data)
