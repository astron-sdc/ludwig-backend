from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Cycle
from ludwig_api.proposal.permissions.cycle import Create
from ludwig_api.proposal.permissions.util import method_permission_classes
from ludwig_api.proposal.serializers.cycle_serializer import CycleSerializer


class CycleViewSet(ModelViewSet):
    serializer_class = CycleSerializer
    queryset = Cycle.objects.all()

    @method_permission_classes([Create])
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_permission_classes([Create])
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)
