from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Cycle
from ludwig_api.proposal.serializers.cycle_serializer import CycleSerializer


class CycleViewSet(ModelViewSet):
    serializer_class = CycleSerializer
    queryset = Cycle.objects.all()
