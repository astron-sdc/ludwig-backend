import logging

from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.faai.constants import EDUPERSON_ENTITLEMENT_CLAIM
from ludwig_api.proposal.models import Call, Proposal, Review, ReviewComment
from ludwig_api.proposal.models.call import CallStatus
from ludwig_api.proposal.models.panel_type import PanelType
from ludwig_api.proposal.permissions.application import is_admin
from ludwig_api.proposal.permissions.call import (
    ManageMembersSciencePanel,
    ManageMembersTechnicalPanel,
    ReadScienceReviewFromCall,
    ReadTechnicalReviewFromCall,
)
from ludwig_api.proposal.permissions.proposal import (
    WriteScienceReviewFromProposal,
    WriteTechnicalReviewFromProposal,
)
from ludwig_api.proposal.serializers.review_serializer import (
    ReviewCommentSerializer,
    ReviewSerializer,
)

logger = logging.getLogger(__name__)


def call_is_direct_action_or_closed_or_403(call):
    if not call.requires_direct_action and call.status != CallStatus.closed:
        raise PermissionDenied(
            "call does not require direct action and call status is not closed"
        )


def user_can_manage_members_or_403(request, call):
    can_manage_sci = ManageMembersSciencePanel().has_object_permission(
        request, None, call
    )
    can_manage_tech = ManageMembersTechnicalPanel().has_object_permission(
        request, None, call
    )
    if not user_is_admin(request) and not (can_manage_sci or can_manage_tech):
        raise PermissionDenied("user is not admin or program committee chair")


def user_can_write_comments_or_403(request, proposal):
    can_write_sci = WriteScienceReviewFromProposal().has_object_permission(
        request, None, proposal
    )
    can_write_tech = WriteTechnicalReviewFromProposal().has_object_permission(
        request, None, proposal
    )
    if not user_is_admin(request) and not (can_write_sci or can_write_tech):
        raise PermissionDenied("user is not authorized to write comments")


def user_can_rate_proposals_or_403(request, proposal):
    can_manage_sci = ManageMembersSciencePanel().has_object_permission(
        request, None, proposal.call
    )
    can_write_sci = WriteScienceReviewFromProposal().has_object_permission(
        request, None, proposal
    )
    # if not user_is_admin(request) and not (can_write_sci or can_manage_sci):
    if not (can_write_sci or can_manage_sci):
        raise PermissionDenied("user is not authorized to rate proposal")


def user_is_admin(request):
    user = getattr(request, "user", None)
    entitlements = getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, [])
    return is_admin(entitlements)


def proposal_is_under_review_or_403(proposal):
    if not proposal.is_under_review():
        raise PermissionDenied("proposal is not under review")


def user_is_not_author_or_403(user, proposal):
    if proposal.is_user_author(user):
        raise PermissionDenied("proposal author cannot review their own proposal")


class ReviewViewSet(ModelViewSet):
    serializer_class = ReviewSerializer

    def get_queryset(self):
        proposal = get_object_or_404(Proposal, pk=self.kwargs["proposal_pk"])
        if proposal.is_user_author(self.request.user):
            return []
        return Review.objects.filter(proposal=proposal)

    def get_serializer_context(self):
        return {"proposal_id": self.kwargs["proposal_pk"], "request": self.request}

    def create(self, request, *args, **kwargs):
        proposal = get_object_or_404(Proposal, pk=self.kwargs["proposal_pk"])
        # TODO move proposal grading to separate method?
        user_can_rate_proposals_or_403(request, proposal)
        proposal_is_under_review_or_403(proposal)
        return super().create(request, *args, **kwargs)

    @action(detail=False, methods=["GET"])
    def current_user_review(self, request, proposal_pk):
        current_user_review = Review.objects.filter(
            proposal=proposal_pk, reviewer_id=request.user.id
        ).first()

        if current_user_review is None:
            raise Http404("No Review matches the given query.")

        return Response(ReviewSerializer(current_user_review).data)

    @transaction.atomic
    @action(detail=False, methods=["put"], name="assign_reviewers")
    def assign_reviewers(self, request, call_pk):
        """
        Assign a reviewer to one or more proposals under review.
        Accepts dict with properties:
            - reviews: a list of reviews (proposal_id, panel_type, reviewer_id)
            - reviewer_type: "primary"/"secondary" reviewer
        """
        call = get_object_or_404(Call, pk=call_pk)
        self.check_object_permissions(request, call)

        call_is_direct_action_or_closed_or_403(call)
        user_can_manage_members_or_403(request, call)
        reviewer_type = request.data.get("reviewer_type")

        for review_to_assign in request.data.get("reviews"):
            proposal_id = review_to_assign.get("proposal_id")
            reviewer_id = review_to_assign.get("reviewer_id")
            panel_type = review_to_assign.get("panel_type")
            proposal = get_object_or_404(Proposal, pk=proposal_id)
            self.check_object_permissions(request, proposal)

            if proposal.is_user_id_author(reviewer_id):
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data={
                        "error": "proposal author cannot be assigned as a reviewer for their own proposal"
                    },
                )

            review_to_assign = {**review_to_assign, "reviewer_type": reviewer_type}

            serializer = ReviewSerializer(
                data=review_to_assign,
                many=False,
                context={
                    "proposal_id": proposal_id,
                    "request": request,
                },
            )
            serializer.is_valid(raise_exception=True)

            # If someone who is not a reviewer leaves a comment, a review without panel_type is created. Re-use this one
            review_without_panel_type = Review.objects.filter(
                proposal_id=proposal_id, reviewer_id=reviewer_id, panel_type=None
            ).first()

            if review_without_panel_type:
                review_without_panel_type.panel_type = panel_type
                review_without_panel_type.reviewer_type = reviewer_type
                review_without_panel_type.save()
            else:
                Review.objects.update_or_create(
                    proposal_id=proposal_id,
                    reviewer_id=reviewer_id,
                    panel_type=panel_type,
                    reviewer_type=reviewer_type,
                )

        return Response(status=status.HTTP_204_NO_CONTENT)


class ReviewCommentViewSet(ModelViewSet):
    serializer_class = ReviewCommentSerializer
    ordering = "created_at"

    def get_queryset(self):
        proposal = get_object_or_404(Proposal, pk=self.kwargs["proposal_pk"])

        if proposal.is_user_author(self.request.user):
            return []

        review_query = ReviewComment.objects.all()

        if review := self.kwargs.get("review_pk"):
            review_query = review_query.filter(review=review)
        else:
            review_query = review_query.filter(review__proposal=proposal)

        allowed_panel_types = self.get_allowed_panel_types(proposal)

        review_query = review_query.filter(review__panel_type__in=allowed_panel_types)

        return review_query.order_by(self.ordering)

    def get_allowed_panel_types(self, proposal):
        can_read_tech_reviews = ReadTechnicalReviewFromCall().has_object_permission(
            request=self.request, view=None, obj=proposal.call
        ) or proposal.is_user_assigned_as_technical_reviewer(self.request.user)

        can_read_science_reviews = ReadScienceReviewFromCall().has_object_permission(
            request=self.request, view=None, obj=proposal.call
        ) or proposal.is_user_assigned_as_scientific_reviewer(self.request.user)

        allowed_panel_types = []
        if can_read_science_reviews:
            allowed_panel_types.append(PanelType.scientific)
        if can_read_tech_reviews:
            allowed_panel_types.append(PanelType.technical)
        return allowed_panel_types

    def get_serializer_context(self):
        context = {
            "proposal_id": self.kwargs["proposal_pk"],
            "request": self.request,
        }

        if review_pk := self.kwargs.get("review_pk"):
            context["review_id"] = review_pk

        return context

    def create(self, request, *args, **kwargs):
        proposal = get_object_or_404(Proposal, pk=self.kwargs["proposal_pk"])

        proposal_is_under_review_or_403(proposal)
        user_can_write_comments_or_403(request, proposal)

        created_comment_response = super().create(request, *args, **kwargs)

        review = Review.objects.get(id=created_comment_response.data["review_id"])
        if review.panel_type is None:
            allowed_panels = self.get_allowed_panel_types(proposal)
            review.panel_type = (
                allowed_panels[0] if allowed_panels else PanelType.technical
            )
            review.save()

        return created_comment_response
