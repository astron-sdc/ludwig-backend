from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Target
from ludwig_api.proposal.models.proposal import Proposal
from ludwig_api.proposal.serializers.target.target_default_serializer import (
    TargetDefaultSerializer,
)
from ludwig_api.proposal.serializers.target.target_upsert_serializer import (
    TargetUpsertSerializer,
)
from ludwig_api.proposal.view_helpers.group_helper import get_or_create_group


class TargetViewSet(ModelViewSet):
    serializer_class = TargetDefaultSerializer

    def get_queryset(self):
        return Target.objects.filter(proposal=self.kwargs["proposal_pk"])

    def perform_create(self, serializer):
        serializer.save(proposal_id=self.kwargs["proposal_pk"])

    @transaction.atomic
    @action(detail=False, methods=["post"])
    def update_proposal_targets(self, request, *args, **kwargs):
        """
        Update proposal targets.

        Notes:
            Targets that contain an id field will update the existing Target that matches that id
            Targets that do not contain an id field will cause a new Target to be created
            Targets that currently exist but are not specified in the request will be deleted

            Targets are related to Groups by means of group_id_number in the incomming json.
            Groups are linked or created .
        Args:
            request (HttpRequest): The HTTP request object.
            *args: Variable length argument list.
            **kwargs: Arbitrary keyword arguments.

        Returns:
            Response: The HTTP response object.

        Raises:
            None.
        """
        serializer = TargetUpsertSerializer(data=request.data, many=True, partial=True)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        provided_targets = serializer.validated_data
        existing_targets = list(self.get_queryset())
        target_map = {target.get("id"): target for target in provided_targets}
        proposal_pk = self.kwargs["proposal_pk"]
        proposal: Proposal = get_object_or_404(Proposal, pk=proposal_pk)

        # creates and updates
        for target in provided_targets:
            group = target.get("group")
            get_or_create_group(group, proposal.id)  # Insert related group
            try:
                existing_target = self.get_queryset().get(pk=target.get("id"))
                target.pop("id", None)
                for key, value in target.items():
                    setattr(existing_target, key, value)
                existing_target.save()
            except ObjectDoesNotExist:
                target["proposal_id"] = kwargs["proposal_pk"]
                target.pop("id", None)
                self.get_queryset().create(**target)

        # deletes
        for target in existing_targets:
            if target.id not in target_map:
                target.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
