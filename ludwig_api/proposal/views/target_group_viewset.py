from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models import Specification
from ludwig_api.proposal.models.target_group import TargetGroup
from ludwig_api.proposal.serializers.target_group_serializer import (
    TargetGroupSerializer,
)


class TargetGroupViewSet(ModelViewSet):
    permission_classes = [AllowAny]
    serializer_class = TargetGroupSerializer

    def get_queryset(self):
        queryset = TargetGroup.objects.all()
        pk = self.kwargs.get("pk")  # Filter op primaire key when present
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        proposal_pk = self.kwargs.get(
            "proposal_pk"
        )  # Filter op proposal_pk when present
        if proposal_pk is not None:
            queryset = queryset.filter(proposal=proposal_pk)

        return queryset

    def destroy(self, request, *args, **kwargs):
        return super().destroy(
            request, *args, **kwargs
        )  # when authentication is here, specific checks have to be made that only  groups with the correct permissions get deleted.

    @action(
        detail=False,
        methods=["delete"],
        url_path="delete-by-ids/(?P<proposalid>[^/.]+)/group(?P<groupid>[^/.]+)",
    )
    def group_delete_by_ids(self, request, groupid=None, proposalid=None):
        try:
            group = TargetGroup.objects.get(pk=groupid, proposal_id=proposalid)
            group.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Specification.DoesNotExist:
            return Response(
                {"error": "Group not found."}, status=status.HTTP_404_NOT_FOUND
            )
