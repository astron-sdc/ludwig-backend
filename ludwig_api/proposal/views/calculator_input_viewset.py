from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated

from ludwig_api.proposal.models.calculator_inputs import CalculatorInput
from ludwig_api.proposal.permissions.proposal import ReadCalculationFromProposal
from ludwig_api.proposal.permissions.util import method_permission_classes
from ludwig_api.proposal.serializers.calculator_inputs_serializer import (
    CalculatorInputSerializer,
)


class CalculatorInputViewset(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """List and get CalculatorInputs"""

    permission_classes = [IsAuthenticated]

    serializer_class = CalculatorInputSerializer

    @method_permission_classes([ReadCalculationFromProposal])
    def get_queryset(self):
        proposal_pk = self.kwargs.get("proposal_pk")
        return CalculatorInput.objects.filter(proposal__id=proposal_pk)
