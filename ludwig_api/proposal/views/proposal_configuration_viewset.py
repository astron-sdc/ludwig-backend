from django.forms import ValidationError
from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from ludwig_api.proposal.models.proposal import Proposal
from ludwig_api.proposal.models.proposal_configuration import ProposalConfiguration
from ludwig_api.proposal.serializers.proposal_configuration_serializer import (
    ProposalConfigurationSerializer,
)


class ProposalConfigurationViewSet(ModelViewSet):
    permission_classes = [AllowAny]

    def get_serializer_class(self):
        return ProposalConfigurationSerializer

    def get_queryset(self):
        queryset = ProposalConfiguration.objects.all()
        proposal_pk = self.kwargs.get(
            "proposal_pk"
        )  # Filter op proposal_pk when present
        if proposal_pk is not None:
            queryset = queryset.filter(proposal=proposal_pk)

        pk = self.kwargs.get("pk")  # Filter op primaire key when present
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        return queryset

    def destroy(self, request, *args, **kwargs):
        # when authentication is here, specific checks have to be made that only  Specifications with the correct permissions get deleted.
        return super().destroy(request, *args, **kwargs)

    @action(detail=False, methods=["GET"])
    def current(self, request, proposal_pk):
        current_config = get_object_or_404(ProposalConfiguration, proposal=proposal_pk)
        return Response(ProposalConfigurationSerializer(current_config).data)

    def perform_create(self, serializer):
        proposal_id = self.kwargs.get("proposal_pk")
        try:
            proposal = Proposal.objects.get(id=proposal_id)
        except Proposal.DoesNotExist:
            raise ValidationError("Proposal does not exist")
        # Save the ProposalConfiguration and link it to the Proposal
        configuration = serializer.save()
        # Set the configuration on the Proposal and save
        proposal.configuration = configuration
        proposal.save()

    def perform_update(self, serializer):
        serializer.save()
