from django.db.models import Q
from rest_framework.generics import GenericAPIView

from ludwig_api.faai.constants import EDUPERSON_ENTITLEMENT_CLAIM
from ludwig_api.proposal.models import Proposal
from ludwig_api.proposal.permissions.application import is_admin
from ludwig_api.proposal.permissions.call import get_call_ids_from_entitlements
from ludwig_api.proposal.permissions.proposal import get_proposal_ids_from_entitlements
from ludwig_api.proposal.serializers.proposal_serializer import ProposalSerializer


class ProposalAuthorizedViewSetMixin(GenericAPIView):
    serializer_class = ProposalSerializer

    def get_queryset(self):
        user = self.request.user
        entitlements = getattr(user, EDUPERSON_ENTITLEMENT_CLAIM, [])
        user_is_admin = is_admin(entitlements)
        proposal_ids = get_proposal_ids_from_entitlements(entitlements)
        call_ids = get_call_ids_from_entitlements(entitlements)

        if user_is_admin:
            return Proposal.objects.all()

        # Otherwise filter by created_by/entitlements
        return Proposal.objects.filter(
            Q(created_by=user.id) | Q(pk__in=proposal_ids) | Q(call__pk__in=call_ids)
        )
