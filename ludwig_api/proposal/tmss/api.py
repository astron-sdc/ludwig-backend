import json

from django.conf import settings
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from rest_framework import status

from ludwig_api.proposal.tmss.models.project import TMSSProject
from ludwig_api.proposal.tmss.models.projectquota import TMSSProjectQuota
from ludwig_api.proposal.tmss.models.resourcetype import (
    convert_response_to_resource_types,
    create_resource_dict,
)
from ludwig_api.proposal.tmss.models.scheduling_set import TMSSSchedulingSet


class TMSS:

    CLEAN_PROJECT_ID = "-1"

    def get_json_headers(self):
        headers = {
            "Content-Type": self.CONTENT_TYPE_JSON,
        }
        return headers

    def __init__(self):
        self.is_caching_of_token_allowed = False  #  not yet fully implemented the caching of tokens. Only needed when speed is becomming a issue
        self.is_token_retrieved = False
        self.BASE_URL = settings.LUDWIG_TMSS_URI
        self.CONTENT_TYPE_JSON = "application/json"
        self.oauth = OAuth2Session(
            client=BackendApplicationClient(
                client_id=settings.LUDWIG_TMSS_CLIENT_ID,
                scope="openid email profile eduperson_entitlement",
            )
        )

    def allow_token_caching(self):
        self.is_caching_of_token_allowed = True

    def get_strategies(self):
        endpoint = "/api/scheduling_unit_observing_strategy_template?state=active"
        return self.__make_tmss_api_call(endpoint)

    def get_cycles(self):
        endpoint = "/api/cycle"
        return self.__make_tmss_api_call(endpoint)

    def get_resources(self):
        endpoint = "/api/resource_type"
        web_result = self.__make_tmss_api_call(endpoint)
        resource_types = convert_response_to_resource_types(web_result)
        resource_dict = create_resource_dict(resource_types)
        return resource_dict

    def get_scheduling_sets(self, project_id):
        if project_id == self.CLEAN_PROJECT_ID:
            return [TMSSSchedulingSet()]
        endpoint = (
            "/api/scheduling_set/?project="
            + project_id
            + "&fields=id,name,description,url"
        )
        schedulingset_result = self.__make_tmss_api_call(endpoint)
        schedulingsets = TMSSSchedulingSet.from_list(
            schedulingset_result.get("results", [])
        )
        return schedulingsets

    def delete_scheduling_set(self, scheduling_set_id):
        endpoint = f"/api/scheduling_set/{scheduling_set_id}"
        headers = self.get_json_headers()
        return self.__make_tmss_api_call(
            endpoint,
            method="DELETE",
            headers=headers,
            return_json=True,
        )

    def post_scheduling_set(self, project_url: str, name: str, description: str):
        endpoint = "/api/scheduling_set"
        headers = self.get_json_headers()
        specification_set = {
            "project": project_url,
            "name": name,
            "description": description,
            "scheduling_unit_drafts": [],
            "generator_doc": {},
        }
        result = self.__make_tmss_api_call(
            endpoint,
            method="POST",
            postdata=json.dumps(specification_set),
            headers=headers,
        )
        return TMSSSchedulingSet.from_dict(result)

    def get_project_with_quotas(self, project):

        if project == self.CLEAN_PROJECT_ID:
            return TMSSProject()
        endpoint = "/api/project/" + project + "?expand=quota"
        projectresult = self.__make_tmss_api_call(endpoint)
        project = TMSSProject.from_dict(projectresult)
        return project

    def get_utc_now(self):
        endpoint = "/api/util/utc"
        return self.__make_tmss_api_call(endpoint, False)

    def get_common_schema_template(self):
        endpoint = "/api/common_schema_template?name=stations&ordering=-id&state=active"
        return self.__make_tmss_api_call(endpoint)

    def get_scheduling_constraints_template(self):
        endpoint = "/api/scheduling_constraints_template/"
        return self.__make_tmss_api_call(endpoint)

    def get_scheduling_unit_draft(self, scheduling_unit_id):
        endpoint = f"/api/scheduling_unit_draft/{scheduling_unit_id}/?fields=id&limit=1"
        return self.__make_tmss_api_call(endpoint, return_json=False)

    def post_is_observable(self, strategy_template_id, specification_definition):
        endpoint = f"/api/scheduling_unit_observing_strategy_template/{strategy_template_id}/is_observable"
        headers = self.get_json_headers()
        return self.__make_tmss_api_call(
            endpoint,
            method="POST",
            postdata=specification_definition,
            headers=headers,
            return_json=False,
        )

    def post_scheduling_unit_definition(
        self,
        strategy_template_id,
        specification_definition,
        scheduling_set_id,
        name,
        description,
        rank,
        priority_queue,
    ):
        endpoint = f"/api/scheduling_unit_observing_strategy_template/{strategy_template_id}/create_scheduling_unit/?scheduling_set_id={scheduling_set_id}&name={name}&description={description}&rank={rank}&priority_queue={priority_queue}"
        headers = self.get_json_headers()
        return self.__make_tmss_api_call(
            endpoint,
            method="POST",
            postdata=json.dumps(specification_definition),
            headers=headers,
            return_json=True,
        )

    def delete_scheduling_unit_definition(self, draft_id):
        endpoint = f"/api/scheduling_unit_draft/{draft_id}"
        headers = self.get_json_headers()
        return self.__make_tmss_api_call(
            endpoint,
            method="DELETE",
            headers=headers,
            return_json=True,
        )

    def put_scheduling_unit_definition(self, draft_id, definition_plus_settings):
        endpoint = f"/api/scheduling_unit_draft/{draft_id}"
        headers = self.get_json_headers()
        return self.__make_tmss_api_call(
            endpoint,
            method="PUT",
            postdata=json.dumps(definition_plus_settings),
            headers=headers,
            return_json=True,
        )

    def post_project(self, project_definition: TMSSProject):
        endpoint = "/api/project/"
        headers = {
            "Content-Type": self.CONTENT_TYPE_JSON,
        }
        return self.__make_tmss_api_call(
            endpoint,
            method="POST",
            postdata=json.dumps(project_definition.__dict__),
            headers=headers,
            return_json=False,
        )

    def put_project(self, project_definition: TMSSProject, project_id):
        endpoint = f"/api/project/{project_id}/"
        headers = self.get_json_headers()
        return self.__make_tmss_api_call(
            endpoint,
            method="PUT",
            postdata=json.dumps(project_definition.__dict__),
            headers=headers,
            return_json=False,
        )

    def post_quota(self, project_quota: TMSSProjectQuota):
        endpoint = "/api/project_quota/"
        headers = self.get_json_headers()
        quota = {
            "project": project_quota.project,
            "resource_type": project_quota.resource_type,
            "value": project_quota.value,
        }
        return self.__make_tmss_api_call(
            endpoint,
            method="POST",
            postdata=json.dumps(quota),
            headers=headers,
            return_json=False,
        )

    def put_quota(self, project_quota: TMSSProjectQuota):
        endpoint = f"/api/project_quota/{project_quota.id}/"
        headers = self.get_json_headers()
        quota = {
            "project": project_quota.project,
            "resource_type": project_quota.resource_type,
            "value": project_quota.value,
        }
        return self.__make_tmss_api_call(
            endpoint,
            method="PUT",
            postdata=json.dumps(quota),
            headers=headers,
            return_json=False,
        )

    def get_task_draft(self):
        endpoint = "/api/task_draft/"
        return self.__make_tmss_api_call(endpoint, method="OPTIONS")

    def get_task_template(self, limit, offset):
        params = {}
        if limit:
            params["limit"] = limit
        if offset:
            params["offset"] = offset
        endpoint = "/api/task_template"
        return self.__make_tmss_api_call(endpoint, True, params)

    def fetch_token_as_json(self):

        data = {
            "username": settings.LUDWIG_TMSS_CLIENT_USERNAME,
            "password": settings.LUDWIG_TMSS_CLIENT_PASSWORD,
        }

        headers = self.get_json_headers()
        response = self.oauth.post(
            settings.LUDWIG_TMSS_TOKEN_URL, json=data, headers=headers
        )

        if response.status_code == status.HTTP_200_OK:
            access_token = response.json().get("token")
            if access_token:
                token = {
                    "token_type": "Bearer",
                    "access_token": access_token,
                }
                return token

        else:
            print("No acess token found in the response")
            return None

    @classmethod
    def is_basic_auth(cls):
        return bool(settings.LUDWIG_TMSS_CLIENT_USERNAME)

    def __set_token(self):
        if self.is_token_retrieved and self.is_caching_of_token_allowed:
            return
        if self.is_basic_auth():
            token = self.fetch_token_as_json()
            if token:
                self.oauth.token = token
        else:
            self.oauth.fetch_token(
                token_url=settings.LUDWIG_TMSS_TOKEN_URL,
                client_id=settings.LUDWIG_TMSS_CLIENT_ID,
                client_secret=settings.LUDWIG_TMSS_CLIENT_SECRET,
            )
        if (
            self.is_caching_of_token_allowed
        ):  # For performance reasons during sync we cache the token
            self.is_token_retrieved = True

    def __make_tmss_api_call(
        self,
        endpoint,
        return_json=True,
        params={},
        method="GET",
        postdata=None,
        headers=None,
    ):
        tmssurl = f"{self.BASE_URL}{endpoint}"
        self.__set_token()
        headers = headers or {}

        # TMSS Sometimes expect x-csrftokens to be present
        if "access_token" in self.oauth.token:
            headers.setdefault("X-CSRFTOKEN", f'{self.oauth.token["access_token"]}')

        # the basic auth flow off tmss expect a token rather then a bearer
        # (Bearer is used for keycloak auth in tmss)

        if self.is_basic_auth() and "access_token" in self.oauth.token:
            headers.setdefault(
                "Authorization", f'Token {self.oauth.token["access_token"]}'
            )
            self.oauth.token = {}

        if method == "OPTIONS":
            res = self.oauth.options(tmssurl, params=params, headers=headers)
        elif method == "POST":
            res = self.oauth.post(
                tmssurl, params=params, data=postdata, headers=headers
            )
        elif method == "DELETE":
            res = self.oauth.delete(
                tmssurl, params=params, data=postdata, headers=headers
            )
        elif method == "PUT":
            res = self.oauth.put(tmssurl, params=params, data=postdata, headers=headers)
        else:
            res = self.oauth.get(tmssurl, params=params, headers=headers)

        if return_json:
            if res.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
                return {"error": res.text}
            return res.json()
        else:
            return res


# Keycloak Client setup
# Enable "sercvice accounts"
# Assign "tmss spectator" role to service account in the role_mapping tab
# In the same tab, click the user and add a "KEYCLOAK_DN" attribute (otherwise the mapper fails silently)
# Optional add one or `eduperson_entitlement` attributes
