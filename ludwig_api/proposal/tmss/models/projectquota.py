from dataclasses import dataclass


@dataclass
class TMSSProjectQuota:
    project: str
    resource_type: str
    value: float
    id: int | None = None
    url: str | None = None

    project_id: str | None = None
    project_quota_archive_location: list[str] | None = None
    project_quota_archive_location_ids: list[str] | None = None

    resource_type_id: str | None = None
