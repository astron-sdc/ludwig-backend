from dataclasses import dataclass


@dataclass
class TMSSProject:
    name: str = ""
    description: str = ""
    url: str = ""
    cycles_ids: list | None = None
    cycles: list | None = None
    rank: int = 6
    trigger_priority: int = 1000
    quota: list[dict] | None = None
    quota_ids: list[int] | None = None
    auto_ingest: bool = False
    can_trigger: bool = False
    enable_qa_workflow: bool = True
    private_data: bool = False
    expert: bool = False
    filler: bool = False
    auto_pin: bool = False
    piggyback_allowed_tbb: bool = False
    piggyback_allowed_aartfaac: bool = False

    def __post_init__(self):
        self.cycles_ids = self.cycles_ids if self.cycles_ids is not None else []
        self.cycles = self.cycles if self.cycles is not None else []
        self.quota = self.quota if self.quota is not None else []
        self.quota_ids = self.quota_ids if self.quota_ids is not None else []

    @classmethod
    def from_dict(cls, data: dict) -> "TMSSProject":
        return cls(
            name=data.get("name", ""),
            url=data.get("url", ""),
            quota=data.get("quota", []),
            quota_ids=data.get("quota_ids", []),
        )
