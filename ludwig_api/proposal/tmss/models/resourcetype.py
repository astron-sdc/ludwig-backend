from dataclasses import dataclass


@dataclass
class ResourceType:
    name: str
    url: str

    @classmethod
    def from_dict(cls, data: dict) -> "ResourceType":
        return cls(
            name=data.get("name", ""),
            url=data.get("url", ""),
        )


def convert_response_to_resource_types(response_json: dict) -> list[ResourceType]:
    resource_types = [
        ResourceType.from_dict(item) for item in response_json.get("results", [])
    ]
    return resource_types


def create_resource_dict(resource_types: list[ResourceType]) -> dict[str, str]:
    return {resource.name: resource.url or "" for resource in resource_types}
