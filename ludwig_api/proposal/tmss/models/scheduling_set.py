from dataclasses import dataclass


@dataclass
class TMSSSchedulingSet:
    id: int = 0
    name: str = ""
    description: str = ""
    url: str = ""

    @classmethod
    def from_dict(cls, data: dict) -> "TMSSSchedulingSet":
        return cls(
            name=data.get("name", ""),
            url=data.get("url", ""),
            id=data.get("id", 0),
            description=data.get("description", ""),
        )

    @classmethod
    def from_list(cls, data_list: list[dict]) -> list["TMSSSchedulingSet"]:
        return [cls.from_dict(item) for item in data_list]
