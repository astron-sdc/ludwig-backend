from rest_framework.serializers import ModelSerializer

from ludwig_api.proposal.tmss.models.projectquota import TMSSProjectQuota


class TMSSProjectQuotaCreationSerializer(ModelSerializer):
    class Meta:
        model = TMSSProjectQuota
        fields = [
            "project",
            "resource",
            "value",
        ]
