from django_fsm.management.commands.graph_transitions import (
    Command as GenerateGraphCommand,
)


class Command(GenerateGraphCommand):
    def __init__(self):
        self.requires_system_checks = []
        super().__init__()
