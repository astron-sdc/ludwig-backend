from django.core.management import BaseCommand

from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.models import Call, Proposal
from ludwig_api.proposal.view_helpers.call_helper import create_call_in_sram
from ludwig_api.proposal.view_helpers.proposal_helper import create_proposal_in_sram


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            help="Dry run; display all proposals and calls which will be changed.",
        )
        parser.add_argument(
            "admin_email", type=str, help="Email address for collaboration admin user."
        )

    def handle(self, *args, **options):
        proposals = Proposal.objects.all()

        if options["dry_run"]:
            self.stdout.write(f"Proposals to be updated ({len(proposals)}):\n")

        for proposal in proposals:
            if options["dry_run"]:
                self.stdout.write(f"{proposal.title} ({proposal.pk})\n")
            else:
                create_proposal_in_sram(
                    proposal,
                    TokenUser(
                        {"email": options["admin_email"], "eduperson_unique_id": ""}
                    ),
                )

        calls = Call.objects.filter(collaboration_identifier="")
        if options["dry_run"]:
            self.stdout.write(f"Calls to be updated ({len(calls)}):\n")

        for call in calls:
            if options["dry_run"]:
                self.stdout.write(f"{call.name} ({call.pk})\n")
            else:
                create_call_in_sram(
                    call,
                    TokenUser(
                        {"email": options["admin_email"], "eduperson_unique_id": ""}
                    ),
                )
