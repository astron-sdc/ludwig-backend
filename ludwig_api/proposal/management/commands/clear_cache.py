from django.core.cache import cache
from django.core.management import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("key", type=str, help="Key for value in cache to clear")

    def handle(self, *args, **options):
        if options["key"]:
            cache.delete(options["key"])
