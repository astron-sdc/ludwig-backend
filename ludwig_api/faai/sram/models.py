from functools import cached_property
from typing import TypedDict

from django.conf import settings
from django.db import models

from ludwig_api.faai.sram.client import SRAMClient


class Collaboration(TypedDict, total=False):
    groups: list["CollaborationGroup"]
    name: str
    short_name: str
    description: str
    services: list[dict]
    disable_join_requests: bool
    disclose_member_information: bool
    disclose_email_information: bool
    website_url: str


class CollaborationGroup(TypedDict):
    identifier: str
    name: str
    short_name: str
    description: str
    collaboration_memberships: list["CollaborationMembership"]


class CollaborationMembership(TypedDict):
    user: "SRAMUser"


class BaseUser(TypedDict):
    name: str
    given_name: str
    family_name: str
    email: str
    schac_home_organisation: str | None
    scoped_affiliation: str | None


class SRAMUser(BaseUser):
    uid: str


class User(BaseUser, total=False):
    eduperson_unique_id: str


class Group(TypedDict):
    identifier: str
    name: str
    short_name: str
    description: str
    members: list["User"]


def collaboration_membership_to_user(membership: CollaborationMembership) -> User:
    user: SRAMUser = membership["user"]
    return User(
        eduperson_unique_id=user["uid"],
        name=user["name"],
        given_name=user["given_name"],
        family_name=user["family_name"],
        email=user["email"],
        schac_home_organisation=user.get("schac_home_organisation"),
        scoped_affiliation=user.get("scoped_affiliation"),
    )


def collaboration_group_to_group(collaboration_group: CollaborationGroup) -> Group:
    return Group(
        identifier=collaboration_group["identifier"],
        name=collaboration_group["name"],
        short_name=collaboration_group["short_name"],
        description=collaboration_group["description"],
        members=[
            collaboration_membership_to_user(member)
            for member in collaboration_group["collaboration_memberships"]
        ],
    )


class CollaborationMixin(models.Model):
    # Group managing permissions for this call
    collaboration_identifier = models.CharField(blank=True, default="")

    @cached_property
    def collaboration(self) -> dict:
        try:
            client = SRAMClient()
            return client.get_collaboration(getattr(self, "collaboration_identifier"))
        except ValueError:
            if settings.SRAM_ENABLED == "0":
                return {}  # TODO: think about mocking away SRAM for integration test
            raise

    @property
    def members(self) -> list[User]:
        return [
            collaboration_membership_to_user(membership)
            for membership in self.collaboration.get("collaboration_memberships", [])
        ]

    @property
    def groups(self) -> list[Group]:
        return [
            collaboration_group_to_group(group)
            for group in self.collaboration.get("groups", [])
        ]

    @property
    def invite_link(self) -> str:
        return SRAMClient.get_invite_url(getattr(self, "collaboration_identifier"))

    @property
    def collaboration_url(self) -> str:
        return SRAMClient.get_collaboration_url(self.collaboration)

    @property
    def members_url(self) -> str:
        return SRAMClient.get_collaboration_url_members(self.collaboration)

    @property
    def groups_url(self) -> str:
        return SRAMClient.get_collaboration_url_groups(self.collaboration)

    @property
    def join_requests_url(self) -> str:
        return SRAMClient.get_collaboration_url_join_requests(self.collaboration)

    def get_group_by_short_name(self, short_name: str) -> Group | None:
        matching_group = [g for g in self.groups if g["short_name"] == short_name]
        return matching_group[0] if matching_group else None

    class Meta:
        abstract = True
