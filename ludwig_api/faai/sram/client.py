import enum

from django.conf import settings
from requests import Session

from ludwig_api.faai.sram.exception import SRAMAPIError


class Role(enum.Enum):
    MEMBER = "member"
    ADMIN = "admin"


class SRAMClient:
    """SRAM API Client

    Based on: https://sram.surf.nl/apidocs/#/"""

    _base_url = settings.SRAM_API_URI

    def __init__(self):
        self._session = Session()
        self._session.headers.update({"Authorization": f"Bearer {settings.SRAM_TOKEN}"})

    def get_organisation(self):
        res = self._session.get(f"{self._base_url}/api/organisations/v1")
        if not res.ok:
            raise SRAMAPIError(res.json())

        return res.json()

    def get_collaboration(self, collaboration_identifier: str):
        if collaboration_identifier == "":
            raise ValueError("Collaboration identifier cannot be an empty string")

        res = self._session.get(
            f"{self._base_url}/api/collaborations/v1/{collaboration_identifier}"
        )
        if not res.ok:
            raise SRAMAPIError(res.json())

        return res.json()

    def create_collaboration(
        self,
        name: str,
        short_name: str,
        description: str,
        administrators: list[str],
        website_url: str,
        disable_join_requests=False,
        disclose_member_information=False,
        disclose_email_information=False,
    ):
        body = {
            "name": name,
            "short_name": short_name,
            "description": description,
            "administrators": administrators,
            "website_url": website_url,
            "disable_join_requests": disable_join_requests,
            "disclose_member_information": disclose_member_information,
            "disclose_email_information": disclose_email_information,
        }
        res = self._session.post(f"{self._base_url}/api/collaborations/v1", json=body)
        if not res.ok:
            raise SRAMAPIError(res.json())

        return res.json()

    def connect_collaboration_service(self, short_name: str, service_entity_id: str):
        body = {"short_name": short_name, "service_entity_id": service_entity_id}
        res = self._session.put(
            f"{self._base_url}/api/collaborations_services/v1/connect_collaboration_service",
            json=body,
        )
        if not res.ok:
            return SRAMAPIError(res.json())

        return res.json()

    def create_group(
        self,
        name: str,
        description: str,
        short_name: str,
        auto_provision_members: bool,
        collaboration_identifier: str,
    ):
        body = {
            "name": name,
            "description": description,
            "short_name": short_name,
            "auto_provision_members": auto_provision_members,
            "collaboration_identifier": collaboration_identifier,
        }
        res = self._session.post(f"{self._base_url}/api/groups/v1", json=body)
        if not res.ok:
            raise SRAMAPIError(res.json())

        return res.json()

    def add_member_to_group(self, group_identifier: str, eduperson_unique_id: str):
        body = {"uid": eduperson_unique_id}
        res = self._session.post(
            f"{self._base_url}/api/groups/v1/{group_identifier}", json=body
        )
        if not res.ok:
            raise SRAMAPIError(res.json())
        return res.json()

    def delete_member_from_group(self, group_identifier: str, eduperson_unique_id: str):
        res = self._session.delete(
            f"{self._base_url}/api/groups/v1/{group_identifier}/members/{eduperson_unique_id}"
        )
        if not res.ok:
            raise SRAMAPIError(res.json())

    def delete_collaboration(self, collaboration_identifier):
        res = self._session.delete(
            f"{self._base_url}/api/collaborations/v1/{collaboration_identifier}"
        )
        if not res.ok:
            raise SRAMAPIError(res.json())

    def create_collaboration_invite(
        self,
        collaboration_identifier: str,
        message: str,
        intended_role: Role,
        sender_name: str,
        invites: list[str],
        groups: list[str],
    ):
        body = {
            "collaboration_identifier": collaboration_identifier,
            "message": message,
            "intended_role": intended_role.value,
            "sender_name": sender_name,
            "invites": invites,
            "groups": groups,
        }
        res = self._session.put(
            f"{self._base_url}/api/invitations/v1/collaboration_invites", json=body
        )
        if not res.ok:
            raise SRAMAPIError(res.json())

        return res.json()

    @staticmethod
    def get_invite_url(collaboration_identifier: str) -> str:
        # https://sram.surf.nl/registration?collaboration=<uuid>
        return f"{settings.SRAM_API_URI}/registration?collaboration={collaboration_identifier}"

    @staticmethod
    def get_collaboration_url(collaboration: dict) -> str:
        # https://sram.surf.nl/collaborations/<numerical id>
        # Note, this redirects to https://sram.surf.nl/collaborations/<numerical id>/about
        return f"{settings.SRAM_API_URI}/collaborations/{collaboration.get('id', '')}"

    @staticmethod
    def get_collaboration_url_members(collaboration: dict) -> str:
        return f"{SRAMClient.get_collaboration_url(collaboration)}/members"

    @staticmethod
    def get_collaboration_url_join_requests(collaboration: dict) -> str:
        return f"{SRAMClient.get_collaboration_url(collaboration)}/joinrequests"

    @staticmethod
    def get_collaboration_url_groups(collaboration: dict) -> str:
        return f"{SRAMClient.get_collaboration_url(collaboration)}/groups"
