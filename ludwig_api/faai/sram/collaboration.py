from ludwig_api.faai.sram.models import Collaboration, Group


def collaboration_contains_group(
    collaboration: Collaboration, group_short_name: str
) -> bool:
    groups = [
        group["short_name"]
        for group in collaboration.get("groups", [])
        if "short_name" in group
    ]
    return group_short_name in groups


def group_contains_user(group: Group, eduperson_unique_id: str) -> bool:
    return eduperson_unique_id in (
        member["eduperson_unique_id"] for member in group["members"]
    )


def collaboration_contains_service(
    collaboration: Collaboration, service_entity_id: str
) -> bool:
    services = [
        service["entity_id"]
        for service in collaboration.get("services", [])
        if "entity_id" in service
    ]
    return service_entity_id in services
