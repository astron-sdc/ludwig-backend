import re

ENTITLEMENT_RE = re.compile(
    r"^urn:mace:surf\.nl:sram:group:(?P<organisation>[^:\s]+)(?P<collaboration>:[^:\s]+)?(?P<group>:[\S]+)?$"
)


def get_collaborations(entitlements: list[str]) -> list[str]:
    """Convert entitlements into a list of unique collaboration names."""
    collaborations = []
    for entitlement in entitlements:
        if match := ENTITLEMENT_RE.match(entitlement):
            if collaboration := match.group("collaboration"):
                collaborations.append(collaboration[1:])
    return list(set(collaborations))


def get_groups_in_collaboration(
    collaboration: str, entitlements: list[str]
) -> list[str]:
    """Get all groups for a given collaboration"""
    groups = []
    for entitlement in entitlements:
        if re_match := ENTITLEMENT_RE.match(entitlement):
            if collab := re_match.group("collaboration"):
                if collab.endswith(collaboration):
                    if group := re_match.group("group"):
                        groups.append(group[1:])
    return groups
