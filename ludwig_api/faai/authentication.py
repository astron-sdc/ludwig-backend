from rest_framework_simplejwt.authentication import JWTStatelessUserAuthentication


class OAUTH2ProxyStatelessAuthentication(JWTStatelessUserAuthentication):
    """OAuth2 Proxy uses the HTT_X_FORWARDED_ACCESS_TOKEN header directly"""

    def get_raw_token(self, header: bytes) -> bytes:
        return header


# Discussion needed if a "native" Django user is needed, or a TokenUser is good enough
# class UpsertJWTAuthentication[AuthUser: AbstractBaseUser](JWTAuthentication):
#     """Allows users to be created and updated on auth"""
#
#     def get_user(self, validated_token: Token) -> AuthUser:
#         """
#         Attempts to find and return a user using the given validated token.
#         """
#         try:
#             user_id = validated_token[api_settings.USER_ID_CLAIM]
#         except KeyError:
#             raise InvalidToken(_("Token contained no recognizable user identification"))
#
#         raise NotImplementedError("Upsert auth has not yet been implemented!")
#
#         try:
#             user = self.user_model.objects.get(**{api_settings.USER_ID_FIELD: user_id})
#             # Update user values based on claims
#
#         except self.user_model.DoesNotExist:
#             # Create user from token claims
#             user = None
#
#         # When a valid token has been provided, it is always active;
#         # therefore no need to check for it
#
#         return user
