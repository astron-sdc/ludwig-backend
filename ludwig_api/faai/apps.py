from django.apps.config import AppConfig


class FaaiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ludwig_api.faai"
