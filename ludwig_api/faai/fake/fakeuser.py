class FakeUser:
    def __init__(self):
        self.uid = "a234b687d089e"
        self.username = "dev"
        self.preferred_username = "the dev"
        self.name = "Dev Op"
        self.first_name = "forgiven"
        self.last_name = "familio"
        self.given_name = "gif"
        self.family_name = "evo"
        self.email = "dev@op"
        self.email_verified = True
        self.eduperson_entitlement = [
            "urn:mace:surf.nl:sram:group:astron_org_1:sdcdevp26",
            "urn:mace:surf.nl:sram:label:astron_org_1:astronsdctest:application",
            "urn:mace:surf.nl:sram:group:astron_org_1:sdc_dev_p31",
            "urn:mace:surf.nl:sram:group:astron_org_1:sdc_dev_c18",
            "urn:mace:surf.nl:sram:group:astron_org_1:sdc_dev_p30",
            "urn:mace:surf.nl:sram:group:astron_org_1:sdc_dev_c13",
            "urn:mace:surf.nl:sram:group:astron_org_1:sdc_dev_p29",
            "urn:mace:surf.nl:sram:group:astron_org_1:sdc_dev_p32",
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
            "urn:mace:surf.nl:sram:group:astron_org_1",
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:proposal_admin",
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:user",
            "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
        ]
        self.eduperson_unique_id = "123123123"
        self.token_type = "ID"
        self.is_authenticated = True
        self.is_superuser = True
        self.typ = "fake"
        self.id = 12345
