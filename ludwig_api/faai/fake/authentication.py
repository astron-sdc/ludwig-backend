from django.conf import settings
from rest_framework.authentication import BaseAuthentication

from ludwig_api.faai.fake.fakeuser import FakeUser


class FakeAuthentication(BaseAuthentication):
    def authenticate(self, request):
        if settings.SRAM_ENABLED != "0":
            return None
        return (self.get_fake_user(), None)

    def get_fake_user(self):
        return FakeUser()
