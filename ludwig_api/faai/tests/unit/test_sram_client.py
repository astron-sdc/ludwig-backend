import pytest
from requests import Response
from rest_framework import status

from ludwig_api.faai.sram.client import Role, SRAMClient
from ludwig_api.faai.sram.exception import SRAMAPIError


def test_get_organisation(mocker):
    client = SRAMClient()
    mock_get = mocker.patch.object(client._session, "get", autospec=True)
    _ = client.get_organisation()
    mock_get.assert_called_once_with("https://sram.surf.nl/api/organisations/v1")


def test_get_collaboration(mocker):
    client = SRAMClient()
    mock_get = mocker.patch.object(client._session, "get", autospec=True)
    client.get_collaboration("42")
    mock_get.assert_called_once_with("https://sram.surf.nl/api/collaborations/v1/42")


def test_get_collaboration__with_empty_string(mocker):
    client = SRAMClient()
    with pytest.raises(ValueError):
        client.get_collaboration("")


def test_get_collaboration__raises_sram_error_on_http_error(mocker):
    client = SRAMClient()
    mock_get = mocker.patch.object(client._session, "get", autospec=True)
    res = Response()
    res.status_code = status.HTTP_418_IM_A_TEAPOT
    res._content = b'{"error": "I am a teapot"}'
    mock_get.return_value = res
    with pytest.raises(SRAMAPIError):
        client.get_collaboration("42")
    mock_get.assert_called_once_with("https://sram.surf.nl/api/collaborations/v1/42")


def test_create_collaboration(mocker):
    client = SRAMClient()
    mock_post = mocker.patch.object(client._session, "post", autospec=True)

    _ = client.create_collaboration(
        name="test_collaboration",
        short_name="tst_collaboration",
        description="Cool collaboration",
        administrators=["test@example.com"],
        website_url="https://example.com",
    )

    mock_post.assert_called_once_with(
        "https://sram.surf.nl/api/collaborations/v1",
        json={
            "name": "test_collaboration",
            "short_name": "tst_collaboration",
            "disable_join_requests": False,
            "disclose_email_information": False,
            "disclose_member_information": False,
            "description": "Cool collaboration",
            "administrators": ["test@example.com"],
            "website_url": "https://example.com",
        },
    )


def test_connect_collaboration_service(mocker):
    client = SRAMClient()
    mock_put = mocker.patch.object(client._session, "put", autospec=True)

    _ = client.connect_collaboration_service(
        short_name="test_collaboration",
        service_entity_id="APP-0893AE93-24A2-4BF2-91E1-8AF51E5CF6E9",
    )  # Fake ID!

    mock_put.assert_called_once_with(
        "https://sram.surf.nl/api/collaborations_services/v1/connect_collaboration_service",
        json={
            "short_name": "test_collaboration",
            "service_entity_id": "APP-0893AE93-24A2-4BF2-91E1-8AF51E5CF6E9",
        },
    )


def test_create_group(mocker):
    client = SRAMClient()
    mock_post = mocker.patch.object(client._session, "post", autospec=True)

    _ = client.create_group(
        name="test_collaboration",
        description="Cool group",
        short_name="test_group",
        auto_provision_members=False,
        collaboration_identifier="a71a2b01-4642-4e1a-b3ac-0a06b2bf66f2",
    )
    mock_post.assert_called_once_with(
        "https://sram.surf.nl/api/groups/v1",
        json={
            "name": "test_collaboration",
            "description": "Cool group",
            "short_name": "test_group",
            "auto_provision_members": False,
            "collaboration_identifier": "a71a2b01-4642-4e1a-b3ac-0a06b2bf66f2",
        },
    )


def test_create_collaboration_invite_success(mocker):
    client = SRAMClient()

    mock_post = mocker.patch.object(client._session, "put", autospec=True)

    _ = client.create_collaboration_invite(
        collaboration_identifier="ABC",
        message="You have been invited",
        intended_role=Role.MEMBER,
        sender_name="Piebe Gealle",
        invites=["test0@example.com", "test1@example.com"],
        groups=["test0", "test1"],
    )

    mock_post.assert_called_once_with(
        "https://sram.surf.nl/api/invitations/v1/collaboration_invites",
        json={
            "collaboration_identifier": "ABC",
            "message": "You have been invited",
            "intended_role": "member",
            "sender_name": "Piebe Gealle",
            "invites": ["test0@example.com", "test1@example.com"],
            "groups": ["test0", "test1"],
        },
    )
