import pytest

from ludwig_api.faai.sram.collaboration import (
    collaboration_contains_group,
    collaboration_contains_service,
    group_contains_user,
)
from ludwig_api.faai.sram.models import (
    Group,
    User,
    collaboration_group_to_group,
    collaboration_membership_to_user,
)


@pytest.fixture
def collaboration_json():
    return {
        "accepted_user_policy": None,
        "collaboration_memberships": [
            {
                "collaboration_id": 1234,
                "created_at": 1718712611,
                "created_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                "expiry_date": None,
                "id": 1234,
                "invitation_id": None,
                "role": "admin",
                "status": "active",
                "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                "user": {
                    "address": None,
                    "affiliation": "member@sram.surf.nl",
                    "application_uid": None,
                    "created_at": 1652430132,
                    "created_by": "system",
                    "edu_members": None,
                    "eduperson_principal_name": "007@astron.nl",
                    "email": "pgealle@example.com",
                    "entitlement": "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
                    "external_tmss_id": "a0c9bdf430362db52355cc1e3c39486cb199c1d8",
                    "family_name": "Gealle",
                    "given_name": "Piebe",
                    "home_organisation_uid": "gealle",
                    "id": 1234,
                    "last_accessed_date": 1721114099,
                    "last_login_date": 1721114099,
                    "mfa_reset_token": None,
                    "name": "Piebe Gealle",
                    "nick_name": None,
                    "pam_last_login_date": None,
                    "schac_home_organisation": "astron.nl",
                    "scoped_affiliation": "employee@astron.nl",
                    "second_fa_uuid": None,
                    "second_factor_auth": None,
                    "ssh_keys": [
                        {
                            "created_at": 1658306834,
                            "id": 774,
                            "ssh_value": "ssh-ed25519 ....",
                            "user_id": 1234,
                        }
                    ],
                    "ssid_required": False,
                    "suspended": False,
                    "uid": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                    "updated_at": 1721114099,
                    "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                    "user_ip_networks": [],
                    "username": "pgealle",
                },
                "user_id": 1234,
            }
        ],
        "collaboration_memberships_count": 1,
        "created_at": 1718712611,
        "created_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
        "description": "Testing CO for a call",
        "disable_join_requests": True,
        "disclose_email_information": False,
        "disclose_member_information": False,
        "expiry_date": None,
        "global_urn": "astron_org_1:callxyz",
        "groups": [
            {
                "auto_provision_members": False,
                "collaboration_id": 3619,
                "collaboration_memberships": [
                    {
                        "collaboration_id": 3619,
                        "user": {
                            "uid": "foo@sram.surf.nl",
                            "name": "foo",
                            "given_name": "foo",
                            "family_name": "",
                            "email": "<EMAIL>",
                            "schac_home_organisation": "",
                            "scoped_affiliation": "",
                        },
                    }
                ],
                "created_at": 1721114341,
                "created_by": "ext_api",
                "description": "",
                "global_urn": "astron_org_1:sdcdevp26:pi",
                "id": 8306,
                "identifier": "a1bb321e-124f-4ca2-b9c9-28117df2a803",
                "invitations": [],
                "name": "pi",
                "service_group": None,
                "service_group_id": None,
                "short_name": "pi",
                "updated_at": 1721114341,
                "updated_by": "ext_api",
            }
        ],
        "id": 1234,
        "identifier": "0ef9b5a0-c636-4ee2-8117-c3f85721d8c9",
        "invitations": [],
        "invitations_count": 0,
        "join_requests": [],
        "last_activity_date": 1721114099,
        "logo": "https://sram.surf.nl/api/images/collaborations/54e3b6d2-3767-4048-a981-988e3cff5882",
        "name": "Call XYZ",
        "organisation": {
            "accepted_user_policy": None,
            "category": "Research",
            "collaboration_creation_allowed": False,
            "collaborations_count": 5,
            "created_at": 1592423551,
            "created_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
            "crm_id": "7c41e5e3-0911-e511-80d0-005056956c1a",
            "description": "Test Organization for Astron testing",
            "id": 9,
            "identifier": "54fe1998-096c-4eea-a4b8-2e6e44b1f863",
            "logo": "https://sram.surf.nl/api/images/organisations/4179a70d-fa77-44ee-9579-e7241686bcd7",
            "name": "Astron TEST",
            "on_boarding_msg": None,
            "organisation_memberships_count": 3,
            "schac_home_organisations": [
                {
                    "created_at": 1613994409,
                    "created_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                    "id": 13,
                    "name": "astron.nl",
                    "organisation_id": 9,
                    "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                }
            ],
            "service_connection_requires_approval": False,
            "services": [],
            "services_restricted": False,
            "short_name": "astron_org_1",
            "units": [],
            "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
            "uuid4": "4179a70d-fa77-44ee-9579-e7241686bcd7",
        },
        "organisation_id": 1234,
        "service_connection_requests": [],
        "services": [
            {
                "abbreviation": "astronsdc_test",
                "accepted_user_policy": "",
                "access_allowed_for_all": False,
                "address": "",
                "allow_restricted_orgs": False,
                "automatic_connection_allowed": True,
                "connection_setting": None,
                "contact_email": "pgealle@example.com",
                "created_at": 1716543192,
                "created_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                "description": "ASTRON SDC Services - Test Instance",
                "entity_id": "APP-ENTITY-ID",
                "id": 12345,
                "identity_type": "",
                "ldap_enabled": False,
                "ldap_identifier": "UUIDv4",
                "ldap_password": None,
                "logo": "https://sram.surf.nl/api/images/services/7d7aa8ce-97e5-48e5-b87e-d6bd5d465cdb",
                "name": "ASTRON SDC Test",
                "non_member_users_access_allowed": False,
                "override_access_allowed_all_connections": False,
                "pam_web_sso_enabled": False,
                "privacy_policy": "https://www.astron.nl/privacy-statement/",
                "scim_bearer_token": None,
                "scim_client_enabled": False,
                "scim_enabled": False,
                "scim_url": None,
                "security_email": "pgealle@example.com",
                "service_memberships": [
                    {
                        "created_at": 1716543192,
                        "created_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                        "id": 1234,
                        "role": "admin",
                        "service_id": 1234,
                        "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                        "user": {
                            "address": None,
                            "affiliation": "member@sram.surf.nl",
                            "application_uid": None,
                            "created_at": 1652430132,
                            "created_by": "system",
                            "edu_members": None,
                            "eduperson_principal_name": "007@astron.nl",
                            "email": "pgealle@example.com",
                            "entitlement": "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
                            "external_tmss_id": "a0c9bdf430362db52355cc1e3c39486cb199c1d8",
                            "family_name": "Gealle",
                            "given_name": "Piebe",
                            "home_organisation_uid": "gealle",
                            "id": 1234,
                            "last_accessed_date": 1721114099,
                            "last_login_date": 1721114099,
                            "mfa_reset_token": None,
                            "name": "Piebe Gealle",
                            "nick_name": None,
                            "pam_last_login_date": None,
                            "schac_home_organisation": "astron.nl",
                            "scoped_affiliation": "employee@astron.nl",
                            "second_fa_uuid": None,
                            "second_factor_auth": None,
                            "ssh_keys": [
                                {
                                    "created_at": 1658306834,
                                    "id": 774,
                                    "ssh_value": "ssh-ed25519 ....",
                                    "user_id": 1234,
                                }
                            ],
                            "ssid_required": False,
                            "suspended": False,
                            "uid": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                            "updated_at": 1721114099,
                            "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                            "user_ip_networks": [],
                            "username": "pgealle",
                        },
                        "user_id": 1234,
                    }
                ],
                "support_email": "",
                "sweep_remove_orphans": False,
                "sweep_scim_daily_rate": 0,
                "sweep_scim_enabled": False,
                "sweep_scim_last_run": None,
                "token_enabled": False,
                "token_validity_days": 0,
                "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
                "uri": "https://sdc-dev.astron.nl/",
                "uri_info": "https://www.astron.nl/research-and-innovation/science-data-centre/",
                "uuid4": "7d7aa8ce-97e5-48e5-b87e-d6bd5d465cdb",
            }
        ],
        "short_name": "callxyz",
        "status": "active",
        "tags": [{"id": 174, "tag_value": "proposal"}],
        "units": [],
        "updated_at": 1721114099,
        "updated_by": "a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
        "uuid4": "54e3b6d2-3767-4048-a981-988e3cff5882",
        "website_url": "",
    }


def test_collaboration_contains_group(collaboration_json):
    assert collaboration_contains_group(collaboration_json, "pi")
    assert not collaboration_contains_group(collaboration_json, "programmer")


def test_collaboration_contains_service(collaboration_json):
    assert collaboration_contains_service(collaboration_json, "APP-ENTITY-ID")
    assert not collaboration_contains_service(collaboration_json, "wikipedia")


def test_collaboration_membership_to_user(collaboration_json):
    expected = User(
        eduperson_unique_id="a0c9bdf430362db52355cc1e3c39486cb199c1d8@sram.surf.nl",
        email="pgealle@example.com",
        family_name="Gealle",
        given_name="Piebe",
        name="Piebe Gealle",
        scoped_affiliation="employee@astron.nl",
        schac_home_organisation="astron.nl",
    )
    memberships = collaboration_json["collaboration_memberships"]
    assert collaboration_membership_to_user(memberships[0]) == expected


def test_collaboration_group_to_group(collaboration_json):
    expected: Group = {
        "identifier": "a1bb321e-124f-4ca2-b9c9-28117df2a803",
        "description": "",
        "members": [
            {
                "eduperson_unique_id": "foo@sram.surf.nl",
                "email": "<EMAIL>",
                "family_name": "",
                "given_name": "foo",
                "name": "foo",
                "scoped_affiliation": "",
                "schac_home_organisation": "",
            }
        ],
        "name": "pi",
        "short_name": "pi",
    }
    assert collaboration_group_to_group(collaboration_json["groups"][0]) == expected


def test_group_contains_user(collaboration_json):
    group: Group = collaboration_group_to_group(collaboration_json["groups"][0])
    assert group_contains_user(group, "foo@sram.surf.nl")

    del collaboration_json["groups"][0]["collaboration_memberships"][0]

    group = collaboration_group_to_group(collaboration_json["groups"][0])
    assert not group_contains_user(group, "foo@sram.surf.nl")
