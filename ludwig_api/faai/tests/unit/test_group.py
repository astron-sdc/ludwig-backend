from ludwig_api.faai.sram.models import (
    Collaboration,
    CollaborationGroup,
    CollaborationMembership,
    Group,
    SRAMUser,
    User,
    collaboration_membership_to_user,
)

collaboration = Collaboration(
    groups=[
        CollaborationGroup(
            identifier="abc",
            collaboration_memberships=[
                CollaborationMembership(
                    user=SRAMUser(
                        uid="foo",
                        name="Foo Bar",
                        given_name="Foo Bar",
                        email="Foo@bar.example.com",
                        family_name="Bar",
                        scoped_affiliation="unaffiliated",
                        schac_home_organisation="n/a",
                    )
                )
            ],
            name="group name",
            short_name="name",
            description="description",
        )
    ]
)


def test_collaboration_membership_to_user():
    user = collaboration_membership_to_user(
        collaboration["groups"][0]["collaboration_memberships"][0]
    )
    assert user["eduperson_unique_id"] == "foo"


def test_create_group_typeddict_equals():
    user_json: User = {
        "eduperson_unique_id": "foo@sram.surf.nl",
        "email": "<EMAIL>",
        "family_name": "",
        "given_name": "foo",
        "name": "foo",
        "scoped_affiliation": "",
        "schac_home_organisation": "",
    }

    first_group: Group = {
        "identifier": "a1bb321e-124f-4ca2-b9c9-28117df2a803",
        "description": "",
        "members": [user_json],
        "name": "pi",
        "short_name": "pi",
    }

    second_group: Group = Group(
        identifier="a1bb321e-124f-4ca2-b9c9-28117df2a803",
        name="pi",
        short_name="pi",
        description="",
        members=[user_json],
    )

    assert first_group == second_group
