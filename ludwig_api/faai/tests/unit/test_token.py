from ludwig_api.faai.token import BearerToken, IDToken


def test_bearer_token():
    token = BearerToken()
    assert token.token_type == "Bearer"


def test_id_token():
    token = IDToken()
    assert token.token_type == "ID"
