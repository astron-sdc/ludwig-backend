import pytest

from ludwig_api.faai.user import TokenUser
from ludwig_api.proposal.tests.conftest import merge_user_entitlements


@pytest.fixture
def token_json():
    return {
        "sub": "fc5b1b91-db71-4884-81f4-579e37b2f0f6",
        "name": "Piebe Gealle",
        "preferred_username": "piebe",
        "given_name": "Piebe",
        "family_name": "Gealle",
        "email": "p.gealle@example.com",
        "email_verified": "true",
        "eduperson_entitlement": [
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:pi",
            "urn:mace:surf.nl:sram:group:astron_org_1",
            "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1:scientists",
            "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1",
            "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
            "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
        ],
        "eduperson_unique_id": "6dd48eb203fcd9ce61d0bcfa7ad69a9632ef0d9c@sram.surf.nl",  # SRAM ID
        "token_type": None,
    }


def test_member_of_group_in_co(token_json):
    token_user = TokenUser(token_json)
    assert token_user.is_member_of_group_in_co("astron_org_1", "onderz_1", "scientists")


def test_member_not_in_group_in_co(token_json):
    token_user = TokenUser(token_json)
    assert not token_user.is_member_of_group_in_co(
        "avengers", "baddies", "evil_scientists"
    )


def test_merge_user_entitlements():
    first_user = TokenUser(
        {"eduperson_unique_id": "123", "eduperson_entitlement": ["foo", "bar"]}
    )
    second_user = TokenUser(
        {"eduperson_unique_id": "123", "eduperson_entitlement": ["bar", "qux"]}
    )

    manually_merged_entitlements = list(
        {
            *first_user.token["eduperson_entitlement"],
            *second_user.token["eduperson_entitlement"],
        }
    )

    merged_user = merge_user_entitlements([first_user, second_user])
    assert merged_user.token["eduperson_entitlement"] == manually_merged_entitlements
