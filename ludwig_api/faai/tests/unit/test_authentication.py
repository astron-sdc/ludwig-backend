from ludwig_api.faai.authentication import OAUTH2ProxyStatelessAuthentication


def test_stateless_auth_get_header():
    auth = OAUTH2ProxyStatelessAuthentication()
    assert auth.get_raw_token(b"42") == b"42"
