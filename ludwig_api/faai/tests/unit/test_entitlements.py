from ludwig_api.faai.sram.entitlements import get_groups_in_collaboration


def test_get_groups_in_collaboration__with_two_collaborations():
    entitlements = [
        "urn:mace:surf.nl:sram:group:my_cool_org:a:b",
        "urn:mace:surf.nl:sram:group:my_cool_org:a",
        "urn:mace:surf.nl:sram:group:my_cool_org:c:d",
        "urn:mace:surf.nl:sram:group:my_cool_org:c",
    ]
    assert get_groups_in_collaboration("a", entitlements) == ["b"]
    assert get_groups_in_collaboration("c", entitlements) == ["d"]
    assert get_groups_in_collaboration("f", entitlements) == []
