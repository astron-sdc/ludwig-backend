import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from ludwig_api.faai.user import TokenUser

token_json = {
    "sub": "fc5b1b91-db71-4884-81f4-579e37b2f0f6",
    "eduperson_unique_id": "fc5b1b91-db71-4884-81f4-579e37b2f0f6@sram.surf.nl",
    "name": "Piebe Gealle",
    "preferred_username": "piebe",
    "given_name": "Piebe",
    "family_name": "Gealle",
    "email": "p.gealle@example.com",
    "email_verified": "true",
    "eduperson_entitlement": [
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:pi",
        "urn:mace:surf.nl:sram:group:astron_org_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1:scientists",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
        "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
    ],
    "token_type": None,
}

token_json_admin = {
    "sub": "fc5b1b91-db71-4884-81f4-579e37b2f0f6",
    "eduperson_unique_id": "fc5b1b91-db71-4884-81f4-579e37b2f0f6@sram.surf.nl",
    "name": "Piebe Gealle",
    "preferred_username": "piebe",
    "given_name": "Piebe",
    "family_name": "Gealle",
    "email": "p.gealle@example.com",
    "email_verified": "true",
    "eduperson_entitlement": [
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:pi",
        "urn:mace:surf.nl:sram:group:astron_org_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1:scientists",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
        "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:proposal_admin",
    ],
    "token_type": None,
}

user_json = {
    "uid": "fc5b1b91-db71-4884-81f4-579e37b2f0f6@sram.surf.nl",
    "eduperson_unique_id": "fc5b1b91-db71-4884-81f4-579e37b2f0f6@sram.surf.nl",
    "name": "Piebe Gealle",
    "preferred_username": "piebe",
    "given_name": "Piebe",
    "family_name": "Gealle",
    "email": "p.gealle@example.com",
    "email_verified": "true",
    "eduperson_entitlement": [
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:pi",
        "urn:mace:surf.nl:sram:group:astron_org_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1:scientists",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
        "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
    ],
    "global_permissions": {
        "is_admin": False,
        "create_cycle": False,
        "create_call": False,
    },
    "token_type": None,
}

user_json_admin = {
    "uid": "fc5b1b91-db71-4884-81f4-579e37b2f0f6@sram.surf.nl",
    "eduperson_unique_id": "fc5b1b91-db71-4884-81f4-579e37b2f0f6@sram.surf.nl",
    "name": "Piebe Gealle",
    "preferred_username": "piebe",
    "given_name": "Piebe",
    "family_name": "Gealle",
    "email": "p.gealle@example.com",
    "email_verified": "true",
    "eduperson_entitlement": [
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:pi",
        "urn:mace:surf.nl:sram:group:astron_org_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1:scientists",
        "urn:mace:surf.nl:sram:group:astron_org_1:onderz_1",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest",
        "urn:mace:surf.nl:sram:group:surf-ram#sram.surf.nl",
        "urn:mace:surf.nl:sram:group:astron_org_1:astronsdctest:proposal_admin",
    ],
    "global_permissions": {
        "is_admin": True,
        "create_cycle": True,
        "create_call": True,
    },
    "token_type": None,
}


@pytest.fixture
def token_user():
    return TokenUser(token=token_json)


@pytest.fixture
def token_user_admin():
    return TokenUser(token=token_json_admin)


@pytest.fixture
def api_client():
    return APIClient()


def test_without_token(api_client):
    res = api_client.get(reverse("userinfo-list"))
    assert res.status_code == status.HTTP_401_UNAUTHORIZED
    data = res.json()
    assert data["detail"] == "token auth failed"


def test_with_token(api_client, token_user):
    api_client.force_authenticate(user=token_user)
    res = api_client.get(reverse("userinfo-list"))
    assert res.status_code == status.HTTP_200_OK
    assert res.json() == user_json


def test_global_permissions(api_client, token_user_admin):
    api_client.force_authenticate(user=token_user_admin)
    res = api_client.get(reverse("userinfo-list"))
    assert res.status_code == status.HTTP_200_OK
    assert res.json() == user_json_admin
