# Ludwig Backend

Backend for the Ludwig Proposal Tool.

## Context Diagram

![](documentation/context-diagram.png)

## Deployments
Test environment: https://sdc-dev.astron.nl/proposal-backend/

## Getting started

This project is built in Python and Docker with the following stack:

| Service    | Framework           |
|------------|---------------------|
| Backend    | Django, DRF, Pytest |
| Monitoring | Prometheus, Grafana |
| Worker     | Celery, RabbitMQ    |
| Database   | Postgres            |


### 1. Install python 3.12
* Windows/Mac: scroll down to the bottom of https://www.python.org/downloads/release/python-3122/
* Linux, debian-based systems:
    ```shell
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt update
    sudo apt install python3.12
    # You might be tempted to replace/symlink the default system python with this version.
    # DON'T DO IT!
    ```

### 2. Set up the repo
```shell
python3.12 -m venv venv  # or use your favourite virtual env tool
source venv/bin/activate
make init
```
You might need to [install `make`](https://stackoverflow.com/questions/2532234/how-to-run-a-makefile-in-windows) if you're on Windows


### 3. Run the server

```shell
systemctl stop postgresql  # only needed if you get error that port 5432 is already in use
make run
```

Then you can find:

| Service             | URL                           |
|---------------------|-------------------------------|
| Django              | http://localhost:8000/        |
| Prometheus          | http://localhost:8000/metrics |
| Flower (celery GUI) | http://localhost:5555/        |
| RabbitMQ GUI        | http://localhost:15672/       |


## Backend data model
<img src="documentation/data-model.png" alt="data model" />

## How to
### Testing
Use [`pytest`](https://pytest.org/) for unit testing.
```shell
make test  # default pytest options, all tests
# or
docker compose exec django pytest  # [your favourite pytest options] [your favourite tests]
```
Integration tests are in a [different repo](https://git.astron.nl/astron-sdc/ludwig-integration).

### New dependencies
Dependencies are managed by `pip-compile`, which the script `make init` should have installed for you.
Add new dependencies to `base.in`, or dev dependencies to `dev.in` (which inherits from base).
Then use `make compile-requirements` to compile them to the respective txt files.
(You shouldn't have to touch the txt files directly.) Then use `pip install` in your venv,
restart the docker containers so the new requirements will be installed in there too.

### Starting a new app
Put all Django apps in the `ludwig_api` directory, like this:
```shell
mkdir ludwig_api/myapp
python manage.py startapp myapp ludwig_api/myapp
```
add `"ludwig_api.myapp"` to `INSTALLED_APPS`, making sure that `MyappConfig.name` attribute is `ludwig_api.myapp`.

### Merge request
There are precommit hooks that does some automatic linting. If your `git push` is blocked by one of them,
you can `git diff` to review what it changed, so you can add them to the commit and push again.

In the CI pipeline there's a code quality check (Sonarcloud).
If the check failed, check the dashboard to see what went wrong:
https://sonarcloud.io/project/pull_requests_list?id=astron-sdc-eval_ludwig-backend


## Contact

This repository is maintained by Team Rainbow. Feel free to contact us on
[Slack](https://radio-observatory.slack.com/archives/C02H4G65ERL)!


## License

Apache License 2.0
