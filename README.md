# Ludwig Backend

Backend for the Ludwig Proposal Tool.

## Context Diagram

![](documentation/context-diagram.png)

## Deployments
Test environment: https://sdc-dev.astron.nl/proposal-backend/

## Getting started

This project is built in Python and Docker with the following stack:

| Service    | Framework           |
|------------|---------------------|
| Backend    | Django, DRF, Pytest |
| Monitoring | Prometheus, Grafana |
| Worker     | Celery, RabbitMQ    |
| Database   | Postgres            |


### 1. Install python 3.12
* Windows/Mac: scroll down to the bottom of https://www.python.org/downloads/release/python-3122/
* Linux, debian-based systems:
    ```shell
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt update
    sudo apt install python3.12
    sudo apt install python3.12-venv
    # You might be tempted to replace/symlink the default system python with this version.
    # DON'T DO IT!
    ```

### 2. Set up the repo
```shell
python3.12 -m venv venv  # or use your favourite virtual env tool
source venv/bin/activate
make init
```
You might need to [install `make`](https://stackoverflow.com/questions/2532234/how-to-run-a-makefile-in-windows) if you're on Windows


### 3. Create a .env file

Copy the contents of the `.env.local` file and fill into a `.env`
Fill in the empty strings. Make sure to choose a unique value for the
`SRAM_COLLABORATION_PREFIX` setting, make sure it only uses lower case letters,
numbers or an underscore `_` .

### 4. Run the server

```shell
systemctl stop postgresql  # only needed if you get error that port 5432 is already in use
make run
```

Then you can find:

| Service             | URL                           |
|---------------------|-------------------------------|
| Django              | http://localhost:8000/        |
| Prometheus          | http://localhost:8000/metrics |
| Flower (celery GUI) | http://localhost:5555/        |
| RabbitMQ GUI        | http://localhost:15672/       |

### 4. Apply pending migrations

On first run or after creating new migrations, apply pending migrations.

```shell
make migrate
```

## Backend data model
<img src="documentation/data-model.png" alt="data model" />

## How to
### Testing
Use [`pytest`](https://pytest.org/) for unit testing.
```shell
make test  # default pytest options, all tests
# or
docker compose exec backend pytest  # [your favourite pytest options] [your favourite tests]
# For example, to run a single test in a file:
docker compose exec backend pytest ludwig_api/proposal/tests/api/calls/test_calls.py::test_get_call_returns_call
```
Integration tests are in a [different repo](https://git.astron.nl/astron-sdc/ludwig-integration).

#### Note.
You can run the pytests test against a local tmss instance. Use the following template to spin up a local tmss instance
tuning cam be done in the .env
Add these for the dev account:

LUDWIG_TMSS_CLIENT_USERNAME="test"
LUDWIG_TMSS_CLIENT_PASSWORD="test"
LUDWIG_TMSS_TOKEN_URL="http://localhost:8008/api/token-auth/"
LUDWIG_TMSS_URI="http://127.0.0.1:8008"
```
version: "3"
networks:
  tmss_network:
  default:
    driver: bridge

services:
  rabbitmq:
    image: rabbitmq:latest
    container_name: rabbitmq
    networks:
      - tmss_network

  tmss-backend:
    image: git.astron.nl:5000/ro/lofar/tmss_django:fc3fc1cd
    environment:
      - RABBITMQ_DEFAULT_USER=guest
      - RABBITMQ_DEFAULT_PASS=guest
      - RABBITMQ_DEFAULT_PORT=5672
      - LOFAR_DEFAULT_BROKER=rabbitmq
      - LOFARENV=TEST
    ports:
      - "8008:8008"
      - "5678:5678"
      - "5444:5444"
    command: bash -c 'source /opt/lofar/lofarinit.sh && ALLOWED_HOSTS=* tmss_test_environment -p 8008 -ldmMsSwV --simulate'     #includes legacy
    restart: unless-stopped
    networks:
      - tmss_network
```
### New dependencies
Dependencies are managed by `pip-compile`, which the script `make init` should have installed for you.
Add new dependencies to `base.in`, or dev dependencies to `dev.in` (which inherits from base).
Then use `make compile-requirements` to compile them to the respective txt files.
(You shouldn't have to touch the txt files directly.) Then use `pip install` in your venv,
restart the docker containers so the new requirements will be installed in there too.

### Starting a new app
Put all Django apps in the `ludwig_api` directory, like this:
```shell
mkdir ludwig_api/myapp
python manage.py startapp myapp ludwig_api/myapp
```
add `"ludwig_api.myapp"` to `INSTALLED_APPS`, making sure that `MyappConfig.name` attribute is `ludwig_api.myapp`.

### Merge request
There are precommit hooks that does some automatic linting. If your `git push` is blocked by one of them,
you can `git diff` to review what it changed, so you can add them to the commit and push again.

In the CI pipeline there's a code quality check (Sonarcloud).
If the check failed, check the dashboard to see what went wrong:
https://sonarcloud.io/project/pull_requests_list?id=astron-sdc-eval_ludwig-backend

### Authentication and authorization

Ludwig supports the OIDC protocol and uses a separate "Backend for Frontend; BFF" container
for handling OIDC configuration and token/session storage. The Django backend api can
therefore be kept simple and completely stateless; relying only on the tokens provided
in the header of a request.

#### Architecture

![Overview of containers for Ludwig](./documentation/container-diagram.png)

#### Where are session tokens "stored"

To prevent leaking of tokens to the browser, an container running [Oauth2-proxy](https://github.com/oauth2-proxy/oauth2-proxy)
is used. This is configured with some static settings in [bff](./bff/oauth2-proxy-dev.cfg)
with different configurations for dev and prod (mainly requiring a secure connection
for production).
Tokens are retrieved by the Oauth2 proxy via our Kecyloak instance. Currently
the tokens are encrypted and stored in the cookie, although a set-up with "opaque" tokens
and key/value store via Redis (or similar) is also supported.

The proxy then translates the cookie value into headers which are then passed to the
backend. In that way, the tokens never leave our "secure" network.

#### Email validation
Since Ludwig should allow to various people from different institutes, we configure
the proxy to allow all email domains. Make sure to toggle the `Trust Email` option
in the Identity Provider in Keycloak, so that emails are marked as verified on
import. This assumes we use SRAM and users are only able to log in after joining
the "main" CO based on an invitation to their email address; thus having verified access
to them.

```ini
email_domains="*"   # allow all emails
```

#### SRAM & Collaborations a.k.a. Group Management

For user group management we make use of [SRAM (Surf Research and Access Management)](https://servicedesk.surf.nl/wiki/display/WIKI/SURF+Research+Access+Management).
Groups are called Collabroations and rely on an invitation system.

For a user to be able to log in to our application, both the user and a service
need to be available in the same collaboration. These relations are then shown as `eduperson_entitlement`.
SURF provides an overview of all [available attributes and scopes](https://servicedesk.surf.nl/wiki/display/WIKI/Attributes+in+SRAM)

These entitlements are used to determine the Role per Call and Proposal.
These roles are then converted into permissions files in the [permissions](./ludwig_api/proposal/permissions/)
directory.

Each service provider (e.g. ASTRON SDC) needs to be registered in SRAM.
To prevent registration of all applications we chose to use Keycloak as a
transparent proxy.

#### Caching

We use the Django Cache Framework, which is a generic API into which you can plug different types of cache. Some common ones are database cache, Memcached and Redis. This generic API is described here: https://docs.djangoproject.com/en/5.1/topics/cache/. In Ludwig, we use the Ludwig Postgres database as caching backend. This is defined in the settings file like so:

```
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "ludwig_cache",
    }
}
```

The existence of this table is ensured by `python manage.py createcachetable` which is part of the CI migrate job. Run this command locally to ensure cache existence, or call `make create-cache-table` for your convenience.

## Contact

This repository is maintained by Team Rainbow. Feel free to contact us on
[Slack](https://radio-observatory.slack.com/archives/C02H4G65ERL)!


## License

Apache License 2.0
