from celery import Celery


app = Celery('tasks', broker='amqp://guest:guest@127.0.0.1:5672//', backend='rpc://')
app.autodiscover_tasks()

@app.task
def hello_world():
    return 1+2
