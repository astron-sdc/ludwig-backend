#!/bin/bash
# Runs e2e test against local backend/frontend.
# Creates a backup of the current db and restores it after the Cypress window is closed.
# Prerequisites:
# - Frontend running on 5173
# - No pending migrations
# - LUDWIG_INTEGRATION_DIRECTORY value set in .env

cypress_interactive=true
set -u # exit script if any expanded var is unset
source .env

# Disable SRAM in env file
SRAM_ENABLED_ORIGINAL_VALUE=$SRAM_ENABLED
if [ "$SRAM_ENABLED_ORIGINAL_VALUE" -eq "1" ]; then
  sed -i 's/SRAM_ENABLED=1/SRAM_ENABLED=0/g' .env
fi

docker compose up --build -d
make backup

# Clean db, load e2e fixtures
docker compose exec backend python manage.py flush --noinput
cp -rT "$LUDWIG_INTEGRATION_DIRECTORY"/fixtures/ ludwig_api/fixtures
docker compose exec backend python manage.py loaddata ludwig_api/fixtures/*

run_cypress() {
  if [ "$cypress_interactive" == true ] ; then
    npx cypress open --config baseUrl="http://localhost:5173/"
  else
    npx cypress run --config baseUrl="http://localhost:5173/"
  fi
}

# Run cypress (assume frontend is already running)
(cd "$LUDWIG_INTEGRATION_DIRECTORY"/cypress && npm install && run_cypress)

# After cypress is closed, clear the test data from db, restore the backup
docker compose exec backend python manage.py flush --noinput
make restore

# set SRAM_ENABLED back to original value and restart backend to apply
if [ "$SRAM_ENABLED_ORIGINAL_VALUE" -eq "1" ]; then
  sed -i 's/SRAM_ENABLED=0/SRAM_ENABLED=1/g' .env
fi

docker compose up --build -d
