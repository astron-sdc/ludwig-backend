empty:
	# Please supply one of the commands from the Makefile

init:
	pip install -r requirements/dev.txt
	pip install setuptools
	pre-commit install --hook-type commit-msg --hook-type pre-push

run:
	docker compose up --build

run-detached:
	docker compose up -d --build

backup:
	docker compose exec backend python manage.py dumpdata --exclude=auth --exclude=sessions --exclude=admin.logentry --exclude=contenttypes >> .run/backup_$$(date '+%Y-%m-%d-%H-%M').json

restore:
	docker compose exec backend python manage.py loaddata $$(ls -t .run/backup* | head -n 1)

bash:
	docker compose exec backend bash

python:
	docker compose exec backend python manage.py shell

test:
	docker compose exec backend pytest

test-coverage:
	docker compose exec backend pytest --cov=ludwig_api --cov-report term --cov-report=html:coverage --cov-report=xml:coverage/coverage.xml --junitxml=report.xml ludwig_api/

compile-requirements:
	pip-compile requirements/base.in --upgrade
	pip-compile requirements/dev.in --upgrade

create-superuser:
	docker compose exec backend python manage.py createsuperuser

create-cache-table:
	docker compose exec backend python manage.py createcachetable

clear-name-cache:
	docker compose exec backend python manage.py clear_cache sram_user_names

migrations:
	python manage.py makemigrations --settings=ludwig_api.settings

migrate:
	docker compose exec backend python manage.py migrate

quick-migrate: migrations migrate

generate-proposal-state-diagram:
	python manage.py generate_graph -o ./documentation/proposal-state-diagram.png proposal.Proposal
