empty:
	# Please supply one of the commands from the Makefile

init:
	pip install -r requirements/dev.txt
	pre-commit install --hook-type commit-msg --hook-type pre-push

run:
	docker compose up --build

bash:
	docker compose exec backend bash

python:
	docker compose exec backend python manage.py shell

test:
	docker compose exec backend pytest

compile-requirements:
	pip-compile requirements/base.in --upgrade
	pip-compile requirements/dev.in --upgrade

create-superuser:
	docker compose exec backend python manage.py createsuperuser

migrations:
	python manage.py makemigrations --settings=ludwig_api.settings

migrate:
	docker compose exec backend python manage.py migrate

quick-migrate: migrations migrate

generate-proposal-state-diagram:
	python manage.py generate_graph -o ./documentation/proposal-state-diagram.png proposal.Proposal

superuser:
	docker compose exec backend python manage.py createsuperuser
